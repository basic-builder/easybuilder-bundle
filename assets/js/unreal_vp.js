require('bootstrap/dist/css/bootstrap.css');
require('font-awesome/css/font-awesome.css');
require('jquery.ripples/dist/jquery.ripples');

require('slick-carousel/slick/slick.css');
require('slick-carousel/slick/slick-theme.css');
require('slick-carousel/slick/slick');
require('../style/unreal_vp.scss');

// require('../style/main.scss');

import('./swiper');
import('./typeAnimation');
import('./contactForm');

$(document).ready(function () {

    $('.unrealMenu').on('click', function () {
        $(this).toggleClass('active');
        $('.vpNavigation').fadeToggle();
    });


    if ($('#scrollTop').length) {
        let winHeight = $(window).height();
        let sectionTop = $('#scrollTop').offset().top - (winHeight - 10);
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > sectionTop) {
                $('.padd img').css({
                    'transform': 'scale(1)',
                    'opacity': 1
                });
            } /*else {
                $('.padd img').css({
                    'transform': 'scale(0.9)',
                    'opacity': 0
                });
            }*/
        });
    }

    let preloader = $('.loading');

    if ($(preloader).length) {
        setTimeout(function () {
            window.scrollTo(0, 0);
            $('body, html').css({
                'overflow': 'initial'
            }, 2000);
            $(preloader).fadeOut();
        },);
    } else {
        $('body, html').css({
            'overflow': 'initial'
        });
    }

    if ($('.logos')) {
        $('.logos').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            arrows: false
        })
    }

    $('.scrolling span').on('click', function (e) {
        e.preventDefault();
        const divTop = $('.mainSection').offset().top;
        $('html, body').animate({
            scrollTop: divTop
        }, 1000, "swing")

    });


    const globalNav = $('.globalNav'),
        globalNavTop = $(globalNav).offset().top;

    if ($(globalNav).length) {

        $(window).on('scroll', function () {
            if ($(this).scrollTop() > globalNavTop) {
                $('nav').addClass('globalFixed');
            } else {
                $('nav').removeClass('globalFixed');
            }
        });
    }

}); //ready