require('swiper/swiper.scss');

const Swiper = require('swiper/js/swiper');

require('swiper/components/effect-fade/effect-fade.scss')



$(document).ready(function (e) {

    if($('.swiper-container').length){
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            speed: 3000,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            fadeEffect: {
                crossFade: true
            },
            effect: "fade",
            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            }
        });
    }

}); // ready