/**
 * @deprecated
 */
// var video = document.querySelector('.topVideo #topVideo'),
//     track = document.querySelector('.topVideo .controlsTrack'),
//     btn = document.querySelector('.topVideo #playPause'),
//     controlsTrack = document.querySelector('.topVideo .controlsTrack'),
//     controlsBar = document.querySelector('.topVideo .controlsBar'),
//     playVideo = document.querySelector('.topVideo .playVideo');
//
// if(video){
//
//     function togglePlay() {
//
//         if (video.paused == true) {
//             video.play();
//             btn.className = 'pause';
//             playVideo.style.display = 'none';
//         } else if (video.paused == false) {
//             video.pause();
//             btn.className = 'play';
//             playVideo.style.display = 'block';
//         }
//     }
//
//     function handleProgress() {
//         controlsTrack.style.width = (video.currentTime / video.duration) * 100 + '%';
//     }
//
//     function scrub(e) {
//         const scrubTime = (e.offsetX / controlsBar.offsetWidth) * video.duration;
//         video.currentTime = scrubTime;
//     }
//
//     btn.onclick = function () {
//         togglePlay();
//     };
//     video.onclick = function () {
//         togglePlay();
//     };
//     playVideo.onclick = function () {
//         togglePlay();
//     };
//     controlsBar.addEventListener('click', scrub);
//
//     let mousedown = false;
//     controlsBar.addEventListener('mousedown', () => mousedown = true);
//     controlsBar.addEventListener('mouseup', () => mousedown = false);
//     controlsBar.addEventListener('mousemove', (e) => mousedown && scrub(e));
//
//     video.addEventListener('timeupdate', handleProgress);
// }

/**
 *
 * @type {Element}
 */
let topVideoButton = document.querySelector('.topVideo .playVideo'),
    topVideo = document.getElementById('topVideo'),
    relVideo = document.getElementById('relVideo'),
    relVideoButton = document.querySelector('.reel .playVideo'),
    teamVideo = document.getElementById('teamVideo'),
    teamVideoButton = document.querySelector('.teamVideo .playVideo'),
    allVideos = document.querySelectorAll('video')
;

function togglePlay(elem) {

    if (elem.paused == true) {

        // allVideos.forEach(function (val, key) {
        //     val.load();
        // });
        elem.play();
        elem.setAttribute('controls', 'controls');
    } /*else if (video.paused == false) {
        elem.pause();

    }*/
}

if (topVideoButton) {
    topVideoButton.addEventListener('click', function () {
        togglePlay(topVideo);
        this.style.display = 'none';
    });
}

if (relVideoButton) {
    relVideoButton.addEventListener('click', function () {
        togglePlay(relVideo);
        document.querySelector('.relText').style.display = 'none';
        this.style.display = 'none';
    });
}

if (teamVideo) {
    teamVideoButton.addEventListener('click', function () {
        togglePlay(teamVideo);
        document.querySelector('.teamVideoText').style.display = 'none';
        this.style.display = 'none';
    });
}