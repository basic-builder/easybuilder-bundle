import '@uppy/core/dist/style.css';

require('uppy/dist/uppy.min.css');
import '@uppy/dashboard/dist/style.css';

const Uppy = require('@uppy/core');
const DragDrop = require('@uppy/drag-drop');
import XHRUpload from '@uppy/xhr-upload';
import ProgressBar from "@uppy/progress-bar";


require('select2/dist/js/select2.full.min');
require('select2/dist/css/select2.css');
require('select2-bootstrap-theme/dist/select2-bootstrap.css');
require('inputmask');

/**
 * translations
 * @private
 */
let _text = {
    ru: {
        'Upload CV': 'Загрузка резюме',
        'Request a Quote': 'Загрузка Quote',
        'remove': 'удалить',
        'uppyError': 'Что-то пошло не так, пожалуйста, перезагрузите страницу и попробуйте еще раз.',
        'Up to 10 MB': 'До 10 МБ',
        'dropHereOr' : 'Перетащите файлы сюда'
    },
    en: {
        'Upload CV': 'Upload CV',
        'Request a Quote': 'Request a Quote',
        'remove': 'remove',
        'uppyError': 'Something went wrong, please reload page and try again.',
        'Up to 10 MB': 'Up to 10 MB',
        'dropHereOr' : 'Drop Here'
    },
    es: {
        'Upload CV': 'Upload CV',
        'Request a Quote': 'Request a Quote',
        'remove': 'remove',
        'uppyError': 'Something went wrong, please reload page and try again.',
        'Up to 10 MB': 'Up to 10 MB',
        'dropHereOr' : 'Drop Here Or'
    }
};
let filesIds = [];
$(document).ready(function (e) {

    /**
     * if flash message exist, clear 6 sec after
     * @type {jQuery|HTMLElement}
     */
    let flashes = $('.flashes');
    if ($(flashes).has($('div')).length) {
        setTimeout(function () {
            $('.flashes').empty();
        }, 6000);
    }

    /**
     * get locale
     * @type {jQuery|undefined|any}
     * @private
     */
    let _locale = $('.languages li.d-none a').attr('class'),
        slug;

    /**
     *  get formData
     */
    $('.requestQuote, .InputFileLabel').on('click', function (e) {
        e.preventDefault();
        startLoader();
        slug = $(this).attr('data-slug');
        getCustomForm(slug, _locale, _text);
    });

    /**
     * cv or quote form open/close
     */
    $('.closeModal').click(function (e) {
        $('.bodyBlack').removeClass('open');
        $('.clientCustomForms').removeClass('open');
        $('.clientCustomFormsData').html('');
    });

    $(document).on('click', '.add-language-collection-widget, .add-areas-of-expertise-widget', function (e) {
        addTag(this)
    });

    $(document).on('click', 'span[data-id]', function () {
        postFileRemove($(this).attr('data-id'), _locale);
    });

    // cv or quote form submit
    $(document).on('submit', 'form[name="quote"], form[name="cv"]', function (e) {
        startLoader();
    });

});

/**
 * add new tag form client form
 * @param elem
 * @param allowDelete
 */
function addTag(elem, allowDelete = true) {
    var list = jQuery(jQuery(elem).attr('data-list-selector'));
    // Try to find the counter of the list or use the length of the list
    var counter = list.data('widget-counter') || list.children().length;
    // grab the prototype template
    var newWidget = list.attr('data-prototype');
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    newWidget = newWidget.replace(/__name__/g, counter);
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data('widget-counter', counter);
    // create a new list element and add it to the list
    var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget);
    newElem.appendTo(list);
    $("select.selected2").select2({
        theme: "bootstrap",
        minimumResultsForSearch: Infinity
    });

    if(allowDelete){
        addTagFormDeleteLink(newElem);
    }
}

/**
 * create remove tag button for client form
 * @param $tagFormLi
 */
function addTagFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button class="collRemove" type="button">×</button>');
    $tagFormLi.append($removeFormButton);
    $removeFormButton.on('click', function (e) {
        $tagFormLi.remove();
    });
}


/**
 * get formData request
 *
 * @param slug
 * @param _locale
 * @param _text
 */
function getCustomForm(slug, _locale, _text) {
    let parentForm = $('.clientCustomForms'),
        parentFormData = $('.clientCustomForms .clientCustomFormsData'),
        parentFormH4 = $('.clientCustomForms h4');
    var xhr = new XMLHttpRequest(), url = _locale === 'en' ? `/custom-form/${slug}` : `/${_locale}/custom-form/${slug}`;
    xhr.open("GET", url);

    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            $('.message .col-12').html('');
            $(parentFormData).html('');
            $(parentFormH4).html('');
            $('.blackModal').remove();
            let data = JSON.parse(xhr.responseText);
            $('.bodyBlack').addClass('open');
            $(parentForm).addClass('open');
            if (xhr.status === 200) {
                switch (slug) {
                    case 'cv':
                        $(parentFormH4).html(_text[_locale]['Upload CV']);
                        break;
                    case 'quote':
                        $(parentFormH4).html(_text[_locale]['Request a Quote']);
                        break;
                    default:
                        break;
                }
                $(parentFormData).append(data.form_html);
                // select 2
                $("select.selected2").select2({
                    theme: "bootstrap",
                    minimumResultsForSearch: Infinity
                });
                // create uppy uploader
                createUppy(_locale);
                // create form tag for language skills
                if ($('.add-language-collection-widget').length){
                    addTag($('.add-language-collection-widget'), false);
                }
                // create form tag for area of expertise
                if ($('.add-areas-of-expertise-widget').length){
                    addTag($('.add-areas-of-expertise-widget'), false);
                }
                // create input mask for phone number
                Inputmask("999 999 9999", {placeholder: "", clearMaskOnLostFocus: true}).mask('.phone');
            } else {
                $(parentFormH4).html(data.message);
            }
        }
    };

    xhr.send();
}


/**
 * remove client form file
 *
 * @param id
 * @param _locale
 */
function postFileRemove(id, _locale) {

    let span = $('span[data-id="' + id + '"]'),
        li = $(span).closest('li')
    ;
    startLoader();
    var xhr = new XMLHttpRequest(),
        url = _locale === 'en' ? `/remove-form-image/${id}` : `/${_locale}/remove-form-image/${id}`;
    xhr.open("POST", url);

    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            $('.blackModal').remove();
            if (xhr.status === 204) {
                $(li).remove();
            } else {
                $('.uppyErrors').text(_text[_locale]['uppyError']);
            }
        }
    };

    xhr.send();
}

/**
 * loading
 * @param text
 * @param title
 */
function startLoader(text = '', title = '') {

    let loader =
        '<div class="blackModal" id="blackModal">' +
        '        <div class="loader"></div>' +
        '    </div>';
    $('body').append(loader);
}

/**
 * create UPPY uploader
 * @param _locale
 */
function createUppy(_locale) {
    const uploadURL = '/galleries/files';

    const uppy = new Uppy({
        debug: true,
        autoProceed: true,
        locale: {
            strings:{
                dropHereOr: _text[_locale]['dropHereOr']
            }
        },
        logger: debugLogger,

        restrictions: {
            maxFileSize: 10900000,
            maxNumberOfFiles: 20,
            minNumberOfFiles: 1,
            allowedFileTypes: ['image/*', '.pdf', '.doc', '.dot', '.wbk', '.docx', '.docm', '.dotx', '.dotm', '.docb', '.txt', '.rtf', '.7z', '.rar', '.xliff', '.xlz', '.xlf', '.sdlxliff']
        }
    })
        .use(XHRUpload, {endpoint: uploadURL, method: 'POST', formData: true, fieldName: 'files[]'})
        .use(ProgressBar, {target: '.ProgressBar', hideAfterFinish: true})
        .use(DragDrop, {
            note: _text[_locale]['Up to 10 MB'],
            target: '.DashboardContainer',
            endpoint: uploadURL,
            method: 'POST',
            formData: true,
            fieldName: 'files[]',
        })
        .on('upload-success', onUploadSuccess('.uploaded-files ol', _locale));
}

/**
 * show uploaded file from uppy in client form
 *
 * @param elForUploadedFiles
 * @param _locale
 * @returns {function(...[*]=)}
 */
const onUploadSuccess = (elForUploadedFiles, _locale) =>
    (file, response) => {
        filesIds.push(response.body.id);
        const id = response.body.id;
        const url = response.body.download_link;
        const fileName = file.name;

        const option = document.createElement('option');
        option.val = id;
        option.setAttribute('selected', 'selected');
        option.text = id;
        document.querySelector('#files').appendChild(option);

        const span = document.createElement('span');
        span.setAttribute('data-id', response.body.id);
        span.innerText = _text[_locale]['remove'];

        const a = document.createElement('a');
        a.href = url;
        a.target = '_blank';
        a.appendChild(document.createTextNode(fileName));

        const li = document.createElement('li');
        li.appendChild(a);
        li.appendChild(span);

        document.querySelector(elForUploadedFiles).appendChild(li);
    };

const debugLogger = {
    debug: (...args) => {
        const debug = console.debug || console.log;
        debug.call(console, `[Uppy] [dsadas]`, ...args);
    },
    warn: (...args) => console.warn(`[Uppy] [WARNING]`, ...args),
    error: (...args) => console.log(`[Uppy] [CRITICAL]`, ...args)
};

