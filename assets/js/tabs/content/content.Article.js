import {
    defaultErrorMsg,
    getLinkAction,
    linkAction,
    messageType,
    showMessage
} from "../../services/utilities";
import {getImageByIdApi, removeImgByIdApi, uploadImage} from "../../api/admin.api";
import showDefaultImageModal from "../../helpers/showDefaultImageModal";

const handleArticleTab = () => {
    const hiddenUploadedImageSelect = document.querySelector('select[article-image-upload]');
    const uploadedOption = hiddenUploadedImageSelect.querySelector('option[selected]');

    const formGroup = $(hiddenUploadedImageSelect).closest('div.form-group');

    const input = $('<input type="file" id="imageUploadInput" class="form-control"/>').on('change', (e) => {
        const selectedFile = e.target.files[0];
        if (selectedFile) {
            uploadField.find('img')?.remove();
            uploadField.find('span.remove')?.remove();

            uploadImage(selectedFile).then(res => {
                if (res?.id) {
                    addUploadedImage(res);
                } else {
                    showMessage(messageType.error, defaultErrorMsg)
                }
            })
        }
    });

    const uploadField = $(`
        <div class="form-group articleEditImageUploadDiv">
            <label for="imageUploadInput">Image</label>
            <div class="form-widget"></div>
        </div>`
    );

    uploadField.find('div.form-widget').append(input)
    uploadField.insertAfter(formGroup);

    const addUploadedImage = (image) => {
        const {download_link, id} = image;

        const img = $(`<img src="${download_link}" alt="" id="${id}"/>`).on('click', () => {
            getImageByIdApi(id).then(response => {
                if (response?.id) {
                    showDefaultImageModal(response);
                } else {
                    showMessage(messageType.error, defaultErrorMsg);
                }
            });
        });

        const remove = $(`<span class="remove">×</span>`).on('click', () => {
            removeImgByIdApi(id).then(removed => {
                if (removed) {
                    uploadField.find('img').remove();
                    uploadField.find('span.remove').remove();
                    hiddenUploadedImageSelect.querySelector('option[selected]').remove();
                    $(input).val('');
                }
            })
        });

        uploadField.append(img);
        uploadField.append(remove);

        const uploadedOption = hiddenUploadedImageSelect.querySelector('option[selected]');

        if (uploadedOption) {
            uploadedOption.value = id;
            uploadedOption.textContent = download_link;
        } else {
            $(hiddenUploadedImageSelect).append(`<option selected="selected" value="${id}">${download_link}</option>`)
        }
    }

    if (uploadedOption) {
        addUploadedImage({
            id: uploadedOption.value,
            'download_link': uploadedOption.textContent
        })
    }
}

export default handleArticleTab;