require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/js/dist/tab');
require('font-awesome/css/font-awesome.css');
require('jquery.ripples/dist/jquery.ripples');

require('slick-carousel/slick/slick.css');
require('slick-carousel/slick/slick-theme.css');
require('slick-carousel/slick/slick');
require('../style/unreal_vp2.scss');

// require('../style/main.scss');

import('./swiper');
import('./typeAnimation');
import('./contactForm_v2');
import('./player');

$(document).ready(function () {

    $('.unrealMenu .burger').on('click', function () {
        $('.unrealMenu').toggleClass('active');

        if ($('.unrealMenu').hasClass('active')) {
            $('.vpNavigation').fadeIn();
            $('body, html').css({
                'overflow': 'hidden'
            });
            $('.globalLogo').addClass('opened');
        } else {
            $('.vpNavigation').fadeOut();
            $('body, html').css({
                'overflow': 'initial'
            });
            $('.globalLogo').removeClass('opened');
        }
    });


    if ($('#scrollTop').length) {
        let winHeight = $(window).height();
        let sectionTop = $('#scrollTop').offset().top - (winHeight - 10);
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > sectionTop) {
                $('.padd img').css({
                    'transform': 'scale(1)',
                    'opacity': 1
                });
            }
        });
    }

    if($('.aboutAnim').length){
        $(window).on('scroll', function () {
            textAnimation('aboutAnim')
        });
    }

    if($('.videoAnim').length){
        $(window).on('scroll', function () {
            textAnimation('videoAnim')
        });
    }

    if($('.createVideosAnim').length){
        $(window).on('scroll', function () {
            textAnimation('createVideosAnim')
        });
    }

    if($('.justCheckAnim').length){
        $(window).on('scroll', function () {
            textAnimation('justCheckAnim')
        });
    }

    if($('.unrealTeamAnim').length){
        $(window).on('scroll', function () {
            textAnimation('unrealTeamAnim')
        });
    }

    let preloader = $('.loading');

    if ($(preloader).length) {
        setTimeout(function () {
            window.scrollTo(0, 0);
            $('body, html').css({
                'overflow': 'initial'
            }, 2000);
            $(preloader).fadeOut();
        },);
    } else {
        $('body, html').css({
            'overflow': 'initial'
        });
    }

    if ($('.teamWorkers')) {
        let k = ('.teamWorkers').length % 2 === 0 ;
        $('.teamWorkers').slick({
            dots: true,
            infinite: k,
            speed: 300,
            slidesToShow: 1,
            variableWidth: true,
            prevArrow: $('.prev'),
            nextArrow: $('.next')
        });
    }

    $('.scrolling span').on('click', function (e) {
        e.preventDefault();
        const divTop = $('.mainSection').offset().top;
        $('html, body').animate({
            scrollTop: divTop
        }, 1000, "swing");

    });


    const globalNav = $('.globalNav'),
        globalNavTop = $(globalNav).offset().top;

    if ($(globalNav).length) {

        $(window).on('scroll', function () {
            if ($(this).scrollTop() > 350) {
                $('nav').css({
                    'background-color' : '#000'
                });
            } else {
                $('nav').css({
                    'background-color': 'transparent'
                })
            }
        });
    }

    function initMap() {
        var icon = {
            url: "/build/img/unreal/logoUnreal.svg", // url
            scaledSize: new google.maps.Size(20, 20), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };
        // The location of Uluru
        var uluru = {lat: 34.165086, lng: -118.395922};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {

                zoom: 14,
                scrollwheel: false,
                center: uluru
            });
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({
            position: uluru,
            animation: google.maps.Animation.BOUNCE,
            map: map,
            icon: icon,
        });
    }

    if (document.getElementById('map') != undefined) {
        initMap();
    }


}); //ready

function textAnimation(parentElem) {
    if ($(this).scrollTop() > $('.'+ parentElem ).offset().top - 400) {
        $('.' + parentElem + ' .leftAnim').css({
            'transform': 'translateX(0)',
            'opacity': 1
        });
        $('.' + parentElem + ' .rightAnim').css({
            'transform': 'translateX(0)',
            'opacity': 1
        });
    }
}