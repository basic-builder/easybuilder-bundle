import {Delete, Get, HandleResponseState, Post} from '../services/Http.Client';
import {defaultErrorMsg, handleResponse} from "../services/utilities";

/*
* Main Menu start
* */
export const getImageByIdApi = (id) =>
    Get(`/image/info/${id}`, {})
        .then(HandleResponseState);

export const removeImgByIdApi = (id) =>
    Delete(`/galleries/${id}`)
        .then(HandleResponseState)

export const updateImgDataApi = (data) =>
    fetch('/galleries/images', {
        method: 'POST',
        body: JSON.stringify(data).toString()
    }).then(res => res.ok && res.status === 200);

export const getUploadedImagesApi = (data) =>
    Post('/uploaded/images', {}, data, true)
        .then(HandleResponseState)

/*
* Main Menu end
* */


/*
* Clients Personal start
* */
export const addNewMessageApi = (data) =>
    Post('/message/new', {}, data)
        .then(res => handleResponse(
            res,
            undefined,
            () => !!res.success,
            false,
            'Your message successfully saved.',
            defaultErrorMsg
        ));
/*
* Clients Personal end
* */

export const uploadImage = (file) => {
    const formData = new FormData();

    formData.append('files[]', file)

    return Post('/galleries/files', {}, formData, true)
}
