require('swiper/swiper.scss');
require('swiper/css/swiper.min.css');

const Swiper = require('swiper/js/swiper');

require('swiper/components/effect-fade/effect-fade.scss')


$(document).ready(function (e) {

    let direction = 'vertical';

    if($(document).width() < 991){
        direction = 'horizontal';
    }

    if ($('.swiper-container').length) {
        var mySwiper = new Swiper('.swiper-container', {
            direction: direction,
            loop: true,
            speed: 3000,
            autoplay: {
                delay: 4500,
                disableOnInteraction: false,
            },
            fadeEffect: {
                crossFade: true
            },
            effect: "fade",
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },

        });
    }

}); // ready