import 'jquery-ui-sortable-npm';

const easyadminDragndropSort = {
    initDraggableEntityRows: function () {

        if (document.body.classList.contains('ea') && document.body.classList.contains('index')) {

            if (!Array.prototype.last) {
                Array.prototype.last = function () {
                    return this[this.length - 1];
                };
            }

            let content = document.getElementById("main");
            let table = content.getElementsByClassName("table")[0];
            if (table && table.classList.contains('allow_sortable')) {
                let dataCount = parseInt(table.getAttribute('data-count'));
                if (dataCount) {
                    let entityClass = table.getAttribute('data-class-name');
                    let thead = table.getElementsByTagName("thead")[0];
                    let theadRows = thead.getElementsByTagName("tr")[0];
                    let tbody = table.getElementsByTagName("tbody")[0];
                    let tableRows = tbody.getElementsByTagName("tr");
                    let dragSrcEl = null; // the object being drug
                    let startPosition = null; // the index of the row element (0 through whatever)
                    let endPosition = null; // the index of the row element being dropped on (0 through whatever)
                    let parent; // the parent element of the dragged item
                    let entityId; // the id (key) of the entity

                    // draw sortable icon
                    for (let row in tableRows) {
                        if (tableRows.hasOwnProperty(row)) {
                            const td = '<td draggable="true">' +
                                '<span><i class="fas fa-arrows-alt"></i></span>' +
                                '</td>';
                            tableRows[row].insertAdjacentHTML('beforeend', td);
                        }
                    }

                    const th = '<th></th>';
                    theadRows.insertAdjacentHTML('beforeend', th);

                    $(tbody).sortable({
                        handle: 'td[draggable="true"]',
                        start: function handleDragStart(e, ui) {
                            dragSrcEl = ui.item[0];
                            entityId = dragSrcEl.getAttribute('data-id');
                            parent = dragSrcEl.parentNode;
                            startPosition = Array.prototype.indexOf.call(parent.children, dragSrcEl);
                        },
                        stop: function (e, ui) {
                            endPosition = Array.prototype.indexOf.call(parent.children, ui.item[0]);
                            if (startPosition !== endPosition) {

                                let xhr = new XMLHttpRequest();
                                xhr.open('GET', (encodeURI('/sort/' + entityClass + '/' + entityId + '/' + endPosition)));
                                xhr.onload = function () {
                                    if (xhr.status !== 200) {
                                        alert("An error occurred while sorting. Please refresh the page and try again.")
                                    }
                                };
                                xhr.send();
                            }

                        }
                    });
                    $(tbody).disableSelection();
                }
            }
        }
    },

    /**
     * Primary Admin initialization method.
     * @returns {boolean}
     */
    init: function () {
        this.initDraggableEntityRows();
        return true;
    }
};

$(document).ready(function () {
    easyadminDragndropSort.init();
});
