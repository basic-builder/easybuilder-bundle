require('bootstrap/dist/css/bootstrap.css');
require('../style/one_page.scss');
require('font-awesome/css/font-awesome.css');

import('./swiper');

import('./contactForm_v2')


$(document).ready(function (e) {

    $('a[data-id="contact"]').on('click', function (e) {
        e.preventDefault();

        let contactOffset = $('.contact').offset().top;

        $("html, body").animate({
            scrollTop: contactOffset
        }, 1400);

    })

});