import '../../style/admin/notification.scss';

export const messageType = {
    error: 'error',
    success: 'success',
    warning: 'warning'
}

export const defaultErrorMsg = 'Something went wrong.';

export const uniqueId = () => {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 5; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

export const showMessage = (type = messageType.warning, message) => {
    const defaultMessagesDiv = $('#defaultNotification');
    const uniqId = uniqueId();
    const messageDiv = `<div class="${type}" id="${uniqId}"><p>${message}</p></div>`;

    if (defaultMessagesDiv.length) {
        defaultMessagesDiv.append(messageDiv)
    } else {
        $('body').append(
            `<div id="defaultNotification">
            ${messageDiv}
         </div>`
        );
    }

    $(`#${uniqId}`).fadeOut(5000, 'swing', function () {
        $(this).remove();

        const defaultMessagesDiv = $('#defaultNotification');
        if(!defaultMessagesDiv.children().length) {
            defaultMessagesDiv.remove()
        }
    })
};

/*
* For debugging purposes
* */
export const stopForSeconds = (second = 2) => {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < second * 1000);
}


/*
* Http start
* */
export const getErrorsFromResponse = responseData => {
    if (responseData.message) {
        if (
            typeof responseData.message === "object" &&
            !Array.isArray(responseData.message)
        )
            return Object.values(responseData.message);
        return null;
    }
    return null;
};

export const handleResponse = (result, prep = undefined, success = undefined, error = undefined, successMessage = undefined, errorMessage = undefined) => {
    if (prep) prep();
    if (result) {
        if (result.success) {
            if (successMessage) showMessage(messageType.success, successMessage);
            if (success) return success(result.data);
            return result.data;
        } else {
            showMessage(messageType.error, getErrorsFromResponse(result));
            return false;
        }
    } else {
        if (errorMessage) showMessage(messageType.error, errorMessage);
        if (error) return error();
        return false;
    }
};

/*
* Http end
* */

export const isLoading = (loading = true) => {
    const exists = document.querySelector('.globalLoading');

    if(loading && !(exists)) {
        const loader = $('<div class="globalLoading"></div>')
        $('body').append(loader);
    }

    if (!loading && exists) {
        $('.globalLoading')?.remove();
    }
};


//Get action key from url
export const linkAction = {
    edit: 'edit',
    show: 'detail',
    main: 'index'
};

export const getLinkAction = () => {
    const urlParams = new URLSearchParams(window.location.search);
    const action = urlParams.get('crudAction');

    return action || false;
}