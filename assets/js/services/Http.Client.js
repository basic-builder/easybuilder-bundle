import "whatwg-fetch";
import {defaultErrorMsg, isLoading, messageType, showMessage} from "./utilities";

export const HandleResponseState = result => result ? result : () => {
	showMessage(messageType.error, defaultErrorMsg);
	return false
};

export const Post = (url, headers = {}, body, plainBody = false) => {
	isLoading();
	if (!headers.Accept) {
		headers.Accept = "application/json, */*";
	}

	return fetch(url, {
		method: "POST",
		headers,
		body: plainBody ? body : JSON.stringify(body).toString()
	})
		.then(response => {
			if (!response.ok || response.status === 401) {
				return false;
			}

			return response.json();
		})
		.finally(() => isLoading(false))
		.catch(data => {
			console.log("Error", data);
		});
};

export const Delete = (url, headers = {}) => {
	isLoading()
	if (!headers.Accept) {
		headers.Accept = "application/json, */*";
	}

	return fetch(url, {
		method: "DELETE",
		headers
	})
		.then(response => !(!response.ok || response.status === 401))
		.finally(() => isLoading(false))
		.catch(data => {
			console.log("Error", data);
		});
};

export const Get = (url, headers = {}) => {
	isLoading();

	if (!headers.Accept) {
		headers.Accept = "application/json, */*";
	}

	return fetch(url, {
		method: "GET",
		headers
	})
		.then(response => {
			if (!response.ok || response.status === 401) {
				return false;
			}

			return response.json();
		})
		.finally(() => isLoading(false))
		.catch(data => {
			console.log("Error", data);
		});
};

export const Put = (url, headers = {}, body) => {
	isLoading();
	if (!headers["Content-Type"]) {
		headers["Content-Type"] = "application/json";
	}

	return fetch(url, {
		method: "PUT",
		headers,
		body: JSON.stringify(body).toString()
	})
		.then(response => {
			if (!response.ok || response.status === 401) {
				return false;
			}

			return response.json();
		})
		.finally(() => isLoading(false))
		.catch(data => {
			console.log("Error", data);
		});
};
