const modalComponent = (content = '', title = '', handleOkClick) => {
    const modal = $(`
        <div class="modalComponent">
            <div class="modalContent">
                <p class="title">${title}</p>
                <button type="button" class="close cancel">x</button>
                <div class="contentDiv">
                    ${content}
                </div>
                <div class="buttons">
                    <button type="button" class="btn btn-primary cancel">Cancel</button>
                    <button type="button" class="btn btn-success">Ok</button>
                </div>
            </div> 
        </div>
    `).click(e => {
        if($(e.target).hasClass('btn-success') && handleOkClick) {

            handleOkClick().then(done => done && modal.remove());

        } else if ($(e.target).hasClass('cancel') || e.target === modal[0]) {
            //Close modal on cancel or outside click
            modal.remove()
        }
    });

    const modalExist = document.querySelector('.modalComponent')
    modalExist && modalExist.remove();

    $('body').append(modal);
}

export default modalComponent;