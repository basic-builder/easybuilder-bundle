import {updateImgDataApi} from "../api/admin.api";
import modalComponent from "./modalComponent";
import {defaultErrorMsg, messageType, showMessage} from "../services/utilities";

const showDefaultImageModal = (res) => {
    const {id, title, url_link, altText, description, is_enabled, downloadLink} = res;

    const modalContent = `
        <div class="galleryModal">
            <div>
                <div class="galleryAdminGrid">
                    <div>
                        <label for="">Title</label>
                        <input type="text" value="${title || ''}" name="modal-item-title">
                    </div>
                    <div>
                        <label for="">Link</label>
                        <input type="text" value="${url_link || ''}" name="modal-item-link">
                    </div>
                    <div>
                        <label for="">Alt text</label>
                        <input type="text" value="${altText || ''}" name="modal-item-alt-text">
                    </div>
                    <div>
                        <input type="checkbox" id="checkbox" name="modal-item-isEnabled" ${is_enabled ? 'checked' : ''}>
                        <label for="checkbox">Is enabled</label>
                    </div>
                </div>
                <div>
                    <label for="">Description</label>
                    <textarea name="modal-item-description">${description || ''}</textarea>
                </div>
                <img src=${downloadLink || ''} alt="">
            </div>
        </div>
        `;

    const handleOkClick = () => {
        return new Promise(resolve => {
            const data = {
                'modal-item-id': id
            };

            const inputs = document.querySelectorAll('.modalContent input, .modalContent textarea');

            inputs.forEach( key => {
                if (key.value?.trim()) {
                    data[key.name] = key.name !== 'modal-item-isEnabled' ? key.value : key.checked;
                } else {
                    data[key.name] = '';
                }
            });

            updateImgDataApi(data).then(updated => {
                if (updated) {
                    showMessage(messageType.success, 'File Successfully Updated');
                    resolve(true)
                } else {
                    showMessage(messageType.error, defaultErrorMsg);
                }
            })
        } )
    }

    modalComponent(modalContent, 'Gallery item',  handleOkClick);
}

export default showDefaultImageModal;