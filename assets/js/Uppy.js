import '@uppy/core/dist/style.css';
import '@uppy/dashboard/dist/style.css';
import Uppy from '@uppy/core';
import Dashboard from '@uppy/dashboard';
import GoogleDrive from '@uppy/google-drive';
import Dropbox from '@uppy/dropbox';
import Instagram from '@uppy/instagram';
import Webcam from '@uppy/webcam';
import XHRUpload from '@uppy/xhr-upload';
import {getImageByIdApi, getUploadedImagesApi, removeImgByIdApi} from "./api/admin.api";
import {defaultErrorMsg, messageType, showMessage} from "./services/utilities";
import showDefaultImageModal from "./helpers/showDefaultImageModal";

const addImagesToGallery = (imagesArr, target) => {
    let imagesHtml = document.createDocumentFragment();
    if (imagesArr?.length) {
        imagesArr.forEach(image => {
            const {src, id, title} = image;
            const imgDiv = document.createElement('div');
            const remove = document.createElement('a');
            const img = document.createElement('img');

            imgDiv.className = 'image-' + id;

            remove.appendChild(document.createTextNode('×'));
            remove.className = 'adminGalleryRemove';
            remove.setAttribute('href', '#');
            remove.setAttribute('data-id', id);

            remove.onclick = () => {
                removeImgByIdApi(id).then(removed => {
                    if (removed) {
                        document.querySelector('.image-' + id).remove();
                        document.querySelector('option[value="' + id + '"]').remove();

                        showMessage(messageType.success, 'File Successfully Deleted');
                    } else {
                        showMessage(messageType.error, defaultErrorMsg)
                    }
                })
            };

            img.onclick = () => getImageByIdApi(id).then(res => {
                if (res?.id) {
                    showDefaultImageModal(res);
                } else {
                    showMessage(messageType.error, defaultErrorMsg);
                }
            });

            let p = document.createElement('p');
            p.classList.add('imgTitle');
            p.textContent = title;

            imgDiv.appendChild(p);
            img.setAttribute('src', src);
            imgDiv.appendChild(remove);
            imgDiv.appendChild(img);
            imagesHtml.appendChild(imgDiv)
        })
    }

    target.append(imagesHtml);
}

const InsertAlreadyUploadedImages = async (select, target) => {
    const alreadyUploadedImages = select.querySelectorAll('option[selected]');
    const uploadedImages = [];

    alreadyUploadedImages?.forEach(file => file.value && uploadedImages.push({
        id: Number(file.value),
        src: file.innerText
    }));

    if (uploadedImages.length) {
        await getUploadedImagesApi([uploadedImages.map(imgObj => imgObj.id)])
            .then(res => {
                res?.length && res.forEach(img => {
                    uploadedImages.filter(obj => {
                        if (obj.id === img.id) {
                            obj.title = img.title || ''
                        }
                    })
                })
            })
        addImagesToGallery(uploadedImages, target)
    }
}

const UppyInit = ({
                      maxFileSize = 31000000,
                      maxNumberOfFiles = 10,
                      minNumberOfFiles = 1,
                      allowedFileTypes = ['image/*'],
                      uploadURL = '/galleries/files',
                      note = 'Images only, 1–10 files, up to 2 MB'
                  }) => {

    const uppySelect = document.querySelector(`select[data-gallery='uppy']`);

    if (uppySelect) {
        const formGroupDiv = uppySelect.closest('.form-group');
        $(formGroupDiv).children().hide()
        const uploadedImagesDiv = $('<div class="adminGallery"></div>');
        $(formGroupDiv).append(uploadedImagesDiv);

        InsertAlreadyUploadedImages(uppySelect, uploadedImagesDiv);

        const up = Uppy({
            debug: true,
            autoProceed: false,
            allowMultipleUploads: false,
            restrictions: {
                maxFileSize,
                maxNumberOfFiles,
                minNumberOfFiles,
                allowedFileTypes
            },
            meta: {
                isMain: false
            },
        })
            .use(Dashboard, {
                trigger: '.UppyModalOpenerBtn',
                inline: true,
                target: formGroupDiv,
                replaceTargetContent: false,
                showProgressDetails: true,
                note,
                height: 470,
                thumbnailWidth: 280,
                showSelectedFiles: true,
                browserBackButtonClose: true
            })
            .use(GoogleDrive, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
            .use(Dropbox, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
            .use(Instagram, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
            .use(Webcam, {
                target: Dashboard,
                title: 'Camera'
            })
            .use(XHRUpload, {endpoint: uploadURL, method: 'POST', formData: true, fieldName: 'files[]'})

        up.on('complete', uploads => {
            const uploadsArr = [];
            if (uploads.successful.length) {
                let optionsHtmlStr = '';

                uploads.successful.forEach(key => {
                    const elemId = key.response.body.id;
                    const downloadLink = key.response.body.download_link;

                    optionsHtmlStr += `<option value=${elemId} selected="selected">${downloadLink}</option>`;

                    uploadsArr.push({id: elemId, src: downloadLink});
                    up.removeFile(key.id);
                });

                if (uploadsArr.length) {
                    //Pushing hidden options to hidden uppy select to save uploaded files on submit.
                    $(uppySelect).append(optionsHtmlStr);

                    addImagesToGallery(uploadsArr, uploadedImagesDiv);

                    showMessage(messageType.success, `File${uploadsArr.length > 1 ? 's' : ''} Successfully Added`);
                }
            }
        })
    }
};

export default UppyInit;