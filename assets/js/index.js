require('../style/index.scss');


$(document).ready(function (e) {

    let span = $('span.verticalLine');

    if($(document).width() > 991){

        $('.leftWeb a').hover(function () {
                $(span).css({
                    'left': 'calc(50% - 150px)',
                    'opacity': 1
                })
            },

            function () {
                $(span).css({
                    'left': '50%',
                    'opacity': 0.4
                })
            });

        $('.rightWeb a').hover(function () {
                $(span).css({
                    'left': 'calc(50% + 150px)',
                    'opacity': 1
                })
            },

            function () {
                $(span).css({
                    'left': '50%',
                    'opacity': 0.4
                })
            })
    }

});//ready