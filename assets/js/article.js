$(document).ready(function (e) {

    $('#Article_isNews').on('change', function (e) {
        isNew(this);
    });
    isNew('#Article_isNews');

});//ready


function isNew(elem) {
    if ($(elem).prop('checked')) {
        $('#Article_subMenu').val('');
        $('#Article_subMenu').closest('.field-association ').slideUp();
    } else {
        $('#Article_subMenu').closest('.field-association ').slideDown();
    }
}