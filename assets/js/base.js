require('bootstrap/dist/css/bootstrap.css');
require('font-awesome/css/font-awesome.css');
require('jquery.ripples/dist/jquery.ripples');

require('slick-carousel/slick/slick.css');
require('slick-carousel/slick/slick-theme.css');
require('slick-carousel/slick/slick');
require('../style/homepage.scss');

require('../style/main.scss');

import('./swiper');
import('./typeAnimation');
import('./contactForm');
import('./clientForm');

$(document).ready(function () {

    // menu
    $('.rightMenuMenu').on('click', function () {
        $(this).toggleClass('active');
        $('.mobileNav').toggleClass('active');
        $('body').toggleClass('overflow');
    });


    // map
    let map;

    function initMap() {
        var uluru = {lat: 54.7214655, lng: 25.2799381}
        map = new google.maps.Map(document.getElementById("map"), {
            center: uluru,
            zoom: 8,
            disableDefaultUI: true,
            point: true
        });
        var marker = new google.maps.Marker({
            position: uluru,
            animation: google.maps.Animation.BOUNCE,
            map: map
        });
    }

    // create map
    initMap();

    $('.languages').on('click', function () {
        $(this).toggleClass('active');
    });


    // right menu height calculation
    calculateRightMenuHeight();
    $(window).resize(function (e) {
        calculateRightMenuHeight()
    });

    // nav scrolling
    $(document).on('scroll', function (e) {
        if ($(this).scrollTop() > 10 ) {
            $('nav').addClass('scrolled');
        } else {
            $('nav').removeClass('scrolled');
        }
    }).trigger('scroll');


    // closing quote/cv form when clicked out
    $('.bodyBlack').mouseup(function (e) {
        let targetOne = $('.clientCustomForms');

        // if (!targetOne.is(e.target) && targetOne.has(e.target).length === 0) {
        if ($(targetOne).hasClass('open')) {
            $('.bodyBlack').removeClass('open');
            $(targetOne).removeClass('open');
            $('.clientCustomFormsData').html('');
        }
    });

}); //ready


function calculateRightMenuHeight() {
    let targetElement = $('header').length ? $('header') : $('div.mainText').length ? $('div.mainText') : $('.loginForm');
    if ($(targetElement).length) {
        let elementHeight = $(targetElement).innerHeight();

        $('.rightMenu').css({
            'height': elementHeight
        });

    }

}