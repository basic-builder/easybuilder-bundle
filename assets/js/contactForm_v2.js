$(document).ready(function (e) {

    $('.contactForm').submit(function (e) {
        e.preventDefault();

        let correctInput = 0;

        let contactInput = $('.contactInput');
        var form = new FormData();
        form.append('subject', 'contact');
        $(contactInput).each(function (key, val) {

            if ($(val).val() !== '') {
                form.append($(val).attr('name'), $(val).val());
                correctInput++;
            } else {
                $(val).css({
                    'border-color': 'red'
                });
            }
        });

        setTimeout(function (e) {
            $(contactInput).css({
                'border-color': '#979797'
            });
        }, 3000);

        if (correctInput === $(contactInput).length) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/contact/form");

            xhr.onreadystatechange = function () {

                if (xhr.readyState === 4) {
                    $('.message .col-12').html('');
                    let data = JSON.parse(xhr.responseText);
                    if (xhr.status === 200) {

                        $(`<div class="alert alert-success text-center">${data.result}</div>`).appendTo('.message .col-12');
                        $(contactInput).val('');

                    } else {

                        Object.values(data.result).forEach(function (val, key) {
                            $(`<div class="alert alert-danger text-center">${val}</div>`).appendTo('.message .col-12');
                        });
                    }
                    setTimeout(function (e) {
                        $('.message .col-12').html('');
                    }, 2500);
                }
            };


            xhr.send(form);
        }

    });
});