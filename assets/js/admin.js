import '../style/admin/admin.scss';
import '../style/admin/global.scss';
import UppyInit from "./Uppy";
import {getLinkAction, linkAction} from "./services/utilities.js";
import handleArticleTab from "./tabs/content/content.Article";

require('../style/admin/sortable.scss');
require('./ui-sortable');

/* todo list slider */
$(document).ready(() => {

    /* Load Uppy if url action is edit and uppy select exist
        * */
    if (document.querySelector(`select[data-gallery='uppy']`)) UppyInit({});


    /*
    * Handle Article tab script
    * !IMPORTANT: This called only on edit action for now.
    * */
    if (document.querySelector('select[article-image-upload]')) handleArticleTab();

});

//todo
// $(document).on('change', '.custom-file-input', function (target) {
//     var input = $(this),
//         numFiles = input.get(0).files ? input.get(0).files.length : 1,
//         label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//     input.trigger('fileselect', [label, numFiles]);
//     /**/
//     target = input.get(0).files[0] ? input.get(0).files[0].name : ' ';
//
//     target = target.length > 15 ? `${target.substring(0, 15)}...` : target;
//
//     $(input).parent().find('label').html(target);
// });


/* todo odessi design */
// todo check with Tiko if we need below code
// $('.article_widget.form-group').removeClass('form-group');
// $('.address_widget.form-group').removeClass('form-group');
// $('.gallery_widget.form-group').removeClass('form-group');
// $('.business_widget.form-group').removeClass('form-group');
//

setTimeout(function () {
    if (document.querySelector('.nax')) {
        console.log('dsada');
        document.querySelector('.nax').closest('.form-group').style.display = 'block'
    }
});
