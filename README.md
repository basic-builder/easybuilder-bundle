# EasyBuilder Bundle

Make a basic informative website integrated EasyAdmin

## Documentation

Check out the documentation on the [official website](https://merxforum.com).

## Support

* `0.x` & `1.x` are old versions not maintained anymore.
  
## Contribution help

```
not used
```

## License

This package is available under the [MIT license](LICENSE).