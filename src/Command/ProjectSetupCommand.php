<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Command;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\User;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\TemplateSettingsRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;

class ProjectSetupCommand extends Command
{
    //constants for project setup
    const IS_ROUTE_DECONSTRUCT = 'route_deconstruct', IS_ROUTE_CONSTRUCT = 'route_construct';

    protected static $defaultName = 'easy-builder:project-setup';

    // get a emtity manager
    private $em;
    private $passwordEncoder;
    private $templateSettingsRepository;
    private $userRepository;
    private $parameterBag;

    public function __construct(EntityManagerInterface $em,
                                UserPasswordEncoderInterface $passwordEncoder,
                                TemplateSettingsRepository $templateSettingsRepository,
                                UserRepository $userRepository,
                                ParameterBagInterface $parameterBag
    )
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->templateSettingsRepository = $templateSettingsRepository;
        $this->userRepository = $userRepository;
        $this->parameterBag = $parameterBag;

        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this
            ->setDescription('This command is used to create project main setup information')
            ->addArgument('action', InputArgument::REQUIRED, 'route_deconstruct or route_construct')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        
        $now = new \DateTime('now');
        $io = new SymfonyStyle($input, $output);

        $action = $input->getArgument('action');
        
        if($action === self::IS_ROUTE_CONSTRUCT) {

            // update a database scham
            $process = new Process(['php7.4', 'bin/console', 'cache:warmup']);
            $process->run();

            // executes after the command finishes
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            $io->write($process->getOutput());
            
            
            $em = $this->em;

            // start make a default user
            $email = 'cms@paremuzyan.com';
            $user = $this->userRepository->findOneBy(['email' => $email]);

            if(!$user) {
                // start make a main user
                $user = new User();
                $user->setEmail($email);
                $user->setIsVerified(true);
                $user->setRoles(['ROLE_SUPER_ADMIN']);
                $user->setPassword(
                    $this->passwordEncoder->encodePassword(
                        $user,
                        sprintf('Admin%s#*', $now->format('Y'))
                    )
                );

                $em->persist($user);

                try {
                    $em->flush();
                }catch (\Exception $exception){

                    $io->error($exception->getMessage());
                }
            }

            $io->success('Main user information successfully inserted . Start insert main template configuration.');

            $templateSettings = $this->templateSettingsRepository->findCurrent();

            if(!$templateSettings) {
                // start make a main template settings
                $templateSettings = new TemplateSettings();
                $templateSettings->setName('Merx');
                $templateSettings->setAddress('13636 Ventura Blvd Unit 103, Sherman Oaks, California 91423');
                $templateSettings->setPhone('+1 818-648-8208');
                $templateSettings->setEmail('cs@merxforum.com');
                $templateSettings->setFb('#');
                $templateSettings->setInstagram('#');

                // start make a main template base dir
                $templateInfo = new TemplateInfo();
                $templateInfo->setBaseDirName('main');
                $templateInfo->setName('Basic');
                $templateSettings->setTemplateInfo($templateInfo);


                $em->persist($templateSettings);

                $io->success('Main template configuration successfully inserted. Start Flush data.');

                try {
                    $em->flush();

                    $io->success('Main data successfully flushed.');
                }catch (\Exception $exception){
                    $io->error("{$exception->getMessage()}");

                    $io->error('Sorry . Cannot flush data please check database configuration on .env file.');
                }
            }

            return 0;
        }
        
        $progressBar = new ProgressBar($output, 14);

        $progressBar->start();
        
        $fileSystem = new Filesystem();
        $projectDir = $this->parameterBag->get('kernel.project_dir');

        $routeLoaderFile = sprintf('%s/config/routes.yaml', $projectDir);

        if($fileSystem->exists($routeLoaderFile)) {

            $routeLoader = Yaml::parseFile($routeLoaderFile);

            $yaml = Yaml::dump(['dashboard'=>$routeLoader['dashboard']]);
            $fileSystem->dumpFile($routeLoaderFile, $yaml);

            $io->success('Routing successfully deconstructed');

        }else{

            $io->error('Routing file not found .');
        }

        $progressBar->advance();

        
        $envFile = sprintf('%s/.env.local', $projectDir);
        $envFileProd = sprintf('%s/.env', $projectDir);

        if($fileSystem->exists($envFile)) {

            $fileSystem->remove($envFile);
        }


        $fileSystem->touch($envFile);

        foreach ($this->getEnvParams() as $param) {

            $fileSystem->appendToFile($envFile, $param);
        }
        
        $fileSystem->copy($envFile, $envFileProd);
        
        $io->write(sprintf('%s file succesfuly created.', $envFile));
        
        $progressBar->advance();

        // make a database
        $process = new Process(['php7.4', 'bin/console', 'doctrine:database:create', '--if-not-exists']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());
        
        $progressBar->advance();
        

        // update a database scham
        $process = new Process(['php7.4', 'bin/console', 'cache:warmup']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $io->write($process->getOutput());
        $progressBar->advance();
        
        
        $process = new Process(['php7.4', 'bin/console', 'doctrine:schema:update', '--force']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());

        $progressBar->advance();
        
        
        
        // update a database scham
        $process = new Process(['php7.4', 'bin/console', 'easy-builder:project-setup', 'route_construct']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $io->write($process->getOutput());
        $progressBar->advance();
        
        // update a database scham
        $process = new Process(['php7.4', 'bin/console', 'cache:warmup']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $io->write($process->getOutput());
        $progressBar->advance();
        
        
        $progressBar->advance();

        $routeLoaderFile = sprintf('%s/config/routes.yaml', $projectDir);

        // start deconstruct routing


            if($fileSystem->exists($routeLoaderFile) && isset($routeLoader)) {

                    $yaml = Yaml::dump($routeLoader);
                    $fileSystem->dumpFile($routeLoaderFile, $yaml);

                    $io->success('Routing successfully constructed');
            }else{

                $io->error('Routing file not found .');
            }

        $progressBar->advance();
        // update a database scham
        $process = new Process(['php7.4', 'bin/console', 'cache:warmup']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());
        
        $progressBar->advance();
        // update a database scham
        $process = new Process(['yarn', 'install']);
        $process->run();
        
        
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());
        $progressBar->advance();

        // update a database scham
        $process = new Process(['yarn', 'encore', 'dev']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());
        $progressBar->advance();

        // update a database scham
        $process = new Process(['php7.4', 'bin/console', 'ckeditor:install', '--clear=drop']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());        
        $progressBar->advance();
        // update a database scham
        $process = new Process(['php7.4', 'bin/console', 'assets:install']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $io->write($process->getOutput());
        $progressBar->advance();

        $progressBar->finish();
        return 0;
    }


    private function getEnvParams() {

        return
            ["APP_ENV=dev\r\n",
                sprintf("APP_SECRET=%s\r\n", $this->generateRandomString(64)),
                "FRONT_URL=http://127.0.0.1:8000\r\n",
                "PHP_V=php74\r\n",
                "DATABASE_URL='mysql://root:root@127.0.0.1:3306/cms_crud1?serverVersion=5.7'\r\n",
                "MAILER_DSN=smtp://web@paremuzyan.com:M2o1B6h4@mail.paremuzyan.com:465/?timeout=60&encryption=ssl&auth_mode=login\r\n",
                "ADMIN_EMAIL=web@paremuzyan.com\r\n",
                "TEMPLATE_DIR=@EasyBuilder/main\r\n"
        ];

    }


    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
