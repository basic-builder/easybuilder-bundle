<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Command;

use BasicBuilder\Bundle\EasyBuilderBundle\Service\ImagesToWebpConvertor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImageToWebpCommand extends Command
{
    protected static $defaultName = 'easy-builder:image-to-webp';

    private $webpConvertor;

    public function __construct(ImagesToWebpConvertor $webpConvertor)
    {
        $this->webpConvertor = $webpConvertor;

        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {



        $io = new SymfonyStyle($input, $output);

        $this->webpConvertor->convertExistingImages();

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
