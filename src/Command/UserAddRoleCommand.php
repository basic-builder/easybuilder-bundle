<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Command;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserAddRoleCommand extends Command
{
    protected static $defaultName = 'easy-builder:user:add-role';
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('user_email', InputArgument::REQUIRED, 'User Email')
            ->addArgument('role', InputArgument::REQUIRED, 'User Role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('user_email');
        $role = $input->getArgument('role');

        if (!$email || !$role) {
            $io->error("User Email or Role Cannot be null . Please enter correct parameters .");
        }

        try{
            $this->addRole($email, $role, $output);

        }catch (\Exception $exception) {

            $output->write("<error>Cannot flush data {$exception->getMessage()}</error>");
        }


        $io->success("The role {$role} has been added to {$email}");

        return 0;
    }

    private function addRole(string $email, string $role, OutputInterface $output) {

        $manager = $this->em;

        $user = $manager->getRepository(User::class)->findOneBy(['email'=>$email]);

        if($user instanceof User === false) {
            $output->write("<error>User by {$email} not found .</error>\n");

            exit;
        }

        $roles = $user->getRoles();

        if(in_array($role, $roles) == false) {

            $roles[] = $role;
            $user->setRoles($roles);
        }

        $manager->persist($user);

        try{
            $manager->flush();
        }catch (\Exception $exception){

            $output->write("<error>Cannot flush data {$exception->getMessage()}</error>\n");
            exit;
        }

        return $user;
    }
}
