<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\AreasOfExpertise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AreasOfExpertiseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('industry', ChoiceType::class, [
                'choices' => $this->getIndustryList(),
                'label' => 'form.industry',
                'attr' => [
                    'class' => 'selected2'
                ]
            ])
            ->add('yearsOfExperience', IntegerType::class, [
                'label' => 'form.years_of_expertise',
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('yearsOfTraining', IntegerType::class, [
                'required' => false,
                'label' => 'form.years_of_training',
                'attr' => [
                    'min' => 0
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AreasOfExpertise::class,
        ]);
    }

    // get industry list
    protected function getIndustryList()
    {
        return [
            'form.medical_clinical' => 'Medical (Clinical research)',
            'form.medical_devices' => 'Medical (Devices)',
            'form.medical_general' => 'Medical (General healthcare)',
            'form.marketing_transcreation' => 'Marketing',
            'form.seo' => 'SEO',
            'form.it' => 'IT and technology',
            'form.gaming' => 'Gaming',
            'form.technical_engineering' => 'Technical and engineering',
            'form.telecommunication' => 'Telecommunication',
            'form.arts_culture' => 'Arts and culture',
            'form.automotive' => 'Automotive',
            'form.legal' => 'Legal',
            'form.general' => 'General',
            'form.business' => 'Business',
            'form.creative_writing' => 'Creative Writing',
            'form.finance_and_accounting' => 'Finance and accounting',
            'form.pharmaceuticals_and_life_sciences' => 'Pharmaceuticals and Life Sciences',
            'form.software_and_web' => 'Software and Web',
            'form.sports' => 'Sports',
            'form.travel_and_hospitality' => 'Travel and hospitality',
        ];
    }
}
