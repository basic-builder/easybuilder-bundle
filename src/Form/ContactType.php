<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['required'=>true, 'label'=>false,
                'attr'=>['placeholder'=>'Name', 'class'=>'name']])
            ->add('email', EmailType::class, ['required'=>true, 'label'=>false,
                'attr'=>['placeholder'=>'Email', 'class'=>'email']])
            ->add('subject', TextType::class, ['required'=>true, 'label'=>false,
                'attr'=>['placeholder'=>'Subject', 'class'=>"subject"]])
            ->add('message', TextareaType::class, ['required'=>true, 'label'=>false,
                'attr'=>['placeholder'=>'Message', 'cols'=>30, 'rows'=>10]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
