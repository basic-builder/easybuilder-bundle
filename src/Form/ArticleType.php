<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    protected $parameterBag;
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $languages = $this->parameterBag->get('jms_i18n_routing.locales');
        $languageDefault = $this->parameterBag->get('jms_i18n_routing.default_locale');
        $builder
            ->add('name', TextType::class)
                ->add('translations', TranslationsType::class, [
                    'label' => false,
                    'attr' => [
                        'css_class' => 'col-12'
                    ],
                    'locales' => $languages,
                    'default_locale' => [$languageDefault],
                    'required_locales' => [$languageDefault],
                    'fields' => [
                        'shortDescription' => [
                            'field_type' => TextareaType::class,
                            'label' => 'Short Description',
                        ],
                        'description' => [
                            'field_type' => CKEditorType::class,
                            'attr' => ['rows' => '20'],
                            'label' => 'Description',
                            'config' => [
                                'toolbar' => 'full',
                                'filebrowserUploadRoute' => 'post_ckeditor_image',
                                'filebrowserUploadRouteParameters' => ['slug' => 'image'],
                                'extraPlugins' => 'templates',
                                'rows' => '20',

                            ],
                        ]
                    ],
                    'excluded_fields' => ['slug']

            ])
            ->add('file', IconType::class, [
                'label' => 'icon'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_article';
    }
}
