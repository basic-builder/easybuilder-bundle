<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form\Fields;


use Symfony\Component\Form\AbstractType;

class TextAreaType extends AbstractType{



    public function getParent(): string
    {
        return \Symfony\Component\Form\Extension\Core\Type\TextareaType::class;
    }

}