<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form\Fields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class ClientEmailType extends AbstractType{

    public function getParent(): string
    {
        return EmailType::class;
    }

}