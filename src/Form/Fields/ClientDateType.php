<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form\Fields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientDateType extends AbstractType{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('widget', 'single_text');
        parent::configureOptions($resolver); // TODO: Change the autogenerated stub
    }

    public function getParent(): string
    {
        return DateTimeType::class;
    }

}