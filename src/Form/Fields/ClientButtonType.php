<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form\Fields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

class ClientButtonType extends AbstractType{



    public function getParent(): string
    {
        return ButtonType::class;
    }

}