<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form\Fields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClientChoiceType extends AbstractType{

    public function getParent(): string
    {
        return ChoiceType::class;
    }

}