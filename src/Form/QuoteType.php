<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Quote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'form.full_name'
            ])
            ->add('company', TextType::class, [
                'label' => 'form.company'
            ])
            ->add('position', TextType::class,[
                'label' => 'form.position',
                'required' => false
            ])
            ->add('phone', TextType::class,[
                'label' => 'form.phone',
                'required' => false,
                'attr' => [
                    'maxLength' => 14,
                    'class' => 'phone'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email'
            ])
            ->add('sourceLanguage', TextType::class, [
                'label' => 'form.source_languages',
                'required' => false
            ])
            ->add('targetLanguage', TextType::class, [
                'label' => 'form.target_languages',
            ])
            ->add('service', ChoiceType::class, [
                'choices' => $this->getServiceList(),
                'label' => 'form.service',
                'multiple' => true,
                'attr' => [
                    'class' => 'selected2'
                ]
            ])
            ->add('deadline', DateTimeType::class, [
                'label' => 'form.deadline',
                'widget' => 'single_text',
                'required' => false,
//                'attr'=>[
//                    'min' => (new \DateTime())->format('Y-m-d\TH:i:s')
//
//                ]
            ])
            ->add('turnaroundTime', IntegerType::class, [
                'label' => 'form.turnaround_time',
                'required' => false,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('notes', TextareaType::class, [
                'label' => 'form.notes',
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'label' => 'form.save_quote',
                'attr' => [
                    'class' => 'saveForm',
                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quote::class,
        ]);
    }

    // get service list
    protected function getServiceList(){
        return [
            'Copywriting' => 'Copywriting',
            'Translation' => 'Translation',
            'Transcreation' => 'Transcreation',
            'Desktop Publishing' => 'Desktop Publishing',
        ];
    }

}
