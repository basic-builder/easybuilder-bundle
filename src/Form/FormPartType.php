<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\FormPart;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormPartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'Label'])
            ->add('isRequired')
          /*  ->add('formDefinition', SingleTemplateType::class, ['dir_path_placeholder'=>'form/template.html.twig'])*/
            ->add('formDefinition', EntityType::class, ['class'=>SingleTemplate::class,
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('fd')
                      ->where('fd.definition = :Fd')
                      ->andWhere('fd.enabled = :Fl')
                      ->setParameter('Fd', SingleTemplate::IS_FORM)
                      ->setParameter('Fl', true)
                      ->orderBy('fd.created', 'DESC');
              },])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormPart::class,
        ]);
    }
}
