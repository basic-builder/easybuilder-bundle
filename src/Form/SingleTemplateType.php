<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SingleTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('dirPath', TextType::class, ['attr'=>['placeholder'=>$options['dir_path_placeholder']]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'dir_path_placeholder'=>'main/template.html.twig',
            'data_class' => SingleTemplate::class,
        ]);
    }
}
