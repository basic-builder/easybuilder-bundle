<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\CV;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CvType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'form.first_name'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'form.last_name'
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email'
            ])
            ->add('phone', TextType::class, [
                'label' => 'form.phone',
                'attr' => [
                    'maxLength' => 14,
                    'class' => 'phone'
                ]
            ])
            ->add('role', ChoiceType::class, [
                'label' => 'form.role',
                'choices' => $this->getRoleList(),
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'selected2'
                ]
            ])
            ->add('motherTongue', LanguageType::class, [
                'label' => 'form.mother_tongue',
                'multiple' => false,
                'attr' => [
                    'class' => 'selected2'
                ]
            ])
            ->add('catTools', ChoiceType::class, [
                'choices' => $this->getCatToolsList(),
                'label' => 'form.cat_tools',
                'multiple' => true,
                'attr' => [
                    'class' => 'selected2'
                ]
            ])
            ->add('availability', ChoiceType::class, [
                'label' => 'form.work_availability',
                'choices' => $this->getAvailabilityList(),
                'required' => false,
                'attr' => [
                    'class' => 'selected2'
                ]
            ])
            ->add('languageSkills', CollectionType::class, [
                'label' => 'form.language_skills',
                'entry_type' => LanguageSkillsType::class,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => true,
                'delete_empty' => true
            ])
            ->add('areasOfExpertises', CollectionType::class, [
                'label' => 'form.areas_of_expertise',
                'entry_type' => AreasOfExpertiseType::class,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => true,
            ])
            ->add('weekendWork', CheckboxType::class, [
                'label' => 'form.weekend_work',
                'required' => false
            ])
            ->add('coverLetter', TextareaType::class, [
                'label' => 'form.cover_letter',
                'required' => false,
                'attr' => [
                    'maxLength' => 300,
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'form.save_cv',
                'attr' => [
                    'class' => 'saveForm',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CV::class,
        ]);
    }

    // roles list
    protected function getRoleList(){
        return [
              'form.translator' => 'Translator',
              'form.editor' => 'Editor',
              'form.proofreader' => 'Proofreader',
              'form.transcreator' => 'Transcreator',
        ];
    }

    // get cat tools list
    protected function getCatToolsList(){
        return [
             'form.sdl_trados' => 'SDL Trados',
             'form.across_crossweb' => 'Across/Crossweb',
             'form.memo_q' => 'MemoQ',
             'form.xtm' => 'XTM',
             'form.memsource' => 'Memsource',
             'form.smartling' => 'Smartling',
             'form.smartcat' => 'SmartCAT',
             'form.other' => 'Other',
        ];
    }



    // get availability list
    protected function getAvailabilityList(){
        return [
           '35_40_hrs' => '35-40 hrs per week',
           '15_25_hrs' => '15-25 hrs per week',
           'limited' => 'limited',
        ];
    }

    // get expertize list
    protected function getLanguages(){
        return [
            'Albanian' => 'Albanian',
            'Arabic(Algeria)' => 'Arabic(Algeria)',
            'Arabic(Egypt)' => 'Arabic(Egypt)',
            'Arabic(Lebanon)' => 'Arabic(Lebanon)',
            'Arabic(U.A.E)' => 'Arabic (U.A.E)',
            'Armenian' => 'Armenian',
            'Azeri' => 'Azeri',
            'Belarusian' => 'Belarusian',
            'Bengali' => 'Bengali',
            'Bosnian(Cyrillic)' => 'Bosnian(Cyrillic)',
            'Bosnian(Latin)' => 'Bosnian(Latin)',
            'Bulgarian' => 'Bulgarian',
            'Burmese' => 'Burmese',
            'Catalan' => 'Catalan',
            'Chinese(Simplified, PRC)' => 'Chinese(Simplified, PRC)',
            'Chinese(Traditional, Taiwan)' => 'Chinese(Traditional, Taiwan)',
            'Croatian' => 'Croatian',
            'Czech ' => 'Czech ',
            'Danish' => 'Danish',
            'Dutch(Belgium)' => 'Dutch(Belgium)',
            'Dutch(Netherlands)' => 'Dutch(Netherlands)',
            'English(United Kindgdom)' => 'English(United Kindgdom)',
            'English(United States)' => 'English(United States)',
            'Estonian' => 'Estonian',
            'Filipino' => 'Filipino',
            'Finnish' => 'Finnish',
            'French(Canada)' => 'French(Canada)',
            'French(France)' => 'French(France)',
            'French(Switzerland)' => 'French(Switzerland)',
            'Georgian' => 'Georgian',
            'German' => 'German',
            'Greek' => 'Greek',
            'Hebrew' => 'Hebrew',
            'Hindi' => 'Hindi',
            'Hungarian' => 'Hungarian',
            'Icelandic' => 'Icelandic',
            'Italian' => 'Italian',
            'Japanese' => 'Japanese',
            'Kazakh' => 'Kazakh',
            'Korean' => 'Korean',
            'Kyrgyz' => 'Kyrgyz',
            'Latvian' => 'Latvian',
            'Lithuanian' => 'Lithuanian',
            'Macedonian(North Macedonia)' => 'Macedonian(North Macedonia)',
            'Norwegian, Bokmål(Norway)' => 'Norwegian, Bokmål(Norway)',
            'Norwegian, Nynorsk(Norway)' => 'Norwegian, Nynorsk(Norway)',
            'Persian' => 'Persian',
            'Polish' => 'Polish',
            'Portuguese(Brazil)' => 'Portuguese(Brazil)',
            'Portuguese(Portugal)' => 'Portuguese(Portugal)',
            'Punjabi(India)' => 'Punjabi(India)',
            'Romanian(Moldova)' => 'Romanian(Moldova)',
            'Romanian' => 'Romanian',
            'Russian' => 'Russian',
            'Serbian(Cyrillic)' => 'Serbian(Cyrillic)',
            'Serbian(Latin)' => 'Serbian(Latin)',
            'Slovak' => 'Slovak',
            'Slovenian' => 'Slovenian',
            'Spanish(Argentina)' => 'Spanish(Argentina)',
            'Spanish(Latin America)' => 'Spanish(Latin America)',
            'Spanish(Spain, International)' => 'Spanish(Spain, International)',
            'Swedish' => 'Swedish',
            'Thai' => 'Thai',
            'Turkish' => 'Turkish',
            'Turkmen' => 'Turkmen',
            'Ukrainian' => 'Ukrainian',
            'Urdu' => 'Urdu',
            'Vietnamese' => 'Vietnamese',
        ];
    }
}
