<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Gallery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GalleryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('item', null, ['attr' => ['data-widget' => 'select2',
                'data-gallery'=>'uppy',
                'multiple' => true, 'class'=>'d-none']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
        ]);
    }
}
