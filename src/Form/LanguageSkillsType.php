<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Form;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\LanguageSkills;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LanguageSkillsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', ChoiceType::class,
                [
                    'label' => 'form.language',
                    'required'=>true,
                    'choices'=>$this->getLanguages(),
                    'data' => 'Spanish(Spain, International)',
                    'attr' => [
                        'class' => 'selected2'
                    ]
                ])
            ->add('level', ChoiceType::class,
                [
                    'label' => 'form.language_level',
                    'required'=>true,
                    'choices'=>$this->getLevelsList(),
                    'attr' => [
                        'class' => 'selected2'
                    ]
                ])
        ;
    }

    // get language level list
    // todo: need to rewrite
    protected function getLevelsList(){
        return [
            'form.level.native'=>'Native',
            'form.level.c2'=>'C2',
            'form.level.c1'=>'C1',
            'form.level.b2'=>'B2',
            'form.level.b1'=>'B1',
            'form.level.a2'=>'A2',
            'form.level.a1'=>'A1',
        ];
    }


    // get expertize list
    protected function getLanguages(){
        return [
            'Albanian' => 'Albanian',
            'Arabic(Algeria)' => 'Arabic(Algeria)',
            'Arabic(Egypt)' => 'Arabic(Egypt)',
            'Arabic(Lebanon)' => 'Arabic(Lebanon)',
            'Arabic(U.A.E)' => 'Arabic (U.A.E)',
            'Armenian' => 'Armenian',
            'Azeri' => 'Azeri',
            'Belarusian' => 'Belarusian',
            'Bengali' => 'Bengali',
            'Bosnian(Cyrillic)' => 'Bosnian(Cyrillic)',
            'Bosnian(Latin)' => 'Bosnian(Latin)',
            'Bulgarian' => 'Bulgarian',
            'Burmese' => 'Burmese',
            'Catalan' => 'Catalan',
            'Chinese(Simplified, PRC)' => 'Chinese(Simplified, PRC)',
            'Chinese(Traditional, Taiwan)' => 'Chinese(Traditional, Taiwan)',
            'Croatian' => 'Croatian',
            'Czech ' => 'Czech ',
            'Danish' => 'Danish',
            'Dutch(Belgium)' => 'Dutch(Belgium)',
            'Dutch(Netherlands)' => 'Dutch(Netherlands)',
            'English(United Kindgdom)' => 'English(United Kindgdom)',
            'English(United States)' => 'English(United States)',
            'Estonian' => 'Estonian',
            'Filipino' => 'Filipino',
            'Finnish' => 'Finnish',
            'French(Canada)' => 'French(Canada)',
            'French(France)' => 'French(France)',
            'French(Switzerland)' => 'French(Switzerland)',
            'Georgian' => 'Georgian',
            'German' => 'German',
            'Greek' => 'Greek',
            'Hebrew' => 'Hebrew',
            'Hindi' => 'Hindi',
            'Hungarian' => 'Hungarian',
            'Icelandic' => 'Icelandic',
            'Italian' => 'Italian',
            'Japanese' => 'Japanese',
            'Kazakh' => 'Kazakh',
            'Korean' => 'Korean',
            'Kyrgyz' => 'Kyrgyz',
            'Latvian' => 'Latvian',
            'Lithuanian' => 'Lithuanian',
            'Macedonian(North Macedonia)' => 'Macedonian(North Macedonia)',
            'Norwegian, Bokmål(Norway)' => 'Norwegian, Bokmål(Norway)',
            'Norwegian, Nynorsk(Norway)' => 'Norwegian, Nynorsk(Norway)',
            'Persian' => 'Persian',
            'Polish' => 'Polish',
            'Portuguese(Brazil)' => 'Portuguese(Brazil)',
            'Portuguese(Portugal)' => 'Portuguese(Portugal)',
            'Punjabi(India)' => 'Punjabi(India)',
            'Romanian(Moldova)' => 'Romanian(Moldova)',
            'Romanian' => 'Romanian',
            'Russian' => 'Russian',
            'Serbian(Cyrillic)' => 'Serbian(Cyrillic)',
            'Serbian(Latin)' => 'Serbian(Latin)',
            'Slovak' => 'Slovak',
            'Slovenian' => 'Slovenian',
            'Spanish(Argentina)' => 'Spanish(Argentina)',
            'Spanish(Latin America)' => 'Spanish(Latin America)',
            'Spanish(Spain, International)' => 'Spanish(Spain, International)',
            'Swedish' => 'Swedish',
            'Thai' => 'Thai',
            'Turkish' => 'Turkish',
            'Turkmen' => 'Turkmen',
            'Ukrainian' => 'Ukrainian',
            'Urdu' => 'Urdu',
            'Vietnamese' => 'Vietnamese',
        ];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LanguageSkills::class,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_language_skills_type';
    }


    public function getBlockPrefix()
    {
        return $this->getName(); // TODO: Change the autogenerated stub
    }
}
