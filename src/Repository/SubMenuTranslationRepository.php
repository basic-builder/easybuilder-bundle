<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SubMenuTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubMenuTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubMenuTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubMenuTranslation[]    findAll()
 * @method SubMenuTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubMenuTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubMenuTranslation::class);
    }

    // /**
    //  * @return SubMenuTranslation[] Returns an array of SubMenuTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubMenuTranslation
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
