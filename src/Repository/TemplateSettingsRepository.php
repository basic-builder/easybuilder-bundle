<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemplateSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplateSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplateSettings[]    findAll()
 * @method TemplateSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemplateSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemplateSettings::class);
    }

    public function findCurrent() {

        return $this->createQueryBuilder('t')
            ->leftJoin('t.templateInfo', 'ti')->addSelect('ti')
            ->orderBy('t.updated', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
    // /**
    //  * @return TemplateSettings[] Returns an array of TemplateSettings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TemplateSettings
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
