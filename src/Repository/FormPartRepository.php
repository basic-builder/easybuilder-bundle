<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\FormPart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FormPart|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormPart|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormPart[]    findAll()
 * @method FormPart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormPartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormPart::class);
    }

    // /**
    //  * @return FormPart[] Returns an array of FormPart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormPart
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
