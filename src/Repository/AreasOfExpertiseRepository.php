<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\AreasOfExpertise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AreasOfExpertise|null find($id, $lockMode = null, $lockVersion = null)
 * @method AreasOfExpertise|null findOneBy(array $criteria, array $orderBy = null)
 * @method AreasOfExpertise[]    findAll()
 * @method AreasOfExpertise[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AreasOfExpertiseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AreasOfExpertise::class);
    }

    // /**
    //  * @return AreasOfExpertise[] Returns an array of AreasOfExpertise objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AreasOfExpertise
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
