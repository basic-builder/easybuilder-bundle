<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\ArticleFilterModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findSingle(string $slug, string $mainMenuSlug = null, string $subMenuSlug = null): ?Article
    {
        $q = $this->createQueryBuilder('a')
            ->leftJoin('a.mainMenu', 'm')->addSelect('m')
            ->leftJoin('a.subMenu', 'sm')->addSelect('sm')
            ->where('a.slug = :aSl')
            ->setParameter('aSl', $slug)

            ;

        !is_null($mainMenuSlug) ? $q->andWhere('m.slug = :mSl')->setParameter('mSl', $mainMenuSlug): '';
        !is_null($subMenuSlug) ? $q->andWhere('sm.slug = :sSl')->setParameter('sSl', $subMenuSlug): '';


        return $q->getQuery()->getOneOrNullResult();
    }

    public function findByArticleType(int $articleType=Article::IS_ARTICLE, int $maxResult=6)
    {

        return $this->createQueryBuilder('a')
            ->innerJoin('a.mainMenu', 'm')->addSelect('m')
            ->where('a.articleType = :aT')
            ->setParameter('aT', $articleType)
            ->setMaxResults($maxResult)
            ->orderBy('a.created', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForHome(ArticleFilterModel $articleFilterModel)
    {

        $q = $this->createQueryBuilder('a')
            ->leftJoin('a.mainMenu', 'm')->addSelect('m')
            ->leftJoin('a.subMenu', 'aSm')->addSelect('aSm')
            ->leftJoin('aSm.mainMenu', 'asmM')->addSelect('asmM')
            ->leftJoin('a.articleGroups', 'aG')->addSelect('aG')
            ->setMaxResults($articleFilterModel->getLimit())
            ->where('a.isMain = :tR')
            ->setParameter('tR', $articleFilterModel->isMain())
            ;

        !is_null($articleFilterModel->getMainMenuSlug()) ?
            $q
                ->andWhere('m.slug = :mS')
                ->setParameter('mS', $articleFilterModel->getMainMenuSlug()) : ''
            ;

        !is_null($articleFilterModel->getSubMenuSlug()) ? $q
            ->andWhere('aSm.slug = :sSl')
            ->setParameter('sSl', $articleFilterModel->getSubMenuSlug()) : '' ;


       count($articleFilterModel->getArticleType()) ? $q->andWhere('a.articleType IN (:aTp)')->setParameter('aTp', $articleFilterModel->getArticleType()): '' ;

       !is_null($articleFilterModel->getArticleGroupSlug()) ? $q->andWhere('aG.slug = :aGs')->setParameter('aGs', $articleFilterModel->getArticleGroupSlug()): '' ;

       !is_null($articleFilterModel->getArticleGroupCategory()) ? $q->andWhere('aG.category = :aGc')->setParameter('aGc', $articleFilterModel->getArticleGroupCategory()): '' ;


        return $q->orderBy('a.'.$articleFilterModel->getOrderBy()['field'], $articleFilterModel->getOrderBy()['order'])
        ->getQuery()
        ->getResult();
    }

    /**
     * @param string $slug
     * @param string|null $mainMenuSlug
     * @param string|null $subMenuSlug
     */
    public function findArticleTarget(int $type, string $subMenuSlug, int $currentId)
    {

        $next = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('a.slug as slug')
            ->from(Article::class, 'a')
            ->innerJoin('a.subMenu', 'sm')
            ->innerJoin('sm.mainMenu', 'mm')
            ->orderBy('a.position', 'ASC')
            ->where('a.articleType = :aT')
            ->andWhere('sm.slug = :sMS')
            ->andWhere('a.id > :aId')
            ->setParameter('aT', $type)
            ->setParameter('sMS', $subMenuSlug)
            ->setParameter('aId', $currentId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult()
            ;

        $preview = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('a.slug as slug')
            ->from(Article::class, 'a')
            ->innerJoin('a.subMenu', 'sm')
            ->innerJoin('sm.mainMenu', 'mm')
            ->orderBy('a.position', 'DESC')
            ->where('a.articleType = :aT')
            ->andWhere('sm.slug = :sMS')
            ->andWhere('a.id < :aId')
            ->setParameter('aT', $type)
            ->setParameter('sMS', $subMenuSlug)
            ->setParameter('aId', $currentId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult()
            ;

        return ['next'=>$next, 'preview'=>$preview];
    }

    /**
     * @param string $slug
     * @param string|null $mainMenuSlug
     * @param string|null $subMenuSlug
     */
    public function findNewsTargets(int $type, string $subMenuSlug, int $currentId)
    {

        $next = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('a.slug as slug')
            ->from(Article::class, 'a')
            ->innerJoin('a.mainMenu', 'mm')
            ->orderBy('a.id', 'ASC')
            ->where('a.articleType = :aT')
            ->andWhere('mm.slug = :sMS')
            ->andWhere('a.id > :aId')
            ->setParameter('aT', $type)
            ->setParameter('sMS', $subMenuSlug)
            ->setParameter('aId', $currentId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult()
            ;

        $preview = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('a.slug as slug')
            ->from(Article::class, 'a')
            ->innerJoin('a.mainMenu', 'mm')
            ->orderBy('a.id', 'DESC')
            ->where('a.articleType = :aT')
            ->andWhere('mm.slug = :sMS')
            ->andWhere('a.id < :aId')
            ->setParameter('aT', $type)
            ->setParameter('sMS', $subMenuSlug)
            ->setParameter('aId', $currentId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult()
            ;

        return ['next'=>$next, 'preview'=>$preview];
    }

    /**
     * @param string $slug
     * @param int $currentPage
     * @param int $articleLimit
     * @return Paginator
     */
    public function findBySubMenu(string $slug, int $currentPage = 1, int $articleLimit = 5)
    {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.subMenu', 'sM')->addSelect('sM')
            ->leftJoin('a.image', 'i')->addSelect('i')
            ->where('sM.slug = :sl')
            ->setParameter('sl', $slug)
            ->orderBy('a.created', 'DESC')
            ->getQuery();


        return $this->paginate($query, $currentPage, $articleLimit);

    }

    /**
     * @param $dql
     * @param int $page
     * @param int $limit
     * @return Paginator
     */
    public function paginate($dql, $page = 1, $limit = 5)
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        return $paginator;
    }

    /**
     * @param string $slug
     * @param int $maxRes
     * @return int|mixed|string
     */
    public function findArticlesBySubMenu(string $slug, int $maxRes)
    {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.subMenu', 'sM')->addSelect('sM')
            ->where('sM.slug = :sl')
            ->setParameter('sl', $slug)
            ->orderBy('a.created', 'DESC')
            ->setMaxResults($maxRes)
            ->getQuery()->getResult();


        return $query;

    }

    /**
     * @param string $slug
     * @param int $maxRes
     * @return int|mixed|string
     */
    public function finSubMenu(string $slug, int $maxRes)
    {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.subMenu', 'sM')->addSelect('sM')
            ->where('sM.slug = :sl')
            ->setParameter('sl', $slug)
            ->orderBy('a.created', 'DESC')
            ->setMaxResults($maxRes)
            ->getQuery()->getResult();


        return $query;
    }



    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
