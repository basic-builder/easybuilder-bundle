<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroupTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArticleGroupTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticleGroupTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticleGroupTypes[]    findAll()
 * @method ArticleGroupTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleGroupTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticleGroupTypes::class);
    }


    /**
     * This repo is used to get article group types for current template
     * @return array|int|string
     */
    public function findForMain() {

        return $this->getEntityManager()->createQueryBuilder()
            ->select('ag.name, ag.slug')
            ->from(ArticleGroupTypes::class, 'ag')
            ->orderBy('ag.position', 'ASC')
            ->getQuery()->getArrayResult()

            ;
    }

    /**
     * @param string $slug
     * @return int|mixed|string
     */
    public function findSectionInfo(string $slug){

        $q = $this->createQueryBuilder('agt')
            ->innerJoin('agt.articleGroups', 'ag')->addSelect('ag')
            ->innerJoin('ag.article', 'a')->addSelect('a')
            ->innerJoin('a.subMenus', 'sm')->addSelect('sm')
            ->innerJoin('sm.mainMenu', 'mm')->addSelect('mm')
            ->where('agt.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getResult()
            ;

        return count($q) ? $q[0] : null;

    }

    // /**
    //  * @return ArticleGroupTypes[] Returns an array of ArticleGroupTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArticleGroupTypes
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
