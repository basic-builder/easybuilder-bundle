<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\MainMenu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use http\Message;

/**
 * @method MainMenu|null find($id, $lockMode = null, $lockVersion = null)
 * @method MainMenu|null findOneBy(array $criteria, array $orderBy = null)
 * @method MainMenu[]    findAll()
 * @method MainMenu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MainMenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MainMenu::class);
    }

     /**
      * @return MainMenu[] Returns an array of MainMenu objects
      */

    public function findForSitemap()
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.subMenus', 's')->addSelect('s')
            ->leftJoin('m.mainArticle', 'ma')->addSelect('ma')
            ->leftJoin('s.mainArticle', 'sa')->addSelect('sa')
            ->leftJoin('s.articles', 'saa')->addSelect('saa')
            ->orderBy('m.created', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

     /**
      * @return MainMenu[] Returns an array of MainMenu objects
      */

    public function findForMain(int $menuDeep = 0)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.subMenus', 's', 'with', 's.enabled = :tr')->addSelect('s')
            ->where('m.enabled = :tr')
            ->andWhere('m.deep LIKE :mD')
            ->setParameter('tr', true)
            ->setParameter('mD', "%".$menuDeep."%")
            ->orderBy('m.position', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


     /**
      * @return MainMenu[] Returns an array of MainMenu objects
      */

    public function findFullInfoBySlug(string $slug): ?MainMenu
    {
        $result = $this->createQueryBuilder('m')
            ->leftJoin('m.subMenus', 's')->addSelect('s')
            ->leftJoin('m.mainArticle', 'ma')->addSelect('ma')
            ->leftJoin('m.articles', 'a')->addSelect('a')
            ->leftJoin('m.gallery', 'g')->addSelect('g')
            ->leftJoin('g.item', 'it')->addSelect('it')
            ->leftJoin('m.singleTemplate', 'st')->addSelect('st')
            ->where('m.slug = :mSl')
            ->setParameter('mSl', $slug)
            ->orderBy('m.position', 'ASC')
            ->getQuery()
            ->getResult();


        return count($result) ? $result[0] : null;
    }


    // /**
    //  * @return MainMenu[] Returns an array of MainMenu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MainMenu
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
