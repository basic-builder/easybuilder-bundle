<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Repository;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\FormInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FormInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormInfo[]    findAll()
 * @method FormInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormInfo::class);
    }

    // /**
    //  * @return FormInfo[] Returns an array of FormInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormInfo
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
