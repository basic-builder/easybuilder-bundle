<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class EasyBuilderExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $config = $processor->processConfiguration(new Configuration(), $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $container->setParameter('easy_builder.upload_dir', $config['upload_dir']);
//        $container->setParameter('a2lix_translation_form.locales', $config['locales']);
//        $container->setParameter('a2lix_translation_form.required_locales', $config['required_locales']);
//        $container->setParameter('a2lix_translation_form.default_locale', $config['default_locale'] ?:
//            $container->getParameter('kernel.default_locale'));

//        $container->setParameter('a2lix_translation_form.templating', $config['templating']);


//        var_dump('We\'re alive!');die;
        // TODO: Implement load() method.
    }

    public function getAlias()
    {
        return 'easy_builder';
    }
}