<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {

        $treeBuilder = new TreeBuilder('easy_builder');
        $rootNode = $this->getRootNode($treeBuilder, 'easy_builder');

        $rootNode->children()
            ->scalarNode('upload_dir')
            ->defaultValue('/../../public/')
            ->isRequired()
            ->end()
            ;


        return $treeBuilder;
        // TODO: Implement getConfigTreeBuilder() method.
    }

    private function getRootNode(TreeBuilder $treeBuilder, $name)
    {
        // BC layer for symfony/config 4.1 and older
        if (!method_exists($treeBuilder, 'getRootNode')) {
            return $treeBuilder->root($name);
        }

        return $treeBuilder->getRootNode();
    }
}