<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminCkEditorField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminIconField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminLanguageField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\ImageOptimizedField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LocaleField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;

class TemplateSettingsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TemplateSettings::class;
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Template Settings')
            ->setEntityLabelInPlural('Template Settings')
            ->setPageTitle(Crud::PAGE_INDEX, 'Template Settings')
            ->setEntityPermission('ROLE_APP_ADMIN_TEMPLATE_SETTINGS_EDITOR')
            ;
    }

    public function configureFields(string $pageName): iterable
    {

        $id = IntegerField::new('id');
        $name = TextField::new('name');
        $templateInfo = AssociationField::new('templateInfo', 'Template')
                        ->setSortable('id')
                        ->setRequired(true);
        $pageTitle = TextField::new('pageTitle');
        $metaTitle = TextField::new('metaTitle');
        $metaDescription = TextField::new('metaDescription');
        $phone = TelephoneField::new('phone');
        $address = TextField::new('address');
        $email = TextField::new('email');
        $fb = UrlField::new('fb');
        $tw = UrlField::new('tw');
        $instagram = UrlField::new('instagram');
        $linkedin = UrlField::new('linkedin');
        $youtube = UrlField::new('youtube');
        $created = DateTimeField::new('created');
        $updated = DateTimeField::new('updated');
        $logo = AdminIconField::new('file', 'Logo');
        $analytics = CodeEditorField::new('analytics', 'Google Analytics');
        $languages = AdminLanguageField::new('languages')
                    ->setFormTypeOptions(['multiple'=>true, 'required'=>false, 'attr'=>['data-widget'=>'select2']])
        ;
        $image = AssociationField::new('image', 'Favicon')
            ->setCssClass('d-none favicon')
            ->setFormTypeOption('attr', ['article-image-upload' => true]);

        $edit = $new = [
            FormField::addPanel('Article')
                ->setProperty('form_tabs')
                ->setFormTypeOptions(['form_tabs' => true])
                ->addCssClass('tab-pane')
            ,
            $name,
            $templateInfo,
            $languages,
            $pageTitle,
            $logo,
            FormField::addPanel('Social'),
            $fb ,
            $tw ,
            $instagram ,
            $linkedin ,
            $youtube ,
            FormField::addPanel('Contact'),
            $phone,
            $email,
            $address,
            FormField::addPanel('SEO'),
            $metaTitle,
            $metaDescription,
            $analytics,
            $image

        ];

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $email, $updated];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $email, $updated, $created, $updated];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $edit;
        }

    }



    public function configureFilters(Filters $filters): Filters
    {
        $filters
            ->add('name')
            ->add('phone')
            ->add('address')
            ->add('email')
            ->add('created')
            ->add('updated')
        ;

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->uploadImage($entityInstance);
        parent::persistEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->uploadImage($entityInstance);
        parent::updateEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }

    private function uploadImage($entityInstance){

        if($entityInstance->getFile()) {
            $entityInstance->uploadFile();
        }

        return $entityInstance;
    }

}
