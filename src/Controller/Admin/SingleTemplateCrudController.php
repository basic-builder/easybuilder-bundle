<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class SingleTemplateCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SingleTemplate::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Single Template')
            ->setEntityLabelInPlural('Single Template')
            ->setPageTitle(Crud::PAGE_INDEX, 'Single Template')
            ->setPaginatorPageSize(30)
            ->showEntityActionsAsDropdown(false)
            ->setPaginatorUseOutputWalkers(true)
            ->setPaginatorFetchJoinCollection(true)
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id');

        $name = TextField::new('name');
        $dirPath = TextField::new('dirPath')
                    ->setFormTypeOption('attr', ['placeholder'=>'main/main_menu.html.twig'])
        ;
        $enabled = BooleanField::new('enabled');
        $created = DateTimeField::new('created');
        $updated = DateTimeField::new('updated');

        $new = [
            $name,
            $dirPath,
            $enabled
        ];

        $index = [
            $id,
            $name,
            $dirPath,
            $enabled,
            $created,
            $updated
        ];


        if (Crud::PAGE_INDEX === $pageName) {
            return $index;
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return $index;
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $new;
        }
    }


    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {

        $queryBuilder = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $queryBuilder
            ->andWhere('entity.definition = :aNt')
            ->setParameter('aNt', SingleTemplate::IS_TEMPLATE);

        return  $queryBuilder;
    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('name')
            ->add('dirPath')
            ->add('enabled')
            ->add('created')
            ->add('updated');

        return $filters;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setDefinition(SingleTemplate::IS_TEMPLATE);

        parent::persistEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }
}
