<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Contact;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Contact::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Contact Us')
            ->setEntityLabelInPlural('Contact Us')
            ->setPageTitle(Crud::PAGE_INDEX, 'Contact Us')
            ->setPaginatorPageSize(30)
            ->showEntityActionsAsDropdown(false)
            ->setPaginatorUseOutputWalkers(true)
            ->setPaginatorFetchJoinCollection(true)
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id');

        $name = TextField::new('name');
        $email = EmailField::new('email');
        $subject = EmailField::new('subject');
        $message = TextareaField::new('message');
        $created = DateTimeField::new('created');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $email, $subject, $message, $created];
        }
        if (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $email, $subject, $message, $created];
        }
    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('name')
            ->add('email')
            ->add('subject')
            ->add('created');

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ;
    }

}
