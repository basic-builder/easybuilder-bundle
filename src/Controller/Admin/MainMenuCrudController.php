<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminArticleField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminArticleShow;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminGalleryField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminGalleryShow;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminTranslationField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\ImageOptimizedField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\MainMenu;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MainMenuCrudController extends AbstractCrudController
{
    protected $parameterBag;
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public static function getEntityFqcn(): string
    {
        return MainMenu::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Main Menu')
            ->setEntityLabelInPlural('Main Menu')
            ->setPageTitle(Crud::PAGE_INDEX, 'Main Menu')
            ->setPaginatorPageSize(30)
            ->setDefaultSort(['position' => 'ASC'])
            ->showEntityActionsAsDropdown(false)
            ->setPaginatorUseOutputWalkers(true)
            ->setPaginatorFetchJoinCollection(true)
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id');

        $name = TextField::new('name');

        $languages = $this->parameterBag->get('jms_i18n_routing.locales');

        $translations = AdminTranslationField::new('translations', 'Title translations')
            ->setFormTypeOptions(
                [
                    'locales' => $languages,
                    'fields'=>
                        [
                            'title' =>[
                                'field_type' => TextType::class,
                                'label' => 'Title',
                            ]
                        ],
                    'excluded_fields'=>['slug'],

                ]);

        $slug = TextField::new('slug');
        $enabled = BooleanField::new('enabled');
        $deep = ChoiceField::new('deep')
            ->setChoices([
                'main'=>MainMenu::IS_MAIN_MENU,
                'in footer'=>MainMenu::IS_FOOTER,
                'Is Internal'=>MainMenu::IS_INTERNAL])
            ->setFormTypeOption('multiple', true)
            ->setFormTypeOption('attr',['data-widget'=>"select2"])
        ;

        $gallery = AdminGalleryField::new('gallery');
        $galleryShow = AdminGalleryShow::new('gallery');

        $mainArticle = AdminArticleField::new('mainArticle', false)->setCssClass('ckEditor');
        $mainArticleShow = AdminArticleShow::new('mainArticle', false);
        $mainImage = ImageOptimizedField::new('mainArticle.downloadLink', 'Main Image');
        $mainArticleName = TextField::new('mainArticle.name', 'Main Article');

        $singleTemplate = AssociationField::new('singleTemplate');

        $metaTitle = TextField::new('metaTitle');
        $metaDescription = TextareaField::new('metaDescription');

        $created = DateTimeField::new('created');

        $new = [
            FormField::addPanel('Main Menu')
                ->setProperty('form_tabs')
                ->setFormTypeOptions(['form_tabs' => true])
                ->addCssClass('tab-pane')
            ,
            $name,
            $deep,
            $translations,
           FormField::addPanel('Gallery'),
            $enabled,
            $gallery,
            $singleTemplate,
            FormField::addPanel('Main Article'),
            $mainArticle,
            FormField::addPanel('SEO')
                ->setIcon('SEO')->addCssClass('optional')
                ->setHelp('SEO number is preferred'),
            $metaTitle,
            $metaDescription
        ];

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $slug, $deep, $mainArticleName, $mainImage, $enabled, $created];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $slug,  $deep, $mainArticleShow, $mainImage, $galleryShow, $enabled, $created];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $new;
        }

    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('name')
            ->add('created');

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER)
            ;
    }

    public function configureAssets(Assets $assets): Assets
    {
//        $assets->addWebpackEncoreEntry('admin_custom');
        return parent::configureAssets($assets); // TODO: Change the autogenerated stub
    }


    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->setGalleryItem($entityInstance);
        $entityInstance = $this->uploadImage($entityInstance);
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->setGalleryItem($entityInstance);
        $entityInstance = $this->uploadImage($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }

    private function setGalleryItem($entityInstance)
    {
        if ($entityInstance->getGallery() && $entityInstance->getGallery()->getItem()->isEmpty() == false) {

            foreach ($entityInstance->getGallery()->getItem() as $item) {

                is_null($item->getGallery()) ? $item->setGallery($entityInstance->getGallery()) : '';
            }
        }

        return $entityInstance;
    }

    private function uploadImage($entityInstance)
    {

        if (is_null($entityInstance->getMainArticle()) == false && $entityInstance->getMainArticle()->getFile()) {
            $entityInstance->getMainArticle()->uploadFile();
        }

        return $entityInstance;
    }
}
