<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminPassword;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCrudController extends AbstractCrudController
{

    protected $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('User')
            ->setEntityLabelInPlural('User')
            ->setPageTitle(Crud::PAGE_INDEX, 'Users list')
            ->setHelp('index', 'The list view overrides the global help message')
            ->setSearchFields(['email'])
            ->setEntityPermission('ROLE_SUPER_ADMIN')
            ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('email', EmailType::class);
        return $filters;
    }

    public function configureFields(string $pageName): iterable
    {
        $email = TextField::new('email');
        $isVerifiedList = BooleanField::new('isVerified');
        $password = AdminPassword::new('plainPassword', 'Password');
        $passwordNew = AdminPassword::new('plainPassword', 'Password', true);

        $roles = ChoiceField::new('roles', 'Roles')
            ->setFormTypeOptions(['attr'=>['data-widget'=>'select2']])
            ->allowMultipleChoices(true)
            ->addCssClass('select2')
            ->renderAsBadges(['data-widget'=>'primary'])
        ->setChoices(
            ['User'=>'ROLE_USER', 'Admin'=>'ROLE_ADMIN', 'Manager'=>'ROLE_MODERATOR', 'Content Manager'=>'ROLE_CONTENT_MANAGER']);

        $id = IntegerField::new('id', 'ID');
        $isVerified = Field::new('isVerified');


        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $email, $isVerifiedList, $roles];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $email, $roles, $password, $isVerified];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [ $email, $passwordNew, $roles];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [ $email, $password, $roles];
        }
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {

        $queryBuilder = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $queryBuilder
            ->andWhere('entity.roles NOT LIKE :rl')
            ->setParameter('rl', "%ROLE_SUPER_ADMIN%");

        return  $queryBuilder;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->passwordEncoder($entityInstance);

        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->passwordEncoder($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }

    /**
     * THis function is used to encode pass for user
     * @param User $entityInstance
     * @return User
     */
    private function passwordEncoder(User $entityInstance): User
    {

        $plainPassword = null;

        if (is_array($this->container->get('request_stack')->getCurrentRequest()->request->get('User')) &&
            array_key_exists('plainPassword', $this->container->get('request_stack')->getCurrentRequest()->request->get('User'))
        ){
            $plainPassword = $this->container->get('request_stack')->getCurrentRequest()->request->get('User')['plainPassword']['first'];

            if(strlen($plainPassword) > 5){

                $entityInstance->setPassword(
                    $this->passwordEncoder->encodePassword(
                        $entityInstance, $plainPassword
                    )
                );

            }
        }

        return $entityInstance;
    }
}
