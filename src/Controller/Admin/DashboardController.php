<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroup;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroupTypes;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Contact;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\CV;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\FormInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Gallery;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\MainMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\News;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Quote;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SubMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    public function configureDashboard(): Dashboard
    {
        $templateSettings = $this->getDoctrine()->getRepository(TemplateSettings::class)->findCurrent();

        return Dashboard::new()
            ->setTitle("{$templateSettings->getName()} Admin Dashboard")
            ->setFaviconPath('/build/images/favicon.png')
            ;
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setEntityLabelInSingular('Admin Dashboard')
            ->setEntityLabelInPlural('Admin Dashboard')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->addFormTheme('@EasyBuilder/form/fields.html.twig')
            ->overrideTemplate('layout','@EasyBuilder/admin/default/layout.html.twig')
            ->setPaginatorPageSize(30)
            ->setEntityPermission('ROLE_ADMIN')
            ;
    }

    /**
     * @Route("/admin/dashboard")
     */
    public function index(): Response
    {
        $templateSettings = $this->getDoctrine()->getRepository(TemplateSettings::class)->findCurrent();

        return $this->render('@EasyBuilder/admin/welcome.html.twig', [
            'dashboard_controller_filepath' => (new \ReflectionClass(static::class))->getFileName(),
            'dashboard_controller_class' => (new \ReflectionClass(static::class))->getShortName(),
            'template_settings'=>$templateSettings
        ]);
    }





    public function configureMenuItems(): iterable
    {
        $templateSettings = $this->getDoctrine()->getRepository(TemplateSettings::class)->findCurrent();
        $settings = [

            MenuItem::linkToCrud('Template Settings', 'far fa-newspaper', TemplateSettings::class)
                ->setController(TemplateSettingsCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_TEMPLATE_SETTINGS_EDITOR")
            ->setAction(Action::EDIT)->setEntityId($templateSettings->getId())
            ,
            MenuItem::linkToCrud('Templates', 'far fa-newspaper', TemplateInfo::class)
                ->setController(TemplateInfoCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_TEMPLATES_ADMIN")

            ,
            MenuItem::linkToCrud('Single Template', 'far fa-newspaper', SingleTemplate::class)
                ->setController(SingleTemplateCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_SINGLE_TEMPLATES_ADMIN"),
            MenuItem::linkToCrud('Form Definition', 'far fa-newspaper', SingleTemplate::class)
                ->setController(FormDefinitionCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_FORM_DEFINITION_ADMIN"),
            MenuItem::linkToCrud('Form Info', 'far fa-newspaper', FormInfo::class)
                ->setController(FormInfoCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_FORM_DEFINITION_ADMIN")
        ];

        $menus = [
            MenuItem::linkToCrud('Main Menu', 'far fa-caret-square-down', MainMenu::class)
                ->setController(MainMenuCrudController::class)
                ->setQueryParameter('sortField', 'position')
                ->setQueryParameter('sortDirection', 'ASC')
                ->setPermission("ROLE_APP_ADMIN_MAIN_MENU_ADMIN")
            ,

            MenuItem::linkToCrud('Sub Menu', 'fas fa-caret-square-down', SubMenu::class)
                ->setPermission("ROLE_APP_ADMIN_SUB_MENU_ADMIN")
            ,
        ];

        $content = [
            MenuItem::linkToCrud('Article Group Types', 'fas fa-layer-group', ArticleGroupTypes::class)
                ->setController(ArticleGroupTypesCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_ARTICLE_GROUP_TYPE_READER"),
        MenuItem::linkToCrud('Article Group', 'fas fa-layer-group', ArticleGroup::class)
                ->setController(ArticleGroupCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_ARTICLE_GROUP_ADMIN")
            ,
            MenuItem::linkToCrud('Article', 'far fa-newspaper', Article::class)
                ->setController(ArticleCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_ARTICLE_ADMIN")
            ,

            MenuItem::linkToCrud('News', 'far fa-newspaper', Article::class)
                ->setController(NewsCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_NEWS_ADMIN")
            ,
            MenuItem::linkToCrud('Gallery', 'fas fa-images', Gallery::class)
                ->setPermission("ROLE_APP_ADMIN_GALLERY_ADMIN")
            ,
            MenuItem::linkToCrud('Contact', 'fas fa-id-card', Contact::class)
                ->setController(ContactCrudController::class)
                ->setPermission("ROLE_APP_ADMIN_CONTACT_ADMIN")
        ];

        yield MenuItem::subMenu('Settings', 'fas fa-cogs')->setSubItems($settings)
            ->setPermission('ROLE_SETTINGS_ADMIN');
        yield MenuItem::subMenu('Menu', 'fas fa-bars')->setSubItems($menus)
            ->setPermission('ROLE_MENU_ADMIN');
        yield MenuItem::subMenu('Content', 'far fa-file-alt')->setSubItems($content)
            ->setPermission('ROLE_CONTENT_ADMIN');
        yield MenuItem::linkToCrud('User', 'fas fa-fas fa-user', User::class)
            ->setPermission('ROLE_APP_ADMIN_USERS_ADMIN');
        yield MenuItem::linkToCrud('CV', 'fas fa-file-signature', CV::class)
            ->setPermission('ROLE_REQUEST_CV_ADMIN');
        yield MenuItem::linkToCrud('Quote', 'fas fa-file-signature', Quote::class)
            ->setPermission('ROLE_REQUEST_QUOTE_ADMIN');
        yield MenuItem::linkToRoute('Home Page', 'fas fa-fas fa-link', 'homepage');
    }


    /**
     * User menu
     * @param UserInterface $user
     * @return UserMenu
     */
    public function configureUserMenu(UserInterface $user): UserMenu
    {
        // Usually it's better to call the parent method because that gives you a
        // user menu with some menu items already created ("sign out", "exit impersonation", etc.)
        // if you prefer to create the user menu from scratch, use: return UserMenu::new()->...
        return parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($user->getUsername())
            // use this method if you don't want to display the name of the user
            ->displayUserName(false)

            // you can return an URL with the avatar image
//            ->setAvatarUrl($user->getProfileImageUrl())
            // use this method if you don't want to display the user image
            ->displayUserAvatar(true)
            // you can also pass an email address to use gravatar's service
            ->setGravatarEmail($user->getEmail())

            // you can use any type of menu item, except submenus
            ->addMenuItems([
//                MenuItem::linkToRoute('My Profile', 'fa fa-id-card', '...', ['...' => '...']),
//                MenuItem::linkToRoute('Settings', 'fa fa-user-cog', '...', ['...' => '...']),
                MenuItem::section(),
//                MenuItem::linkToLogout('Logout', 'fa fa-sign-out'),
            ]);
    }
}
