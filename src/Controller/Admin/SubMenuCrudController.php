<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminArticleField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminTranslationField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SubMenu;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SubMenuCrudController extends AbstractCrudController
{

    protected $parameterBag;
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public static function getEntityFqcn(): string
    {
        return SubMenu::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Submenu')
            ->setEntityLabelInPlural('Submenu')
            ->setDefaultSort(['position' => 'ASC'])
            ->setPageTitle(Crud::PAGE_INDEX, 'Submenu');
    }

    public function configureFields(string $pageName): iterable
    {

        $name = TextField::new('name');

        $languages = $this->parameterBag->get('jms_i18n_routing.locales');
        $languageDefault = $this->parameterBag->get('jms_i18n_routing.default_locale');
        $translations = AdminTranslationField::new('translations', 'Title translations')
            ->setFormTypeOptions(
                [
                    'locales' => $languages,
                    'fields'=>
                        [
                            'title' =>[
                                'field_type' => TextType::class,
                                'label' => 'Title',
                            ]
                        ],
                    'excluded_fields'=>['slug']
                ]);
        $enabled = BooleanField::new('enabled');
        $isInMainMenu = BooleanField::new('isInMainMenu', 'Show under main menu');
        $mainArticle = AdminArticleField::new('mainArticle', false)
            ->setCssClass('ckEditor')
        ;

        $image = AssociationField::new('image', 'Image')
            ->setCssClass('d-none')
            ->setFormTypeOption('attr', ['article-image-upload' => true]);

        $mainMenu = AssociationField::new('mainMenu', false);
        $mainMenuList = AssociationField::new('mainMenu');
        $id = IntegerField::new('id');
        $metaTitle = TextField::new('metaTitle');
        $metaDescription = TextareaField::new('metaDescription');
        $created = DateTimeField::new('created');
        $updated = DateTimeField::new('updated');

        $new = [
            FormField::addPanel('Submenu')
                ->setProperty('form_tabs')
                ->setFormTypeOptions(['form_tabs' => true])
                ->addCssClass('tab-pane')
            ,
            $name,
            $mainMenu,
            $enabled,
            $isInMainMenu,
            $translations,

            FormField::addPanel('Main Article'),
            $mainArticle,
            $image,
            FormField::addPanel('SEO')
                ->setIcon('SEO')->addCssClass('optional')
                ->setHelp('SEO number is preferred'),
            $metaTitle,
            $metaDescription
        ];

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $enabled, $isInMainMenu, $mainMenuList, $created, $updated];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $mainArticle];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $new;
        }

    }


    public function configureFilters(Filters $filters): Filters
    {
        $filters
            ->add('name')
            ->add('mainMenu')
            ->add('enabled')
            ->add(BooleanFilter::new('isInMainMenu', 'Show under main menu'))
            ->add('created')
            ->add('updated')
        ;

        return $filters;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->uploadImage($entityInstance);
        parent::persistEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->uploadImage($entityInstance);
        parent::updateEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }

    private function uploadImage($entityInstance)
    {

        if (is_null($entityInstance->getMainArticle()) == false && $entityInstance->getMainArticle()->getFile()) {
            $entityInstance->getMainArticle()->uploadFile();
        }

        return $entityInstance;
    }

}
