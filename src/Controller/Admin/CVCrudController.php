<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminAreaOfExpertiseField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminImagesField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminLanguageSkillsField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\CV;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CVCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CV::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('CV')
            ->setEntityLabelInPlural('CV')
            ->setPageTitle(Crud::PAGE_INDEX, 'CV');

    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('created')
            ->add('weekendWork')
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id');
        $fullName = TextField::new('fullName');
        $email = TextField::new('email');
        $phone = TextField::new('phone');
        $role = ArrayField::new('role');

        $expertise = AdminAreaOfExpertiseField::new('areasOfExpertises');
        $languageSkills = AdminLanguageSkillsField::new('languageSkills');

        $availability = TextField::new('availability');
        $motherTongue = TextField::new('motherTongue');
        $catTools = ArrayField::new('catTools');
        $weekendWork = BooleanField::new('weekendWork');
        $coverLetter = TextareaField::new('coverLetter');
        $created = DateTimeField::new('created');
        $files = AdminImagesField::new('Images', 'Files');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $fullName, $email, $created];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $fullName, $email, $phone, $role, $expertise, $availability, $languageSkills, $catTools, $motherTongue, $weekendWork, $coverLetter, $files, $created];
        }
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable(Crud::PAGE_NEW, Crud::PAGE_EDIT)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ;
    }

}
