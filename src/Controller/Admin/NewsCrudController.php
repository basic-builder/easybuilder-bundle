<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminCkEditorField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminIconField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminTranslationField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminTranslationShow;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\ImageOptimizedField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;

class NewsCrudController extends AbstractCrudController
{
    protected $parameterBag;
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public static function getEntityFqcn(): string
    {
        return Article::class;
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('News')
            ->setEntityLabelInPlural('News')
            ->setPageTitle(Crud::PAGE_INDEX, 'News')

            ;
    }

    public function configureFields(string $pageName): iterable
    {

        $id = IntegerField::new('id');
        $name = TextField::new('name');
        $enabled = BooleanField::new('enabled');
        $isMain = BooleanField::new('isMain');
        $articleType = ChoiceField::new('articleType')
                        ->setChoices([
                            'Article'=>Article::IS_ARTICLE,
                            'News'=>Article::IS_NEWS])
        ;
        $created = DateTimeField::new('created');
        $updated = DateTimeField::new('updated');
        $mainMenu = AssociationField::new('mainMenu');
        $file = AdminIconField::new('file', '');
        $image = AssociationField::new('image', 'Image')
            ->setCssClass('d-none')
            ->setFormTypeOption('attr', ['article-image-upload' => true]);
        $metaTitle = TextField::new('metaTitle');
        $metaDescription = TextareaField::new('metaDescription');
        $mainImage = ImageOptimizedField::new('downloadLink', 'Main Image');
        $title = AdminTranslationShow::new('title');
        $shortDescription = AdminTranslationShow::new('shortDescription');
        $description = AdminTranslationShow::new('description', null, true);


        $languages = $this->parameterBag->get('jms_i18n_routing.locales');
        $languageDefault = $this->parameterBag->get('jms_i18n_routing.default_locale');

        $translation = AdminTranslationField::new('translations', false)
            ->setCssClass('ckEditor')
            ->setFormTypeOptions(
                [
                    'locales' => $languages,
                    'fields' => [
                        'description' => [
                            'field_type' => CKEditorType::class,
                            'attr' => ['rows' => '20'],
                            'label' => 'Description',
                            'config' => [
                                'toolbar' => 'full',
                                'filebrowserUploadRoute' => 'post_ckeditor_image',
                                'filebrowserUploadRouteParameters' => ['slug' => 'image'],
                                'extraPlugins' => 'templates',
                                'rows' => '20',

                            ],
                        ]
                    ],
                    'excluded_fields' => ['slug']
                ]
            );


        $edit = $new = [
            FormField::addPanel('News')
                ->setProperty('form_tabs')
                ->setFormTypeOptions(['form_tabs' => true])
                ->addCssClass('tab-pane')
            ,
            $name,
            $mainMenu,
            $image,
            $file,
            FormField::addPanel('News Translation'),
            $translation,
            FormField::addPanel('SEO')
                ->setIcon('SEO')->addCssClass('optional')
                ->setHelp('SEO number is preferred'),
            $metaTitle,
            $metaDescription
        ];

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $title, $enabled, $isMain, $mainMenu, $mainImage, $updated];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $title, $mainMenu,  $enabled, $isMain, $shortDescription, $description, $created, $updated];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $edit;
        }

    }



    public function configureFilters(Filters $filters): Filters
    {
        $filters
            ->add('name')
            ->add('mainMenu')
            ->add('enabled')
            ->add(BooleanFilter::new('isMain'))
            ->add('created')
            ->add('updated')
        ;

        return $filters;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {

        $queryBuilder = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $queryBuilder
            ->andWhere('entity.articleType = :aNt')
            ->setParameter('aNt', Article::IS_NEWS);

        return  $queryBuilder;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER)
            ;
    }

    public function configureAssets(Assets $assets): Assets
    {
        return Assets::new()
            ->addJsFile('build/js/article.js');
    }

    public function createNewForm(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormInterface
    {
        $entityDto->getInstance()->setArticleType(Article::IS_NEWS);
        return parent::createNewForm($entityDto, $formOptions, $context); // TODO: Change the autogenerated stub
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->uploadImage($entityInstance);
        $entityInstance->setArticleType(Article::IS_NEWS);
        parent::persistEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance = $this->uploadImage($entityInstance);
        $entityInstance->setArticleType(Article::IS_NEWS);
        parent::updateEntity($entityManager, $entityInstance); // TODO: Change the autogenerated stub
    }

    private function uploadImage($entityInstance){

        if($entityInstance->getFile()) {
            $entityInstance->uploadFile();
        }

        return $entityInstance;
    }


}
