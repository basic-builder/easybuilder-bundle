<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\FormInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\FormPartType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class FormInfoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return FormInfo::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $name = 'Form Info';

        return $crud
            ->setEntityLabelInSingular($name)
            ->setEntityLabelInPlural($name)
            ->setPageTitle(Crud::PAGE_INDEX, $name)
            ->setPaginatorPageSize(30)
            ->showEntityActionsAsDropdown(false)
            ->setPaginatorUseOutputWalkers(true)
            ->setPaginatorFetchJoinCollection(true)
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id');

        $name = TextField::new('name');

     $formPart = CollectionField::new('formPart')
            ->allowAdd(true)
            ->allowDelete(true)
            ->setEntryIsComplex(true)
            ->setRequired(true)
            ->setEntryType(FormPartType::class)
        ;


        $created = DateTimeField::new('created');
        $updated = DateTimeField::new('updated');

        $new = [
            $name,
            $formPart
        ];

        $index = [
            $id,
            $name,
            $formPart,
            $created,
            $updated
        ];


        if (Crud::PAGE_INDEX === $pageName) {
            return $index;
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return $index;
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $new;
        }
    }


   /* public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {

        $queryBuilder = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $queryBuilder
            ->andWhere('entity.definition = :aNt')
            ->setParameter('aNt', SingleTemplate::IS_FORM);

        return  $queryBuilder;
    }*/

    public function configureFilters(Filters $filters): Filters
    {
        $filters->add('name')
            ->add('formPart')
            ->add('created')
            ->add('updated');

        return $filters;
    }
}
