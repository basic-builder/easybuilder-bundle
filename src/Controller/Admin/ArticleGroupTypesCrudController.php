<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminCkEditorField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminIconField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminSingleTemplateField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminTranslationField;
use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\ImageOptimizedField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroup;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroupTypes;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class ArticleGroupTypesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArticleGroupTypes::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Article Group Types')
            ->setEntityLabelInPlural('Article Group Types')
            ->setPageTitle(Crud::PAGE_INDEX, 'Article Group Types')
            // add default sort by {{ position }} for change positionable at admin panel
            ->setDefaultSort(['position' => 'ASC'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {

        $id = IntegerField::new('id');

        $name = TextField::new('name');
        $singleTemplate = AdminSingleTemplateField::new('template');

        $created = DateTimeField::new('created');
        $updated = DateTimeField::new('updated');


        $edit = $new = [
            FormField::addPanel('Article Group')
                ->setProperty('form_tabs')
                ->setFormTypeOptions(['form_tabs' => true])
                ->addCssClass('tab-pane')
            ,
            $name, $singleTemplate
        ];

        $list = [
           $id, $name, $created, $updated
        ];

        if (Crud::PAGE_INDEX === $pageName) {
            return $list;
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return $list;
        } elseif (Crud::PAGE_NEW === $pageName) {
            return $new;
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return $edit;
        }

    }


    public function configureFilters(Filters $filters): Filters
    {
        $filters
            ->add('name')
            ->add('created')
            ->add('updated')
        ;

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER)
            ;
    }

}
