<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Admin;

use BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field\AdminImagesField;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Quote;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class QuoteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Quote::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Quote')
            ->setEntityLabelInPlural('Quote')
            ->setPageTitle(Crud::PAGE_INDEX, 'Quote');

    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
        ->add('deadline')
        ->add('created')
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id');
        $name = TextField::new('name', 'Full name');
        $company = TextField::new('company');
        $position = TextField::new('position');
        $phone = TextField::new('phone');
        $email = TextField::new('email');
        $sourceLanguages = TextField::new('sourceLanguage');
        $targetLanguages = TextField::new('targetLanguage');
        $service = ArrayField::new('service');
        $deadline = DateTimeField::new('deadline');
        $turnaroundTime = IntegerField::new('turnaroundTime');
        $created = DateTimeField::new('created');
        $notes = TextareaField::new('notes', 'Notes');
        $files = AdminImagesField::new('Images', 'Files');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $email, $created];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id,
                $name,
                $email,
                $phone,
                $targetLanguages,
                $sourceLanguages,
                $company,
                $position,
                $service,
                $deadline,
                $turnaroundTime,
                $created,
                $notes,
                $files
            ];
        }
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable(Crud::PAGE_NEW, Crud::PAGE_EDIT)
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }


}
