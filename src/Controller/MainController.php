<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Gallery;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Image;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\MainMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SubMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\User;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\ContactType;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\RegistrationFormType;
use BasicBuilder\Bundle\EasyBuilderBundle\Security\AppMainAuthenticator;
use BasicBuilder\Bundle\EasyBuilderBundle\Security\EmailVerifier;
use JMS\Serializer\SerializerInterface;
use PhpParser\Node\Expr\Array_;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class MainController extends AbstractController
{

    /**
     * @Route("/remove-image/{filename}/{object}", name="remove_image")
     * @param $filename
     * @param $object
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function removeImageAction($filename, $object)
    {
        try {
            // get entity manager
            $em = $this->getDoctrine()->getManager();

            // get object by className
            $object = $em->getRepository($object)->findOneBy(array('fileName' => $filename));


            // get origin file path
            $filePath = $object->getAbsolutePath() . $object->getFileName();

            // get doctrine
            $em = $this->getDoctrine()->getManager();

            // check file and remove
            if (file_exists($filePath) && is_file($filePath)) {
                unlink($filePath);
            }

            $object->setFileName(null);
            $object->setFileOriginalName(null);

            $em->persist($object);
            $em->flush();

            return $this->redirect($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            throw $e;
        }

    }
}
