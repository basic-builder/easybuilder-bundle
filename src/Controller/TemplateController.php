<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroup;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\ArticleGroupTypes;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Gallery;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\MainMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SubMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\ArticleFilterModel;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\SingleTemplateInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\TemplateControllerInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ArticleGroupRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ArticleGroupTypesRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TemplateController extends AbstractController implements TemplateControllerInterface
{

    public function index(Request $request)
    {
        $template = $request->get('template') ? $request->get('template') : 'main';
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('enabled_filter');

        $slider = $em->getRepository(Gallery::class)->findBy(['isMain'=>true], ['updated'=>'DESC'], 1, 0);

        $articleFilter = new ArticleFilterModel();

        switch($this->getParameter('templateDir')){
            case 'unreal_vp2':
                $articleFilter->setLimit(8);
                $articleFilter->setArticleType([Article::IS_NEWS]);
                $articleFilter->setMainMenuSlug('Works');
                break;
            case 'one_page':
                $articleFilter->setLimit(8);
                $articleFilter->setArticleType([Article::IS_NEWS]);
                $articleFilter->setMainMenuSlug('News');
                break;
            default:
                break;

        }

        $articles = $em->getRepository(Article::class)->findForHome($articleFilter);

        if(count($slider)>0) {
            $slider = $slider[0] ;
        }else{
            $slider =  $em->getRepository(Gallery::class)->findBy([],['updated'=>'DESC'], 1, 0) ;
            $slider ? $slider = $slider[0] : '';
        }

        return $this->render("{$template}/index.html.twig", ['slider'=> $slider, 'articles'=>$articles]);

    }

    public function sitemap(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('enabled_filter');

        $menu = $em->getRepository(MainMenu::class)->findForSitemap();

        $response = new Response($this->renderView('main/sitemap.xml.twig', ['menues'=>$menu]), Response::HTTP_OK);

        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }

    public function mainMenu(Request $request, string $slug) {

        $template = $request->get('template') ? $request->get('template') : 'main';

        $template.='/main_menu.html.twig';

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('enabled_filter');

        $menu = $em->getRepository(MainMenu::class)->findFullInfoBySlug($slug);

        if($menu instanceof MainMenu === false) {

            throw new HttpException(Response::HTTP_NOT_FOUND, "Page Not Found.");
        }

        if($menu instanceof SingleTemplateInterface && $menu->getSingleTemplate() instanceof SingleTemplate) {

            $template = $menu->getSingleTemplate()->getDirPath();
        }

        return $this->render("{$template}", ['menu' => $menu]);
    }

    public function menuArticle(Request $request) {

        $template = $request->get('template') ? $request->get('template') : 'main';

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('enabled_filter');

        $article = $em->getRepository(Article::class)->findByArticleType(Article::IS_NEWS);

        if(count($article) == 0) {

            throw new HttpException(Response::HTTP_NOT_FOUND, "Article not found.");
        }

        return $this->render("{$template}/news_home_list.html.twig", ['news' => $article]);
    }

    public function subMenu(Request $request, string $slug, string $subSlug) {

        $template = $request->get('template') ? $request->get('template') : 'main';

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('enabled_filter');

        $thisPage = $request->query->get('page') ? $request->query->get('page') : 1;

        $limit = 6;
        $articles = $em->getRepository(Article::class)->findBySubMenu($subSlug, $thisPage, $limit);
        $maxPages = ceil(count($articles) / $limit);
        $menu = $em->getRepository(SubMenu::class)->findOneBy(['slug'=>$subSlug]);

        if($menu instanceof SubMenu){
            return $this->render("{$template}/sub_menu.html.twig", ['subMenus'=> $menu, 'articles' => $articles, 'limit' => $limit, 'maxPages' => $maxPages, 'thisPage' => $thisPage]);
        }

        $article = $em->getRepository(Article::class)->findSingle($subSlug, $slug);

        if($article instanceof Article ) {

            $articleElems = $em->getRepository(Article::class)->findNewsTargets(Article::IS_NEWS, $slug, $article->getId());


            return $this->render("{$template}/menu_article_single.html.twig", ['article' => $article, 'articleElems' => $articleElems]);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, "Page Not Found.");
    }


    public function article(Request $request, string $slug, string $subSlug, string $slugArticle) {

        $template = $request->get('template') ? $request->get('template') : 'main';

        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('enabled_filter');

        $menu = $em->getRepository(SubMenu::class)->findOneBy(['slug'=>$subSlug]);
        $article = $em->getRepository(Article::class)->findOneBy(['slug'=>$slugArticle]);
        $subArticles = $em->getRepository(Article::class)->findArticlesBySubMenu($subSlug, 4);

        $articleElems = $em->getRepository(Article::class)->findArticleTarget(Article::IS_ARTICLE, $article->getSubMenu()->getSlug(), $article->getId());

        if(!$menu || !$article) {
            throw new HttpException(Response::HTTP_NOT_FOUND, "Page Not Found.");
        }

        return $this->render("{$template}/article.html.twig", ['subMenu'=>$menu, 'articleElems'=> $articleElems, 'article'=> $article, 'subArticles' => $subArticles]);
    }

    public function articleCategory(Request $request, ArticleGroupRepository $repository, $category = 'member', $img=false) {

        $template = $request->get('template') ? $request->get('template') : 'main';

        if($category == 'member' || $category == 'partner'){
            $template .= "/team_member_slider.html.twig";

            $articleGroup = $repository->findOneBy(['category' => $category]);
        }elseif ($category == 'services'){
            $template .= "/services_information.html.twig";

            $articleGroup = $repository->findOneBy(['category' => $category]);
        }

        if($articleGroup instanceof SingleTemplateInterface && $articleGroup->getSingleTemplate() instanceof SingleTemplate) {

            $template = $articleGroup->getSingleTemplate()->getDirPath();
        }

        return $this->render("{$template}", ['articleGroup'=>$articleGroup, 'img'=>$img]);
    }

    /**
     * This action is used to get article group types by action with view
     *
     * @param Request $request
     * @param ArticleGroupTypesRepository $repository
     * @param string $sectionSlug
     * @return JsonResponse|Response
     */
    public function sectionDefinition(Request $request, ArticleGroupTypesRepository $repository, string $sectionSlug) {

        $section = $repository->findSectionInfo($sectionSlug);

        if($section instanceof ArticleGroupTypes && $section->getTemplate() instanceof SingleTemplate && $section->getArticleGroups()->isEmpty() === false) {

            $template = $section->getTemplate()->getDirPath();
        }else{
            return $this->json(['code'=>Response::HTTP_NOT_FOUND, 'message'=>sprintf("Article Group for '%s' not found, please create from admin dashboard.", $sectionSlug)], Response::HTTP_OK);
        }

        return $this->render("{$template}", ['articleGroups'=>$section->getArticleGroups()->first()]);
    }
}
