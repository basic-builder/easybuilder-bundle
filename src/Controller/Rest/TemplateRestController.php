<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Controller\Rest;


use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Contact;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\CV;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\FormInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Image;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Quote;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\User;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\ClientCustomFormType;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\CvType;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\Fields\TextAreaType;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\QuoteType;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\TemplateControllerInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\FormInfoRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TemplateRestController
 * @package BasicBuilder\Bundle\EasyBuilderBundle\Controller\Rest
 */
class TemplateRestController extends AbstractController
{


    /**
     * @Route("/ckeditor/image", name="post_ckeditor_image", methods={"POST"})
     * @param Request $request
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function postImage(Request $request)
    {

        if ($this->getUser() instanceof User === false) {

            throw new HttpException(Response::HTTP_NOT_FOUND, "Page Not Found.");
        }
        // get request
        $request = $this->get('request_stack')->getCurrentRequest();

        //get uploaded file when exist
        $file = $request->files->get('upload');

        if ($file) {
            $em = $this->getDoctrine()->getManager();

            // create image
            $object = new Image();
            $object->setFile($file);
            $object->setEnabled(true);
            $object->uploadFile();

            $em->persist($object);
            $em->flush();

            return new JsonResponse(['fileName' => $object->getFileOriginalName(), 'uploaded' => 1, 'url' => $object->getDownloadLink()], Response::HTTP_OK);
        }

        return new JsonResponse(['fileName' => '', 'uploaded' => 0, 'url' => ''], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @Route("/image/info/{id}", name="image-info", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function getImage(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $image = $em->getRepository(Image::class)->find($id);

        return new JsonResponse(['id' => $image->getId(),
            'downloadLink' => $image->getDownloadLink(),
            'name' => $image->getFileOriginalName(),
            'url_link' => $image->getUrlLink(),
            'altText' => $image->getAltText(),
            'title' => $image->getTitle(),
            'description' => $image->getDescription(),
            'is_enabled' => $image->getEnabled()
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     * @Route("/galleries/files", name="gallery-files", methods={"POST"})
     *
     */
    public function postFile(Request $request)
    {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        ini_set('upload_max_filesize', '51mb');
        set_time_limit(300);

        $em = $this->getDoctrine()->getManager();
        $files = $request->files;

        foreach ($files as $key => $file) {

            $image = new Image();
            $image->setDescription($request->get('description') ? $request->get('description') : null);
            $image->setUrlLink($request->get('urlLink') ? $request->get('urlLink') : null);
            $image->setTitle($request->get('title') ? $request->get('title') : null);
            $image->setAltText($request->get('altText') ? $request->get('altText') : null);
            $image->setFile($file[0]);
//            try {

            //todo: ned to rewrite
                $image->uploadFile();

//            }catch (\Exception $exception){

//                dump($exception->getMessage());
//            }

            $em->persist($image);
            $em->flush();

            return new JsonResponse(['id' => $image->getId(), 'download_link' => $image->getDownloadLink(), 'name' => $image->getFileOriginalName()], Response::HTTP_OK);

        }

        return new JsonResponse(false, 403);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/galleries/images", name="gallery-image", methods={"POST"})
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function postGallery(Request $request)
    {

        $content = json_decode($request->getContent(), true);

        $id = array_key_exists('modal-item-id', $content) ? $content['modal-item-id'] : null;
        $title = array_key_exists('modal-item-title', $content) ? $content['modal-item-title'] : null;
        $urlLink = array_key_exists('modal-item-link', $content) ? $content['modal-item-link'] : null;
        $altText = array_key_exists('modal-item-alt-text', $content) ? $content['modal-item-alt-text'] : null;
        $description = array_key_exists('modal-item-description', $content) ? $content['modal-item-description'] : null;
//        $isEnabled = array_key_exists('modal-item-isEnabled', $content) ? true : null;


        if (is_null($id) || is_null($title) || is_null($altText) || is_null($description)) {
            return new JsonResponse(['message' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        $image = $em->getRepository(Image::class)->find($id);
        (array_key_exists('modal-item-isEnabled', $content) && $content['modal-item-isEnabled'] == 'on') ? $image->setEnabled(true) : false;
        $image->setTitle($title);
        $image->setUrlLink($urlLink);
        $image->setAltText($altText);
        $image->setDescription($description);
        /*if($content['modal-item-isEnabled'] == 'on' && $isEnabled){
            $image->setEnabled(true);
        }else{
            $image->setEnabled(false);
        }*/

        $em->persist($image);

        try {
            $em->flush();

            return new JsonResponse(['image' => $image], Response::HTTP_OK);
        } catch (\Exception $exception) {

            return new JsonResponse(['message' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/uploaded/images", name="uploaded-images", methods={"POST"})
     *
     * @Security("is_granted('ROLE_ADMIN')")
     * @todo need to check serializer
     */
    public function postUploadedImage(Request $request, SerializerInterface $serializer)
    {

        $data = $request->getContent();
        $data = explode(',', $data);

        $em = $this->getDoctrine()->getManager();

        $img = $em->getRepository(Image::class)->findAllBy($data);


        return new JsonResponse($img, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/galleries/{id}", name="gallery-remove", methods={"GET", "DELETE"})
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteImage(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $img = $em->getRepository(Image::class)->find($id);

        $em->remove($img);
        $em->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/contact/form", name="contact-form", methods={"POST"})
     */
    public function contactForm(Request $request, ValidatorInterface $validator)
    {

        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $name = array_key_exists('name', $data) ? $data['name'] : null;
        $email = array_key_exists('email', $data) ? $data['email'] : null;
        $message = array_key_exists('message', $data) ? $data['message'] : null;

        if (is_null($name) || is_null($email) || is_null($message)) {
            return new JsonResponse(['result' => 'Data is incomplete'], Response::HTTP_BAD_REQUEST);
        }

        $contact = new Contact();


        $contact->setName($name);
        $contact->setEmail($email);
        $contact->setSubject('contact message');
        $contact->setMessage($message);

        $errors = $validator->validate($contact, null, ['insert-message']);

        // todo error check, not work don't know why (
        if (count($errors) > 0) {

            $errorMessages = [];
            foreach ($errors as $error) {


                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return new JsonResponse(['result' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $em->persist($contact);
        try {
            $em->flush();
            return new JsonResponse(['result' => ['message' => 'Your message sent']], Response::HTTP_OK);

        } catch (\Exception $e) {
            return new JsonResponse(['result' => ['message' =>  'Something went wrong']], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/form/info/draw/{slug}", name="get-form-info", methods={"GET", "POST"})
     */
    public function getFormInfo(Request $request,
                                FormInfoRepository $repository,
                                $slug)
    {

        // todo ? need some work
        $em = $this->getDoctrine()->getManager();

        $formInfo = $repository->findOneBy(['slug' => $slug]);

        if ($formInfo instanceof FormInfo == false) {
            return $this->json(['data' => null, 'message' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $form = $this->createForm(ClientCustomFormType::class);
        foreach ($formInfo->getFormPart() as $key => $item) {
            $form->add($item->getSlug(), 'BasicBuilder\Bundle\EasyBuilderBundle\\Form\\Fields\\' . $item->getFormDefinition()->getDirPath(), ['label' => $item->getName()]);
        }

        $form->add('submit', SubmitType::class, ['label' => sprintf('Save %s', $formInfo->getName())]);

        if ($request->isMethod('POST')) {
            $formName = $request->query->get('form');
            $data = $form->handleRequest($request);
            $data = $data->getData();

//            $form->setData($data);

            dump($formName);
            dump($data);
            dump($form->getData());
        }

        $formHtml = $this->renderView('form_draw_quote.html.twig', ['form_info' => $form->createView(), 'form_diff' => $formInfo->getSlug()]);
        $result ['form_html'] = $formHtml;

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * This function to used for get and set to CV or Quote information
     * @param Request $request
     * @Route("/custom-form/{className}", name="custom_form", methods={"GET", "POST"})
     */
    public function customFormsAction(Request $request, TranslatorInterface $translator, ValidatorInterface $validator, MailerInterface $mailer, $className)
    {

        // get className (cv, quote)
        if (!is_null($className)) {
            if ($className == 'cv') {
                $customForm = $this->createForm(CvType::class);
                $formDiff = 'cv';
            } elseif ($className == 'quote') {
                $customForm = $this->createForm(QuoteType::class);
                $formDiff = 'quote';
            } else {
                return $this->json(['data' => null, 'message' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
            }
        }
        // when form sent
        $form = $customForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {

            if ($customForm->isSubmitted()) {

                $form = $form->getData();

                $fileIds = $request->request->get('files');

                foreach ($fileIds as $fileId) {
                    if (strlen($fileId) && is_null($fileId) == false) {
                        $file = $em->getRepository(Image::class)->findOneBy(['id' => $fileId]);

                        if ($file instanceof Image) {
                            if ($className == 'cv'){
                                $file->setCvFiles($form);
                            }elseif ($className == 'quote'){
                                $file->setQuoteFiles($form);
                            }else {
                                $this->addFlash('danger', [$translator->trans('form.not_valid_form')]);
                                return $this->redirectToRoute('homepage');
                            }
                        }
                    }
                }

                // get errors if they are
                $errors = $validator->validate($form);
                if (count($errors)){
                    $errorMessages = [];
                    foreach ($errors as $error) {

                        $errorMessages[] = $error->getMessage();
                    }

                    $this->addFlash('danger', $errorMessages);
                    return $this->redirectToRoute('homepage');
                }

                // if all is good
                $em->persist($form);
                try {

                    $adminEmail = $this->getParameter('adminEmail');
                    $mailerTo = $this->getParameter('mailerTo');

                    $email = (new TemplatedEmail())
                        ->from(new Address($adminEmail))
                        ->to(new Address($mailerTo))
                        ->subject('We have a new message')
                        ->context(['data' => "New {$className} form by {$form->getEmail()}"])
                        ->htmlTemplate('email/base_send_email.html.twig')
                    ;

                    $mailer->send($email);

                    $em->flush();
                    $this->addFlash('success', [$translator->trans(sprintf('form.success.%s', $className ))]);
                    return $this->redirectToRoute('homepage');
                } catch (\Exception $exception) {
                    // when some error exist
                    $this->addFlash('danger', [$translator->trans('form.not_valid_form')]);
                    return $this->redirectToRoute('homepage');
                }
            } else {
                // when form is not valid
                $this->addFlash('danger', [$translator->trans('form.not_valid_form')]);
                return $this->redirectToRoute('homepage');
            }
        }

        $formHtml = $this->renderView("main/form_draw_{$className}.html.twig", ['form_info' => $customForm->createView(), 'formDiff' => $formDiff]);
        $result ['form_html'] = $formHtml;

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * This function used for element sorting
     *
     * @param $entityClass
     * @param $id
     * @param $position
     * @return bool|JsonResponse
     * @throws \ReflectionException
     * @Route("/sort/{entityClass}/{id}/{position}", name="easyadmin_dragndrop_sort")
     */
    public function sortAction(Request $request, $entityClass, $id, $position)
    {

        // reflect the class
        try {
            $rc = new \ReflectionClass($entityClass);
        } catch (\ReflectionException $error) {
            throw new \ReflectionException("The class name " . $entityClass . "  cannot be reflected.");
        }

        $em = $this->getDoctrine()->getManager();
        // find entity by id
        $entity = $em->getRepository($rc->getName())->find($id);

        if (is_null($entity)) {
            throw new NotFoundHttpException("The entity was not found");
        }

        // set position
        $entity->setPosition($position);
        $em->persist($entity);
        try {
            $em->flush();
        }catch (\Exception $exception){
            return new JsonResponse([
                'success' => false,
                'message' => $exception
            ], Response::HTTP_INTERNAL_SERVER_ERROR);

        }

        return new JsonResponse([
            'success' => true,
            'message' => 'ok'
        ], Response::HTTP_OK);

    }

    /**
     * this function used for remove cv or quote image
     * @Route("/remove-form-image/{id}", name="remove_form_image", methods={"POST"})
     */
    public function removeFormImageAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        // get object by className
        $object = $em->getRepository(Image::class)->findOneBy(array('id' => $id));

        // get origin file path
        $filePath = $object->getAbsolutePath() . $object->getFileName();

        // get doctrine
        $em = $this->getDoctrine()->getManager();

        // check file and remove
        if (file_exists($filePath) && is_file($filePath)) {
            unlink($filePath);
        }

        $em->remove($object);

        try {
            $em->flush();
            return new JsonResponse([
                null,
            ], Response::HTTP_NO_CONTENT);
        } catch (\Exception $exception) {

        }

    }

}