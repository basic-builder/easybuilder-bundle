<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ArrayUnsetExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('unset', [$this, 'unset'], [ 'needs_context' => true]),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('function_name', [$this, 'doSomething']),
        ];
    }

    public function unset(&$context, $variable, $key = null)
    {
        dump($context);
        dump($variable);
        dump($key);

        unset($context[$variable][1]);
//        unset($variable[1]);
        dump($context);

//        if ($key === null) {
//            dump('sss');
//            unset($context[$variable]) }
//        else{
//            if (isset($context[$variable])) unset($context[$variable][$key]);
//        }
//        dump($context);
//        return $context;
        // ...
    }
}
