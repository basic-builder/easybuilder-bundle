<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class JsonDecodeExtension extends AbstractExtension
{
      public function getName()
    {
        return 'some.extension';
    }

    public function getFilters() {
        return array(
            new TwigFilter('json_decode', array($this, 'jsonDecode'))
        );
    }

    public function jsonDecode($str) {
        return json_decode($str, true);
    }
}
