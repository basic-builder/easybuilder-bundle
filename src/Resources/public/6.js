(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./assets/js/swiper.js":
/*!*****************************!*\
  !*** ./assets/js/swiper.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(/*! swiper/swiper.scss */ "./node_modules/swiper/swiper.scss");

__webpack_require__(/*! swiper/css/swiper.min.css */ "./node_modules/swiper/css/swiper.min.css");

var Swiper = __webpack_require__(/*! swiper/js/swiper */ "./node_modules/swiper/js/swiper.js");

__webpack_require__(/*! swiper/components/effect-fade/effect-fade.scss */ "./node_modules/swiper/components/effect-fade/effect-fade.scss");

$(document).ready(function (e) {
  var direction = 'vertical';

  if ($(document).width() < 991) {
    direction = 'horizontal';
  }

  if ($('.swiper-container').length) {
    var mySwiper = new Swiper('.swiper-container', {
      direction: direction,
      loop: true,
      speed: 3000,
      autoplay: {
        delay: 4500,
        disableOnInteraction: false
      },
      fadeEffect: {
        crossFade: true
      },
      effect: "fade",
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    });
  }
}); // ready
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvc3dpcGVyLmpzIl0sIm5hbWVzIjpbInJlcXVpcmUiLCJTd2lwZXIiLCIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImUiLCJkaXJlY3Rpb24iLCJ3aWR0aCIsImxlbmd0aCIsIm15U3dpcGVyIiwibG9vcCIsInNwZWVkIiwiYXV0b3BsYXkiLCJkZWxheSIsImRpc2FibGVPbkludGVyYWN0aW9uIiwiZmFkZUVmZmVjdCIsImNyb3NzRmFkZSIsImVmZmVjdCIsInBhZ2luYXRpb24iLCJlbCIsImNsaWNrYWJsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUFBLDREQUFPLENBQUMsNkRBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQywyRUFBRCxDQUFQOztBQUVBLElBQU1DLE1BQU0sR0FBR0QsbUJBQU8sQ0FBQyw0REFBRCxDQUF0Qjs7QUFFQUEsbUJBQU8sQ0FBQyxxSEFBRCxDQUFQOztBQUdBRSxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFVBQVVDLENBQVYsRUFBYTtBQUUzQixNQUFJQyxTQUFTLEdBQUcsVUFBaEI7O0FBRUEsTUFBR0osQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUksS0FBWixLQUFzQixHQUF6QixFQUE2QjtBQUN6QkQsYUFBUyxHQUFHLFlBQVo7QUFDSDs7QUFFRCxNQUFJSixDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1Qk0sTUFBM0IsRUFBbUM7QUFDL0IsUUFBSUMsUUFBUSxHQUFHLElBQUlSLE1BQUosQ0FBVyxtQkFBWCxFQUFnQztBQUMzQ0ssZUFBUyxFQUFFQSxTQURnQztBQUUzQ0ksVUFBSSxFQUFFLElBRnFDO0FBRzNDQyxXQUFLLEVBQUUsSUFIb0M7QUFJM0NDLGNBQVEsRUFBRTtBQUNOQyxhQUFLLEVBQUUsSUFERDtBQUVOQyw0QkFBb0IsRUFBRTtBQUZoQixPQUppQztBQVEzQ0MsZ0JBQVUsRUFBRTtBQUNSQyxpQkFBUyxFQUFFO0FBREgsT0FSK0I7QUFXM0NDLFlBQU0sRUFBRSxNQVhtQztBQVkzQ0MsZ0JBQVUsRUFBRTtBQUNSQyxVQUFFLEVBQUUsb0JBREk7QUFFUkMsaUJBQVMsRUFBRTtBQUZIO0FBWitCLEtBQWhDLENBQWY7QUFrQkg7QUFFSixDQTdCRCxFLENBNkJJLFEiLCJmaWxlIjoiNi5qcyIsInNvdXJjZXNDb250ZW50IjpbInJlcXVpcmUoJ3N3aXBlci9zd2lwZXIuc2NzcycpO1xucmVxdWlyZSgnc3dpcGVyL2Nzcy9zd2lwZXIubWluLmNzcycpO1xuXG5jb25zdCBTd2lwZXIgPSByZXF1aXJlKCdzd2lwZXIvanMvc3dpcGVyJyk7XG5cbnJlcXVpcmUoJ3N3aXBlci9jb21wb25lbnRzL2VmZmVjdC1mYWRlL2VmZmVjdC1mYWRlLnNjc3MnKVxuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uIChlKSB7XG5cbiAgICBsZXQgZGlyZWN0aW9uID0gJ3ZlcnRpY2FsJztcblxuICAgIGlmKCQoZG9jdW1lbnQpLndpZHRoKCkgPCA5OTEpe1xuICAgICAgICBkaXJlY3Rpb24gPSAnaG9yaXpvbnRhbCc7XG4gICAgfVxuXG4gICAgaWYgKCQoJy5zd2lwZXItY29udGFpbmVyJykubGVuZ3RoKSB7XG4gICAgICAgIHZhciBteVN3aXBlciA9IG5ldyBTd2lwZXIoJy5zd2lwZXItY29udGFpbmVyJywge1xuICAgICAgICAgICAgZGlyZWN0aW9uOiBkaXJlY3Rpb24sXG4gICAgICAgICAgICBsb29wOiB0cnVlLFxuICAgICAgICAgICAgc3BlZWQ6IDMwMDAsXG4gICAgICAgICAgICBhdXRvcGxheToge1xuICAgICAgICAgICAgICAgIGRlbGF5OiA0NTAwLFxuICAgICAgICAgICAgICAgIGRpc2FibGVPbkludGVyYWN0aW9uOiBmYWxzZSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBmYWRlRWZmZWN0OiB7XG4gICAgICAgICAgICAgICAgY3Jvc3NGYWRlOiB0cnVlXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZWZmZWN0OiBcImZhZGVcIixcbiAgICAgICAgICAgIHBhZ2luYXRpb246IHtcbiAgICAgICAgICAgICAgICBlbDogJy5zd2lwZXItcGFnaW5hdGlvbicsXG4gICAgICAgICAgICAgICAgY2xpY2thYmxlOiB0cnVlXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgIH0pO1xuICAgIH1cblxufSk7IC8vIHJlYWR5Il0sInNvdXJjZVJvb3QiOiIifQ==