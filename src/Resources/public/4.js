(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./assets/js/typeAnimation.js":
/*!************************************!*\
  !*** ./assets/js/typeAnimation.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {__webpack_require__(/*! core-js/modules/web.timers.js */ "./node_modules/core-js/modules/web.timers.js");

$(document).ready(function (e) {
  if ($('.single_text h3').length) {
    var typeAnimationIt = function typeAnimationIt() {
      var humanize = Math.round(Math.random() * (200 - 30)) + 30;
      timeOut = setTimeout(function () {
        charVal++;
        var txtLen = lengths[currentValue],
            type = textOpts[currentValue].substring(0, charVal);
        $writer.html(type + cursor);
        typeAnimationIt();

        if (charVal == txtLen) {
          clearTimeout(timeOut);
          $('.cursor').html('');
        }
      }, humanize);
    };

    typeAnimationIt();
    var textOpts = [$('.single_text h3').text()],
        timeOut,
        len = textOpts.length,
        $writer = $('.single_text h3'),
        currentValue = 0,
        charVal = 0,
        cursor = '<span class="cursor">|</span>',
        lengths = [];
    $.each(textOpts, function (index, value) {
      lengths.push(value.length);
    });
  }
}); //ready
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./node_modules/core-js/modules/web.timers.js":
/*!****************************************************!*\
  !*** ./node_modules/core-js/modules/web.timers.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var global = __webpack_require__(/*! ../internals/global */ "./node_modules/core-js/internals/global.js");
var userAgent = __webpack_require__(/*! ../internals/engine-user-agent */ "./node_modules/core-js/internals/engine-user-agent.js");

var slice = [].slice;
var MSIE = /MSIE .\./.test(userAgent); // <- dirty ie9- check

var wrap = function (scheduler) {
  return function (handler, timeout /* , ...arguments */) {
    var boundArgs = arguments.length > 2;
    var args = boundArgs ? slice.call(arguments, 2) : undefined;
    return scheduler(boundArgs ? function () {
      // eslint-disable-next-line no-new-func -- spec requirement
      (typeof handler == 'function' ? handler : Function(handler)).apply(this, args);
    } : handler, timeout);
  };
};

// ie9- setTimeout & setInterval additional parameters fix
// https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#timers
$({ global: true, bind: true, forced: MSIE }, {
  // `setTimeout` method
  // https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#dom-settimeout
  setTimeout: wrap(global.setTimeout),
  // `setInterval` method
  // https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#dom-setinterval
  setInterval: wrap(global.setInterval)
});


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvdHlwZUFuaW1hdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9tb2R1bGVzL3dlYi50aW1lcnMuanMiXSwibmFtZXMiOlsiJCIsImRvY3VtZW50IiwicmVhZHkiLCJlIiwibGVuZ3RoIiwidHlwZUFuaW1hdGlvbkl0IiwiaHVtYW5pemUiLCJNYXRoIiwicm91bmQiLCJyYW5kb20iLCJ0aW1lT3V0Iiwic2V0VGltZW91dCIsImNoYXJWYWwiLCJ0eHRMZW4iLCJsZW5ndGhzIiwiY3VycmVudFZhbHVlIiwidHlwZSIsInRleHRPcHRzIiwic3Vic3RyaW5nIiwiJHdyaXRlciIsImh0bWwiLCJjdXJzb3IiLCJjbGVhclRpbWVvdXQiLCJ0ZXh0IiwibGVuIiwiZWFjaCIsImluZGV4IiwidmFsdWUiLCJwdXNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUVBQSxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFVBQVVDLENBQVYsRUFBYTtBQUUzQixNQUFHSCxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQkksTUFBeEIsRUFBK0I7QUFBQSxRQWNsQkMsZUFka0IsR0FjM0IsU0FBU0EsZUFBVCxHQUEyQjtBQUN2QixVQUFJQyxRQUFRLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsTUFBaUIsTUFBTSxFQUF2QixDQUFYLElBQXlDLEVBQXhEO0FBQ0FDLGFBQU8sR0FBR0MsVUFBVSxDQUFDLFlBQVk7QUFDN0JDLGVBQU87QUFDUCxZQUFJQyxNQUFNLEdBQUdDLE9BQU8sQ0FBQ0MsWUFBRCxDQUFwQjtBQUFBLFlBQ0lDLElBQUksR0FBR0MsUUFBUSxDQUFDRixZQUFELENBQVIsQ0FBdUJHLFNBQXZCLENBQWlDLENBQWpDLEVBQW9DTixPQUFwQyxDQURYO0FBRUFPLGVBQU8sQ0FBQ0MsSUFBUixDQUFhSixJQUFJLEdBQUdLLE1BQXBCO0FBQ0FoQix1QkFBZTs7QUFDZixZQUFJTyxPQUFPLElBQUlDLE1BQWYsRUFBdUI7QUFDbkJTLHNCQUFZLENBQUNaLE9BQUQsQ0FBWjtBQUNBVixXQUFDLENBQUMsU0FBRCxDQUFELENBQWFvQixJQUFiLENBQWtCLEVBQWxCO0FBQ0g7QUFDSixPQVZtQixFQVVqQmQsUUFWaUIsQ0FBcEI7QUFXSCxLQTNCMEI7O0FBQzNCRCxtQkFBZTtBQUNmLFFBQUlZLFFBQVEsR0FBRyxDQUFDakIsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ1QixJQUFyQixFQUFELENBQWY7QUFBQSxRQUNJYixPQURKO0FBQUEsUUFFSWMsR0FBRyxHQUFHUCxRQUFRLENBQUNiLE1BRm5CO0FBQUEsUUFHSWUsT0FBTyxHQUFHbkIsQ0FBQyxDQUFDLGlCQUFELENBSGY7QUFBQSxRQUlJZSxZQUFZLEdBQUcsQ0FKbkI7QUFBQSxRQUlzQkgsT0FBTyxHQUFHLENBSmhDO0FBQUEsUUFLSVMsTUFBTSxHQUFHLCtCQUxiO0FBQUEsUUFNSVAsT0FBTyxHQUFHLEVBTmQ7QUFRQWQsS0FBQyxDQUFDeUIsSUFBRixDQUFPUixRQUFQLEVBQWlCLFVBQVVTLEtBQVYsRUFBaUJDLEtBQWpCLEVBQXdCO0FBQ3JDYixhQUFPLENBQUNjLElBQVIsQ0FBYUQsS0FBSyxDQUFDdkIsTUFBbkI7QUFDSCxLQUZEO0FBa0JIO0FBRUosQ0FoQ0QsRSxDQWdDSSxPOzs7Ozs7Ozs7Ozs7QUNsQ0osUUFBUSxtQkFBTyxDQUFDLHVFQUFxQjtBQUNyQyxhQUFhLG1CQUFPLENBQUMsdUVBQXFCO0FBQzFDLGdCQUFnQixtQkFBTyxDQUFDLDZGQUFnQzs7QUFFeEQ7QUFDQSxzQ0FBc0M7O0FBRXRDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUcseUNBQXlDO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMiLCJmaWxlIjoiNC5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoZSkge1xuXG4gICAgaWYoJCgnLnNpbmdsZV90ZXh0IGgzJykubGVuZ3RoKXtcbiAgICAgICAgdHlwZUFuaW1hdGlvbkl0KCk7XG4gICAgICAgIHZhciB0ZXh0T3B0cyA9IFskKCcuc2luZ2xlX3RleHQgaDMnKS50ZXh0KCldLFxuICAgICAgICAgICAgdGltZU91dCxcbiAgICAgICAgICAgIGxlbiA9IHRleHRPcHRzLmxlbmd0aCxcbiAgICAgICAgICAgICR3cml0ZXIgPSAkKCcuc2luZ2xlX3RleHQgaDMnKSxcbiAgICAgICAgICAgIGN1cnJlbnRWYWx1ZSA9IDAsIGNoYXJWYWwgPSAwLFxuICAgICAgICAgICAgY3Vyc29yID0gJzxzcGFuIGNsYXNzPVwiY3Vyc29yXCI+fDwvc3Bhbj4nLFxuICAgICAgICAgICAgbGVuZ3RocyA9IFtdO1xuXG4gICAgICAgICQuZWFjaCh0ZXh0T3B0cywgZnVuY3Rpb24gKGluZGV4LCB2YWx1ZSkge1xuICAgICAgICAgICAgbGVuZ3Rocy5wdXNoKHZhbHVlLmxlbmd0aCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGZ1bmN0aW9uIHR5cGVBbmltYXRpb25JdCgpIHtcbiAgICAgICAgICAgIHZhciBodW1hbml6ZSA9IE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqICgyMDAgLSAzMCkpICsgMzA7XG4gICAgICAgICAgICB0aW1lT3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY2hhclZhbCsrO1xuICAgICAgICAgICAgICAgIHZhciB0eHRMZW4gPSBsZW5ndGhzW2N1cnJlbnRWYWx1ZV0sXG4gICAgICAgICAgICAgICAgICAgIHR5cGUgPSB0ZXh0T3B0c1tjdXJyZW50VmFsdWVdLnN1YnN0cmluZygwLCBjaGFyVmFsKTtcbiAgICAgICAgICAgICAgICAkd3JpdGVyLmh0bWwodHlwZSArIGN1cnNvcik7XG4gICAgICAgICAgICAgICAgdHlwZUFuaW1hdGlvbkl0KCk7XG4gICAgICAgICAgICAgICAgaWYgKGNoYXJWYWwgPT0gdHh0TGVuKSB7XG4gICAgICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aW1lT3V0KTtcbiAgICAgICAgICAgICAgICAgICAgJCgnLmN1cnNvcicpLmh0bWwoJycpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgaHVtYW5pemUpO1xuICAgICAgICB9XG4gICAgfVxuXG59KTsgLy9yZWFkeSIsInZhciAkID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2V4cG9ydCcpO1xudmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9nbG9iYWwnKTtcbnZhciB1c2VyQWdlbnQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZW5naW5lLXVzZXItYWdlbnQnKTtcblxudmFyIHNsaWNlID0gW10uc2xpY2U7XG52YXIgTVNJRSA9IC9NU0lFIC5cXC4vLnRlc3QodXNlckFnZW50KTsgLy8gPC0gZGlydHkgaWU5LSBjaGVja1xuXG52YXIgd3JhcCA9IGZ1bmN0aW9uIChzY2hlZHVsZXIpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChoYW5kbGVyLCB0aW1lb3V0IC8qICwgLi4uYXJndW1lbnRzICovKSB7XG4gICAgdmFyIGJvdW5kQXJncyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyO1xuICAgIHZhciBhcmdzID0gYm91bmRBcmdzID8gc2xpY2UuY2FsbChhcmd1bWVudHMsIDIpIDogdW5kZWZpbmVkO1xuICAgIHJldHVybiBzY2hlZHVsZXIoYm91bmRBcmdzID8gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLW5ldy1mdW5jIC0tIHNwZWMgcmVxdWlyZW1lbnRcbiAgICAgICh0eXBlb2YgaGFuZGxlciA9PSAnZnVuY3Rpb24nID8gaGFuZGxlciA6IEZ1bmN0aW9uKGhhbmRsZXIpKS5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICB9IDogaGFuZGxlciwgdGltZW91dCk7XG4gIH07XG59O1xuXG4vLyBpZTktIHNldFRpbWVvdXQgJiBzZXRJbnRlcnZhbCBhZGRpdGlvbmFsIHBhcmFtZXRlcnMgZml4XG4vLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS90aW1lcnMtYW5kLXVzZXItcHJvbXB0cy5odG1sI3RpbWVyc1xuJCh7IGdsb2JhbDogdHJ1ZSwgYmluZDogdHJ1ZSwgZm9yY2VkOiBNU0lFIH0sIHtcbiAgLy8gYHNldFRpbWVvdXRgIG1ldGhvZFxuICAvLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS90aW1lcnMtYW5kLXVzZXItcHJvbXB0cy5odG1sI2RvbS1zZXR0aW1lb3V0XG4gIHNldFRpbWVvdXQ6IHdyYXAoZ2xvYmFsLnNldFRpbWVvdXQpLFxuICAvLyBgc2V0SW50ZXJ2YWxgIG1ldGhvZFxuICAvLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS90aW1lcnMtYW5kLXVzZXItcHJvbXB0cy5odG1sI2RvbS1zZXRpbnRlcnZhbFxuICBzZXRJbnRlcnZhbDogd3JhcChnbG9iYWwuc2V0SW50ZXJ2YWwpXG59KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=