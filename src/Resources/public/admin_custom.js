(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin_custom"],{

/***/ "./assets/js/Uppy.js":
/*!***************************!*\
  !*** ./assets/js/Uppy.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var regenerator_runtime_runtime_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! regenerator-runtime/runtime.js */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_number_constructor_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.number.constructor.js */ "./node_modules/core-js/modules/es.number.constructor.js");
/* harmony import */ var core_js_modules_es_number_constructor_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_constructor_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.filter.js */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _uppy_core_dist_style_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @uppy/core/dist/style.css */ "./node_modules/@uppy/core/dist/style.css");
/* harmony import */ var _uppy_core_dist_style_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_uppy_core_dist_style_css__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _uppy_dashboard_dist_style_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @uppy/dashboard/dist/style.css */ "./node_modules/@uppy/dashboard/dist/style.css");
/* harmony import */ var _uppy_dashboard_dist_style_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_uppy_dashboard_dist_style_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _uppy_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @uppy/core */ "./node_modules/@uppy/core/lib/index.js");
/* harmony import */ var _uppy_core__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_uppy_core__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _uppy_dashboard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @uppy/dashboard */ "./node_modules/@uppy/dashboard/lib/index.js");
/* harmony import */ var _uppy_dashboard__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_uppy_dashboard__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _uppy_google_drive__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @uppy/google-drive */ "./node_modules/@uppy/google-drive/lib/index.js");
/* harmony import */ var _uppy_google_drive__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_uppy_google_drive__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _uppy_dropbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @uppy/dropbox */ "./node_modules/@uppy/dropbox/lib/index.js");
/* harmony import */ var _uppy_dropbox__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_uppy_dropbox__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _uppy_instagram__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @uppy/instagram */ "./node_modules/@uppy/instagram/lib/index.js");
/* harmony import */ var _uppy_instagram__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_uppy_instagram__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _uppy_webcam__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @uppy/webcam */ "./node_modules/@uppy/webcam/lib/index.js");
/* harmony import */ var _uppy_webcam__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_uppy_webcam__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @uppy/xhr-upload */ "./node_modules/@uppy/xhr-upload/lib/index.js");
/* harmony import */ var _uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _api_admin_api__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./api/admin.api */ "./assets/js/api/admin.api.js");
/* harmony import */ var _services_utilities__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/utilities */ "./assets/js/services/utilities.js");
/* harmony import */ var _helpers_showDefaultImageModal__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./helpers/showDefaultImageModal */ "./assets/js/helpers/showDefaultImageModal.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }






















var addImagesToGallery = function addImagesToGallery(imagesArr, target) {
  var imagesHtml = document.createDocumentFragment();

  if (imagesArr !== null && imagesArr !== void 0 && imagesArr.length) {
    imagesArr.forEach(function (image) {
      var src = image.src,
          id = image.id,
          title = image.title;
      var imgDiv = document.createElement('div');
      var remove = document.createElement('a');
      var img = document.createElement('img');
      imgDiv.className = 'image-' + id;
      remove.appendChild(document.createTextNode('×'));
      remove.className = 'adminGalleryRemove';
      remove.setAttribute('href', '#');
      remove.setAttribute('data-id', id);

      remove.onclick = function () {
        Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_18__["removeImgByIdApi"])(id).then(function (removed) {
          if (removed) {
            document.querySelector('.image-' + id).remove();
            document.querySelector('option[value="' + id + '"]').remove();
            Object(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["messageType"].success, 'File Successfully Deleted');
          } else {
            Object(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["messageType"].error, _services_utilities__WEBPACK_IMPORTED_MODULE_19__["defaultErrorMsg"]);
          }
        });
      };

      img.onclick = function () {
        return Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_18__["getImageByIdApi"])(id).then(function (res) {
          if (res !== null && res !== void 0 && res.id) {
            Object(_helpers_showDefaultImageModal__WEBPACK_IMPORTED_MODULE_20__["default"])(res);
          } else {
            Object(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["messageType"].error, _services_utilities__WEBPACK_IMPORTED_MODULE_19__["defaultErrorMsg"]);
          }
        });
      };

      var p = document.createElement('p');
      p.classList.add('imgTitle');
      p.textContent = title;
      imgDiv.appendChild(p);
      img.setAttribute('src', src);
      imgDiv.appendChild(remove);
      imgDiv.appendChild(img);
      imagesHtml.appendChild(imgDiv);
    });
  }

  target.append(imagesHtml);
};

var InsertAlreadyUploadedImages = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(select, target) {
    var alreadyUploadedImages, uploadedImages;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            alreadyUploadedImages = select.querySelectorAll('option[selected]');
            uploadedImages = [];
            alreadyUploadedImages === null || alreadyUploadedImages === void 0 ? void 0 : alreadyUploadedImages.forEach(function (file) {
              return file.value && uploadedImages.push({
                id: Number(file.value),
                src: file.innerText
              });
            });

            if (!uploadedImages.length) {
              _context.next = 7;
              break;
            }

            _context.next = 6;
            return Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_18__["getUploadedImagesApi"])([uploadedImages.map(function (imgObj) {
              return imgObj.id;
            })]).then(function (res) {
              (res === null || res === void 0 ? void 0 : res.length) && res.forEach(function (img) {
                uploadedImages.filter(function (obj) {
                  if (obj.id === img.id) {
                    obj.title = img.title || '';
                  }
                });
              });
            });

          case 6:
            addImagesToGallery(uploadedImages, target);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function InsertAlreadyUploadedImages(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var UppyInit = function UppyInit(_ref2) {
  var _ref2$maxFileSize = _ref2.maxFileSize,
      maxFileSize = _ref2$maxFileSize === void 0 ? 2100000 : _ref2$maxFileSize,
      _ref2$maxNumberOfFile = _ref2.maxNumberOfFiles,
      maxNumberOfFiles = _ref2$maxNumberOfFile === void 0 ? 10 : _ref2$maxNumberOfFile,
      _ref2$minNumberOfFile = _ref2.minNumberOfFiles,
      minNumberOfFiles = _ref2$minNumberOfFile === void 0 ? 1 : _ref2$minNumberOfFile,
      _ref2$allowedFileType = _ref2.allowedFileTypes,
      allowedFileTypes = _ref2$allowedFileType === void 0 ? ['image/*'] : _ref2$allowedFileType,
      _ref2$uploadURL = _ref2.uploadURL,
      uploadURL = _ref2$uploadURL === void 0 ? '/galleries/files' : _ref2$uploadURL,
      _ref2$note = _ref2.note,
      note = _ref2$note === void 0 ? 'Images only, 1–10 files, up to 2 MB' : _ref2$note;
  var uppySelect = document.querySelector("select[data-gallery='uppy']");

  if (uppySelect) {
    var formGroupDiv = uppySelect.closest('.form-group');
    $(formGroupDiv).children().hide();
    var uploadedImagesDiv = $('<div class="adminGallery"></div>');
    $(formGroupDiv).append(uploadedImagesDiv);
    InsertAlreadyUploadedImages(uppySelect, uploadedImagesDiv);
    var up = _uppy_core__WEBPACK_IMPORTED_MODULE_11___default()({
      debug: true,
      autoProceed: false,
      allowMultipleUploads: false,
      restrictions: {
        maxFileSize: maxFileSize,
        maxNumberOfFiles: maxNumberOfFiles,
        minNumberOfFiles: minNumberOfFiles,
        allowedFileTypes: allowedFileTypes
      },
      meta: {
        isMain: false
      }
    }).use(_uppy_dashboard__WEBPACK_IMPORTED_MODULE_12___default.a, {
      trigger: '.UppyModalOpenerBtn',
      inline: true,
      target: formGroupDiv,
      replaceTargetContent: false,
      showProgressDetails: true,
      note: note,
      height: 470,
      thumbnailWidth: 280,
      showSelectedFiles: true,
      browserBackButtonClose: true
    }).use(_uppy_google_drive__WEBPACK_IMPORTED_MODULE_13___default.a, {
      target: _uppy_dashboard__WEBPACK_IMPORTED_MODULE_12___default.a,
      companionUrl: 'https://companion.uppy.io'
    }).use(_uppy_dropbox__WEBPACK_IMPORTED_MODULE_14___default.a, {
      target: _uppy_dashboard__WEBPACK_IMPORTED_MODULE_12___default.a,
      companionUrl: 'https://companion.uppy.io'
    }).use(_uppy_instagram__WEBPACK_IMPORTED_MODULE_15___default.a, {
      target: _uppy_dashboard__WEBPACK_IMPORTED_MODULE_12___default.a,
      companionUrl: 'https://companion.uppy.io'
    }).use(_uppy_webcam__WEBPACK_IMPORTED_MODULE_16___default.a, {
      target: _uppy_dashboard__WEBPACK_IMPORTED_MODULE_12___default.a,
      title: 'Camera'
    }).use(_uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_17___default.a, {
      endpoint: uploadURL,
      method: 'POST',
      formData: true,
      fieldName: 'files[]'
    });
    up.on('complete', function (uploads) {
      var uploadsArr = [];

      if (uploads.successful.length) {
        var optionsHtmlStr = '';
        uploads.successful.forEach(function (key) {
          var elemId = key.response.body.id;
          var downloadLink = key.response.body.download_link;
          optionsHtmlStr += "<option value=".concat(elemId, " selected=\"selected\">").concat(downloadLink, "</option>");
          uploadsArr.push({
            id: elemId,
            src: downloadLink
          });
          up.removeFile(key.id);
        });

        if (uploadsArr.length) {
          //Pushing hidden options to hidden uppy select to save uploaded files on submit.
          $(uppySelect).append(optionsHtmlStr);
          addImagesToGallery(uploadsArr, uploadedImagesDiv);
          Object(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_19__["messageType"].success, "File".concat(uploadsArr.length > 1 ? 's' : '', " Successfully Added"));
        }
      }
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (UppyInit);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/admin.js":
/*!****************************!*\
  !*** ./assets/js/admin.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/web.timers.js */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _style_admin_admin_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../style/admin/admin.scss */ "./assets/style/admin/admin.scss");
/* harmony import */ var _style_admin_admin_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_style_admin_admin_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_admin_global_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/admin/global.scss */ "./assets/style/admin/global.scss");
/* harmony import */ var _style_admin_global_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_admin_global_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Uppy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Uppy */ "./assets/js/Uppy.js");
/* harmony import */ var _services_utilities_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/utilities.js */ "./assets/js/services/utilities.js");
/* harmony import */ var _tabs_content_content_Article__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs/content/content.Article */ "./assets/js/tabs/content/content.Article.js");







__webpack_require__(/*! ../style/admin/sortable.scss */ "./assets/style/admin/sortable.scss");

__webpack_require__(/*! ./ui-sortable */ "./assets/js/ui-sortable.js");

console.log(document.querySelector('select[article-image-upload]'));
/* todo list slider */

$(document).ready(function () {
  /* Load Uppy if url action is edit and uppy select exist
      * */
  if (document.querySelector("select[data-gallery='uppy']")) Object(_Uppy__WEBPACK_IMPORTED_MODULE_3__["default"])({});
  /*
  * Handle Article tab script
  * !IMPORTANT: This called only on edit action for now.
  * */

  console.log(document.querySelector('select[article-image-upload]'));
  if (document.querySelector('select[article-image-upload]')) Object(_tabs_content_content_Article__WEBPACK_IMPORTED_MODULE_5__["default"])();
}); //todo
// $(document).on('change', '.custom-file-input', function (target) {
//     var input = $(this),
//         numFiles = input.get(0).files ? input.get(0).files.length : 1,
//         label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//     input.trigger('fileselect', [label, numFiles]);
//     /**/
//     target = input.get(0).files[0] ? input.get(0).files[0].name : ' ';
//
//     target = target.length > 15 ? `${target.substring(0, 15)}...` : target;
//
//     $(input).parent().find('label').html(target);
// });

/* todo odessi design */
// todo check with Tiko if we need below code
// $('.article_widget.form-group').removeClass('form-group');
// $('.address_widget.form-group').removeClass('form-group');
// $('.gallery_widget.form-group').removeClass('form-group');
// $('.business_widget.form-group').removeClass('form-group');
//

setTimeout(function () {
  if (document.querySelector('.nax')) {
    console.log('dsada');
    document.querySelector('.nax').closest('.form-group').style.display = 'block';
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/api/admin.api.js":
/*!************************************!*\
  !*** ./assets/js/api/admin.api.js ***!
  \************************************/
/*! exports provided: getImageByIdApi, removeImgByIdApi, updateImgDataApi, getUploadedImagesApi, addNewMessageApi, uploadImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getImageByIdApi", function() { return getImageByIdApi; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeImgByIdApi", function() { return removeImgByIdApi; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateImgDataApi", function() { return updateImgDataApi; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUploadedImagesApi", function() { return getUploadedImagesApi; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNewMessageApi", function() { return addNewMessageApi; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadImage", function() { return uploadImage; });
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string.js */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_Http_Client__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/Http.Client */ "./assets/js/services/Http.Client.js");
/* harmony import */ var _services_utilities__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/utilities */ "./assets/js/services/utilities.js");






/*
* Main Menu start
* */

var getImageByIdApi = function getImageByIdApi(id) {
  return Object(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["Get"])("/image/info/".concat(id), {}).then(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["HandleResponseState"]);
};
var removeImgByIdApi = function removeImgByIdApi(id) {
  return Object(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["Delete"])("/galleries/".concat(id)).then(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["HandleResponseState"]);
};
var updateImgDataApi = function updateImgDataApi(data) {
  return fetch('/galleries/images', {
    method: 'POST',
    body: JSON.stringify(data).toString()
  }).then(function (res) {
    return res.ok && res.status === 200;
  });
};
var getUploadedImagesApi = function getUploadedImagesApi(data) {
  return Object(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["Post"])('/uploaded/images', {}, data, true).then(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["HandleResponseState"]);
};
/*
* Main Menu end
* */

/*
* Clients Personal start
* */

var addNewMessageApi = function addNewMessageApi(data) {
  return Object(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["Post"])('/message/new', {}, data).then(function (res) {
    return Object(_services_utilities__WEBPACK_IMPORTED_MODULE_5__["handleResponse"])(res, undefined, function () {
      return !!res.success;
    }, false, 'Your message successfully saved.', _services_utilities__WEBPACK_IMPORTED_MODULE_5__["defaultErrorMsg"]);
  });
};
/*
* Clients Personal end
* */

var uploadImage = function uploadImage(file) {
  var formData = new FormData();
  formData.append('files[]', file);
  return Object(_services_Http_Client__WEBPACK_IMPORTED_MODULE_4__["Post"])('/galleries/files', {}, formData, true);
};

/***/ }),

/***/ "./assets/js/helpers/modalComponent.js":
/*!*********************************************!*\
  !*** ./assets/js/helpers/modalComponent.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__);


var modalComponent = function modalComponent() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var handleOkClick = arguments.length > 2 ? arguments[2] : undefined;
  var modal = $("\n        <div class=\"modalComponent\">\n            <div class=\"modalContent\">\n                <p class=\"title\">".concat(title, "</p>\n                <button type=\"button\" class=\"close cancel\">x</button>\n                <div class=\"contentDiv\">\n                    ").concat(content, "\n                </div>\n                <div class=\"buttons\">\n                    <button type=\"button\" class=\"btn btn-primary cancel\">Cancel</button>\n                    <button type=\"button\" class=\"btn btn-success\">Ok</button>\n                </div>\n            </div> \n        </div>\n    ")).click(function (e) {
    if ($(e.target).hasClass('btn-success') && handleOkClick) {
      handleOkClick().then(function (done) {
        return done && modal.remove();
      });
    } else if ($(e.target).hasClass('cancel') || e.target === modal[0]) {
      //Close modal on cancel or outside click
      modal.remove();
    }
  });
  var modalExist = document.querySelector('.modalComponent');
  modalExist && modalExist.remove();
  $('body').append(modal);
};

/* harmony default export */ __webpack_exports__["default"] = (modalComponent);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/helpers/showDefaultImageModal.js":
/*!****************************************************!*\
  !*** ./assets/js/helpers/showDefaultImageModal.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_string_trim_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.string.trim.js */ "./node_modules/core-js/modules/es.string.trim.js");
/* harmony import */ var core_js_modules_es_string_trim_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_trim_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _api_admin_api__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../api/admin.api */ "./assets/js/api/admin.api.js");
/* harmony import */ var _modalComponent__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./modalComponent */ "./assets/js/helpers/modalComponent.js");
/* harmony import */ var _services_utilities__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/utilities */ "./assets/js/services/utilities.js");













var showDefaultImageModal = function showDefaultImageModal(res) {
  var id = res.id,
      title = res.title,
      url_link = res.url_link,
      altText = res.altText,
      description = res.description,
      is_enabled = res.is_enabled,
      downloadLink = res.downloadLink;
  var modalContent = "\n        <div class=\"galleryModal\">\n            <div>\n                <div class=\"galleryAdminGrid\">\n                    <div>\n                        <label for=\"\">Title</label>\n                        <input type=\"text\" value=\"".concat(title || '', "\" name=\"modal-item-title\">\n                    </div>\n                    <div>\n                        <label for=\"\">Link</label>\n                        <input type=\"text\" value=\"").concat(url_link || '', "\" name=\"modal-item-link\">\n                    </div>\n                    <div>\n                        <label for=\"\">Alt text</label>\n                        <input type=\"text\" value=\"").concat(altText || '', "\" name=\"modal-item-alt-text\">\n                    </div>\n                    <div>\n                        <input type=\"checkbox\" id=\"checkbox\" name=\"modal-item-isEnabled\" ").concat(is_enabled ? 'checked' : '', ">\n                        <label for=\"checkbox\">Is enabled</label>\n                    </div>\n                </div>\n                <div>\n                    <label for=\"\">Description</label>\n                    <textarea name=\"modal-item-description\">").concat(description || '', "</textarea>\n                </div>\n                <img src=").concat(downloadLink || '', " alt=\"\">\n            </div>\n        </div>\n        ");

  var handleOkClick = function handleOkClick() {
    return new Promise(function (resolve) {
      var data = {
        'modal-item-id': id
      };
      var inputs = document.querySelectorAll('.modalContent input, .modalContent textarea');
      inputs.forEach(function (key) {
        var _key$value;

        if ((_key$value = key.value) !== null && _key$value !== void 0 && _key$value.trim()) {
          data[key.name] = key.name !== 'modal-item-isEnabled' ? key.value : key.checked;
        } else {
          data[key.name] = '';
        }
      });
      Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_9__["updateImgDataApi"])(data).then(function (updated) {
        if (updated) {
          Object(_services_utilities__WEBPACK_IMPORTED_MODULE_11__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_11__["messageType"].success, 'File Successfully Updated');
          resolve(true);
        } else {
          Object(_services_utilities__WEBPACK_IMPORTED_MODULE_11__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_11__["messageType"].error, _services_utilities__WEBPACK_IMPORTED_MODULE_11__["defaultErrorMsg"]);
        }
      });
    });
  };

  Object(_modalComponent__WEBPACK_IMPORTED_MODULE_10__["default"])(modalContent, 'Gallery item', handleOkClick);
};

/* harmony default export */ __webpack_exports__["default"] = (showDefaultImageModal);

/***/ }),

/***/ "./assets/js/services/Http.Client.js":
/*!*******************************************!*\
  !*** ./assets/js/services/Http.Client.js ***!
  \*******************************************/
/*! exports provided: HandleResponseState, Post, Delete, Get, Put */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HandleResponseState", function() { return HandleResponseState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Post", function() { return Post; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Delete", function() { return Delete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Get", function() { return Get; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Put", function() { return Put; });
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_promise_finally_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.promise.finally.js */ "./node_modules/core-js/modules/es.promise.finally.js");
/* harmony import */ var core_js_modules_es_promise_finally_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_finally_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string.js */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! whatwg-fetch */ "./node_modules/whatwg-fetch/fetch.js");
/* harmony import */ var _utilities__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utilities */ "./assets/js/services/utilities.js");







var HandleResponseState = function HandleResponseState(result) {
  return result ? result : function () {
    Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["showMessage"])(_utilities__WEBPACK_IMPORTED_MODULE_6__["messageType"].error, _utilities__WEBPACK_IMPORTED_MODULE_6__["defaultErrorMsg"]);
    return false;
  };
};
var Post = function Post(url) {
  var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var body = arguments.length > 2 ? arguments[2] : undefined;
  var plainBody = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])();

  if (!headers.Accept) {
    headers.Accept = "application/json, */*";
  }

  return fetch(url, {
    method: "POST",
    headers: headers,
    body: plainBody ? body : JSON.stringify(body).toString()
  }).then(function (response) {
    if (!response.ok || response.status === 401) {
      return false;
    }

    return response.json();
  })["finally"](function () {
    return Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])(false);
  })["catch"](function (data) {
    console.log("Error", data);
  });
};
var Delete = function Delete(url) {
  var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])();

  if (!headers.Accept) {
    headers.Accept = "application/json, */*";
  }

  return fetch(url, {
    method: "DELETE",
    headers: headers
  }).then(function (response) {
    return !(!response.ok || response.status === 401);
  })["finally"](function () {
    return Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])(false);
  })["catch"](function (data) {
    console.log("Error", data);
  });
};
var Get = function Get(url) {
  var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])();

  if (!headers.Accept) {
    headers.Accept = "application/json, */*";
  }

  return fetch(url, {
    method: "GET",
    headers: headers
  }).then(function (response) {
    if (!response.ok || response.status === 401) {
      return false;
    }

    return response.json();
  })["finally"](function () {
    return Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])(false);
  })["catch"](function (data) {
    console.log("Error", data);
  });
};
var Put = function Put(url) {
  var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var body = arguments.length > 2 ? arguments[2] : undefined;
  Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])();

  if (!headers["Content-Type"]) {
    headers["Content-Type"] = "application/json";
  }

  return fetch(url, {
    method: "PUT",
    headers: headers,
    body: JSON.stringify(body).toString()
  }).then(function (response) {
    if (!response.ok || response.status === 401) {
      return false;
    }

    return response.json();
  })["finally"](function () {
    return Object(_utilities__WEBPACK_IMPORTED_MODULE_6__["isLoading"])(false);
  })["catch"](function (data) {
    console.log("Error", data);
  });
};

/***/ }),

/***/ "./assets/js/services/utilities.js":
/*!*****************************************!*\
  !*** ./assets/js/services/utilities.js ***!
  \*****************************************/
/*! exports provided: messageType, defaultErrorMsg, uniqueId, showMessage, stopForSeconds, getErrorsFromResponse, handleResponse, isLoading, linkAction, getLinkAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "messageType", function() { return messageType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultErrorMsg", function() { return defaultErrorMsg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uniqueId", function() { return uniqueId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showMessage", function() { return showMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stopForSeconds", function() { return stopForSeconds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getErrorsFromResponse", function() { return getErrorsFromResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleResponse", function() { return handleResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoading", function() { return isLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "linkAction", function() { return linkAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLinkAction", function() { return getLinkAction; });
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_date_now_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.date.now.js */ "./node_modules/core-js/modules/es.date.now.js");
/* harmony import */ var core_js_modules_es_date_now_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_now_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.is-array.js */ "./node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_object_values_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.object.values.js */ "./node_modules/core-js/modules/es.object.values.js");
/* harmony import */ var core_js_modules_es_object_values_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_values_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_web_url_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/web.url.js */ "./node_modules/core-js/modules/web.url.js");
/* harmony import */ var core_js_modules_web_url_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_url_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.search.js */ "./node_modules/core-js/modules/es.string.search.js");
/* harmony import */ var core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_search_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _style_admin_notification_scss__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../style/admin/notification.scss */ "./assets/style/admin/notification.scss");
/* harmony import */ var _style_admin_notification_scss__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_style_admin_notification_scss__WEBPACK_IMPORTED_MODULE_15__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

















var messageType = {
  error: 'error',
  success: 'success',
  warning: 'warning'
};
var defaultErrorMsg = 'Something went wrong.';
var uniqueId = function uniqueId() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
};
var showMessage = function showMessage() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : messageType.warning;
  var message = arguments.length > 1 ? arguments[1] : undefined;
  var defaultMessagesDiv = $('#defaultNotification');
  var uniqId = uniqueId();
  var messageDiv = "<div class=\"".concat(type, "\" id=\"").concat(uniqId, "\"><p>").concat(message, "</p></div>");

  if (defaultMessagesDiv.length) {
    defaultMessagesDiv.append(messageDiv);
  } else {
    $('body').append("<div id=\"defaultNotification\">\n            ".concat(messageDiv, "\n         </div>"));
  }

  $("#".concat(uniqId)).fadeOut(5000, 'swing', function () {
    $(this).remove();
    var defaultMessagesDiv = $('#defaultNotification');

    if (!defaultMessagesDiv.children().length) {
      defaultMessagesDiv.remove();
    }
  });
};
/*
* For debugging purposes
* */

var stopForSeconds = function stopForSeconds() {
  var second = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 2;
  var date = Date.now();
  var currentDate = null;

  do {
    currentDate = Date.now();
  } while (currentDate - date < second * 1000);
};
/*
* Http start
* */

var getErrorsFromResponse = function getErrorsFromResponse(responseData) {
  if (responseData.message) {
    if (_typeof(responseData.message) === "object" && !Array.isArray(responseData.message)) return Object.values(responseData.message);
    return null;
  }

  return null;
};
var handleResponse = function handleResponse(result) {
  var prep = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
  var success = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
  var error = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
  var successMessage = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : undefined;
  var errorMessage = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : undefined;
  if (prep) prep();

  if (result) {
    if (result.success) {
      if (successMessage) showMessage(messageType.success, successMessage);
      if (success) return success(result.data);
      return result.data;
    } else {
      showMessage(messageType.error, getErrorsFromResponse(result));
      return false;
    }
  } else {
    if (errorMessage) showMessage(messageType.error, errorMessage);
    if (error) return error();
    return false;
  }
};
/*
* Http end
* */

var isLoading = function isLoading() {
  var loading = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
  var exists = document.querySelector('.globalLoading');

  if (loading && !exists) {
    var loader = $('<div class="globalLoading"></div>');
    $('body').append(loader);
  }

  if (!loading && exists) {
    var _$;

    (_$ = $('.globalLoading')) === null || _$ === void 0 ? void 0 : _$.remove();
  }
}; //Get action key from url

var linkAction = {
  edit: 'edit',
  show: 'detail',
  main: 'index'
};
var getLinkAction = function getLinkAction() {
  var urlParams = new URLSearchParams(window.location.search);
  var action = urlParams.get('crudAction');
  return action || false;
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/tabs/content/content.Article.js":
/*!***************************************************!*\
  !*** ./assets/js/tabs/content/content.Article.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.find.js */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _services_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/utilities */ "./assets/js/services/utilities.js");
/* harmony import */ var _api_admin_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../api/admin.api */ "./assets/js/api/admin.api.js");
/* harmony import */ var _helpers_showDefaultImageModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/showDefaultImageModal */ "./assets/js/helpers/showDefaultImageModal.js");






var handleArticleTab = function handleArticleTab() {
  var hiddenUploadedImageSelect = document.querySelector('select[article-image-upload]');
  var uploadedOption = hiddenUploadedImageSelect.querySelector('option[selected]');
  var formGroup = $(hiddenUploadedImageSelect).closest('div.form-group');
  var input = $('<input type="file" id="imageUploadInput" class="form-control"/>').on('change', function (e) {
    var selectedFile = e.target.files[0];

    if (selectedFile) {
      var _uploadField$find, _uploadField$find2;

      (_uploadField$find = uploadField.find('img')) === null || _uploadField$find === void 0 ? void 0 : _uploadField$find.remove();
      (_uploadField$find2 = uploadField.find('span.remove')) === null || _uploadField$find2 === void 0 ? void 0 : _uploadField$find2.remove();
      Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_3__["uploadImage"])(selectedFile).then(function (res) {
        if (res !== null && res !== void 0 && res.id) {
          addUploadedImage(res);
        } else {
          Object(_services_utilities__WEBPACK_IMPORTED_MODULE_2__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_2__["messageType"].error, _services_utilities__WEBPACK_IMPORTED_MODULE_2__["defaultErrorMsg"]);
        }
      });
    }
  });
  var uploadField = $("\n        <div class=\"form-group articleEditImageUploadDiv\">\n            <label for=\"imageUploadInput\">Image</label>\n            <div class=\"form-widget\"></div>\n        </div>");
  uploadField.find('div.form-widget').append(input);
  uploadField.insertAfter(formGroup);

  var addUploadedImage = function addUploadedImage(image) {
    var download_link = image.download_link,
        id = image.id;
    var img = $("<img src=\"".concat(download_link, "\" alt=\"\" id=\"").concat(id, "\"/>")).on('click', function () {
      Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_3__["getImageByIdApi"])(id).then(function (response) {
        if (response !== null && response !== void 0 && response.id) {
          Object(_helpers_showDefaultImageModal__WEBPACK_IMPORTED_MODULE_4__["default"])(response);
        } else {
          Object(_services_utilities__WEBPACK_IMPORTED_MODULE_2__["showMessage"])(_services_utilities__WEBPACK_IMPORTED_MODULE_2__["messageType"].error, _services_utilities__WEBPACK_IMPORTED_MODULE_2__["defaultErrorMsg"]);
        }
      });
    });
    var remove = $("<span class=\"remove\">\xD7</span>").on('click', function () {
      Object(_api_admin_api__WEBPACK_IMPORTED_MODULE_3__["removeImgByIdApi"])(id).then(function (removed) {
        if (removed) {
          uploadField.find('img').remove();
          uploadField.find('span.remove').remove();
          hiddenUploadedImageSelect.querySelector('option[selected]').remove();
          $(input).val('');
        }
      });
    });
    uploadField.append(img);
    uploadField.append(remove);
    var uploadedOption = hiddenUploadedImageSelect.querySelector('option[selected]');

    if (uploadedOption) {
      uploadedOption.value = id;
      uploadedOption.textContent = download_link;
    } else {
      $(hiddenUploadedImageSelect).append("<option selected=\"selected\" value=\"".concat(id, "\">").concat(download_link, "</option>"));
    }
  };

  if (uploadedOption) {
    addUploadedImage({
      id: uploadedOption.value,
      'download_link': uploadedOption.textContent
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (handleArticleTab);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/ui-sortable.js":
/*!**********************************!*\
  !*** ./assets/js/ui-sortable.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.parse-int.js */ "./node_modules/core-js/modules/es.parse-int.js");
/* harmony import */ var core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.index-of.js */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery_ui_sortable_npm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery-ui-sortable-npm */ "./node_modules/jquery-ui-sortable-npm/jquery-ui-sortable.js");
/* harmony import */ var jquery_ui_sortable_npm__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery_ui_sortable_npm__WEBPACK_IMPORTED_MODULE_2__);



var easyadminDragndropSort = {
  initDraggableEntityRows: function initDraggableEntityRows() {
    if (document.body.classList.contains('ea') && document.body.classList.contains('index')) {
      if (!Array.prototype.last) {
        Array.prototype.last = function () {
          return this[this.length - 1];
        };
      }

      var content = document.getElementById("main");
      var table = content.getElementsByClassName("table")[0];

      if (table && table.classList.contains('allow_sortable')) {
        var dataCount = parseInt(table.getAttribute('data-count'));

        if (dataCount) {
          var entityClass = table.getAttribute('data-class-name');
          var thead = table.getElementsByTagName("thead")[0];
          var theadRows = thead.getElementsByTagName("tr")[0];
          var tbody = table.getElementsByTagName("tbody")[0];
          var tableRows = tbody.getElementsByTagName("tr");
          var dragSrcEl = null; // the object being drug

          var startPosition = null; // the index of the row element (0 through whatever)

          var endPosition = null; // the index of the row element being dropped on (0 through whatever)

          var parent; // the parent element of the dragged item

          var entityId; // the id (key) of the entity
          // draw sortable icon

          for (var row in tableRows) {
            if (tableRows.hasOwnProperty(row)) {
              var td = '<td draggable="true">' + '<span><i class="fas fa-arrows-alt"></i></span>' + '</td>';
              tableRows[row].insertAdjacentHTML('beforeend', td);
            }
          }

          var th = '<th></th>';
          theadRows.insertAdjacentHTML('beforeend', th);
          $(tbody).sortable({
            handle: 'td[draggable="true"]',
            start: function handleDragStart(e, ui) {
              dragSrcEl = ui.item[0];
              entityId = dragSrcEl.getAttribute('data-id');
              parent = dragSrcEl.parentNode;
              startPosition = Array.prototype.indexOf.call(parent.children, dragSrcEl);
            },
            stop: function stop(e, ui) {
              endPosition = Array.prototype.indexOf.call(parent.children, ui.item[0]);

              if (startPosition !== endPosition) {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', encodeURI('/sort/' + entityClass + '/' + entityId + '/' + endPosition));

                xhr.onload = function () {
                  if (xhr.status !== 200) {
                    alert("An error occurred while sorting. Please refresh the page and try again.");
                  }
                };

                xhr.send();
              }
            }
          });
          $(tbody).disableSelection();
        }
      }
    }
  },

  /**
   * Primary Admin initialization method.
   * @returns {boolean}
   */
  init: function init() {
    this.initDraggableEntityRows();
    return true;
  }
};
$(document).ready(function () {
  console.log('dsadasdasd');
  easyadminDragndropSort.init();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/style/admin/admin.scss":
/*!***************************************!*\
  !*** ./assets/style/admin/admin.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/style/admin/global.scss":
/*!****************************************!*\
  !*** ./assets/style/admin/global.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/style/admin/notification.scss":
/*!**********************************************!*\
  !*** ./assets/style/admin/notification.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/style/admin/sortable.scss":
/*!******************************************!*\
  !*** ./assets/style/admin/sortable.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/js/admin.js","runtime","vendors~admin_custom~base",0,"vendors~admin_custom"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvVXBweS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYWRtaW4uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwaS9hZG1pbi5hcGkuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2hlbHBlcnMvbW9kYWxDb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2hlbHBlcnMvc2hvd0RlZmF1bHRJbWFnZU1vZGFsLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9zZXJ2aWNlcy9IdHRwLkNsaWVudC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvc2VydmljZXMvdXRpbGl0aWVzLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy90YWJzL2NvbnRlbnQvY29udGVudC5BcnRpY2xlLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy91aS1zb3J0YWJsZS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvc3R5bGUvYWRtaW4vYWRtaW4uc2NzcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvc3R5bGUvYWRtaW4vZ2xvYmFsLnNjc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3N0eWxlL2FkbWluL25vdGlmaWNhdGlvbi5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9zdHlsZS9hZG1pbi9zb3J0YWJsZS5zY3NzIl0sIm5hbWVzIjpbImFkZEltYWdlc1RvR2FsbGVyeSIsImltYWdlc0FyciIsInRhcmdldCIsImltYWdlc0h0bWwiLCJkb2N1bWVudCIsImNyZWF0ZURvY3VtZW50RnJhZ21lbnQiLCJsZW5ndGgiLCJmb3JFYWNoIiwiaW1hZ2UiLCJzcmMiLCJpZCIsInRpdGxlIiwiaW1nRGl2IiwiY3JlYXRlRWxlbWVudCIsInJlbW92ZSIsImltZyIsImNsYXNzTmFtZSIsImFwcGVuZENoaWxkIiwiY3JlYXRlVGV4dE5vZGUiLCJzZXRBdHRyaWJ1dGUiLCJvbmNsaWNrIiwicmVtb3ZlSW1nQnlJZEFwaSIsInRoZW4iLCJyZW1vdmVkIiwicXVlcnlTZWxlY3RvciIsInNob3dNZXNzYWdlIiwibWVzc2FnZVR5cGUiLCJzdWNjZXNzIiwiZXJyb3IiLCJkZWZhdWx0RXJyb3JNc2ciLCJnZXRJbWFnZUJ5SWRBcGkiLCJyZXMiLCJzaG93RGVmYXVsdEltYWdlTW9kYWwiLCJwIiwiY2xhc3NMaXN0IiwiYWRkIiwidGV4dENvbnRlbnQiLCJhcHBlbmQiLCJJbnNlcnRBbHJlYWR5VXBsb2FkZWRJbWFnZXMiLCJzZWxlY3QiLCJhbHJlYWR5VXBsb2FkZWRJbWFnZXMiLCJxdWVyeVNlbGVjdG9yQWxsIiwidXBsb2FkZWRJbWFnZXMiLCJmaWxlIiwidmFsdWUiLCJwdXNoIiwiTnVtYmVyIiwiaW5uZXJUZXh0IiwiZ2V0VXBsb2FkZWRJbWFnZXNBcGkiLCJtYXAiLCJpbWdPYmoiLCJmaWx0ZXIiLCJvYmoiLCJVcHB5SW5pdCIsIm1heEZpbGVTaXplIiwibWF4TnVtYmVyT2ZGaWxlcyIsIm1pbk51bWJlck9mRmlsZXMiLCJhbGxvd2VkRmlsZVR5cGVzIiwidXBsb2FkVVJMIiwibm90ZSIsInVwcHlTZWxlY3QiLCJmb3JtR3JvdXBEaXYiLCJjbG9zZXN0IiwiJCIsImNoaWxkcmVuIiwiaGlkZSIsInVwbG9hZGVkSW1hZ2VzRGl2IiwidXAiLCJVcHB5IiwiZGVidWciLCJhdXRvUHJvY2VlZCIsImFsbG93TXVsdGlwbGVVcGxvYWRzIiwicmVzdHJpY3Rpb25zIiwibWV0YSIsImlzTWFpbiIsInVzZSIsIkRhc2hib2FyZCIsInRyaWdnZXIiLCJpbmxpbmUiLCJyZXBsYWNlVGFyZ2V0Q29udGVudCIsInNob3dQcm9ncmVzc0RldGFpbHMiLCJoZWlnaHQiLCJ0aHVtYm5haWxXaWR0aCIsInNob3dTZWxlY3RlZEZpbGVzIiwiYnJvd3NlckJhY2tCdXR0b25DbG9zZSIsIkdvb2dsZURyaXZlIiwiY29tcGFuaW9uVXJsIiwiRHJvcGJveCIsIkluc3RhZ3JhbSIsIldlYmNhbSIsIlhIUlVwbG9hZCIsImVuZHBvaW50IiwibWV0aG9kIiwiZm9ybURhdGEiLCJmaWVsZE5hbWUiLCJvbiIsInVwbG9hZHMiLCJ1cGxvYWRzQXJyIiwic3VjY2Vzc2Z1bCIsIm9wdGlvbnNIdG1sU3RyIiwia2V5IiwiZWxlbUlkIiwicmVzcG9uc2UiLCJib2R5IiwiZG93bmxvYWRMaW5rIiwiZG93bmxvYWRfbGluayIsInJlbW92ZUZpbGUiLCJyZXF1aXJlIiwiY29uc29sZSIsImxvZyIsInJlYWR5IiwiaGFuZGxlQXJ0aWNsZVRhYiIsInNldFRpbWVvdXQiLCJzdHlsZSIsImRpc3BsYXkiLCJHZXQiLCJIYW5kbGVSZXNwb25zZVN0YXRlIiwiRGVsZXRlIiwidXBkYXRlSW1nRGF0YUFwaSIsImRhdGEiLCJmZXRjaCIsIkpTT04iLCJzdHJpbmdpZnkiLCJ0b1N0cmluZyIsIm9rIiwic3RhdHVzIiwiUG9zdCIsImFkZE5ld01lc3NhZ2VBcGkiLCJoYW5kbGVSZXNwb25zZSIsInVuZGVmaW5lZCIsInVwbG9hZEltYWdlIiwiRm9ybURhdGEiLCJtb2RhbENvbXBvbmVudCIsImNvbnRlbnQiLCJoYW5kbGVPa0NsaWNrIiwibW9kYWwiLCJjbGljayIsImUiLCJoYXNDbGFzcyIsImRvbmUiLCJtb2RhbEV4aXN0IiwidXJsX2xpbmsiLCJhbHRUZXh0IiwiZGVzY3JpcHRpb24iLCJpc19lbmFibGVkIiwibW9kYWxDb250ZW50IiwiUHJvbWlzZSIsInJlc29sdmUiLCJpbnB1dHMiLCJ0cmltIiwibmFtZSIsImNoZWNrZWQiLCJ1cGRhdGVkIiwicmVzdWx0IiwidXJsIiwiaGVhZGVycyIsInBsYWluQm9keSIsImlzTG9hZGluZyIsIkFjY2VwdCIsImpzb24iLCJQdXQiLCJ3YXJuaW5nIiwidW5pcXVlSWQiLCJ0ZXh0IiwicG9zc2libGUiLCJpIiwiY2hhckF0IiwiTWF0aCIsImZsb29yIiwicmFuZG9tIiwidHlwZSIsIm1lc3NhZ2UiLCJkZWZhdWx0TWVzc2FnZXNEaXYiLCJ1bmlxSWQiLCJtZXNzYWdlRGl2IiwiZmFkZU91dCIsInN0b3BGb3JTZWNvbmRzIiwic2Vjb25kIiwiZGF0ZSIsIkRhdGUiLCJub3ciLCJjdXJyZW50RGF0ZSIsImdldEVycm9yc0Zyb21SZXNwb25zZSIsInJlc3BvbnNlRGF0YSIsIkFycmF5IiwiaXNBcnJheSIsIk9iamVjdCIsInZhbHVlcyIsInByZXAiLCJzdWNjZXNzTWVzc2FnZSIsImVycm9yTWVzc2FnZSIsImxvYWRpbmciLCJleGlzdHMiLCJsb2FkZXIiLCJsaW5rQWN0aW9uIiwiZWRpdCIsInNob3ciLCJtYWluIiwiZ2V0TGlua0FjdGlvbiIsInVybFBhcmFtcyIsIlVSTFNlYXJjaFBhcmFtcyIsIndpbmRvdyIsImxvY2F0aW9uIiwic2VhcmNoIiwiYWN0aW9uIiwiZ2V0IiwiaGlkZGVuVXBsb2FkZWRJbWFnZVNlbGVjdCIsInVwbG9hZGVkT3B0aW9uIiwiZm9ybUdyb3VwIiwiaW5wdXQiLCJzZWxlY3RlZEZpbGUiLCJmaWxlcyIsInVwbG9hZEZpZWxkIiwiZmluZCIsImFkZFVwbG9hZGVkSW1hZ2UiLCJpbnNlcnRBZnRlciIsInZhbCIsImVhc3lhZG1pbkRyYWduZHJvcFNvcnQiLCJpbml0RHJhZ2dhYmxlRW50aXR5Um93cyIsImNvbnRhaW5zIiwicHJvdG90eXBlIiwibGFzdCIsImdldEVsZW1lbnRCeUlkIiwidGFibGUiLCJnZXRFbGVtZW50c0J5Q2xhc3NOYW1lIiwiZGF0YUNvdW50IiwicGFyc2VJbnQiLCJnZXRBdHRyaWJ1dGUiLCJlbnRpdHlDbGFzcyIsInRoZWFkIiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJ0aGVhZFJvd3MiLCJ0Ym9keSIsInRhYmxlUm93cyIsImRyYWdTcmNFbCIsInN0YXJ0UG9zaXRpb24iLCJlbmRQb3NpdGlvbiIsInBhcmVudCIsImVudGl0eUlkIiwicm93IiwiaGFzT3duUHJvcGVydHkiLCJ0ZCIsImluc2VydEFkamFjZW50SFRNTCIsInRoIiwic29ydGFibGUiLCJoYW5kbGUiLCJzdGFydCIsImhhbmRsZURyYWdTdGFydCIsInVpIiwiaXRlbSIsInBhcmVudE5vZGUiLCJpbmRleE9mIiwiY2FsbCIsInN0b3AiLCJ4aHIiLCJYTUxIdHRwUmVxdWVzdCIsIm9wZW4iLCJlbmNvZGVVUkkiLCJvbmxvYWQiLCJhbGVydCIsInNlbmQiLCJkaXNhYmxlU2VsZWN0aW9uIiwiaW5pdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNQSxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFNBQUQsRUFBWUMsTUFBWixFQUF1QjtBQUM5QyxNQUFJQyxVQUFVLEdBQUdDLFFBQVEsQ0FBQ0Msc0JBQVQsRUFBakI7O0FBQ0EsTUFBSUosU0FBSixhQUFJQSxTQUFKLGVBQUlBLFNBQVMsQ0FBRUssTUFBZixFQUF1QjtBQUNuQkwsYUFBUyxDQUFDTSxPQUFWLENBQWtCLFVBQUFDLEtBQUssRUFBSTtBQUN2QixVQUFPQyxHQUFQLEdBQXlCRCxLQUF6QixDQUFPQyxHQUFQO0FBQUEsVUFBWUMsRUFBWixHQUF5QkYsS0FBekIsQ0FBWUUsRUFBWjtBQUFBLFVBQWdCQyxLQUFoQixHQUF5QkgsS0FBekIsQ0FBZ0JHLEtBQWhCO0FBQ0EsVUFBTUMsTUFBTSxHQUFHUixRQUFRLENBQUNTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtBQUNBLFVBQU1DLE1BQU0sR0FBR1YsUUFBUSxDQUFDUyxhQUFULENBQXVCLEdBQXZCLENBQWY7QUFDQSxVQUFNRSxHQUFHLEdBQUdYLFFBQVEsQ0FBQ1MsYUFBVCxDQUF1QixLQUF2QixDQUFaO0FBRUFELFlBQU0sQ0FBQ0ksU0FBUCxHQUFtQixXQUFXTixFQUE5QjtBQUVBSSxZQUFNLENBQUNHLFdBQVAsQ0FBbUJiLFFBQVEsQ0FBQ2MsY0FBVCxDQUF3QixHQUF4QixDQUFuQjtBQUNBSixZQUFNLENBQUNFLFNBQVAsR0FBbUIsb0JBQW5CO0FBQ0FGLFlBQU0sQ0FBQ0ssWUFBUCxDQUFvQixNQUFwQixFQUE0QixHQUE1QjtBQUNBTCxZQUFNLENBQUNLLFlBQVAsQ0FBb0IsU0FBcEIsRUFBK0JULEVBQS9COztBQUVBSSxZQUFNLENBQUNNLE9BQVAsR0FBaUIsWUFBTTtBQUNuQkMsZ0ZBQWdCLENBQUNYLEVBQUQsQ0FBaEIsQ0FBcUJZLElBQXJCLENBQTBCLFVBQUFDLE9BQU8sRUFBSTtBQUNqQyxjQUFJQSxPQUFKLEVBQWE7QUFDVG5CLG9CQUFRLENBQUNvQixhQUFULENBQXVCLFlBQVlkLEVBQW5DLEVBQXVDSSxNQUF2QztBQUNBVixvQkFBUSxDQUFDb0IsYUFBVCxDQUF1QixtQkFBbUJkLEVBQW5CLEdBQXdCLElBQS9DLEVBQXFESSxNQUFyRDtBQUVBVyxvRkFBVyxDQUFDQyxnRUFBVyxDQUFDQyxPQUFiLEVBQXNCLDJCQUF0QixDQUFYO0FBQ0gsV0FMRCxNQUtPO0FBQ0hGLG9GQUFXLENBQUNDLGdFQUFXLENBQUNFLEtBQWIsRUFBb0JDLG9FQUFwQixDQUFYO0FBQ0g7QUFDSixTQVREO0FBVUgsT0FYRDs7QUFhQWQsU0FBRyxDQUFDSyxPQUFKLEdBQWM7QUFBQSxlQUFNVSx1RUFBZSxDQUFDcEIsRUFBRCxDQUFmLENBQW9CWSxJQUFwQixDQUF5QixVQUFBUyxHQUFHLEVBQUk7QUFDaEQsY0FBSUEsR0FBSixhQUFJQSxHQUFKLGVBQUlBLEdBQUcsQ0FBRXJCLEVBQVQsRUFBYTtBQUNUc0IsMkZBQXFCLENBQUNELEdBQUQsQ0FBckI7QUFDSCxXQUZELE1BRU87QUFDSE4sb0ZBQVcsQ0FBQ0MsZ0VBQVcsQ0FBQ0UsS0FBYixFQUFvQkMsb0VBQXBCLENBQVg7QUFDSDtBQUNKLFNBTm1CLENBQU47QUFBQSxPQUFkOztBQVFBLFVBQUlJLENBQUMsR0FBRzdCLFFBQVEsQ0FBQ1MsYUFBVCxDQUF1QixHQUF2QixDQUFSO0FBQ0FvQixPQUFDLENBQUNDLFNBQUYsQ0FBWUMsR0FBWixDQUFnQixVQUFoQjtBQUNBRixPQUFDLENBQUNHLFdBQUYsR0FBZ0J6QixLQUFoQjtBQUVBQyxZQUFNLENBQUNLLFdBQVAsQ0FBbUJnQixDQUFuQjtBQUNBbEIsU0FBRyxDQUFDSSxZQUFKLENBQWlCLEtBQWpCLEVBQXdCVixHQUF4QjtBQUNBRyxZQUFNLENBQUNLLFdBQVAsQ0FBbUJILE1BQW5CO0FBQ0FGLFlBQU0sQ0FBQ0ssV0FBUCxDQUFtQkYsR0FBbkI7QUFDQVosZ0JBQVUsQ0FBQ2MsV0FBWCxDQUF1QkwsTUFBdkI7QUFDSCxLQTNDRDtBQTRDSDs7QUFFRFYsUUFBTSxDQUFDbUMsTUFBUCxDQUFjbEMsVUFBZDtBQUNILENBbEREOztBQW9EQSxJQUFNbUMsMkJBQTJCO0FBQUEscUVBQUcsaUJBQU9DLE1BQVAsRUFBZXJDLE1BQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQzFCc0MsaUNBRDBCLEdBQ0ZELE1BQU0sQ0FBQ0UsZ0JBQVAsQ0FBd0Isa0JBQXhCLENBREU7QUFFMUJDLDBCQUYwQixHQUVULEVBRlM7QUFJaENGLGlDQUFxQixTQUFyQixJQUFBQSxxQkFBcUIsV0FBckIsWUFBQUEscUJBQXFCLENBQUVqQyxPQUF2QixDQUErQixVQUFBb0MsSUFBSTtBQUFBLHFCQUFJQSxJQUFJLENBQUNDLEtBQUwsSUFBY0YsY0FBYyxDQUFDRyxJQUFmLENBQW9CO0FBQ3JFbkMsa0JBQUUsRUFBRW9DLE1BQU0sQ0FBQ0gsSUFBSSxDQUFDQyxLQUFOLENBRDJEO0FBRXJFbkMsbUJBQUcsRUFBRWtDLElBQUksQ0FBQ0k7QUFGMkQsZUFBcEIsQ0FBbEI7QUFBQSxhQUFuQzs7QUFKZ0MsaUJBUzVCTCxjQUFjLENBQUNwQyxNQVRhO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsbUJBVXRCMEMsNEVBQW9CLENBQUMsQ0FBQ04sY0FBYyxDQUFDTyxHQUFmLENBQW1CLFVBQUFDLE1BQU07QUFBQSxxQkFBSUEsTUFBTSxDQUFDeEMsRUFBWDtBQUFBLGFBQXpCLENBQUQsQ0FBRCxDQUFwQixDQUNEWSxJQURDLENBQ0ksVUFBQVMsR0FBRyxFQUFJO0FBQ1QsZUFBQUEsR0FBRyxTQUFILElBQUFBLEdBQUcsV0FBSCxZQUFBQSxHQUFHLENBQUV6QixNQUFMLEtBQWV5QixHQUFHLENBQUN4QixPQUFKLENBQVksVUFBQVEsR0FBRyxFQUFJO0FBQzlCMkIsOEJBQWMsQ0FBQ1MsTUFBZixDQUFzQixVQUFBQyxHQUFHLEVBQUk7QUFDekIsc0JBQUlBLEdBQUcsQ0FBQzFDLEVBQUosS0FBV0ssR0FBRyxDQUFDTCxFQUFuQixFQUF1QjtBQUNuQjBDLHVCQUFHLENBQUN6QyxLQUFKLEdBQVlJLEdBQUcsQ0FBQ0osS0FBSixJQUFhLEVBQXpCO0FBQ0g7QUFDSixpQkFKRDtBQUtILGVBTmMsQ0FBZjtBQU9ILGFBVEMsQ0FWc0I7O0FBQUE7QUFvQjVCWCw4QkFBa0IsQ0FBQzBDLGNBQUQsRUFBaUJ4QyxNQUFqQixDQUFsQjs7QUFwQjRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQUg7O0FBQUEsa0JBQTNCb0MsMkJBQTJCO0FBQUE7QUFBQTtBQUFBLEdBQWpDOztBQXdCQSxJQUFNZSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxRQU9PO0FBQUEsZ0NBTkZDLFdBTUU7QUFBQSxNQU5GQSxXQU1FLGtDQU5ZLE9BTVo7QUFBQSxvQ0FMRkMsZ0JBS0U7QUFBQSxNQUxGQSxnQkFLRSxzQ0FMaUIsRUFLakI7QUFBQSxvQ0FKRkMsZ0JBSUU7QUFBQSxNQUpGQSxnQkFJRSxzQ0FKaUIsQ0FJakI7QUFBQSxvQ0FIRkMsZ0JBR0U7QUFBQSxNQUhGQSxnQkFHRSxzQ0FIaUIsQ0FBQyxTQUFELENBR2pCO0FBQUEsOEJBRkZDLFNBRUU7QUFBQSxNQUZGQSxTQUVFLGdDQUZVLGtCQUVWO0FBQUEseUJBREZDLElBQ0U7QUFBQSxNQURGQSxJQUNFLDJCQURLLHFDQUNMO0FBRXBCLE1BQU1DLFVBQVUsR0FBR3hELFFBQVEsQ0FBQ29CLGFBQVQsK0JBQW5COztBQUVBLE1BQUlvQyxVQUFKLEVBQWdCO0FBQ1osUUFBTUMsWUFBWSxHQUFHRCxVQUFVLENBQUNFLE9BQVgsQ0FBbUIsYUFBbkIsQ0FBckI7QUFDQUMsS0FBQyxDQUFDRixZQUFELENBQUQsQ0FBZ0JHLFFBQWhCLEdBQTJCQyxJQUEzQjtBQUNBLFFBQU1DLGlCQUFpQixHQUFHSCxDQUFDLENBQUMsa0NBQUQsQ0FBM0I7QUFDQUEsS0FBQyxDQUFDRixZQUFELENBQUQsQ0FBZ0J4QixNQUFoQixDQUF1QjZCLGlCQUF2QjtBQUVBNUIsK0JBQTJCLENBQUNzQixVQUFELEVBQWFNLGlCQUFiLENBQTNCO0FBRUEsUUFBTUMsRUFBRSxHQUFHQyxrREFBSSxDQUFDO0FBQ1pDLFdBQUssRUFBRSxJQURLO0FBRVpDLGlCQUFXLEVBQUUsS0FGRDtBQUdaQywwQkFBb0IsRUFBRSxLQUhWO0FBSVpDLGtCQUFZLEVBQUU7QUFDVmxCLG1CQUFXLEVBQVhBLFdBRFU7QUFFVkMsd0JBQWdCLEVBQWhCQSxnQkFGVTtBQUdWQyx3QkFBZ0IsRUFBaEJBLGdCQUhVO0FBSVZDLHdCQUFnQixFQUFoQkE7QUFKVSxPQUpGO0FBVVpnQixVQUFJLEVBQUU7QUFDRkMsY0FBTSxFQUFFO0FBRE47QUFWTSxLQUFELENBQUosQ0FjTkMsR0FkTSxDQWNGQyx1REFkRSxFQWNTO0FBQ1pDLGFBQU8sRUFBRSxxQkFERztBQUVaQyxZQUFNLEVBQUUsSUFGSTtBQUdaNUUsWUFBTSxFQUFFMkQsWUFISTtBQUlaa0IsMEJBQW9CLEVBQUUsS0FKVjtBQUtaQyx5QkFBbUIsRUFBRSxJQUxUO0FBTVpyQixVQUFJLEVBQUpBLElBTlk7QUFPWnNCLFlBQU0sRUFBRSxHQVBJO0FBUVpDLG9CQUFjLEVBQUUsR0FSSjtBQVNaQyx1QkFBaUIsRUFBRSxJQVRQO0FBVVpDLDRCQUFzQixFQUFFO0FBVlosS0FkVCxFQTBCTlQsR0ExQk0sQ0EwQkZVLDBEQTFCRSxFQTBCVztBQUFDbkYsWUFBTSxFQUFFMEUsdURBQVQ7QUFBb0JVLGtCQUFZLEVBQUU7QUFBbEMsS0ExQlgsRUEyQk5YLEdBM0JNLENBMkJGWSxxREEzQkUsRUEyQk87QUFBQ3JGLFlBQU0sRUFBRTBFLHVEQUFUO0FBQW9CVSxrQkFBWSxFQUFFO0FBQWxDLEtBM0JQLEVBNEJOWCxHQTVCTSxDQTRCRmEsdURBNUJFLEVBNEJTO0FBQUN0RixZQUFNLEVBQUUwRSx1REFBVDtBQUFvQlUsa0JBQVksRUFBRTtBQUFsQyxLQTVCVCxFQTZCTlgsR0E3Qk0sQ0E2QkZjLG9EQTdCRSxFQTZCTTtBQUNUdkYsWUFBTSxFQUFFMEUsdURBREM7QUFFVGpFLFdBQUssRUFBRTtBQUZFLEtBN0JOLEVBaUNOZ0UsR0FqQ00sQ0FpQ0ZlLHdEQWpDRSxFQWlDUztBQUFDQyxjQUFRLEVBQUVqQyxTQUFYO0FBQXNCa0MsWUFBTSxFQUFFLE1BQTlCO0FBQXNDQyxjQUFRLEVBQUUsSUFBaEQ7QUFBc0RDLGVBQVMsRUFBRTtBQUFqRSxLQWpDVCxDQUFYO0FBbUNBM0IsTUFBRSxDQUFDNEIsRUFBSCxDQUFNLFVBQU4sRUFBa0IsVUFBQUMsT0FBTyxFQUFJO0FBQ3pCLFVBQU1DLFVBQVUsR0FBRyxFQUFuQjs7QUFDQSxVQUFJRCxPQUFPLENBQUNFLFVBQVIsQ0FBbUI1RixNQUF2QixFQUErQjtBQUMzQixZQUFJNkYsY0FBYyxHQUFHLEVBQXJCO0FBRUFILGVBQU8sQ0FBQ0UsVUFBUixDQUFtQjNGLE9BQW5CLENBQTJCLFVBQUE2RixHQUFHLEVBQUk7QUFDOUIsY0FBTUMsTUFBTSxHQUFHRCxHQUFHLENBQUNFLFFBQUosQ0FBYUMsSUFBYixDQUFrQjdGLEVBQWpDO0FBQ0EsY0FBTThGLFlBQVksR0FBR0osR0FBRyxDQUFDRSxRQUFKLENBQWFDLElBQWIsQ0FBa0JFLGFBQXZDO0FBRUFOLHdCQUFjLDRCQUFxQkUsTUFBckIsb0NBQW1ERyxZQUFuRCxjQUFkO0FBRUFQLG9CQUFVLENBQUNwRCxJQUFYLENBQWdCO0FBQUNuQyxjQUFFLEVBQUUyRixNQUFMO0FBQWE1RixlQUFHLEVBQUUrRjtBQUFsQixXQUFoQjtBQUNBckMsWUFBRSxDQUFDdUMsVUFBSCxDQUFjTixHQUFHLENBQUMxRixFQUFsQjtBQUNILFNBUkQ7O0FBVUEsWUFBSXVGLFVBQVUsQ0FBQzNGLE1BQWYsRUFBdUI7QUFDbkI7QUFDQXlELFdBQUMsQ0FBQ0gsVUFBRCxDQUFELENBQWN2QixNQUFkLENBQXFCOEQsY0FBckI7QUFFQW5HLDRCQUFrQixDQUFDaUcsVUFBRCxFQUFhL0IsaUJBQWIsQ0FBbEI7QUFFQXpDLGtGQUFXLENBQUNDLGdFQUFXLENBQUNDLE9BQWIsZ0JBQTZCc0UsVUFBVSxDQUFDM0YsTUFBWCxHQUFvQixDQUFwQixHQUF3QixHQUF4QixHQUE4QixFQUEzRCx5QkFBWDtBQUNIO0FBQ0o7QUFDSixLQXhCRDtBQXlCSDtBQUNKLENBaEZEOztBQWtGZStDLHVFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBc0QsbUJBQU8sQ0FBQyx3RUFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLGlEQUFELENBQVA7O0FBRUFDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZekcsUUFBUSxDQUFDb0IsYUFBVCxDQUF1Qiw4QkFBdkIsQ0FBWjtBQUNBOztBQUNBdUMsQ0FBQyxDQUFDM0QsUUFBRCxDQUFELENBQVkwRyxLQUFaLENBQWtCLFlBQU07QUFFcEI7QUFDSjtBQUNJLE1BQUkxRyxRQUFRLENBQUNvQixhQUFULCtCQUFKLEVBQTJENkIscURBQVEsQ0FBQyxFQUFELENBQVI7QUFHM0Q7QUFDSjtBQUNBO0FBQ0E7O0FBQ0l1RCxTQUFPLENBQUNDLEdBQVIsQ0FBWXpHLFFBQVEsQ0FBQ29CLGFBQVQsQ0FBdUIsOEJBQXZCLENBQVo7QUFDQSxNQUFJcEIsUUFBUSxDQUFDb0IsYUFBVCxDQUF1Qiw4QkFBdkIsQ0FBSixFQUE0RHVGLDZFQUFnQjtBQUUvRSxDQWRELEUsQ0FnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUFDLFVBQVUsQ0FBQyxZQUFZO0FBQ25CLE1BQUk1RyxRQUFRLENBQUNvQixhQUFULENBQXVCLE1BQXZCLENBQUosRUFBb0M7QUFDaENvRixXQUFPLENBQUNDLEdBQVIsQ0FBWSxPQUFaO0FBQ0F6RyxZQUFRLENBQUNvQixhQUFULENBQXVCLE1BQXZCLEVBQStCc0MsT0FBL0IsQ0FBdUMsYUFBdkMsRUFBc0RtRCxLQUF0RCxDQUE0REMsT0FBNUQsR0FBc0UsT0FBdEU7QUFDSDtBQUNKLENBTFMsQ0FBVixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbERBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7O0FBQ08sSUFBTXBGLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ3BCLEVBQUQ7QUFBQSxTQUMzQnlHLGlFQUFHLHVCQUFnQnpHLEVBQWhCLEdBQXNCLEVBQXRCLENBQUgsQ0FDS1ksSUFETCxDQUNVOEYseUVBRFYsQ0FEMkI7QUFBQSxDQUF4QjtBQUlBLElBQU0vRixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNYLEVBQUQ7QUFBQSxTQUM1QjJHLG9FQUFNLHNCQUFlM0csRUFBZixFQUFOLENBQ0tZLElBREwsQ0FDVThGLHlFQURWLENBRDRCO0FBQUEsQ0FBekI7QUFJQSxJQUFNRSxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNDLElBQUQ7QUFBQSxTQUM1QkMsS0FBSyxDQUFDLG1CQUFELEVBQXNCO0FBQ3ZCNUIsVUFBTSxFQUFFLE1BRGU7QUFFdkJXLFFBQUksRUFBRWtCLElBQUksQ0FBQ0MsU0FBTCxDQUFlSCxJQUFmLEVBQXFCSSxRQUFyQjtBQUZpQixHQUF0QixDQUFMLENBR0dyRyxJQUhILENBR1EsVUFBQVMsR0FBRztBQUFBLFdBQUlBLEdBQUcsQ0FBQzZGLEVBQUosSUFBVTdGLEdBQUcsQ0FBQzhGLE1BQUosS0FBZSxHQUE3QjtBQUFBLEdBSFgsQ0FENEI7QUFBQSxDQUF6QjtBQU1BLElBQU03RSxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUN1RSxJQUFEO0FBQUEsU0FDaENPLGtFQUFJLENBQUMsa0JBQUQsRUFBcUIsRUFBckIsRUFBeUJQLElBQXpCLEVBQStCLElBQS9CLENBQUosQ0FDS2pHLElBREwsQ0FDVThGLHlFQURWLENBRGdDO0FBQUEsQ0FBN0I7QUFJUDtBQUNBO0FBQ0E7O0FBR0E7QUFDQTtBQUNBOztBQUNPLElBQU1XLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsQ0FBQ1IsSUFBRDtBQUFBLFNBQzVCTyxrRUFBSSxDQUFDLGNBQUQsRUFBaUIsRUFBakIsRUFBcUJQLElBQXJCLENBQUosQ0FDS2pHLElBREwsQ0FDVSxVQUFBUyxHQUFHO0FBQUEsV0FBSWlHLDBFQUFjLENBQ3ZCakcsR0FEdUIsRUFFdkJrRyxTQUZ1QixFQUd2QjtBQUFBLGFBQU0sQ0FBQyxDQUFDbEcsR0FBRyxDQUFDSixPQUFaO0FBQUEsS0FIdUIsRUFJdkIsS0FKdUIsRUFLdkIsa0NBTHVCLEVBTXZCRSxtRUFOdUIsQ0FBbEI7QUFBQSxHQURiLENBRDRCO0FBQUEsQ0FBekI7QUFVUDtBQUNBO0FBQ0E7O0FBRU8sSUFBTXFHLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQUN2RixJQUFELEVBQVU7QUFDakMsTUFBTWtELFFBQVEsR0FBRyxJQUFJc0MsUUFBSixFQUFqQjtBQUVBdEMsVUFBUSxDQUFDeEQsTUFBVCxDQUFnQixTQUFoQixFQUEyQk0sSUFBM0I7QUFFQSxTQUFPbUYsa0VBQUksQ0FBQyxrQkFBRCxFQUFxQixFQUFyQixFQUF5QmpDLFFBQXpCLEVBQW1DLElBQW5DLENBQVg7QUFDSCxDQU5NLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUNQLElBQU11QyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLEdBQTZDO0FBQUEsTUFBNUNDLE9BQTRDLHVFQUFsQyxFQUFrQztBQUFBLE1BQTlCMUgsS0FBOEIsdUVBQXRCLEVBQXNCO0FBQUEsTUFBbEIySCxhQUFrQjtBQUNoRSxNQUFNQyxLQUFLLEdBQUd4RSxDQUFDLGtJQUdnQnBELEtBSGhCLDhKQU1HMEgsT0FOSCwyVEFBRCxDQWNYRyxLQWRXLENBY0wsVUFBQUMsQ0FBQyxFQUFJO0FBQ1YsUUFBRzFFLENBQUMsQ0FBQzBFLENBQUMsQ0FBQ3ZJLE1BQUgsQ0FBRCxDQUFZd0ksUUFBWixDQUFxQixhQUFyQixLQUF1Q0osYUFBMUMsRUFBeUQ7QUFFckRBLG1CQUFhLEdBQUdoSCxJQUFoQixDQUFxQixVQUFBcUgsSUFBSTtBQUFBLGVBQUlBLElBQUksSUFBSUosS0FBSyxDQUFDekgsTUFBTixFQUFaO0FBQUEsT0FBekI7QUFFSCxLQUpELE1BSU8sSUFBSWlELENBQUMsQ0FBQzBFLENBQUMsQ0FBQ3ZJLE1BQUgsQ0FBRCxDQUFZd0ksUUFBWixDQUFxQixRQUFyQixLQUFrQ0QsQ0FBQyxDQUFDdkksTUFBRixLQUFhcUksS0FBSyxDQUFDLENBQUQsQ0FBeEQsRUFBNkQ7QUFDaEU7QUFDQUEsV0FBSyxDQUFDekgsTUFBTjtBQUNIO0FBQ0osR0F2QmEsQ0FBZDtBQXlCQSxNQUFNOEgsVUFBVSxHQUFHeEksUUFBUSxDQUFDb0IsYUFBVCxDQUF1QixpQkFBdkIsQ0FBbkI7QUFDQW9ILFlBQVUsSUFBSUEsVUFBVSxDQUFDOUgsTUFBWCxFQUFkO0FBRUFpRCxHQUFDLENBQUMsTUFBRCxDQUFELENBQVUxQixNQUFWLENBQWlCa0csS0FBakI7QUFDSCxDQTlCRDs7QUFnQ2VILDZFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaENBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNcEcscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDRCxHQUFELEVBQVM7QUFDbkMsTUFBT3JCLEVBQVAsR0FBOEVxQixHQUE5RSxDQUFPckIsRUFBUDtBQUFBLE1BQVdDLEtBQVgsR0FBOEVvQixHQUE5RSxDQUFXcEIsS0FBWDtBQUFBLE1BQWtCa0ksUUFBbEIsR0FBOEU5RyxHQUE5RSxDQUFrQjhHLFFBQWxCO0FBQUEsTUFBNEJDLE9BQTVCLEdBQThFL0csR0FBOUUsQ0FBNEIrRyxPQUE1QjtBQUFBLE1BQXFDQyxXQUFyQyxHQUE4RWhILEdBQTlFLENBQXFDZ0gsV0FBckM7QUFBQSxNQUFrREMsVUFBbEQsR0FBOEVqSCxHQUE5RSxDQUFrRGlILFVBQWxEO0FBQUEsTUFBOER4QyxZQUE5RCxHQUE4RXpFLEdBQTlFLENBQThEeUUsWUFBOUQ7QUFFQSxNQUFNeUMsWUFBWSxpUUFNOEJ0SSxLQUFLLElBQUksRUFOdkMsOE1BVThCa0ksUUFBUSxJQUFJLEVBVjFDLGlOQWM4QkMsT0FBTyxJQUFJLEVBZHpDLHFNQWlCcUVFLFVBQVUsR0FBRyxTQUFILEdBQWUsRUFqQjlGLHNSQXVCd0NELFdBQVcsSUFBSSxFQXZCdkQsMkVBeUJLdkMsWUFBWSxJQUFJLEVBekJyQiw2REFBbEI7O0FBOEJBLE1BQU04QixhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBQU07QUFDeEIsV0FBTyxJQUFJWSxPQUFKLENBQVksVUFBQUMsT0FBTyxFQUFJO0FBQzFCLFVBQU01QixJQUFJLEdBQUc7QUFDVCx5QkFBaUI3RztBQURSLE9BQWI7QUFJQSxVQUFNMEksTUFBTSxHQUFHaEosUUFBUSxDQUFDcUMsZ0JBQVQsQ0FBMEIsNkNBQTFCLENBQWY7QUFFQTJHLFlBQU0sQ0FBQzdJLE9BQVAsQ0FBZ0IsVUFBQTZGLEdBQUcsRUFBSTtBQUFBOztBQUNuQiwwQkFBSUEsR0FBRyxDQUFDeEQsS0FBUix1Q0FBSSxXQUFXeUcsSUFBWCxFQUFKLEVBQXVCO0FBQ25COUIsY0FBSSxDQUFDbkIsR0FBRyxDQUFDa0QsSUFBTCxDQUFKLEdBQWlCbEQsR0FBRyxDQUFDa0QsSUFBSixLQUFhLHNCQUFiLEdBQXNDbEQsR0FBRyxDQUFDeEQsS0FBMUMsR0FBa0R3RCxHQUFHLENBQUNtRCxPQUF2RTtBQUNILFNBRkQsTUFFTztBQUNIaEMsY0FBSSxDQUFDbkIsR0FBRyxDQUFDa0QsSUFBTCxDQUFKLEdBQWlCLEVBQWpCO0FBQ0g7QUFDSixPQU5EO0FBUUFoQyw2RUFBZ0IsQ0FBQ0MsSUFBRCxDQUFoQixDQUF1QmpHLElBQXZCLENBQTRCLFVBQUFrSSxPQUFPLEVBQUk7QUFDbkMsWUFBSUEsT0FBSixFQUFhO0FBQ1QvSCxrRkFBVyxDQUFDQyxnRUFBVyxDQUFDQyxPQUFiLEVBQXNCLDJCQUF0QixDQUFYO0FBQ0F3SCxpQkFBTyxDQUFDLElBQUQsQ0FBUDtBQUNILFNBSEQsTUFHTztBQUNIMUgsa0ZBQVcsQ0FBQ0MsZ0VBQVcsQ0FBQ0UsS0FBYixFQUFvQkMsb0VBQXBCLENBQVg7QUFDSDtBQUNKLE9BUEQ7QUFRSCxLQXZCTSxDQUFQO0FBd0JILEdBekJEOztBQTJCQXVHLGtFQUFjLENBQUNhLFlBQUQsRUFBZSxjQUFmLEVBQWdDWCxhQUFoQyxDQUFkO0FBQ0gsQ0E3REQ7O0FBK0RldEcsb0ZBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuRUE7QUFDQTtBQUVPLElBQU1vRixtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUFxQyxNQUFNO0FBQUEsU0FBSUEsTUFBTSxHQUFHQSxNQUFILEdBQVksWUFBTTtBQUNwRWhJLGtFQUFXLENBQUNDLHNEQUFXLENBQUNFLEtBQWIsRUFBb0JDLDBEQUFwQixDQUFYO0FBQ0EsV0FBTyxLQUFQO0FBQ0EsR0FId0M7QUFBQSxDQUFsQztBQUtBLElBQU1pRyxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFDNEIsR0FBRCxFQUFnRDtBQUFBLE1BQTFDQyxPQUEwQyx1RUFBaEMsRUFBZ0M7QUFBQSxNQUE1QnBELElBQTRCO0FBQUEsTUFBdEJxRCxTQUFzQix1RUFBVixLQUFVO0FBQ25FQyw4REFBUzs7QUFDVCxNQUFJLENBQUNGLE9BQU8sQ0FBQ0csTUFBYixFQUFxQjtBQUNwQkgsV0FBTyxDQUFDRyxNQUFSLEdBQWlCLHVCQUFqQjtBQUNBOztBQUVELFNBQU90QyxLQUFLLENBQUNrQyxHQUFELEVBQU07QUFDakI5RCxVQUFNLEVBQUUsTUFEUztBQUVqQitELFdBQU8sRUFBUEEsT0FGaUI7QUFHakJwRCxRQUFJLEVBQUVxRCxTQUFTLEdBQUdyRCxJQUFILEdBQVVrQixJQUFJLENBQUNDLFNBQUwsQ0FBZW5CLElBQWYsRUFBcUJvQixRQUFyQjtBQUhSLEdBQU4sQ0FBTCxDQUtMckcsSUFMSyxDQUtBLFVBQUFnRixRQUFRLEVBQUk7QUFDakIsUUFBSSxDQUFDQSxRQUFRLENBQUNzQixFQUFWLElBQWdCdEIsUUFBUSxDQUFDdUIsTUFBVCxLQUFvQixHQUF4QyxFQUE2QztBQUM1QyxhQUFPLEtBQVA7QUFDQTs7QUFFRCxXQUFPdkIsUUFBUSxDQUFDeUQsSUFBVCxFQUFQO0FBQ0EsR0FYSyxhQVlHO0FBQUEsV0FBTUYsNERBQVMsQ0FBQyxLQUFELENBQWY7QUFBQSxHQVpILFdBYUMsVUFBQXRDLElBQUksRUFBSTtBQUNkWCxXQUFPLENBQUNDLEdBQVIsQ0FBWSxPQUFaLEVBQXFCVSxJQUFyQjtBQUNBLEdBZkssQ0FBUDtBQWdCQSxDQXRCTTtBQXdCQSxJQUFNRixNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFDcUMsR0FBRCxFQUF1QjtBQUFBLE1BQWpCQyxPQUFpQix1RUFBUCxFQUFPO0FBQzVDRSw4REFBUzs7QUFDVCxNQUFJLENBQUNGLE9BQU8sQ0FBQ0csTUFBYixFQUFxQjtBQUNwQkgsV0FBTyxDQUFDRyxNQUFSLEdBQWlCLHVCQUFqQjtBQUNBOztBQUVELFNBQU90QyxLQUFLLENBQUNrQyxHQUFELEVBQU07QUFDakI5RCxVQUFNLEVBQUUsUUFEUztBQUVqQitELFdBQU8sRUFBUEE7QUFGaUIsR0FBTixDQUFMLENBSUxySSxJQUpLLENBSUEsVUFBQWdGLFFBQVE7QUFBQSxXQUFJLEVBQUUsQ0FBQ0EsUUFBUSxDQUFDc0IsRUFBVixJQUFnQnRCLFFBQVEsQ0FBQ3VCLE1BQVQsS0FBb0IsR0FBdEMsQ0FBSjtBQUFBLEdBSlIsYUFLRztBQUFBLFdBQU1nQyw0REFBUyxDQUFDLEtBQUQsQ0FBZjtBQUFBLEdBTEgsV0FNQyxVQUFBdEMsSUFBSSxFQUFJO0FBQ2RYLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVosRUFBcUJVLElBQXJCO0FBQ0EsR0FSSyxDQUFQO0FBU0EsQ0FmTTtBQWlCQSxJQUFNSixHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFDdUMsR0FBRCxFQUF1QjtBQUFBLE1BQWpCQyxPQUFpQix1RUFBUCxFQUFPO0FBQ3pDRSw4REFBUzs7QUFFVCxNQUFJLENBQUNGLE9BQU8sQ0FBQ0csTUFBYixFQUFxQjtBQUNwQkgsV0FBTyxDQUFDRyxNQUFSLEdBQWlCLHVCQUFqQjtBQUNBOztBQUVELFNBQU90QyxLQUFLLENBQUNrQyxHQUFELEVBQU07QUFDakI5RCxVQUFNLEVBQUUsS0FEUztBQUVqQitELFdBQU8sRUFBUEE7QUFGaUIsR0FBTixDQUFMLENBSUxySSxJQUpLLENBSUEsVUFBQWdGLFFBQVEsRUFBSTtBQUNqQixRQUFJLENBQUNBLFFBQVEsQ0FBQ3NCLEVBQVYsSUFBZ0J0QixRQUFRLENBQUN1QixNQUFULEtBQW9CLEdBQXhDLEVBQTZDO0FBQzVDLGFBQU8sS0FBUDtBQUNBOztBQUVELFdBQU92QixRQUFRLENBQUN5RCxJQUFULEVBQVA7QUFDQSxHQVZLLGFBV0c7QUFBQSxXQUFNRiw0REFBUyxDQUFDLEtBQUQsQ0FBZjtBQUFBLEdBWEgsV0FZQyxVQUFBdEMsSUFBSSxFQUFJO0FBQ2RYLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVosRUFBcUJVLElBQXJCO0FBQ0EsR0FkSyxDQUFQO0FBZUEsQ0F0Qk07QUF3QkEsSUFBTXlDLEdBQUcsR0FBRyxTQUFOQSxHQUFNLENBQUNOLEdBQUQsRUFBNkI7QUFBQSxNQUF2QkMsT0FBdUIsdUVBQWIsRUFBYTtBQUFBLE1BQVRwRCxJQUFTO0FBQy9Dc0QsOERBQVM7O0FBQ1QsTUFBSSxDQUFDRixPQUFPLENBQUMsY0FBRCxDQUFaLEVBQThCO0FBQzdCQSxXQUFPLENBQUMsY0FBRCxDQUFQLEdBQTBCLGtCQUExQjtBQUNBOztBQUVELFNBQU9uQyxLQUFLLENBQUNrQyxHQUFELEVBQU07QUFDakI5RCxVQUFNLEVBQUUsS0FEUztBQUVqQitELFdBQU8sRUFBUEEsT0FGaUI7QUFHakJwRCxRQUFJLEVBQUVrQixJQUFJLENBQUNDLFNBQUwsQ0FBZW5CLElBQWYsRUFBcUJvQixRQUFyQjtBQUhXLEdBQU4sQ0FBTCxDQUtMckcsSUFMSyxDQUtBLFVBQUFnRixRQUFRLEVBQUk7QUFDakIsUUFBSSxDQUFDQSxRQUFRLENBQUNzQixFQUFWLElBQWdCdEIsUUFBUSxDQUFDdUIsTUFBVCxLQUFvQixHQUF4QyxFQUE2QztBQUM1QyxhQUFPLEtBQVA7QUFDQTs7QUFFRCxXQUFPdkIsUUFBUSxDQUFDeUQsSUFBVCxFQUFQO0FBQ0EsR0FYSyxhQVlHO0FBQUEsV0FBTUYsNERBQVMsQ0FBQyxLQUFELENBQWY7QUFBQSxHQVpILFdBYUMsVUFBQXRDLElBQUksRUFBSTtBQUNkWCxXQUFPLENBQUNDLEdBQVIsQ0FBWSxPQUFaLEVBQXFCVSxJQUFyQjtBQUNBLEdBZkssQ0FBUDtBQWdCQSxDQXRCTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6RVA7QUFFTyxJQUFNN0YsV0FBVyxHQUFHO0FBQ3ZCRSxPQUFLLEVBQUUsT0FEZ0I7QUFFdkJELFNBQU8sRUFBRSxTQUZjO0FBR3ZCc0ksU0FBTyxFQUFFO0FBSGMsQ0FBcEI7QUFNQSxJQUFNcEksZUFBZSxHQUFHLHVCQUF4QjtBQUVBLElBQU1xSSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQUFNO0FBQzFCLE1BQUlDLElBQUksR0FBRyxFQUFYO0FBQ0EsTUFBTUMsUUFBUSxHQUFHLGdFQUFqQjs7QUFFQSxPQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsQ0FBcEIsRUFBdUJBLENBQUMsRUFBeEI7QUFBNEJGLFFBQUksSUFBSUMsUUFBUSxDQUFDRSxNQUFULENBQWdCQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0QsSUFBSSxDQUFDRSxNQUFMLEtBQWdCTCxRQUFRLENBQUM5SixNQUFwQyxDQUFoQixDQUFSO0FBQTVCOztBQUVBLFNBQU82SixJQUFQO0FBQ0gsQ0FQTTtBQVNBLElBQU0xSSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUF5QztBQUFBLE1BQXhDaUosSUFBd0MsdUVBQWpDaEosV0FBVyxDQUFDdUksT0FBcUI7QUFBQSxNQUFaVSxPQUFZO0FBQ2hFLE1BQU1DLGtCQUFrQixHQUFHN0csQ0FBQyxDQUFDLHNCQUFELENBQTVCO0FBQ0EsTUFBTThHLE1BQU0sR0FBR1gsUUFBUSxFQUF2QjtBQUNBLE1BQU1ZLFVBQVUsMEJBQWtCSixJQUFsQixxQkFBK0JHLE1BQS9CLG1CQUE2Q0YsT0FBN0MsZUFBaEI7O0FBRUEsTUFBSUMsa0JBQWtCLENBQUN0SyxNQUF2QixFQUErQjtBQUMzQnNLLHNCQUFrQixDQUFDdkksTUFBbkIsQ0FBMEJ5SSxVQUExQjtBQUNILEdBRkQsTUFFTztBQUNIL0csS0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVMUIsTUFBVix5REFFTXlJLFVBRk47QUFLSDs7QUFFRC9HLEdBQUMsWUFBSzhHLE1BQUwsRUFBRCxDQUFnQkUsT0FBaEIsQ0FBd0IsSUFBeEIsRUFBOEIsT0FBOUIsRUFBdUMsWUFBWTtBQUMvQ2hILEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWpELE1BQVI7QUFFQSxRQUFNOEosa0JBQWtCLEdBQUc3RyxDQUFDLENBQUMsc0JBQUQsQ0FBNUI7O0FBQ0EsUUFBRyxDQUFDNkcsa0JBQWtCLENBQUM1RyxRQUFuQixHQUE4QjFELE1BQWxDLEVBQTBDO0FBQ3RDc0ssd0JBQWtCLENBQUM5SixNQUFuQjtBQUNIO0FBQ0osR0FQRDtBQVFILENBdkJNO0FBeUJQO0FBQ0E7QUFDQTs7QUFDTyxJQUFNa0ssY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixHQUFnQjtBQUFBLE1BQWZDLE1BQWUsdUVBQU4sQ0FBTTtBQUMxQyxNQUFNQyxJQUFJLEdBQUdDLElBQUksQ0FBQ0MsR0FBTCxFQUFiO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLElBQWxCOztBQUNBLEtBQUc7QUFDQ0EsZUFBVyxHQUFHRixJQUFJLENBQUNDLEdBQUwsRUFBZDtBQUNILEdBRkQsUUFFU0MsV0FBVyxHQUFHSCxJQUFkLEdBQXFCRCxNQUFNLEdBQUcsSUFGdkM7QUFHSCxDQU5NO0FBU1A7QUFDQTtBQUNBOztBQUNPLElBQU1LLHFCQUFxQixHQUFHLFNBQXhCQSxxQkFBd0IsQ0FBQUMsWUFBWSxFQUFJO0FBQ2pELE1BQUlBLFlBQVksQ0FBQ1osT0FBakIsRUFBMEI7QUFDdEIsUUFDSSxRQUFPWSxZQUFZLENBQUNaLE9BQXBCLE1BQWdDLFFBQWhDLElBQ0EsQ0FBQ2EsS0FBSyxDQUFDQyxPQUFOLENBQWNGLFlBQVksQ0FBQ1osT0FBM0IsQ0FGTCxFQUlJLE9BQU9lLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjSixZQUFZLENBQUNaLE9BQTNCLENBQVA7QUFDSixXQUFPLElBQVA7QUFDSDs7QUFDRCxTQUFPLElBQVA7QUFDSCxDQVZNO0FBWUEsSUFBTTNDLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ3lCLE1BQUQsRUFBNEg7QUFBQSxNQUFuSG1DLElBQW1ILHVFQUE1RzNELFNBQTRHO0FBQUEsTUFBakd0RyxPQUFpRyx1RUFBdkZzRyxTQUF1RjtBQUFBLE1BQTVFckcsS0FBNEUsdUVBQXBFcUcsU0FBb0U7QUFBQSxNQUF6RDRELGNBQXlELHVFQUF4QzVELFNBQXdDO0FBQUEsTUFBN0I2RCxZQUE2Qix1RUFBZDdELFNBQWM7QUFDdEosTUFBSTJELElBQUosRUFBVUEsSUFBSTs7QUFDZCxNQUFJbkMsTUFBSixFQUFZO0FBQ1IsUUFBSUEsTUFBTSxDQUFDOUgsT0FBWCxFQUFvQjtBQUNoQixVQUFJa0ssY0FBSixFQUFvQnBLLFdBQVcsQ0FBQ0MsV0FBVyxDQUFDQyxPQUFiLEVBQXNCa0ssY0FBdEIsQ0FBWDtBQUNwQixVQUFJbEssT0FBSixFQUFhLE9BQU9BLE9BQU8sQ0FBQzhILE1BQU0sQ0FBQ2xDLElBQVIsQ0FBZDtBQUNiLGFBQU9rQyxNQUFNLENBQUNsQyxJQUFkO0FBQ0gsS0FKRCxNQUlPO0FBQ0g5RixpQkFBVyxDQUFDQyxXQUFXLENBQUNFLEtBQWIsRUFBb0IwSixxQkFBcUIsQ0FBQzdCLE1BQUQsQ0FBekMsQ0FBWDtBQUNBLGFBQU8sS0FBUDtBQUNIO0FBQ0osR0FURCxNQVNPO0FBQ0gsUUFBSXFDLFlBQUosRUFBa0JySyxXQUFXLENBQUNDLFdBQVcsQ0FBQ0UsS0FBYixFQUFvQmtLLFlBQXBCLENBQVg7QUFDbEIsUUFBSWxLLEtBQUosRUFBVyxPQUFPQSxLQUFLLEVBQVo7QUFDWCxXQUFPLEtBQVA7QUFDSDtBQUNKLENBaEJNO0FBa0JQO0FBQ0E7QUFDQTs7QUFFTyxJQUFNaUksU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBb0I7QUFBQSxNQUFuQmtDLE9BQW1CLHVFQUFULElBQVM7QUFDekMsTUFBTUMsTUFBTSxHQUFHNUwsUUFBUSxDQUFDb0IsYUFBVCxDQUF1QixnQkFBdkIsQ0FBZjs7QUFFQSxNQUFHdUssT0FBTyxJQUFJLENBQUVDLE1BQWhCLEVBQXlCO0FBQ3JCLFFBQU1DLE1BQU0sR0FBR2xJLENBQUMsQ0FBQyxtQ0FBRCxDQUFoQjtBQUNBQSxLQUFDLENBQUMsTUFBRCxDQUFELENBQVUxQixNQUFWLENBQWlCNEosTUFBakI7QUFDSDs7QUFFRCxNQUFJLENBQUNGLE9BQUQsSUFBWUMsTUFBaEIsRUFBd0I7QUFBQTs7QUFDcEIsVUFBQWpJLENBQUMsQ0FBQyxnQkFBRCxDQUFELDBDQUFxQmpELE1BQXJCO0FBQ0g7QUFDSixDQVhNLEMsQ0FjUDs7QUFDTyxJQUFNb0wsVUFBVSxHQUFHO0FBQ3RCQyxNQUFJLEVBQUUsTUFEZ0I7QUFFdEJDLE1BQUksRUFBRSxRQUZnQjtBQUd0QkMsTUFBSSxFQUFFO0FBSGdCLENBQW5CO0FBTUEsSUFBTUMsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixHQUFNO0FBQy9CLE1BQU1DLFNBQVMsR0FBRyxJQUFJQyxlQUFKLENBQW9CQyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLE1BQXBDLENBQWxCO0FBQ0EsTUFBTUMsTUFBTSxHQUFHTCxTQUFTLENBQUNNLEdBQVYsQ0FBYyxZQUFkLENBQWY7QUFFQSxTQUFPRCxNQUFNLElBQUksS0FBakI7QUFDSCxDQUxNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEhQO0FBT0E7QUFDQTs7QUFFQSxJQUFNN0YsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixHQUFNO0FBQzNCLE1BQU0rRix5QkFBeUIsR0FBRzFNLFFBQVEsQ0FBQ29CLGFBQVQsQ0FBdUIsOEJBQXZCLENBQWxDO0FBQ0EsTUFBTXVMLGNBQWMsR0FBR0QseUJBQXlCLENBQUN0TCxhQUExQixDQUF3QyxrQkFBeEMsQ0FBdkI7QUFFQSxNQUFNd0wsU0FBUyxHQUFHakosQ0FBQyxDQUFDK0kseUJBQUQsQ0FBRCxDQUE2QmhKLE9BQTdCLENBQXFDLGdCQUFyQyxDQUFsQjtBQUVBLE1BQU1tSixLQUFLLEdBQUdsSixDQUFDLENBQUMsaUVBQUQsQ0FBRCxDQUFxRWdDLEVBQXJFLENBQXdFLFFBQXhFLEVBQWtGLFVBQUMwQyxDQUFELEVBQU87QUFDbkcsUUFBTXlFLFlBQVksR0FBR3pFLENBQUMsQ0FBQ3ZJLE1BQUYsQ0FBU2lOLEtBQVQsQ0FBZSxDQUFmLENBQXJCOztBQUNBLFFBQUlELFlBQUosRUFBa0I7QUFBQTs7QUFDZCwyQkFBQUUsV0FBVyxDQUFDQyxJQUFaLENBQWlCLEtBQWpCLHlFQUF5QnZNLE1BQXpCO0FBQ0EsNEJBQUFzTSxXQUFXLENBQUNDLElBQVosQ0FBaUIsYUFBakIsMkVBQWlDdk0sTUFBakM7QUFFQW9ILHdFQUFXLENBQUNnRixZQUFELENBQVgsQ0FBMEI1TCxJQUExQixDQUErQixVQUFBUyxHQUFHLEVBQUk7QUFDbEMsWUFBSUEsR0FBSixhQUFJQSxHQUFKLGVBQUlBLEdBQUcsQ0FBRXJCLEVBQVQsRUFBYTtBQUNUNE0sMEJBQWdCLENBQUN2TCxHQUFELENBQWhCO0FBQ0gsU0FGRCxNQUVPO0FBQ0hOLGlGQUFXLENBQUNDLCtEQUFXLENBQUNFLEtBQWIsRUFBb0JDLG1FQUFwQixDQUFYO0FBQ0g7QUFDSixPQU5EO0FBT0g7QUFDSixHQWRhLENBQWQ7QUFnQkEsTUFBTXVMLFdBQVcsR0FBR3JKLENBQUMsNExBQXJCO0FBT0FxSixhQUFXLENBQUNDLElBQVosQ0FBaUIsaUJBQWpCLEVBQW9DaEwsTUFBcEMsQ0FBMkM0SyxLQUEzQztBQUNBRyxhQUFXLENBQUNHLFdBQVosQ0FBd0JQLFNBQXhCOztBQUVBLE1BQU1NLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsQ0FBQzlNLEtBQUQsRUFBVztBQUNoQyxRQUFPaUcsYUFBUCxHQUE0QmpHLEtBQTVCLENBQU9pRyxhQUFQO0FBQUEsUUFBc0IvRixFQUF0QixHQUE0QkYsS0FBNUIsQ0FBc0JFLEVBQXRCO0FBRUEsUUFBTUssR0FBRyxHQUFHZ0QsQ0FBQyxzQkFBYzBDLGFBQWQsOEJBQTJDL0YsRUFBM0MsVUFBRCxDQUFxRHFGLEVBQXJELENBQXdELE9BQXhELEVBQWlFLFlBQU07QUFDL0VqRSw0RUFBZSxDQUFDcEIsRUFBRCxDQUFmLENBQW9CWSxJQUFwQixDQUF5QixVQUFBZ0YsUUFBUSxFQUFJO0FBQ2pDLFlBQUlBLFFBQUosYUFBSUEsUUFBSixlQUFJQSxRQUFRLENBQUU1RixFQUFkLEVBQWtCO0FBQ2RzQix3RkFBcUIsQ0FBQ3NFLFFBQUQsQ0FBckI7QUFDSCxTQUZELE1BRU87QUFDSDdFLGlGQUFXLENBQUNDLCtEQUFXLENBQUNFLEtBQWIsRUFBb0JDLG1FQUFwQixDQUFYO0FBQ0g7QUFDSixPQU5EO0FBT0gsS0FSVyxDQUFaO0FBVUEsUUFBTWYsTUFBTSxHQUFHaUQsQ0FBQyxzQ0FBRCxDQUFtQ2dDLEVBQW5DLENBQXNDLE9BQXRDLEVBQStDLFlBQU07QUFDaEUxRSw2RUFBZ0IsQ0FBQ1gsRUFBRCxDQUFoQixDQUFxQlksSUFBckIsQ0FBMEIsVUFBQUMsT0FBTyxFQUFJO0FBQ2pDLFlBQUlBLE9BQUosRUFBYTtBQUNUNkwscUJBQVcsQ0FBQ0MsSUFBWixDQUFpQixLQUFqQixFQUF3QnZNLE1BQXhCO0FBQ0FzTSxxQkFBVyxDQUFDQyxJQUFaLENBQWlCLGFBQWpCLEVBQWdDdk0sTUFBaEM7QUFDQWdNLG1DQUF5QixDQUFDdEwsYUFBMUIsQ0FBd0Msa0JBQXhDLEVBQTREVixNQUE1RDtBQUNBaUQsV0FBQyxDQUFDa0osS0FBRCxDQUFELENBQVNPLEdBQVQsQ0FBYSxFQUFiO0FBQ0g7QUFDSixPQVBEO0FBUUgsS0FUYyxDQUFmO0FBV0FKLGVBQVcsQ0FBQy9LLE1BQVosQ0FBbUJ0QixHQUFuQjtBQUNBcU0sZUFBVyxDQUFDL0ssTUFBWixDQUFtQnZCLE1BQW5CO0FBRUEsUUFBTWlNLGNBQWMsR0FBR0QseUJBQXlCLENBQUN0TCxhQUExQixDQUF3QyxrQkFBeEMsQ0FBdkI7O0FBRUEsUUFBSXVMLGNBQUosRUFBb0I7QUFDaEJBLG9CQUFjLENBQUNuSyxLQUFmLEdBQXVCbEMsRUFBdkI7QUFDQXFNLG9CQUFjLENBQUMzSyxXQUFmLEdBQTZCcUUsYUFBN0I7QUFDSCxLQUhELE1BR087QUFDSDFDLE9BQUMsQ0FBQytJLHlCQUFELENBQUQsQ0FBNkJ6SyxNQUE3QixpREFBMEUzQixFQUExRSxnQkFBaUYrRixhQUFqRjtBQUNIO0FBQ0osR0FuQ0Q7O0FBcUNBLE1BQUlzRyxjQUFKLEVBQW9CO0FBQ2hCTyxvQkFBZ0IsQ0FBQztBQUNiNU0sUUFBRSxFQUFFcU0sY0FBYyxDQUFDbkssS0FETjtBQUViLHVCQUFpQm1LLGNBQWMsQ0FBQzNLO0FBRm5CLEtBQUQsQ0FBaEI7QUFJSDtBQUNKLENBM0VEOztBQTZFZTJFLCtFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2RkE7QUFFQSxJQUFNMEcsc0JBQXNCLEdBQUc7QUFDM0JDLHlCQUF1QixFQUFFLG1DQUFZO0FBRWpDLFFBQUl0TixRQUFRLENBQUNtRyxJQUFULENBQWNyRSxTQUFkLENBQXdCeUwsUUFBeEIsQ0FBaUMsSUFBakMsS0FBMEN2TixRQUFRLENBQUNtRyxJQUFULENBQWNyRSxTQUFkLENBQXdCeUwsUUFBeEIsQ0FBaUMsT0FBakMsQ0FBOUMsRUFBeUY7QUFFckYsVUFBSSxDQUFDbkMsS0FBSyxDQUFDb0MsU0FBTixDQUFnQkMsSUFBckIsRUFBMkI7QUFDdkJyQyxhQUFLLENBQUNvQyxTQUFOLENBQWdCQyxJQUFoQixHQUF1QixZQUFZO0FBQy9CLGlCQUFPLEtBQUssS0FBS3ZOLE1BQUwsR0FBYyxDQUFuQixDQUFQO0FBQ0gsU0FGRDtBQUdIOztBQUVELFVBQUkrSCxPQUFPLEdBQUdqSSxRQUFRLENBQUMwTixjQUFULENBQXdCLE1BQXhCLENBQWQ7QUFDQSxVQUFJQyxLQUFLLEdBQUcxRixPQUFPLENBQUMyRixzQkFBUixDQUErQixPQUEvQixFQUF3QyxDQUF4QyxDQUFaOztBQUNBLFVBQUlELEtBQUssSUFBSUEsS0FBSyxDQUFDN0wsU0FBTixDQUFnQnlMLFFBQWhCLENBQXlCLGdCQUF6QixDQUFiLEVBQXlEO0FBQ3JELFlBQUlNLFNBQVMsR0FBR0MsUUFBUSxDQUFDSCxLQUFLLENBQUNJLFlBQU4sQ0FBbUIsWUFBbkIsQ0FBRCxDQUF4Qjs7QUFDQSxZQUFJRixTQUFKLEVBQWU7QUFDWCxjQUFJRyxXQUFXLEdBQUdMLEtBQUssQ0FBQ0ksWUFBTixDQUFtQixpQkFBbkIsQ0FBbEI7QUFDQSxjQUFJRSxLQUFLLEdBQUdOLEtBQUssQ0FBQ08sb0JBQU4sQ0FBMkIsT0FBM0IsRUFBb0MsQ0FBcEMsQ0FBWjtBQUNBLGNBQUlDLFNBQVMsR0FBR0YsS0FBSyxDQUFDQyxvQkFBTixDQUEyQixJQUEzQixFQUFpQyxDQUFqQyxDQUFoQjtBQUNBLGNBQUlFLEtBQUssR0FBR1QsS0FBSyxDQUFDTyxvQkFBTixDQUEyQixPQUEzQixFQUFvQyxDQUFwQyxDQUFaO0FBQ0EsY0FBSUcsU0FBUyxHQUFHRCxLQUFLLENBQUNGLG9CQUFOLENBQTJCLElBQTNCLENBQWhCO0FBQ0EsY0FBSUksU0FBUyxHQUFHLElBQWhCLENBTlcsQ0FNVzs7QUFDdEIsY0FBSUMsYUFBYSxHQUFHLElBQXBCLENBUFcsQ0FPZTs7QUFDMUIsY0FBSUMsV0FBVyxHQUFHLElBQWxCLENBUlcsQ0FRYTs7QUFDeEIsY0FBSUMsTUFBSixDQVRXLENBU0M7O0FBQ1osY0FBSUMsUUFBSixDQVZXLENBVUc7QUFFZDs7QUFDQSxlQUFLLElBQUlDLEdBQVQsSUFBZ0JOLFNBQWhCLEVBQTJCO0FBQ3ZCLGdCQUFJQSxTQUFTLENBQUNPLGNBQVYsQ0FBeUJELEdBQXpCLENBQUosRUFBbUM7QUFDL0Isa0JBQU1FLEVBQUUsR0FBRywwQkFDUCxnREFETyxHQUVQLE9BRko7QUFHQVIsdUJBQVMsQ0FBQ00sR0FBRCxDQUFULENBQWVHLGtCQUFmLENBQWtDLFdBQWxDLEVBQStDRCxFQUEvQztBQUNIO0FBQ0o7O0FBRUQsY0FBTUUsRUFBRSxHQUFHLFdBQVg7QUFDQVosbUJBQVMsQ0FBQ1csa0JBQVYsQ0FBNkIsV0FBN0IsRUFBMENDLEVBQTFDO0FBRUFwTCxXQUFDLENBQUN5SyxLQUFELENBQUQsQ0FBU1ksUUFBVCxDQUFrQjtBQUNkQyxrQkFBTSxFQUFFLHNCQURNO0FBRWRDLGlCQUFLLEVBQUUsU0FBU0MsZUFBVCxDQUF5QjlHLENBQXpCLEVBQTRCK0csRUFBNUIsRUFBZ0M7QUFDbkNkLHVCQUFTLEdBQUdjLEVBQUUsQ0FBQ0MsSUFBSCxDQUFRLENBQVIsQ0FBWjtBQUNBWCxzQkFBUSxHQUFHSixTQUFTLENBQUNQLFlBQVYsQ0FBdUIsU0FBdkIsQ0FBWDtBQUNBVSxvQkFBTSxHQUFHSCxTQUFTLENBQUNnQixVQUFuQjtBQUNBZiwyQkFBYSxHQUFHbkQsS0FBSyxDQUFDb0MsU0FBTixDQUFnQitCLE9BQWhCLENBQXdCQyxJQUF4QixDQUE2QmYsTUFBTSxDQUFDN0ssUUFBcEMsRUFBOEMwSyxTQUE5QyxDQUFoQjtBQUNILGFBUGE7QUFRZG1CLGdCQUFJLEVBQUUsY0FBVXBILENBQVYsRUFBYStHLEVBQWIsRUFBaUI7QUFDbkJaLHlCQUFXLEdBQUdwRCxLQUFLLENBQUNvQyxTQUFOLENBQWdCK0IsT0FBaEIsQ0FBd0JDLElBQXhCLENBQTZCZixNQUFNLENBQUM3SyxRQUFwQyxFQUE4Q3dMLEVBQUUsQ0FBQ0MsSUFBSCxDQUFRLENBQVIsQ0FBOUMsQ0FBZDs7QUFDQSxrQkFBSWQsYUFBYSxLQUFLQyxXQUF0QixFQUFtQztBQUUvQixvQkFBSWtCLEdBQUcsR0FBRyxJQUFJQyxjQUFKLEVBQVY7QUFDQUQsbUJBQUcsQ0FBQ0UsSUFBSixDQUFTLEtBQVQsRUFBaUJDLFNBQVMsQ0FBQyxXQUFXN0IsV0FBWCxHQUF5QixHQUF6QixHQUErQlUsUUFBL0IsR0FBMEMsR0FBMUMsR0FBZ0RGLFdBQWpELENBQTFCOztBQUNBa0IsbUJBQUcsQ0FBQ0ksTUFBSixHQUFhLFlBQVk7QUFDckIsc0JBQUlKLEdBQUcsQ0FBQ2pJLE1BQUosS0FBZSxHQUFuQixFQUF3QjtBQUNwQnNJLHlCQUFLLENBQUMseUVBQUQsQ0FBTDtBQUNIO0FBQ0osaUJBSkQ7O0FBS0FMLG1CQUFHLENBQUNNLElBQUo7QUFDSDtBQUVKO0FBdEJhLFdBQWxCO0FBd0JBck0sV0FBQyxDQUFDeUssS0FBRCxDQUFELENBQVM2QixnQkFBVDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEdBcEUwQjs7QUFzRTNCO0FBQ0o7QUFDQTtBQUNBO0FBQ0lDLE1BQUksRUFBRSxnQkFBWTtBQUNkLFNBQUs1Qyx1QkFBTDtBQUNBLFdBQU8sSUFBUDtBQUNIO0FBN0UwQixDQUEvQjtBQWdGQTNKLENBQUMsQ0FBQzNELFFBQUQsQ0FBRCxDQUFZMEcsS0FBWixDQUFrQixZQUFZO0FBQzFCRixTQUFPLENBQUNDLEdBQVIsQ0FBWSxZQUFaO0FBQ0E0Ryx3QkFBc0IsQ0FBQzZDLElBQXZCO0FBQ0gsQ0FIRCxFOzs7Ozs7Ozs7Ozs7QUNsRkEsdUM7Ozs7Ozs7Ozs7O0FDQUEsdUM7Ozs7Ozs7Ozs7O0FDQUEsdUM7Ozs7Ozs7Ozs7O0FDQUEsdUMiLCJmaWxlIjoiYWRtaW5fY3VzdG9tLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICdAdXBweS9jb3JlL2Rpc3Qvc3R5bGUuY3NzJztcbmltcG9ydCAnQHVwcHkvZGFzaGJvYXJkL2Rpc3Qvc3R5bGUuY3NzJztcbmltcG9ydCBVcHB5IGZyb20gJ0B1cHB5L2NvcmUnO1xuaW1wb3J0IERhc2hib2FyZCBmcm9tICdAdXBweS9kYXNoYm9hcmQnO1xuaW1wb3J0IEdvb2dsZURyaXZlIGZyb20gJ0B1cHB5L2dvb2dsZS1kcml2ZSc7XG5pbXBvcnQgRHJvcGJveCBmcm9tICdAdXBweS9kcm9wYm94JztcbmltcG9ydCBJbnN0YWdyYW0gZnJvbSAnQHVwcHkvaW5zdGFncmFtJztcbmltcG9ydCBXZWJjYW0gZnJvbSAnQHVwcHkvd2ViY2FtJztcbmltcG9ydCBYSFJVcGxvYWQgZnJvbSAnQHVwcHkveGhyLXVwbG9hZCc7XG5pbXBvcnQge2dldEltYWdlQnlJZEFwaSwgZ2V0VXBsb2FkZWRJbWFnZXNBcGksIHJlbW92ZUltZ0J5SWRBcGl9IGZyb20gXCIuL2FwaS9hZG1pbi5hcGlcIjtcbmltcG9ydCB7ZGVmYXVsdEVycm9yTXNnLCBtZXNzYWdlVHlwZSwgc2hvd01lc3NhZ2V9IGZyb20gXCIuL3NlcnZpY2VzL3V0aWxpdGllc1wiO1xuaW1wb3J0IHNob3dEZWZhdWx0SW1hZ2VNb2RhbCBmcm9tIFwiLi9oZWxwZXJzL3Nob3dEZWZhdWx0SW1hZ2VNb2RhbFwiO1xuXG5jb25zdCBhZGRJbWFnZXNUb0dhbGxlcnkgPSAoaW1hZ2VzQXJyLCB0YXJnZXQpID0+IHtcbiAgICBsZXQgaW1hZ2VzSHRtbCA9IGRvY3VtZW50LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKTtcbiAgICBpZiAoaW1hZ2VzQXJyPy5sZW5ndGgpIHtcbiAgICAgICAgaW1hZ2VzQXJyLmZvckVhY2goaW1hZ2UgPT4ge1xuICAgICAgICAgICAgY29uc3Qge3NyYywgaWQsIHRpdGxlfSA9IGltYWdlO1xuICAgICAgICAgICAgY29uc3QgaW1nRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICBjb25zdCByZW1vdmUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG4gICAgICAgICAgICBjb25zdCBpbWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKTtcblxuICAgICAgICAgICAgaW1nRGl2LmNsYXNzTmFtZSA9ICdpbWFnZS0nICsgaWQ7XG5cbiAgICAgICAgICAgIHJlbW92ZS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSgnw5cnKSk7XG4gICAgICAgICAgICByZW1vdmUuY2xhc3NOYW1lID0gJ2FkbWluR2FsbGVyeVJlbW92ZSc7XG4gICAgICAgICAgICByZW1vdmUuc2V0QXR0cmlidXRlKCdocmVmJywgJyMnKTtcbiAgICAgICAgICAgIHJlbW92ZS5zZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnLCBpZCk7XG5cbiAgICAgICAgICAgIHJlbW92ZS5vbmNsaWNrID0gKCkgPT4ge1xuICAgICAgICAgICAgICAgIHJlbW92ZUltZ0J5SWRBcGkoaWQpLnRoZW4ocmVtb3ZlZCA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZW1vdmVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaW1hZ2UtJyArIGlkKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ29wdGlvblt2YWx1ZT1cIicgKyBpZCArICdcIl0nKS5yZW1vdmUoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgc2hvd01lc3NhZ2UobWVzc2FnZVR5cGUuc3VjY2VzcywgJ0ZpbGUgU3VjY2Vzc2Z1bGx5IERlbGV0ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3dNZXNzYWdlKG1lc3NhZ2VUeXBlLmVycm9yLCBkZWZhdWx0RXJyb3JNc2cpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaW1nLm9uY2xpY2sgPSAoKSA9PiBnZXRJbWFnZUJ5SWRBcGkoaWQpLnRoZW4ocmVzID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzPy5pZCkge1xuICAgICAgICAgICAgICAgICAgICBzaG93RGVmYXVsdEltYWdlTW9kYWwocmVzKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzaG93TWVzc2FnZShtZXNzYWdlVHlwZS5lcnJvciwgZGVmYXVsdEVycm9yTXNnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgbGV0IHAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgICAgICBwLmNsYXNzTGlzdC5hZGQoJ2ltZ1RpdGxlJyk7XG4gICAgICAgICAgICBwLnRleHRDb250ZW50ID0gdGl0bGU7XG5cbiAgICAgICAgICAgIGltZ0Rpdi5hcHBlbmRDaGlsZChwKTtcbiAgICAgICAgICAgIGltZy5zZXRBdHRyaWJ1dGUoJ3NyYycsIHNyYyk7XG4gICAgICAgICAgICBpbWdEaXYuYXBwZW5kQ2hpbGQocmVtb3ZlKTtcbiAgICAgICAgICAgIGltZ0Rpdi5hcHBlbmRDaGlsZChpbWcpO1xuICAgICAgICAgICAgaW1hZ2VzSHRtbC5hcHBlbmRDaGlsZChpbWdEaXYpXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgdGFyZ2V0LmFwcGVuZChpbWFnZXNIdG1sKTtcbn1cblxuY29uc3QgSW5zZXJ0QWxyZWFkeVVwbG9hZGVkSW1hZ2VzID0gYXN5bmMgKHNlbGVjdCwgdGFyZ2V0KSA9PiB7XG4gICAgY29uc3QgYWxyZWFkeVVwbG9hZGVkSW1hZ2VzID0gc2VsZWN0LnF1ZXJ5U2VsZWN0b3JBbGwoJ29wdGlvbltzZWxlY3RlZF0nKTtcbiAgICBjb25zdCB1cGxvYWRlZEltYWdlcyA9IFtdO1xuXG4gICAgYWxyZWFkeVVwbG9hZGVkSW1hZ2VzPy5mb3JFYWNoKGZpbGUgPT4gZmlsZS52YWx1ZSAmJiB1cGxvYWRlZEltYWdlcy5wdXNoKHtcbiAgICAgICAgaWQ6IE51bWJlcihmaWxlLnZhbHVlKSxcbiAgICAgICAgc3JjOiBmaWxlLmlubmVyVGV4dFxuICAgIH0pKTtcblxuICAgIGlmICh1cGxvYWRlZEltYWdlcy5sZW5ndGgpIHtcbiAgICAgICAgYXdhaXQgZ2V0VXBsb2FkZWRJbWFnZXNBcGkoW3VwbG9hZGVkSW1hZ2VzLm1hcChpbWdPYmogPT4gaW1nT2JqLmlkKV0pXG4gICAgICAgICAgICAudGhlbihyZXMgPT4ge1xuICAgICAgICAgICAgICAgIHJlcz8ubGVuZ3RoICYmIHJlcy5mb3JFYWNoKGltZyA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHVwbG9hZGVkSW1hZ2VzLmZpbHRlcihvYmogPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9iai5pZCA9PT0gaW1nLmlkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JqLnRpdGxlID0gaW1nLnRpdGxlIHx8ICcnXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgIGFkZEltYWdlc1RvR2FsbGVyeSh1cGxvYWRlZEltYWdlcywgdGFyZ2V0KVxuICAgIH1cbn1cblxuY29uc3QgVXBweUluaXQgPSAoe1xuICAgICAgICAgICAgICAgICAgICAgIG1heEZpbGVTaXplID0gMjEwMDAwMCxcbiAgICAgICAgICAgICAgICAgICAgICBtYXhOdW1iZXJPZkZpbGVzID0gMTAsXG4gICAgICAgICAgICAgICAgICAgICAgbWluTnVtYmVyT2ZGaWxlcyA9IDEsXG4gICAgICAgICAgICAgICAgICAgICAgYWxsb3dlZEZpbGVUeXBlcyA9IFsnaW1hZ2UvKiddLFxuICAgICAgICAgICAgICAgICAgICAgIHVwbG9hZFVSTCA9ICcvZ2FsbGVyaWVzL2ZpbGVzJyxcbiAgICAgICAgICAgICAgICAgICAgICBub3RlID0gJ0ltYWdlcyBvbmx5LCAx4oCTMTAgZmlsZXMsIHVwIHRvIDIgTUInXG4gICAgICAgICAgICAgICAgICB9KSA9PiB7XG5cbiAgICBjb25zdCB1cHB5U2VsZWN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihgc2VsZWN0W2RhdGEtZ2FsbGVyeT0ndXBweSddYCk7XG5cbiAgICBpZiAodXBweVNlbGVjdCkge1xuICAgICAgICBjb25zdCBmb3JtR3JvdXBEaXYgPSB1cHB5U2VsZWN0LmNsb3Nlc3QoJy5mb3JtLWdyb3VwJyk7XG4gICAgICAgICQoZm9ybUdyb3VwRGl2KS5jaGlsZHJlbigpLmhpZGUoKVxuICAgICAgICBjb25zdCB1cGxvYWRlZEltYWdlc0RpdiA9ICQoJzxkaXYgY2xhc3M9XCJhZG1pbkdhbGxlcnlcIj48L2Rpdj4nKTtcbiAgICAgICAgJChmb3JtR3JvdXBEaXYpLmFwcGVuZCh1cGxvYWRlZEltYWdlc0Rpdik7XG5cbiAgICAgICAgSW5zZXJ0QWxyZWFkeVVwbG9hZGVkSW1hZ2VzKHVwcHlTZWxlY3QsIHVwbG9hZGVkSW1hZ2VzRGl2KTtcblxuICAgICAgICBjb25zdCB1cCA9IFVwcHkoe1xuICAgICAgICAgICAgZGVidWc6IHRydWUsXG4gICAgICAgICAgICBhdXRvUHJvY2VlZDogZmFsc2UsXG4gICAgICAgICAgICBhbGxvd011bHRpcGxlVXBsb2FkczogZmFsc2UsXG4gICAgICAgICAgICByZXN0cmljdGlvbnM6IHtcbiAgICAgICAgICAgICAgICBtYXhGaWxlU2l6ZSxcbiAgICAgICAgICAgICAgICBtYXhOdW1iZXJPZkZpbGVzLFxuICAgICAgICAgICAgICAgIG1pbk51bWJlck9mRmlsZXMsXG4gICAgICAgICAgICAgICAgYWxsb3dlZEZpbGVUeXBlc1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG1ldGE6IHtcbiAgICAgICAgICAgICAgICBpc01haW46IGZhbHNlXG4gICAgICAgICAgICB9LFxuICAgICAgICB9KVxuICAgICAgICAgICAgLnVzZShEYXNoYm9hcmQsIHtcbiAgICAgICAgICAgICAgICB0cmlnZ2VyOiAnLlVwcHlNb2RhbE9wZW5lckJ0bicsXG4gICAgICAgICAgICAgICAgaW5saW5lOiB0cnVlLFxuICAgICAgICAgICAgICAgIHRhcmdldDogZm9ybUdyb3VwRGl2LFxuICAgICAgICAgICAgICAgIHJlcGxhY2VUYXJnZXRDb250ZW50OiBmYWxzZSxcbiAgICAgICAgICAgICAgICBzaG93UHJvZ3Jlc3NEZXRhaWxzOiB0cnVlLFxuICAgICAgICAgICAgICAgIG5vdGUsXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0NzAsXG4gICAgICAgICAgICAgICAgdGh1bWJuYWlsV2lkdGg6IDI4MCxcbiAgICAgICAgICAgICAgICBzaG93U2VsZWN0ZWRGaWxlczogdHJ1ZSxcbiAgICAgICAgICAgICAgICBicm93c2VyQmFja0J1dHRvbkNsb3NlOiB0cnVlXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnVzZShHb29nbGVEcml2ZSwge3RhcmdldDogRGFzaGJvYXJkLCBjb21wYW5pb25Vcmw6ICdodHRwczovL2NvbXBhbmlvbi51cHB5LmlvJ30pXG4gICAgICAgICAgICAudXNlKERyb3Bib3gsIHt0YXJnZXQ6IERhc2hib2FyZCwgY29tcGFuaW9uVXJsOiAnaHR0cHM6Ly9jb21wYW5pb24udXBweS5pbyd9KVxuICAgICAgICAgICAgLnVzZShJbnN0YWdyYW0sIHt0YXJnZXQ6IERhc2hib2FyZCwgY29tcGFuaW9uVXJsOiAnaHR0cHM6Ly9jb21wYW5pb24udXBweS5pbyd9KVxuICAgICAgICAgICAgLnVzZShXZWJjYW0sIHtcbiAgICAgICAgICAgICAgICB0YXJnZXQ6IERhc2hib2FyZCxcbiAgICAgICAgICAgICAgICB0aXRsZTogJ0NhbWVyYSdcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudXNlKFhIUlVwbG9hZCwge2VuZHBvaW50OiB1cGxvYWRVUkwsIG1ldGhvZDogJ1BPU1QnLCBmb3JtRGF0YTogdHJ1ZSwgZmllbGROYW1lOiAnZmlsZXNbXSd9KVxuXG4gICAgICAgIHVwLm9uKCdjb21wbGV0ZScsIHVwbG9hZHMgPT4ge1xuICAgICAgICAgICAgY29uc3QgdXBsb2Fkc0FyciA9IFtdO1xuICAgICAgICAgICAgaWYgKHVwbG9hZHMuc3VjY2Vzc2Z1bC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBsZXQgb3B0aW9uc0h0bWxTdHIgPSAnJztcblxuICAgICAgICAgICAgICAgIHVwbG9hZHMuc3VjY2Vzc2Z1bC5mb3JFYWNoKGtleSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVsZW1JZCA9IGtleS5yZXNwb25zZS5ib2R5LmlkO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBkb3dubG9hZExpbmsgPSBrZXkucmVzcG9uc2UuYm9keS5kb3dubG9hZF9saW5rO1xuXG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbnNIdG1sU3RyICs9IGA8b3B0aW9uIHZhbHVlPSR7ZWxlbUlkfSBzZWxlY3RlZD1cInNlbGVjdGVkXCI+JHtkb3dubG9hZExpbmt9PC9vcHRpb24+YDtcblxuICAgICAgICAgICAgICAgICAgICB1cGxvYWRzQXJyLnB1c2goe2lkOiBlbGVtSWQsIHNyYzogZG93bmxvYWRMaW5rfSk7XG4gICAgICAgICAgICAgICAgICAgIHVwLnJlbW92ZUZpbGUoa2V5LmlkKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIGlmICh1cGxvYWRzQXJyLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAvL1B1c2hpbmcgaGlkZGVuIG9wdGlvbnMgdG8gaGlkZGVuIHVwcHkgc2VsZWN0IHRvIHNhdmUgdXBsb2FkZWQgZmlsZXMgb24gc3VibWl0LlxuICAgICAgICAgICAgICAgICAgICAkKHVwcHlTZWxlY3QpLmFwcGVuZChvcHRpb25zSHRtbFN0cik7XG5cbiAgICAgICAgICAgICAgICAgICAgYWRkSW1hZ2VzVG9HYWxsZXJ5KHVwbG9hZHNBcnIsIHVwbG9hZGVkSW1hZ2VzRGl2KTtcblxuICAgICAgICAgICAgICAgICAgICBzaG93TWVzc2FnZShtZXNzYWdlVHlwZS5zdWNjZXNzLCBgRmlsZSR7dXBsb2Fkc0Fyci5sZW5ndGggPiAxID8gJ3MnIDogJyd9IFN1Y2Nlc3NmdWxseSBBZGRlZGApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBVcHB5SW5pdDsiLCJpbXBvcnQgJy4uL3N0eWxlL2FkbWluL2FkbWluLnNjc3MnO1xuaW1wb3J0ICcuLi9zdHlsZS9hZG1pbi9nbG9iYWwuc2Nzcyc7XG5pbXBvcnQgVXBweUluaXQgZnJvbSBcIi4vVXBweVwiO1xuaW1wb3J0IHtnZXRMaW5rQWN0aW9uLCBsaW5rQWN0aW9ufSBmcm9tIFwiLi9zZXJ2aWNlcy91dGlsaXRpZXMuanNcIjtcbmltcG9ydCBoYW5kbGVBcnRpY2xlVGFiIGZyb20gXCIuL3RhYnMvY29udGVudC9jb250ZW50LkFydGljbGVcIjtcblxucmVxdWlyZSgnLi4vc3R5bGUvYWRtaW4vc29ydGFibGUuc2NzcycpO1xucmVxdWlyZSgnLi91aS1zb3J0YWJsZScpO1xuXG5jb25zb2xlLmxvZyhkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWxlY3RbYXJ0aWNsZS1pbWFnZS11cGxvYWRdJykpO1xuLyogdG9kbyBsaXN0IHNsaWRlciAqL1xuJChkb2N1bWVudCkucmVhZHkoKCkgPT4ge1xuXG4gICAgLyogTG9hZCBVcHB5IGlmIHVybCBhY3Rpb24gaXMgZWRpdCBhbmQgdXBweSBzZWxlY3QgZXhpc3RcbiAgICAgICAgKiAqL1xuICAgIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBzZWxlY3RbZGF0YS1nYWxsZXJ5PSd1cHB5J11gKSkgVXBweUluaXQoe30pO1xuXG5cbiAgICAvKlxuICAgICogSGFuZGxlIEFydGljbGUgdGFiIHNjcmlwdFxuICAgICogIUlNUE9SVEFOVDogVGhpcyBjYWxsZWQgb25seSBvbiBlZGl0IGFjdGlvbiBmb3Igbm93LlxuICAgICogKi9cbiAgICBjb25zb2xlLmxvZyhkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWxlY3RbYXJ0aWNsZS1pbWFnZS11cGxvYWRdJykpO1xuICAgIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWxlY3RbYXJ0aWNsZS1pbWFnZS11cGxvYWRdJykpIGhhbmRsZUFydGljbGVUYWIoKTtcblxufSk7XG5cbi8vdG9kb1xuLy8gJChkb2N1bWVudCkub24oJ2NoYW5nZScsICcuY3VzdG9tLWZpbGUtaW5wdXQnLCBmdW5jdGlvbiAodGFyZ2V0KSB7XG4vLyAgICAgdmFyIGlucHV0ID0gJCh0aGlzKSxcbi8vICAgICAgICAgbnVtRmlsZXMgPSBpbnB1dC5nZXQoMCkuZmlsZXMgPyBpbnB1dC5nZXQoMCkuZmlsZXMubGVuZ3RoIDogMSxcbi8vICAgICAgICAgbGFiZWwgPSBpbnB1dC52YWwoKS5yZXBsYWNlKC9cXFxcL2csICcvJykucmVwbGFjZSgvLipcXC8vLCAnJyk7XG4vLyAgICAgaW5wdXQudHJpZ2dlcignZmlsZXNlbGVjdCcsIFtsYWJlbCwgbnVtRmlsZXNdKTtcbi8vICAgICAvKiovXG4vLyAgICAgdGFyZ2V0ID0gaW5wdXQuZ2V0KDApLmZpbGVzWzBdID8gaW5wdXQuZ2V0KDApLmZpbGVzWzBdLm5hbWUgOiAnICc7XG4vL1xuLy8gICAgIHRhcmdldCA9IHRhcmdldC5sZW5ndGggPiAxNSA/IGAke3RhcmdldC5zdWJzdHJpbmcoMCwgMTUpfS4uLmAgOiB0YXJnZXQ7XG4vL1xuLy8gICAgICQoaW5wdXQpLnBhcmVudCgpLmZpbmQoJ2xhYmVsJykuaHRtbCh0YXJnZXQpO1xuLy8gfSk7XG5cblxuLyogdG9kbyBvZGVzc2kgZGVzaWduICovXG4vLyB0b2RvIGNoZWNrIHdpdGggVGlrbyBpZiB3ZSBuZWVkIGJlbG93IGNvZGVcbi8vICQoJy5hcnRpY2xlX3dpZGdldC5mb3JtLWdyb3VwJykucmVtb3ZlQ2xhc3MoJ2Zvcm0tZ3JvdXAnKTtcbi8vICQoJy5hZGRyZXNzX3dpZGdldC5mb3JtLWdyb3VwJykucmVtb3ZlQ2xhc3MoJ2Zvcm0tZ3JvdXAnKTtcbi8vICQoJy5nYWxsZXJ5X3dpZGdldC5mb3JtLWdyb3VwJykucmVtb3ZlQ2xhc3MoJ2Zvcm0tZ3JvdXAnKTtcbi8vICQoJy5idXNpbmVzc193aWRnZXQuZm9ybS1ncm91cCcpLnJlbW92ZUNsYXNzKCdmb3JtLWdyb3VwJyk7XG4vL1xuXG5zZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm5heCcpKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdkc2FkYScpO1xuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubmF4JykuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKS5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJ1xuICAgIH1cbn0pO1xuIiwiaW1wb3J0IHtEZWxldGUsIEdldCwgSGFuZGxlUmVzcG9uc2VTdGF0ZSwgUG9zdH0gZnJvbSAnLi4vc2VydmljZXMvSHR0cC5DbGllbnQnO1xuaW1wb3J0IHtkZWZhdWx0RXJyb3JNc2csIGhhbmRsZVJlc3BvbnNlfSBmcm9tIFwiLi4vc2VydmljZXMvdXRpbGl0aWVzXCI7XG5cbi8qXG4qIE1haW4gTWVudSBzdGFydFxuKiAqL1xuZXhwb3J0IGNvbnN0IGdldEltYWdlQnlJZEFwaSA9IChpZCkgPT5cbiAgICBHZXQoYC9pbWFnZS9pbmZvLyR7aWR9YCwge30pXG4gICAgICAgIC50aGVuKEhhbmRsZVJlc3BvbnNlU3RhdGUpO1xuXG5leHBvcnQgY29uc3QgcmVtb3ZlSW1nQnlJZEFwaSA9IChpZCkgPT5cbiAgICBEZWxldGUoYC9nYWxsZXJpZXMvJHtpZH1gKVxuICAgICAgICAudGhlbihIYW5kbGVSZXNwb25zZVN0YXRlKVxuXG5leHBvcnQgY29uc3QgdXBkYXRlSW1nRGF0YUFwaSA9IChkYXRhKSA9PlxuICAgIGZldGNoKCcvZ2FsbGVyaWVzL2ltYWdlcycsIHtcbiAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGRhdGEpLnRvU3RyaW5nKClcbiAgICB9KS50aGVuKHJlcyA9PiByZXMub2sgJiYgcmVzLnN0YXR1cyA9PT0gMjAwKTtcblxuZXhwb3J0IGNvbnN0IGdldFVwbG9hZGVkSW1hZ2VzQXBpID0gKGRhdGEpID0+XG4gICAgUG9zdCgnL3VwbG9hZGVkL2ltYWdlcycsIHt9LCBkYXRhLCB0cnVlKVxuICAgICAgICAudGhlbihIYW5kbGVSZXNwb25zZVN0YXRlKVxuXG4vKlxuKiBNYWluIE1lbnUgZW5kXG4qICovXG5cblxuLypcbiogQ2xpZW50cyBQZXJzb25hbCBzdGFydFxuKiAqL1xuZXhwb3J0IGNvbnN0IGFkZE5ld01lc3NhZ2VBcGkgPSAoZGF0YSkgPT5cbiAgICBQb3N0KCcvbWVzc2FnZS9uZXcnLCB7fSwgZGF0YSlcbiAgICAgICAgLnRoZW4ocmVzID0+IGhhbmRsZVJlc3BvbnNlKFxuICAgICAgICAgICAgcmVzLFxuICAgICAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICAgICAgKCkgPT4gISFyZXMuc3VjY2VzcyxcbiAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgJ1lvdXIgbWVzc2FnZSBzdWNjZXNzZnVsbHkgc2F2ZWQuJyxcbiAgICAgICAgICAgIGRlZmF1bHRFcnJvck1zZ1xuICAgICAgICApKTtcbi8qXG4qIENsaWVudHMgUGVyc29uYWwgZW5kXG4qICovXG5cbmV4cG9ydCBjb25zdCB1cGxvYWRJbWFnZSA9IChmaWxlKSA9PiB7XG4gICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcblxuICAgIGZvcm1EYXRhLmFwcGVuZCgnZmlsZXNbXScsIGZpbGUpXG5cbiAgICByZXR1cm4gUG9zdCgnL2dhbGxlcmllcy9maWxlcycsIHt9LCBmb3JtRGF0YSwgdHJ1ZSlcbn1cbiIsImNvbnN0IG1vZGFsQ29tcG9uZW50ID0gKGNvbnRlbnQgPSAnJywgdGl0bGUgPSAnJywgaGFuZGxlT2tDbGljaykgPT4ge1xuICAgIGNvbnN0IG1vZGFsID0gJChgXG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbENvbXBvbmVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsQ29udGVudFwiPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwidGl0bGVcIj4ke3RpdGxlfTwvcD5cbiAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImNsb3NlIGNhbmNlbFwiPng8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudERpdlwiPlxuICAgICAgICAgICAgICAgICAgICAke2NvbnRlbnR9XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImJ1dHRvbnNcIj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgY2FuY2VsXCI+Q2FuY2VsPC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+T2s8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PiBcbiAgICAgICAgPC9kaXY+XG4gICAgYCkuY2xpY2soZSA9PiB7XG4gICAgICAgIGlmKCQoZS50YXJnZXQpLmhhc0NsYXNzKCdidG4tc3VjY2VzcycpICYmIGhhbmRsZU9rQ2xpY2spIHtcblxuICAgICAgICAgICAgaGFuZGxlT2tDbGljaygpLnRoZW4oZG9uZSA9PiBkb25lICYmIG1vZGFsLnJlbW92ZSgpKTtcblxuICAgICAgICB9IGVsc2UgaWYgKCQoZS50YXJnZXQpLmhhc0NsYXNzKCdjYW5jZWwnKSB8fCBlLnRhcmdldCA9PT0gbW9kYWxbMF0pIHtcbiAgICAgICAgICAgIC8vQ2xvc2UgbW9kYWwgb24gY2FuY2VsIG9yIG91dHNpZGUgY2xpY2tcbiAgICAgICAgICAgIG1vZGFsLnJlbW92ZSgpXG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIGNvbnN0IG1vZGFsRXhpc3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubW9kYWxDb21wb25lbnQnKVxuICAgIG1vZGFsRXhpc3QgJiYgbW9kYWxFeGlzdC5yZW1vdmUoKTtcblxuICAgICQoJ2JvZHknKS5hcHBlbmQobW9kYWwpO1xufVxuXG5leHBvcnQgZGVmYXVsdCBtb2RhbENvbXBvbmVudDsiLCJpbXBvcnQge3VwZGF0ZUltZ0RhdGFBcGl9IGZyb20gXCIuLi9hcGkvYWRtaW4uYXBpXCI7XG5pbXBvcnQgbW9kYWxDb21wb25lbnQgZnJvbSBcIi4vbW9kYWxDb21wb25lbnRcIjtcbmltcG9ydCB7ZGVmYXVsdEVycm9yTXNnLCBtZXNzYWdlVHlwZSwgc2hvd01lc3NhZ2V9IGZyb20gXCIuLi9zZXJ2aWNlcy91dGlsaXRpZXNcIjtcblxuY29uc3Qgc2hvd0RlZmF1bHRJbWFnZU1vZGFsID0gKHJlcykgPT4ge1xuICAgIGNvbnN0IHtpZCwgdGl0bGUsIHVybF9saW5rLCBhbHRUZXh0LCBkZXNjcmlwdGlvbiwgaXNfZW5hYmxlZCwgZG93bmxvYWRMaW5rfSA9IHJlcztcblxuICAgIGNvbnN0IG1vZGFsQ29udGVudCA9IGBcbiAgICAgICAgPGRpdiBjbGFzcz1cImdhbGxlcnlNb2RhbFwiPlxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZ2FsbGVyeUFkbWluR3JpZFwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIlwiPlRpdGxlPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIHZhbHVlPVwiJHt0aXRsZSB8fCAnJ31cIiBuYW1lPVwibW9kYWwtaXRlbS10aXRsZVwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJcIj5MaW5rPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIHZhbHVlPVwiJHt1cmxfbGluayB8fCAnJ31cIiBuYW1lPVwibW9kYWwtaXRlbS1saW5rXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIlwiPkFsdCB0ZXh0PC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIHZhbHVlPVwiJHthbHRUZXh0IHx8ICcnfVwiIG5hbWU9XCJtb2RhbC1pdGVtLWFsdC10ZXh0XCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGlkPVwiY2hlY2tib3hcIiBuYW1lPVwibW9kYWwtaXRlbS1pc0VuYWJsZWRcIiAke2lzX2VuYWJsZWQgPyAnY2hlY2tlZCcgOiAnJ30+XG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiY2hlY2tib3hcIj5JcyBlbmFibGVkPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIlwiPkRlc2NyaXB0aW9uPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPHRleHRhcmVhIG5hbWU9XCJtb2RhbC1pdGVtLWRlc2NyaXB0aW9uXCI+JHtkZXNjcmlwdGlvbiB8fCAnJ308L3RleHRhcmVhPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPSR7ZG93bmxvYWRMaW5rIHx8ICcnfSBhbHQ9XCJcIj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgYDtcblxuICAgIGNvbnN0IGhhbmRsZU9rQ2xpY2sgPSAoKSA9PiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICAgICAgICAgICAgJ21vZGFsLWl0ZW0taWQnOiBpZFxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgY29uc3QgaW5wdXRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm1vZGFsQ29udGVudCBpbnB1dCwgLm1vZGFsQ29udGVudCB0ZXh0YXJlYScpO1xuXG4gICAgICAgICAgICBpbnB1dHMuZm9yRWFjaCgga2V5ID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoa2V5LnZhbHVlPy50cmltKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgZGF0YVtrZXkubmFtZV0gPSBrZXkubmFtZSAhPT0gJ21vZGFsLWl0ZW0taXNFbmFibGVkJyA/IGtleS52YWx1ZSA6IGtleS5jaGVja2VkO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGRhdGFba2V5Lm5hbWVdID0gJyc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHVwZGF0ZUltZ0RhdGFBcGkoZGF0YSkudGhlbih1cGRhdGVkID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodXBkYXRlZCkge1xuICAgICAgICAgICAgICAgICAgICBzaG93TWVzc2FnZShtZXNzYWdlVHlwZS5zdWNjZXNzLCAnRmlsZSBTdWNjZXNzZnVsbHkgVXBkYXRlZCcpO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2hvd01lc3NhZ2UobWVzc2FnZVR5cGUuZXJyb3IsIGRlZmF1bHRFcnJvck1zZyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSApXG4gICAgfVxuXG4gICAgbW9kYWxDb21wb25lbnQobW9kYWxDb250ZW50LCAnR2FsbGVyeSBpdGVtJywgIGhhbmRsZU9rQ2xpY2spO1xufVxuXG5leHBvcnQgZGVmYXVsdCBzaG93RGVmYXVsdEltYWdlTW9kYWw7IiwiaW1wb3J0IFwid2hhdHdnLWZldGNoXCI7XG5pbXBvcnQge2RlZmF1bHRFcnJvck1zZywgaXNMb2FkaW5nLCBtZXNzYWdlVHlwZSwgc2hvd01lc3NhZ2V9IGZyb20gXCIuL3V0aWxpdGllc1wiO1xuXG5leHBvcnQgY29uc3QgSGFuZGxlUmVzcG9uc2VTdGF0ZSA9IHJlc3VsdCA9PiByZXN1bHQgPyByZXN1bHQgOiAoKSA9PiB7XG5cdHNob3dNZXNzYWdlKG1lc3NhZ2VUeXBlLmVycm9yLCBkZWZhdWx0RXJyb3JNc2cpO1xuXHRyZXR1cm4gZmFsc2Vcbn07XG5cbmV4cG9ydCBjb25zdCBQb3N0ID0gKHVybCwgaGVhZGVycyA9IHt9LCBib2R5LCBwbGFpbkJvZHkgPSBmYWxzZSkgPT4ge1xuXHRpc0xvYWRpbmcoKTtcblx0aWYgKCFoZWFkZXJzLkFjY2VwdCkge1xuXHRcdGhlYWRlcnMuQWNjZXB0ID0gXCJhcHBsaWNhdGlvbi9qc29uLCAqLypcIjtcblx0fVxuXG5cdHJldHVybiBmZXRjaCh1cmwsIHtcblx0XHRtZXRob2Q6IFwiUE9TVFwiLFxuXHRcdGhlYWRlcnMsXG5cdFx0Ym9keTogcGxhaW5Cb2R5ID8gYm9keSA6IEpTT04uc3RyaW5naWZ5KGJvZHkpLnRvU3RyaW5nKClcblx0fSlcblx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRpZiAoIXJlc3BvbnNlLm9rIHx8IHJlc3BvbnNlLnN0YXR1cyA9PT0gNDAxKSB7XG5cdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcblx0XHR9KVxuXHRcdC5maW5hbGx5KCgpID0+IGlzTG9hZGluZyhmYWxzZSkpXG5cdFx0LmNhdGNoKGRhdGEgPT4ge1xuXHRcdFx0Y29uc29sZS5sb2coXCJFcnJvclwiLCBkYXRhKTtcblx0XHR9KTtcbn07XG5cbmV4cG9ydCBjb25zdCBEZWxldGUgPSAodXJsLCBoZWFkZXJzID0ge30pID0+IHtcblx0aXNMb2FkaW5nKClcblx0aWYgKCFoZWFkZXJzLkFjY2VwdCkge1xuXHRcdGhlYWRlcnMuQWNjZXB0ID0gXCJhcHBsaWNhdGlvbi9qc29uLCAqLypcIjtcblx0fVxuXG5cdHJldHVybiBmZXRjaCh1cmwsIHtcblx0XHRtZXRob2Q6IFwiREVMRVRFXCIsXG5cdFx0aGVhZGVyc1xuXHR9KVxuXHRcdC50aGVuKHJlc3BvbnNlID0+ICEoIXJlc3BvbnNlLm9rIHx8IHJlc3BvbnNlLnN0YXR1cyA9PT0gNDAxKSlcblx0XHQuZmluYWxseSgoKSA9PiBpc0xvYWRpbmcoZmFsc2UpKVxuXHRcdC5jYXRjaChkYXRhID0+IHtcblx0XHRcdGNvbnNvbGUubG9nKFwiRXJyb3JcIiwgZGF0YSk7XG5cdFx0fSk7XG59O1xuXG5leHBvcnQgY29uc3QgR2V0ID0gKHVybCwgaGVhZGVycyA9IHt9KSA9PiB7XG5cdGlzTG9hZGluZygpO1xuXG5cdGlmICghaGVhZGVycy5BY2NlcHQpIHtcblx0XHRoZWFkZXJzLkFjY2VwdCA9IFwiYXBwbGljYXRpb24vanNvbiwgKi8qXCI7XG5cdH1cblxuXHRyZXR1cm4gZmV0Y2godXJsLCB7XG5cdFx0bWV0aG9kOiBcIkdFVFwiLFxuXHRcdGhlYWRlcnNcblx0fSlcblx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRpZiAoIXJlc3BvbnNlLm9rIHx8IHJlc3BvbnNlLnN0YXR1cyA9PT0gNDAxKSB7XG5cdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcblx0XHR9KVxuXHRcdC5maW5hbGx5KCgpID0+IGlzTG9hZGluZyhmYWxzZSkpXG5cdFx0LmNhdGNoKGRhdGEgPT4ge1xuXHRcdFx0Y29uc29sZS5sb2coXCJFcnJvclwiLCBkYXRhKTtcblx0XHR9KTtcbn07XG5cbmV4cG9ydCBjb25zdCBQdXQgPSAodXJsLCBoZWFkZXJzID0ge30sIGJvZHkpID0+IHtcblx0aXNMb2FkaW5nKCk7XG5cdGlmICghaGVhZGVyc1tcIkNvbnRlbnQtVHlwZVwiXSkge1xuXHRcdGhlYWRlcnNbXCJDb250ZW50LVR5cGVcIl0gPSBcImFwcGxpY2F0aW9uL2pzb25cIjtcblx0fVxuXG5cdHJldHVybiBmZXRjaCh1cmwsIHtcblx0XHRtZXRob2Q6IFwiUFVUXCIsXG5cdFx0aGVhZGVycyxcblx0XHRib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KS50b1N0cmluZygpXG5cdH0pXG5cdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0aWYgKCFyZXNwb25zZS5vayB8fCByZXNwb25zZS5zdGF0dXMgPT09IDQwMSkge1xuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiByZXNwb25zZS5qc29uKCk7XG5cdFx0fSlcblx0XHQuZmluYWxseSgoKSA9PiBpc0xvYWRpbmcoZmFsc2UpKVxuXHRcdC5jYXRjaChkYXRhID0+IHtcblx0XHRcdGNvbnNvbGUubG9nKFwiRXJyb3JcIiwgZGF0YSk7XG5cdFx0fSk7XG59O1xuIiwiaW1wb3J0ICcuLi8uLi9zdHlsZS9hZG1pbi9ub3RpZmljYXRpb24uc2Nzcyc7XG5cbmV4cG9ydCBjb25zdCBtZXNzYWdlVHlwZSA9IHtcbiAgICBlcnJvcjogJ2Vycm9yJyxcbiAgICBzdWNjZXNzOiAnc3VjY2VzcycsXG4gICAgd2FybmluZzogJ3dhcm5pbmcnXG59XG5cbmV4cG9ydCBjb25zdCBkZWZhdWx0RXJyb3JNc2cgPSAnU29tZXRoaW5nIHdlbnQgd3JvbmcuJztcblxuZXhwb3J0IGNvbnN0IHVuaXF1ZUlkID0gKCkgPT4ge1xuICAgIGxldCB0ZXh0ID0gXCJcIjtcbiAgICBjb25zdCBwb3NzaWJsZSA9IFwiQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ejAxMjM0NTY3ODlcIjtcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgNTsgaSsrKSB0ZXh0ICs9IHBvc3NpYmxlLmNoYXJBdChNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBwb3NzaWJsZS5sZW5ndGgpKTtcblxuICAgIHJldHVybiB0ZXh0O1xufTtcblxuZXhwb3J0IGNvbnN0IHNob3dNZXNzYWdlID0gKHR5cGUgPSBtZXNzYWdlVHlwZS53YXJuaW5nLCBtZXNzYWdlKSA9PiB7XG4gICAgY29uc3QgZGVmYXVsdE1lc3NhZ2VzRGl2ID0gJCgnI2RlZmF1bHROb3RpZmljYXRpb24nKTtcbiAgICBjb25zdCB1bmlxSWQgPSB1bmlxdWVJZCgpO1xuICAgIGNvbnN0IG1lc3NhZ2VEaXYgPSBgPGRpdiBjbGFzcz1cIiR7dHlwZX1cIiBpZD1cIiR7dW5pcUlkfVwiPjxwPiR7bWVzc2FnZX08L3A+PC9kaXY+YDtcblxuICAgIGlmIChkZWZhdWx0TWVzc2FnZXNEaXYubGVuZ3RoKSB7XG4gICAgICAgIGRlZmF1bHRNZXNzYWdlc0Rpdi5hcHBlbmQobWVzc2FnZURpdilcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKCdib2R5JykuYXBwZW5kKFxuICAgICAgICAgICAgYDxkaXYgaWQ9XCJkZWZhdWx0Tm90aWZpY2F0aW9uXCI+XG4gICAgICAgICAgICAke21lc3NhZ2VEaXZ9XG4gICAgICAgICA8L2Rpdj5gXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgJChgIyR7dW5pcUlkfWApLmZhZGVPdXQoNTAwMCwgJ3N3aW5nJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAkKHRoaXMpLnJlbW92ZSgpO1xuXG4gICAgICAgIGNvbnN0IGRlZmF1bHRNZXNzYWdlc0RpdiA9ICQoJyNkZWZhdWx0Tm90aWZpY2F0aW9uJyk7XG4gICAgICAgIGlmKCFkZWZhdWx0TWVzc2FnZXNEaXYuY2hpbGRyZW4oKS5sZW5ndGgpIHtcbiAgICAgICAgICAgIGRlZmF1bHRNZXNzYWdlc0Rpdi5yZW1vdmUoKVxuICAgICAgICB9XG4gICAgfSlcbn07XG5cbi8qXG4qIEZvciBkZWJ1Z2dpbmcgcHVycG9zZXNcbiogKi9cbmV4cG9ydCBjb25zdCBzdG9wRm9yU2Vjb25kcyA9IChzZWNvbmQgPSAyKSA9PiB7XG4gICAgY29uc3QgZGF0ZSA9IERhdGUubm93KCk7XG4gICAgbGV0IGN1cnJlbnREYXRlID0gbnVsbDtcbiAgICBkbyB7XG4gICAgICAgIGN1cnJlbnREYXRlID0gRGF0ZS5ub3coKTtcbiAgICB9IHdoaWxlIChjdXJyZW50RGF0ZSAtIGRhdGUgPCBzZWNvbmQgKiAxMDAwKTtcbn1cblxuXG4vKlxuKiBIdHRwIHN0YXJ0XG4qICovXG5leHBvcnQgY29uc3QgZ2V0RXJyb3JzRnJvbVJlc3BvbnNlID0gcmVzcG9uc2VEYXRhID0+IHtcbiAgICBpZiAocmVzcG9uc2VEYXRhLm1lc3NhZ2UpIHtcbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgdHlwZW9mIHJlc3BvbnNlRGF0YS5tZXNzYWdlID09PSBcIm9iamVjdFwiICYmXG4gICAgICAgICAgICAhQXJyYXkuaXNBcnJheShyZXNwb25zZURhdGEubWVzc2FnZSlcbiAgICAgICAgKVxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC52YWx1ZXMocmVzcG9uc2VEYXRhLm1lc3NhZ2UpO1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG59O1xuXG5leHBvcnQgY29uc3QgaGFuZGxlUmVzcG9uc2UgPSAocmVzdWx0LCBwcmVwID0gdW5kZWZpbmVkLCBzdWNjZXNzID0gdW5kZWZpbmVkLCBlcnJvciA9IHVuZGVmaW5lZCwgc3VjY2Vzc01lc3NhZ2UgPSB1bmRlZmluZWQsIGVycm9yTWVzc2FnZSA9IHVuZGVmaW5lZCkgPT4ge1xuICAgIGlmIChwcmVwKSBwcmVwKCk7XG4gICAgaWYgKHJlc3VsdCkge1xuICAgICAgICBpZiAocmVzdWx0LnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgIGlmIChzdWNjZXNzTWVzc2FnZSkgc2hvd01lc3NhZ2UobWVzc2FnZVR5cGUuc3VjY2Vzcywgc3VjY2Vzc01lc3NhZ2UpO1xuICAgICAgICAgICAgaWYgKHN1Y2Nlc3MpIHJldHVybiBzdWNjZXNzKHJlc3VsdC5kYXRhKTtcbiAgICAgICAgICAgIHJldHVybiByZXN1bHQuZGF0YTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHNob3dNZXNzYWdlKG1lc3NhZ2VUeXBlLmVycm9yLCBnZXRFcnJvcnNGcm9tUmVzcG9uc2UocmVzdWx0KSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoZXJyb3JNZXNzYWdlKSBzaG93TWVzc2FnZShtZXNzYWdlVHlwZS5lcnJvciwgZXJyb3JNZXNzYWdlKTtcbiAgICAgICAgaWYgKGVycm9yKSByZXR1cm4gZXJyb3IoKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn07XG5cbi8qXG4qIEh0dHAgZW5kXG4qICovXG5cbmV4cG9ydCBjb25zdCBpc0xvYWRpbmcgPSAobG9hZGluZyA9IHRydWUpID0+IHtcbiAgICBjb25zdCBleGlzdHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZ2xvYmFsTG9hZGluZycpO1xuXG4gICAgaWYobG9hZGluZyAmJiAhKGV4aXN0cykpIHtcbiAgICAgICAgY29uc3QgbG9hZGVyID0gJCgnPGRpdiBjbGFzcz1cImdsb2JhbExvYWRpbmdcIj48L2Rpdj4nKVxuICAgICAgICAkKCdib2R5JykuYXBwZW5kKGxvYWRlcik7XG4gICAgfVxuXG4gICAgaWYgKCFsb2FkaW5nICYmIGV4aXN0cykge1xuICAgICAgICAkKCcuZ2xvYmFsTG9hZGluZycpPy5yZW1vdmUoKTtcbiAgICB9XG59O1xuXG5cbi8vR2V0IGFjdGlvbiBrZXkgZnJvbSB1cmxcbmV4cG9ydCBjb25zdCBsaW5rQWN0aW9uID0ge1xuICAgIGVkaXQ6ICdlZGl0JyxcbiAgICBzaG93OiAnZGV0YWlsJyxcbiAgICBtYWluOiAnaW5kZXgnXG59O1xuXG5leHBvcnQgY29uc3QgZ2V0TGlua0FjdGlvbiA9ICgpID0+IHtcbiAgICBjb25zdCB1cmxQYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2gpO1xuICAgIGNvbnN0IGFjdGlvbiA9IHVybFBhcmFtcy5nZXQoJ2NydWRBY3Rpb24nKTtcblxuICAgIHJldHVybiBhY3Rpb24gfHwgZmFsc2U7XG59IiwiaW1wb3J0IHtcbiAgICBkZWZhdWx0RXJyb3JNc2csXG4gICAgZ2V0TGlua0FjdGlvbixcbiAgICBsaW5rQWN0aW9uLFxuICAgIG1lc3NhZ2VUeXBlLFxuICAgIHNob3dNZXNzYWdlXG59IGZyb20gXCIuLi8uLi9zZXJ2aWNlcy91dGlsaXRpZXNcIjtcbmltcG9ydCB7Z2V0SW1hZ2VCeUlkQXBpLCByZW1vdmVJbWdCeUlkQXBpLCB1cGxvYWRJbWFnZX0gZnJvbSBcIi4uLy4uL2FwaS9hZG1pbi5hcGlcIjtcbmltcG9ydCBzaG93RGVmYXVsdEltYWdlTW9kYWwgZnJvbSBcIi4uLy4uL2hlbHBlcnMvc2hvd0RlZmF1bHRJbWFnZU1vZGFsXCI7XG5cbmNvbnN0IGhhbmRsZUFydGljbGVUYWIgPSAoKSA9PiB7XG4gICAgY29uc3QgaGlkZGVuVXBsb2FkZWRJbWFnZVNlbGVjdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3NlbGVjdFthcnRpY2xlLWltYWdlLXVwbG9hZF0nKTtcbiAgICBjb25zdCB1cGxvYWRlZE9wdGlvbiA9IGhpZGRlblVwbG9hZGVkSW1hZ2VTZWxlY3QucXVlcnlTZWxlY3Rvcignb3B0aW9uW3NlbGVjdGVkXScpO1xuXG4gICAgY29uc3QgZm9ybUdyb3VwID0gJChoaWRkZW5VcGxvYWRlZEltYWdlU2VsZWN0KS5jbG9zZXN0KCdkaXYuZm9ybS1ncm91cCcpO1xuXG4gICAgY29uc3QgaW5wdXQgPSAkKCc8aW5wdXQgdHlwZT1cImZpbGVcIiBpZD1cImltYWdlVXBsb2FkSW5wdXRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiLz4nKS5vbignY2hhbmdlJywgKGUpID0+IHtcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRGaWxlID0gZS50YXJnZXQuZmlsZXNbMF07XG4gICAgICAgIGlmIChzZWxlY3RlZEZpbGUpIHtcbiAgICAgICAgICAgIHVwbG9hZEZpZWxkLmZpbmQoJ2ltZycpPy5yZW1vdmUoKTtcbiAgICAgICAgICAgIHVwbG9hZEZpZWxkLmZpbmQoJ3NwYW4ucmVtb3ZlJyk/LnJlbW92ZSgpO1xuXG4gICAgICAgICAgICB1cGxvYWRJbWFnZShzZWxlY3RlZEZpbGUpLnRoZW4ocmVzID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzPy5pZCkge1xuICAgICAgICAgICAgICAgICAgICBhZGRVcGxvYWRlZEltYWdlKHJlcyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2hvd01lc3NhZ2UobWVzc2FnZVR5cGUuZXJyb3IsIGRlZmF1bHRFcnJvck1zZylcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICBjb25zdCB1cGxvYWRGaWVsZCA9ICQoYFxuICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBhcnRpY2xlRWRpdEltYWdlVXBsb2FkRGl2XCI+XG4gICAgICAgICAgICA8bGFiZWwgZm9yPVwiaW1hZ2VVcGxvYWRJbnB1dFwiPkltYWdlPC9sYWJlbD5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXdpZGdldFwiPjwvZGl2PlxuICAgICAgICA8L2Rpdj5gXG4gICAgKTtcblxuICAgIHVwbG9hZEZpZWxkLmZpbmQoJ2Rpdi5mb3JtLXdpZGdldCcpLmFwcGVuZChpbnB1dClcbiAgICB1cGxvYWRGaWVsZC5pbnNlcnRBZnRlcihmb3JtR3JvdXApO1xuXG4gICAgY29uc3QgYWRkVXBsb2FkZWRJbWFnZSA9IChpbWFnZSkgPT4ge1xuICAgICAgICBjb25zdCB7ZG93bmxvYWRfbGluaywgaWR9ID0gaW1hZ2U7XG5cbiAgICAgICAgY29uc3QgaW1nID0gJChgPGltZyBzcmM9XCIke2Rvd25sb2FkX2xpbmt9XCIgYWx0PVwiXCIgaWQ9XCIke2lkfVwiLz5gKS5vbignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgICBnZXRJbWFnZUJ5SWRBcGkoaWQpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZT8uaWQpIHtcbiAgICAgICAgICAgICAgICAgICAgc2hvd0RlZmF1bHRJbWFnZU1vZGFsKHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzaG93TWVzc2FnZShtZXNzYWdlVHlwZS5lcnJvciwgZGVmYXVsdEVycm9yTXNnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29uc3QgcmVtb3ZlID0gJChgPHNwYW4gY2xhc3M9XCJyZW1vdmVcIj7Dlzwvc3Bhbj5gKS5vbignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgICByZW1vdmVJbWdCeUlkQXBpKGlkKS50aGVuKHJlbW92ZWQgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChyZW1vdmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIHVwbG9hZEZpZWxkLmZpbmQoJ2ltZycpLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICB1cGxvYWRGaWVsZC5maW5kKCdzcGFuLnJlbW92ZScpLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICBoaWRkZW5VcGxvYWRlZEltYWdlU2VsZWN0LnF1ZXJ5U2VsZWN0b3IoJ29wdGlvbltzZWxlY3RlZF0nKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgJChpbnB1dCkudmFsKCcnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICB9KTtcblxuICAgICAgICB1cGxvYWRGaWVsZC5hcHBlbmQoaW1nKTtcbiAgICAgICAgdXBsb2FkRmllbGQuYXBwZW5kKHJlbW92ZSk7XG5cbiAgICAgICAgY29uc3QgdXBsb2FkZWRPcHRpb24gPSBoaWRkZW5VcGxvYWRlZEltYWdlU2VsZWN0LnF1ZXJ5U2VsZWN0b3IoJ29wdGlvbltzZWxlY3RlZF0nKTtcblxuICAgICAgICBpZiAodXBsb2FkZWRPcHRpb24pIHtcbiAgICAgICAgICAgIHVwbG9hZGVkT3B0aW9uLnZhbHVlID0gaWQ7XG4gICAgICAgICAgICB1cGxvYWRlZE9wdGlvbi50ZXh0Q29udGVudCA9IGRvd25sb2FkX2xpbms7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKGhpZGRlblVwbG9hZGVkSW1hZ2VTZWxlY3QpLmFwcGVuZChgPG9wdGlvbiBzZWxlY3RlZD1cInNlbGVjdGVkXCIgdmFsdWU9XCIke2lkfVwiPiR7ZG93bmxvYWRfbGlua308L29wdGlvbj5gKVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHVwbG9hZGVkT3B0aW9uKSB7XG4gICAgICAgIGFkZFVwbG9hZGVkSW1hZ2Uoe1xuICAgICAgICAgICAgaWQ6IHVwbG9hZGVkT3B0aW9uLnZhbHVlLFxuICAgICAgICAgICAgJ2Rvd25sb2FkX2xpbmsnOiB1cGxvYWRlZE9wdGlvbi50ZXh0Q29udGVudFxuICAgICAgICB9KVxuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgaGFuZGxlQXJ0aWNsZVRhYjsiLCJpbXBvcnQgJ2pxdWVyeS11aS1zb3J0YWJsZS1ucG0nO1xuXG5jb25zdCBlYXN5YWRtaW5EcmFnbmRyb3BTb3J0ID0ge1xuICAgIGluaXREcmFnZ2FibGVFbnRpdHlSb3dzOiBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdlYScpICYmIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdpbmRleCcpKSB7XG5cbiAgICAgICAgICAgIGlmICghQXJyYXkucHJvdG90eXBlLmxhc3QpIHtcbiAgICAgICAgICAgICAgICBBcnJheS5wcm90b3R5cGUubGFzdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXNbdGhpcy5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgY29udGVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpblwiKTtcbiAgICAgICAgICAgIGxldCB0YWJsZSA9IGNvbnRlbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInRhYmxlXCIpWzBdO1xuICAgICAgICAgICAgaWYgKHRhYmxlICYmIHRhYmxlLmNsYXNzTGlzdC5jb250YWlucygnYWxsb3dfc29ydGFibGUnKSkge1xuICAgICAgICAgICAgICAgIGxldCBkYXRhQ291bnQgPSBwYXJzZUludCh0YWJsZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtY291bnQnKSk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGFDb3VudCkge1xuICAgICAgICAgICAgICAgICAgICBsZXQgZW50aXR5Q2xhc3MgPSB0YWJsZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtY2xhc3MtbmFtZScpO1xuICAgICAgICAgICAgICAgICAgICBsZXQgdGhlYWQgPSB0YWJsZS5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRoZWFkXCIpWzBdO1xuICAgICAgICAgICAgICAgICAgICBsZXQgdGhlYWRSb3dzID0gdGhlYWQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ0clwiKVswXTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRib2R5ID0gdGFibGUuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ0Ym9keVwiKVswXTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRhYmxlUm93cyA9IHRib2R5LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwidHJcIik7XG4gICAgICAgICAgICAgICAgICAgIGxldCBkcmFnU3JjRWwgPSBudWxsOyAvLyB0aGUgb2JqZWN0IGJlaW5nIGRydWdcbiAgICAgICAgICAgICAgICAgICAgbGV0IHN0YXJ0UG9zaXRpb24gPSBudWxsOyAvLyB0aGUgaW5kZXggb2YgdGhlIHJvdyBlbGVtZW50ICgwIHRocm91Z2ggd2hhdGV2ZXIpXG4gICAgICAgICAgICAgICAgICAgIGxldCBlbmRQb3NpdGlvbiA9IG51bGw7IC8vIHRoZSBpbmRleCBvZiB0aGUgcm93IGVsZW1lbnQgYmVpbmcgZHJvcHBlZCBvbiAoMCB0aHJvdWdoIHdoYXRldmVyKVxuICAgICAgICAgICAgICAgICAgICBsZXQgcGFyZW50OyAvLyB0aGUgcGFyZW50IGVsZW1lbnQgb2YgdGhlIGRyYWdnZWQgaXRlbVxuICAgICAgICAgICAgICAgICAgICBsZXQgZW50aXR5SWQ7IC8vIHRoZSBpZCAoa2V5KSBvZiB0aGUgZW50aXR5XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gZHJhdyBzb3J0YWJsZSBpY29uXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IHJvdyBpbiB0YWJsZVJvd3MpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0YWJsZVJvd3MuaGFzT3duUHJvcGVydHkocm93KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRkID0gJzx0ZCBkcmFnZ2FibGU9XCJ0cnVlXCI+JyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8c3Bhbj48aSBjbGFzcz1cImZhcyBmYS1hcnJvd3MtYWx0XCI+PC9pPjwvc3Bhbj4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzwvdGQ+JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWJsZVJvd3Nbcm93XS5pbnNlcnRBZGphY2VudEhUTUwoJ2JlZm9yZWVuZCcsIHRkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRoID0gJzx0aD48L3RoPic7XG4gICAgICAgICAgICAgICAgICAgIHRoZWFkUm93cy5pbnNlcnRBZGphY2VudEhUTUwoJ2JlZm9yZWVuZCcsIHRoKTtcblxuICAgICAgICAgICAgICAgICAgICAkKHRib2R5KS5zb3J0YWJsZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGU6ICd0ZFtkcmFnZ2FibGU9XCJ0cnVlXCJdJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0OiBmdW5jdGlvbiBoYW5kbGVEcmFnU3RhcnQoZSwgdWkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkcmFnU3JjRWwgPSB1aS5pdGVtWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudGl0eUlkID0gZHJhZ1NyY0VsLmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudCA9IGRyYWdTcmNFbC5wYXJlbnROb2RlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0UG9zaXRpb24gPSBBcnJheS5wcm90b3R5cGUuaW5kZXhPZi5jYWxsKHBhcmVudC5jaGlsZHJlbiwgZHJhZ1NyY0VsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBzdG9wOiBmdW5jdGlvbiAoZSwgdWkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbmRQb3NpdGlvbiA9IEFycmF5LnByb3RvdHlwZS5pbmRleE9mLmNhbGwocGFyZW50LmNoaWxkcmVuLCB1aS5pdGVtWzBdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3RhcnRQb3NpdGlvbiAhPT0gZW5kUG9zaXRpb24pIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHhoci5vcGVuKCdHRVQnLCAoZW5jb2RlVVJJKCcvc29ydC8nICsgZW50aXR5Q2xhc3MgKyAnLycgKyBlbnRpdHlJZCArICcvJyArIGVuZFBvc2l0aW9uKSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB4aHIub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHhoci5zdGF0dXMgIT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiQW4gZXJyb3Igb2NjdXJyZWQgd2hpbGUgc29ydGluZy4gUGxlYXNlIHJlZnJlc2ggdGhlIHBhZ2UgYW5kIHRyeSBhZ2Fpbi5cIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeGhyLnNlbmQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICQodGJvZHkpLmRpc2FibGVTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUHJpbWFyeSBBZG1pbiBpbml0aWFsaXphdGlvbiBtZXRob2QuXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLmluaXREcmFnZ2FibGVFbnRpdHlSb3dzKCk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbn07XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICBjb25zb2xlLmxvZygnZHNhZGFzZGFzZCcpO1xuICAgIGVhc3lhZG1pbkRyYWduZHJvcFNvcnQuaW5pdCgpO1xufSk7XG4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9