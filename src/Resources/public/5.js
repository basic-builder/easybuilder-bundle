(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./assets/js/clientForm.js":
/*!*********************************!*\
  !*** ./assets/js/clientForm.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($, jQuery) {/* harmony import */ var core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/web.timers.js */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _uppy_core_dist_style_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @uppy/core/dist/style.css */ "./node_modules/@uppy/core/dist/style.css");
/* harmony import */ var _uppy_core_dist_style_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_uppy_core_dist_style_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _uppy_dashboard_dist_style_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @uppy/dashboard/dist/style.css */ "./node_modules/@uppy/dashboard/dist/style.css");
/* harmony import */ var _uppy_dashboard_dist_style_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_uppy_dashboard_dist_style_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @uppy/xhr-upload */ "./node_modules/@uppy/xhr-upload/lib/index.js");
/* harmony import */ var _uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _uppy_progress_bar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @uppy/progress-bar */ "./node_modules/@uppy/progress-bar/lib/index.js");
/* harmony import */ var _uppy_progress_bar__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_uppy_progress_bar__WEBPACK_IMPORTED_MODULE_8__);







__webpack_require__(/*! uppy/dist/uppy.min.css */ "./node_modules/uppy/dist/uppy.min.css");



var Uppy = __webpack_require__(/*! @uppy/core */ "./node_modules/@uppy/core/lib/index.js");

var DragDrop = __webpack_require__(/*! @uppy/drag-drop */ "./node_modules/@uppy/drag-drop/lib/index.js");




__webpack_require__(/*! select2/dist/js/select2.full.min */ "./node_modules/select2/dist/js/select2.full.min.js");

__webpack_require__(/*! select2/dist/css/select2.css */ "./node_modules/select2/dist/css/select2.css");

__webpack_require__(/*! select2-bootstrap-theme/dist/select2-bootstrap.css */ "./node_modules/select2-bootstrap-theme/dist/select2-bootstrap.css");

__webpack_require__(/*! inputmask */ "./node_modules/inputmask/index.js");
/**
 * translations
 * @private
 */


var _text = {
  ru: {
    'Upload CV': 'Загрузка резюме',
    'Request a Quote': 'Загрузка Quote',
    'remove': 'удалить',
    'uppyError': 'Что-то пошло не так, пожалуйста, перезагрузите страницу и попробуйте еще раз.',
    'Up to 10 MB': 'До 10 МБ',
    'dropHereOr': 'Перетащите файлы сюда'
  },
  en: {
    'Upload CV': 'Upload CV',
    'Request a Quote': 'Request a Quote',
    'remove': 'remove',
    'uppyError': 'Something went wrong, please reload page and try again.',
    'Up to 10 MB': 'Up to 10 MB',
    'dropHereOr': 'Drop Here'
  },
  es: {
    'Upload CV': 'Upload CV',
    'Request a Quote': 'Request a Quote',
    'remove': 'remove',
    'uppyError': 'Something went wrong, please reload page and try again.',
    'Up to 10 MB': 'Up to 10 MB',
    'dropHereOr': 'Drop Here Or'
  }
};
var filesIds = [];
$(document).ready(function (e) {
  /**
   * if flash message exist, clear 6 sec after
   * @type {jQuery|HTMLElement}
   */
  var flashes = $('.flashes');

  if ($(flashes).has($('div')).length) {
    setTimeout(function () {
      $('.flashes').empty();
    }, 6000);
  }
  /**
   * get locale
   * @type {jQuery|undefined|any}
   * @private
   */


  var _locale = $('.languages li.d-none a').attr('class'),
      slug;
  /**
   *  get formData
   */


  $('.requestQuote, .InputFileLabel').on('click', function (e) {
    e.preventDefault();
    startLoader();
    slug = $(this).attr('data-slug');
    getCustomForm(slug, _locale, _text);
  });
  /**
   * cv or quote form open/close
   */

  $('.closeModal').click(function (e) {
    $('.bodyBlack').removeClass('open');
    $('.clientCustomForms').removeClass('open');
    $('.clientCustomFormsData').html('');
  });
  $(document).on('click', '.add-language-collection-widget, .add-areas-of-expertise-widget', function (e) {
    addTag(this);
  });
  $(document).on('click', 'span[data-id]', function () {
    postFileRemove($(this).attr('data-id'), _locale);
  }); // cv or quote form submit

  $(document).on('submit', 'form[name="quote"], form[name="cv"]', function (e) {
    startLoader();
  });
});
/**
 * add new tag form client form
 * @param elem
 * @param allowDelete
 */

function addTag(elem) {
  var allowDelete = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var list = jQuery(jQuery(elem).attr('data-list-selector')); // Try to find the counter of the list or use the length of the list

  var counter = list.data('widget-counter') || list.children().length; // grab the prototype template

  var newWidget = list.attr('data-prototype'); // replace the "__name__" used in the id and name of the prototype
  // with a number that's unique to your emails
  // end name attribute looks like name="contact[emails][2]"

  newWidget = newWidget.replace(/__name__/g, counter); // Increase the counter

  counter++; // And store it, the length cannot be used if deleting widgets is allowed

  list.data('widget-counter', counter); // create a new list element and add it to the list

  var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget);
  newElem.appendTo(list);
  $("select.selected2").select2({
    theme: "bootstrap",
    minimumResultsForSearch: Infinity
  });

  if (allowDelete) {
    addTagFormDeleteLink(newElem);
  }
}
/**
 * create remove tag button for client form
 * @param $tagFormLi
 */


function addTagFormDeleteLink($tagFormLi) {
  var $removeFormButton = $('<button class="collRemove" type="button">×</button>');
  $tagFormLi.append($removeFormButton);
  $removeFormButton.on('click', function (e) {
    $tagFormLi.remove();
  });
}
/**
 * get formData request
 *
 * @param slug
 * @param _locale
 * @param _text
 */


function getCustomForm(slug, _locale, _text) {
  var parentForm = $('.clientCustomForms'),
      parentFormData = $('.clientCustomForms .clientCustomFormsData'),
      parentFormH4 = $('.clientCustomForms h4');
  var xhr = new XMLHttpRequest(),
      url = _locale === 'en' ? "/custom-form/".concat(slug) : "/".concat(_locale, "/custom-form/").concat(slug);
  xhr.open("GET", url);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      $('.message .col-12').html('');
      $(parentFormData).html('');
      $(parentFormH4).html('');
      $('.blackModal').remove();
      var data = JSON.parse(xhr.responseText);
      $('.bodyBlack').addClass('open');
      $(parentForm).addClass('open');

      if (xhr.status === 200) {
        switch (slug) {
          case 'cv':
            $(parentFormH4).html(_text[_locale]['Upload CV']);
            break;

          case 'quote':
            $(parentFormH4).html(_text[_locale]['Request a Quote']);
            break;

          default:
            break;
        }

        $(parentFormData).append(data.form_html); // select 2

        $("select.selected2").select2({
          theme: "bootstrap",
          minimumResultsForSearch: Infinity
        }); // create uppy uploader

        createUppy(_locale); // create form tag for language skills

        if ($('.add-language-collection-widget').length) {
          addTag($('.add-language-collection-widget'), false);
        } // create form tag for area of expertise


        if ($('.add-areas-of-expertise-widget').length) {
          addTag($('.add-areas-of-expertise-widget'), false);
        } // create input mask for phone number


        Inputmask("999 999 9999", {
          placeholder: "",
          clearMaskOnLostFocus: true
        }).mask('.phone');
      } else {
        $(parentFormH4).html(data.message);
      }
    }
  };

  xhr.send();
}
/**
 * remove client form file
 *
 * @param id
 * @param _locale
 */


function postFileRemove(id, _locale) {
  var span = $('span[data-id="' + id + '"]'),
      li = $(span).closest('li');
  startLoader();
  var xhr = new XMLHttpRequest(),
      url = _locale === 'en' ? "/remove-form-image/".concat(id) : "/".concat(_locale, "/remove-form-image/").concat(id);
  xhr.open("POST", url);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      $('.blackModal').remove();

      if (xhr.status === 204) {
        $(li).remove();
      } else {
        $('.uppyErrors').text(_text[_locale]['uppyError']);
      }
    }
  };

  xhr.send();
}
/**
 * loading
 * @param text
 * @param title
 */


function startLoader() {
  var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var loader = '<div class="blackModal" id="blackModal">' + '        <div class="loader"></div>' + '    </div>';
  $('body').append(loader);
}
/**
 * create UPPY uploader
 * @param _locale
 */


function createUppy(_locale) {
  var uploadURL = '/galleries/files';
  var uppy = new Uppy({
    debug: true,
    autoProceed: true,
    locale: {
      strings: {
        dropHereOr: _text[_locale]['dropHereOr']
      }
    },
    logger: debugLogger,
    restrictions: {
      maxFileSize: 10900000,
      maxNumberOfFiles: 20,
      minNumberOfFiles: 1,
      allowedFileTypes: ['image/*', '.pdf', '.doc', '.dot', '.wbk', '.docx', '.docm', '.dotx', '.dotm', '.docb', '.txt', '.rtf', '.7z', '.rar', '.xliff', '.xlz', '.xlf', '.sdlxliff']
    }
  }).use(_uppy_xhr_upload__WEBPACK_IMPORTED_MODULE_7___default.a, {
    endpoint: uploadURL,
    method: 'POST',
    formData: true,
    fieldName: 'files[]'
  }).use(_uppy_progress_bar__WEBPACK_IMPORTED_MODULE_8___default.a, {
    target: '.ProgressBar',
    hideAfterFinish: true
  }).use(DragDrop, {
    note: _text[_locale]['Up to 10 MB'],
    target: '.DashboardContainer',
    endpoint: uploadURL,
    method: 'POST',
    formData: true,
    fieldName: 'files[]'
  }).on('upload-success', onUploadSuccess('.uploaded-files ol', _locale));
}
/**
 * show uploaded file from uppy in client form
 *
 * @param elForUploadedFiles
 * @param _locale
 * @returns {function(...[*]=)}
 */


var onUploadSuccess = function onUploadSuccess(elForUploadedFiles, _locale) {
  return function (file, response) {
    filesIds.push(response.body.id);
    var id = response.body.id;
    var url = response.body.download_link;
    var fileName = file.name;
    var option = document.createElement('option');
    option.val = id;
    option.setAttribute('selected', 'selected');
    option.text = id;
    document.querySelector('#files').appendChild(option);
    var span = document.createElement('span');
    span.setAttribute('data-id', response.body.id);
    span.innerText = _text[_locale]['remove'];
    var a = document.createElement('a');
    a.href = url;
    a.target = '_blank';
    a.appendChild(document.createTextNode(fileName));
    var li = document.createElement('li');
    li.appendChild(a);
    li.appendChild(span);
    document.querySelector(elForUploadedFiles).appendChild(li);
  };
};

var debugLogger = {
  debug: function debug() {
    var debug = console.debug || console.log;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    debug.call.apply(debug, [console, "[Uppy] [dsadas]"].concat(args));
  },
  warn: function warn() {
    var _console;

    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    return (_console = console).warn.apply(_console, ["[Uppy] [WARNING]"].concat(args));
  },
  error: function error() {
    var _console2;

    for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      args[_key3] = arguments[_key3];
    }

    return (_console2 = console).log.apply(_console2, ["[Uppy] [CRITICAL]"].concat(args));
  }
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY2xpZW50Rm9ybS5qcyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwiVXBweSIsIkRyYWdEcm9wIiwiX3RleHQiLCJydSIsImVuIiwiZXMiLCJmaWxlc0lkcyIsIiQiLCJkb2N1bWVudCIsInJlYWR5IiwiZSIsImZsYXNoZXMiLCJoYXMiLCJsZW5ndGgiLCJzZXRUaW1lb3V0IiwiZW1wdHkiLCJfbG9jYWxlIiwiYXR0ciIsInNsdWciLCJvbiIsInByZXZlbnREZWZhdWx0Iiwic3RhcnRMb2FkZXIiLCJnZXRDdXN0b21Gb3JtIiwiY2xpY2siLCJyZW1vdmVDbGFzcyIsImh0bWwiLCJhZGRUYWciLCJwb3N0RmlsZVJlbW92ZSIsImVsZW0iLCJhbGxvd0RlbGV0ZSIsImxpc3QiLCJqUXVlcnkiLCJjb3VudGVyIiwiZGF0YSIsImNoaWxkcmVuIiwibmV3V2lkZ2V0IiwicmVwbGFjZSIsIm5ld0VsZW0iLCJhcHBlbmRUbyIsInNlbGVjdDIiLCJ0aGVtZSIsIm1pbmltdW1SZXN1bHRzRm9yU2VhcmNoIiwiSW5maW5pdHkiLCJhZGRUYWdGb3JtRGVsZXRlTGluayIsIiR0YWdGb3JtTGkiLCIkcmVtb3ZlRm9ybUJ1dHRvbiIsImFwcGVuZCIsInJlbW92ZSIsInBhcmVudEZvcm0iLCJwYXJlbnRGb3JtRGF0YSIsInBhcmVudEZvcm1INCIsInhociIsIlhNTEh0dHBSZXF1ZXN0IiwidXJsIiwib3BlbiIsIm9ucmVhZHlzdGF0ZWNoYW5nZSIsInJlYWR5U3RhdGUiLCJKU09OIiwicGFyc2UiLCJyZXNwb25zZVRleHQiLCJhZGRDbGFzcyIsInN0YXR1cyIsImZvcm1faHRtbCIsImNyZWF0ZVVwcHkiLCJJbnB1dG1hc2siLCJwbGFjZWhvbGRlciIsImNsZWFyTWFza09uTG9zdEZvY3VzIiwibWFzayIsIm1lc3NhZ2UiLCJzZW5kIiwiaWQiLCJzcGFuIiwibGkiLCJjbG9zZXN0IiwidGV4dCIsInRpdGxlIiwibG9hZGVyIiwidXBsb2FkVVJMIiwidXBweSIsImRlYnVnIiwiYXV0b1Byb2NlZWQiLCJsb2NhbGUiLCJzdHJpbmdzIiwiZHJvcEhlcmVPciIsImxvZ2dlciIsImRlYnVnTG9nZ2VyIiwicmVzdHJpY3Rpb25zIiwibWF4RmlsZVNpemUiLCJtYXhOdW1iZXJPZkZpbGVzIiwibWluTnVtYmVyT2ZGaWxlcyIsImFsbG93ZWRGaWxlVHlwZXMiLCJ1c2UiLCJYSFJVcGxvYWQiLCJlbmRwb2ludCIsIm1ldGhvZCIsImZvcm1EYXRhIiwiZmllbGROYW1lIiwiUHJvZ3Jlc3NCYXIiLCJ0YXJnZXQiLCJoaWRlQWZ0ZXJGaW5pc2giLCJub3RlIiwib25VcGxvYWRTdWNjZXNzIiwiZWxGb3JVcGxvYWRlZEZpbGVzIiwiZmlsZSIsInJlc3BvbnNlIiwicHVzaCIsImJvZHkiLCJkb3dubG9hZF9saW5rIiwiZmlsZU5hbWUiLCJuYW1lIiwib3B0aW9uIiwiY3JlYXRlRWxlbWVudCIsInZhbCIsInNldEF0dHJpYnV0ZSIsInF1ZXJ5U2VsZWN0b3IiLCJhcHBlbmRDaGlsZCIsImlubmVyVGV4dCIsImEiLCJocmVmIiwiY3JlYXRlVGV4dE5vZGUiLCJjb25zb2xlIiwibG9nIiwiYXJncyIsImNhbGwiLCJ3YXJuIiwiZXJyb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQUEsbUJBQU8sQ0FBQyxxRUFBRCxDQUFQOztBQUNBOztBQUVBLElBQU1DLElBQUksR0FBR0QsbUJBQU8sQ0FBQywwREFBRCxDQUFwQjs7QUFDQSxJQUFNRSxRQUFRLEdBQUdGLG1CQUFPLENBQUMsb0VBQUQsQ0FBeEI7O0FBQ0E7QUFDQTs7QUFHQUEsbUJBQU8sQ0FBQyw0RkFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLGlGQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsNkhBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxvREFBRCxDQUFQO0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQUlHLEtBQUssR0FBRztBQUNSQyxJQUFFLEVBQUU7QUFDQSxpQkFBYSxpQkFEYjtBQUVBLHVCQUFtQixnQkFGbkI7QUFHQSxjQUFVLFNBSFY7QUFJQSxpQkFBYSwrRUFKYjtBQUtBLG1CQUFlLFVBTGY7QUFNQSxrQkFBZTtBQU5mLEdBREk7QUFTUkMsSUFBRSxFQUFFO0FBQ0EsaUJBQWEsV0FEYjtBQUVBLHVCQUFtQixpQkFGbkI7QUFHQSxjQUFVLFFBSFY7QUFJQSxpQkFBYSx5REFKYjtBQUtBLG1CQUFlLGFBTGY7QUFNQSxrQkFBZTtBQU5mLEdBVEk7QUFpQlJDLElBQUUsRUFBRTtBQUNBLGlCQUFhLFdBRGI7QUFFQSx1QkFBbUIsaUJBRm5CO0FBR0EsY0FBVSxRQUhWO0FBSUEsaUJBQWEseURBSmI7QUFLQSxtQkFBZSxhQUxmO0FBTUEsa0JBQWU7QUFOZjtBQWpCSSxDQUFaO0FBMEJBLElBQUlDLFFBQVEsR0FBRyxFQUFmO0FBQ0FDLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsVUFBVUMsQ0FBVixFQUFhO0FBRTNCO0FBQ0o7QUFDQTtBQUNBO0FBQ0ksTUFBSUMsT0FBTyxHQUFHSixDQUFDLENBQUMsVUFBRCxDQUFmOztBQUNBLE1BQUlBLENBQUMsQ0FBQ0ksT0FBRCxDQUFELENBQVdDLEdBQVgsQ0FBZUwsQ0FBQyxDQUFDLEtBQUQsQ0FBaEIsRUFBeUJNLE1BQTdCLEVBQXFDO0FBQ2pDQyxjQUFVLENBQUMsWUFBWTtBQUNuQlAsT0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjUSxLQUFkO0FBQ0gsS0FGUyxFQUVQLElBRk8sQ0FBVjtBQUdIO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0ksTUFBSUMsT0FBTyxHQUFHVCxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QlUsSUFBNUIsQ0FBaUMsT0FBakMsQ0FBZDtBQUFBLE1BQ0lDLElBREo7QUFHQTtBQUNKO0FBQ0E7OztBQUNJWCxHQUFDLENBQUMsZ0NBQUQsQ0FBRCxDQUFvQ1ksRUFBcEMsQ0FBdUMsT0FBdkMsRUFBZ0QsVUFBVVQsQ0FBVixFQUFhO0FBQ3pEQSxLQUFDLENBQUNVLGNBQUY7QUFDQUMsZUFBVztBQUNYSCxRQUFJLEdBQUdYLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsSUFBUixDQUFhLFdBQWIsQ0FBUDtBQUNBSyxpQkFBYSxDQUFDSixJQUFELEVBQU9GLE9BQVAsRUFBZ0JkLEtBQWhCLENBQWI7QUFDSCxHQUxEO0FBT0E7QUFDSjtBQUNBOztBQUNJSyxHQUFDLENBQUMsYUFBRCxDQUFELENBQWlCZ0IsS0FBakIsQ0FBdUIsVUFBVWIsQ0FBVixFQUFhO0FBQ2hDSCxLQUFDLENBQUMsWUFBRCxDQUFELENBQWdCaUIsV0FBaEIsQ0FBNEIsTUFBNUI7QUFDQWpCLEtBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCaUIsV0FBeEIsQ0FBb0MsTUFBcEM7QUFDQWpCLEtBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCa0IsSUFBNUIsQ0FBaUMsRUFBakM7QUFDSCxHQUpEO0FBTUFsQixHQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZVyxFQUFaLENBQWUsT0FBZixFQUF3QixpRUFBeEIsRUFBMkYsVUFBVVQsQ0FBVixFQUFhO0FBQ3BHZ0IsVUFBTSxDQUFDLElBQUQsQ0FBTjtBQUNILEdBRkQ7QUFJQW5CLEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlXLEVBQVosQ0FBZSxPQUFmLEVBQXdCLGVBQXhCLEVBQXlDLFlBQVk7QUFDakRRLGtCQUFjLENBQUNwQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLElBQVIsQ0FBYSxTQUFiLENBQUQsRUFBMEJELE9BQTFCLENBQWQ7QUFDSCxHQUZELEVBNUMyQixDQWdEM0I7O0FBQ0FULEdBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlXLEVBQVosQ0FBZSxRQUFmLEVBQXlCLHFDQUF6QixFQUFnRSxVQUFVVCxDQUFWLEVBQWE7QUFDekVXLGVBQVc7QUFDZCxHQUZEO0FBSUgsQ0FyREQ7QUF1REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxTQUFTSyxNQUFULENBQWdCRSxJQUFoQixFQUEwQztBQUFBLE1BQXBCQyxXQUFvQix1RUFBTixJQUFNO0FBQ3RDLE1BQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDQSxNQUFNLENBQUNILElBQUQsQ0FBTixDQUFhWCxJQUFiLENBQWtCLG9CQUFsQixDQUFELENBQWpCLENBRHNDLENBRXRDOztBQUNBLE1BQUllLE9BQU8sR0FBR0YsSUFBSSxDQUFDRyxJQUFMLENBQVUsZ0JBQVYsS0FBK0JILElBQUksQ0FBQ0ksUUFBTCxHQUFnQnJCLE1BQTdELENBSHNDLENBSXRDOztBQUNBLE1BQUlzQixTQUFTLEdBQUdMLElBQUksQ0FBQ2IsSUFBTCxDQUFVLGdCQUFWLENBQWhCLENBTHNDLENBTXRDO0FBQ0E7QUFDQTs7QUFDQWtCLFdBQVMsR0FBR0EsU0FBUyxDQUFDQyxPQUFWLENBQWtCLFdBQWxCLEVBQStCSixPQUEvQixDQUFaLENBVHNDLENBVXRDOztBQUNBQSxTQUFPLEdBWCtCLENBWXRDOztBQUNBRixNQUFJLENBQUNHLElBQUwsQ0FBVSxnQkFBVixFQUE0QkQsT0FBNUIsRUFic0MsQ0FjdEM7O0FBQ0EsTUFBSUssT0FBTyxHQUFHTixNQUFNLENBQUNELElBQUksQ0FBQ2IsSUFBTCxDQUFVLGtCQUFWLENBQUQsQ0FBTixDQUFzQ1EsSUFBdEMsQ0FBMkNVLFNBQTNDLENBQWQ7QUFDQUUsU0FBTyxDQUFDQyxRQUFSLENBQWlCUixJQUFqQjtBQUNBdkIsR0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JnQyxPQUF0QixDQUE4QjtBQUMxQkMsU0FBSyxFQUFFLFdBRG1CO0FBRTFCQywyQkFBdUIsRUFBRUM7QUFGQyxHQUE5Qjs7QUFLQSxNQUFHYixXQUFILEVBQWU7QUFDWGMsd0JBQW9CLENBQUNOLE9BQUQsQ0FBcEI7QUFDSDtBQUNKO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQVNNLG9CQUFULENBQThCQyxVQUE5QixFQUEwQztBQUN0QyxNQUFJQyxpQkFBaUIsR0FBR3RDLENBQUMsQ0FBQyxxREFBRCxDQUF6QjtBQUNBcUMsWUFBVSxDQUFDRSxNQUFYLENBQWtCRCxpQkFBbEI7QUFDQUEsbUJBQWlCLENBQUMxQixFQUFsQixDQUFxQixPQUFyQixFQUE4QixVQUFVVCxDQUFWLEVBQWE7QUFDdkNrQyxjQUFVLENBQUNHLE1BQVg7QUFDSCxHQUZEO0FBR0g7QUFHRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBU3pCLGFBQVQsQ0FBdUJKLElBQXZCLEVBQTZCRixPQUE3QixFQUFzQ2QsS0FBdEMsRUFBNkM7QUFDekMsTUFBSThDLFVBQVUsR0FBR3pDLENBQUMsQ0FBQyxvQkFBRCxDQUFsQjtBQUFBLE1BQ0kwQyxjQUFjLEdBQUcxQyxDQUFDLENBQUMsMkNBQUQsQ0FEdEI7QUFBQSxNQUVJMkMsWUFBWSxHQUFHM0MsQ0FBQyxDQUFDLHVCQUFELENBRnBCO0FBR0EsTUFBSTRDLEdBQUcsR0FBRyxJQUFJQyxjQUFKLEVBQVY7QUFBQSxNQUFnQ0MsR0FBRyxHQUFHckMsT0FBTyxLQUFLLElBQVosMEJBQW1DRSxJQUFuQyxlQUFnREYsT0FBaEQsMEJBQXVFRSxJQUF2RSxDQUF0QztBQUNBaUMsS0FBRyxDQUFDRyxJQUFKLENBQVMsS0FBVCxFQUFnQkQsR0FBaEI7O0FBRUFGLEtBQUcsQ0FBQ0ksa0JBQUosR0FBeUIsWUFBWTtBQUVqQyxRQUFJSixHQUFHLENBQUNLLFVBQUosS0FBbUIsQ0FBdkIsRUFBMEI7QUFDdEJqRCxPQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQmtCLElBQXRCLENBQTJCLEVBQTNCO0FBQ0FsQixPQUFDLENBQUMwQyxjQUFELENBQUQsQ0FBa0J4QixJQUFsQixDQUF1QixFQUF2QjtBQUNBbEIsT0FBQyxDQUFDMkMsWUFBRCxDQUFELENBQWdCekIsSUFBaEIsQ0FBcUIsRUFBckI7QUFDQWxCLE9BQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJ3QyxNQUFqQjtBQUNBLFVBQUlkLElBQUksR0FBR3dCLElBQUksQ0FBQ0MsS0FBTCxDQUFXUCxHQUFHLENBQUNRLFlBQWYsQ0FBWDtBQUNBcEQsT0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQnFELFFBQWhCLENBQXlCLE1BQXpCO0FBQ0FyRCxPQUFDLENBQUN5QyxVQUFELENBQUQsQ0FBY1ksUUFBZCxDQUF1QixNQUF2Qjs7QUFDQSxVQUFJVCxHQUFHLENBQUNVLE1BQUosS0FBZSxHQUFuQixFQUF3QjtBQUNwQixnQkFBUTNDLElBQVI7QUFDSSxlQUFLLElBQUw7QUFDSVgsYUFBQyxDQUFDMkMsWUFBRCxDQUFELENBQWdCekIsSUFBaEIsQ0FBcUJ2QixLQUFLLENBQUNjLE9BQUQsQ0FBTCxDQUFlLFdBQWYsQ0FBckI7QUFDQTs7QUFDSixlQUFLLE9BQUw7QUFDSVQsYUFBQyxDQUFDMkMsWUFBRCxDQUFELENBQWdCekIsSUFBaEIsQ0FBcUJ2QixLQUFLLENBQUNjLE9BQUQsQ0FBTCxDQUFlLGlCQUFmLENBQXJCO0FBQ0E7O0FBQ0o7QUFDSTtBQVJSOztBQVVBVCxTQUFDLENBQUMwQyxjQUFELENBQUQsQ0FBa0JILE1BQWxCLENBQXlCYixJQUFJLENBQUM2QixTQUE5QixFQVhvQixDQVlwQjs7QUFDQXZELFNBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCZ0MsT0FBdEIsQ0FBOEI7QUFDMUJDLGVBQUssRUFBRSxXQURtQjtBQUUxQkMsaUNBQXVCLEVBQUVDO0FBRkMsU0FBOUIsRUFib0IsQ0FpQnBCOztBQUNBcUIsa0JBQVUsQ0FBQy9DLE9BQUQsQ0FBVixDQWxCb0IsQ0FtQnBCOztBQUNBLFlBQUlULENBQUMsQ0FBQyxpQ0FBRCxDQUFELENBQXFDTSxNQUF6QyxFQUFnRDtBQUM1Q2EsZ0JBQU0sQ0FBQ25CLENBQUMsQ0FBQyxpQ0FBRCxDQUFGLEVBQXVDLEtBQXZDLENBQU47QUFDSCxTQXRCbUIsQ0F1QnBCOzs7QUFDQSxZQUFJQSxDQUFDLENBQUMsZ0NBQUQsQ0FBRCxDQUFvQ00sTUFBeEMsRUFBK0M7QUFDM0NhLGdCQUFNLENBQUNuQixDQUFDLENBQUMsZ0NBQUQsQ0FBRixFQUFzQyxLQUF0QyxDQUFOO0FBQ0gsU0ExQm1CLENBMkJwQjs7O0FBQ0F5RCxpQkFBUyxDQUFDLGNBQUQsRUFBaUI7QUFBQ0MscUJBQVcsRUFBRSxFQUFkO0FBQWtCQyw4QkFBb0IsRUFBRTtBQUF4QyxTQUFqQixDQUFULENBQXlFQyxJQUF6RSxDQUE4RSxRQUE5RTtBQUNILE9BN0JELE1BNkJPO0FBQ0g1RCxTQUFDLENBQUMyQyxZQUFELENBQUQsQ0FBZ0J6QixJQUFoQixDQUFxQlEsSUFBSSxDQUFDbUMsT0FBMUI7QUFDSDtBQUNKO0FBQ0osR0EzQ0Q7O0FBNkNBakIsS0FBRyxDQUFDa0IsSUFBSjtBQUNIO0FBR0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTMUMsY0FBVCxDQUF3QjJDLEVBQXhCLEVBQTRCdEQsT0FBNUIsRUFBcUM7QUFFakMsTUFBSXVELElBQUksR0FBR2hFLENBQUMsQ0FBQyxtQkFBbUIrRCxFQUFuQixHQUF3QixJQUF6QixDQUFaO0FBQUEsTUFDSUUsRUFBRSxHQUFHakUsQ0FBQyxDQUFDZ0UsSUFBRCxDQUFELENBQVFFLE9BQVIsQ0FBZ0IsSUFBaEIsQ0FEVDtBQUdBcEQsYUFBVztBQUNYLE1BQUk4QixHQUFHLEdBQUcsSUFBSUMsY0FBSixFQUFWO0FBQUEsTUFDSUMsR0FBRyxHQUFHckMsT0FBTyxLQUFLLElBQVosZ0NBQXlDc0QsRUFBekMsZUFBb0R0RCxPQUFwRCxnQ0FBaUZzRCxFQUFqRixDQURWO0FBRUFuQixLQUFHLENBQUNHLElBQUosQ0FBUyxNQUFULEVBQWlCRCxHQUFqQjs7QUFFQUYsS0FBRyxDQUFDSSxrQkFBSixHQUF5QixZQUFZO0FBRWpDLFFBQUlKLEdBQUcsQ0FBQ0ssVUFBSixLQUFtQixDQUF2QixFQUEwQjtBQUN0QmpELE9BQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJ3QyxNQUFqQjs7QUFDQSxVQUFJSSxHQUFHLENBQUNVLE1BQUosS0FBZSxHQUFuQixFQUF3QjtBQUNwQnRELFNBQUMsQ0FBQ2lFLEVBQUQsQ0FBRCxDQUFNekIsTUFBTjtBQUNILE9BRkQsTUFFTztBQUNIeEMsU0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQm1FLElBQWpCLENBQXNCeEUsS0FBSyxDQUFDYyxPQUFELENBQUwsQ0FBZSxXQUFmLENBQXRCO0FBQ0g7QUFDSjtBQUNKLEdBVkQ7O0FBWUFtQyxLQUFHLENBQUNrQixJQUFKO0FBQ0g7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTaEQsV0FBVCxHQUE0QztBQUFBLE1BQXZCcUQsSUFBdUIsdUVBQWhCLEVBQWdCO0FBQUEsTUFBWkMsS0FBWSx1RUFBSixFQUFJO0FBRXhDLE1BQUlDLE1BQU0sR0FDTiw2Q0FDQSxvQ0FEQSxHQUVBLFlBSEo7QUFJQXJFLEdBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXVDLE1BQVYsQ0FBaUI4QixNQUFqQjtBQUNIO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQVNiLFVBQVQsQ0FBb0IvQyxPQUFwQixFQUE2QjtBQUN6QixNQUFNNkQsU0FBUyxHQUFHLGtCQUFsQjtBQUVBLE1BQU1DLElBQUksR0FBRyxJQUFJOUUsSUFBSixDQUFTO0FBQ2xCK0UsU0FBSyxFQUFFLElBRFc7QUFFbEJDLGVBQVcsRUFBRSxJQUZLO0FBR2xCQyxVQUFNLEVBQUU7QUFDSkMsYUFBTyxFQUFDO0FBQ0pDLGtCQUFVLEVBQUVqRixLQUFLLENBQUNjLE9BQUQsQ0FBTCxDQUFlLFlBQWY7QUFEUjtBQURKLEtBSFU7QUFRbEJvRSxVQUFNLEVBQUVDLFdBUlU7QUFVbEJDLGdCQUFZLEVBQUU7QUFDVkMsaUJBQVcsRUFBRSxRQURIO0FBRVZDLHNCQUFnQixFQUFFLEVBRlI7QUFHVkMsc0JBQWdCLEVBQUUsQ0FIUjtBQUlWQyxzQkFBZ0IsRUFBRSxDQUFDLFNBQUQsRUFBWSxNQUFaLEVBQW9CLE1BQXBCLEVBQTRCLE1BQTVCLEVBQW9DLE1BQXBDLEVBQTRDLE9BQTVDLEVBQXFELE9BQXJELEVBQThELE9BQTlELEVBQXVFLE9BQXZFLEVBQWdGLE9BQWhGLEVBQXlGLE1BQXpGLEVBQWlHLE1BQWpHLEVBQXlHLEtBQXpHLEVBQWdILE1BQWhILEVBQXdILFFBQXhILEVBQWtJLE1BQWxJLEVBQTBJLE1BQTFJLEVBQWtKLFdBQWxKO0FBSlI7QUFWSSxHQUFULEVBaUJSQyxHQWpCUSxDQWlCSkMsdURBakJJLEVBaUJPO0FBQUNDLFlBQVEsRUFBRWhCLFNBQVg7QUFBc0JpQixVQUFNLEVBQUUsTUFBOUI7QUFBc0NDLFlBQVEsRUFBRSxJQUFoRDtBQUFzREMsYUFBUyxFQUFFO0FBQWpFLEdBakJQLEVBa0JSTCxHQWxCUSxDQWtCSk0seURBbEJJLEVBa0JTO0FBQUNDLFVBQU0sRUFBRSxjQUFUO0FBQXlCQyxtQkFBZSxFQUFFO0FBQTFDLEdBbEJULEVBbUJSUixHQW5CUSxDQW1CSjFGLFFBbkJJLEVBbUJNO0FBQ1htRyxRQUFJLEVBQUVsRyxLQUFLLENBQUNjLE9BQUQsQ0FBTCxDQUFlLGFBQWYsQ0FESztBQUVYa0YsVUFBTSxFQUFFLHFCQUZHO0FBR1hMLFlBQVEsRUFBRWhCLFNBSEM7QUFJWGlCLFVBQU0sRUFBRSxNQUpHO0FBS1hDLFlBQVEsRUFBRSxJQUxDO0FBTVhDLGFBQVMsRUFBRTtBQU5BLEdBbkJOLEVBMkJSN0UsRUEzQlEsQ0EyQkwsZ0JBM0JLLEVBMkJha0YsZUFBZSxDQUFDLG9CQUFELEVBQXVCckYsT0FBdkIsQ0EzQjVCLENBQWI7QUE0Qkg7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsSUFBTXFGLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ0Msa0JBQUQsRUFBcUJ0RixPQUFyQjtBQUFBLFNBQ3BCLFVBQUN1RixJQUFELEVBQU9DLFFBQVAsRUFBb0I7QUFDaEJsRyxZQUFRLENBQUNtRyxJQUFULENBQWNELFFBQVEsQ0FBQ0UsSUFBVCxDQUFjcEMsRUFBNUI7QUFDQSxRQUFNQSxFQUFFLEdBQUdrQyxRQUFRLENBQUNFLElBQVQsQ0FBY3BDLEVBQXpCO0FBQ0EsUUFBTWpCLEdBQUcsR0FBR21ELFFBQVEsQ0FBQ0UsSUFBVCxDQUFjQyxhQUExQjtBQUNBLFFBQU1DLFFBQVEsR0FBR0wsSUFBSSxDQUFDTSxJQUF0QjtBQUVBLFFBQU1DLE1BQU0sR0FBR3RHLFFBQVEsQ0FBQ3VHLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZjtBQUNBRCxVQUFNLENBQUNFLEdBQVAsR0FBYTFDLEVBQWI7QUFDQXdDLFVBQU0sQ0FBQ0csWUFBUCxDQUFvQixVQUFwQixFQUFnQyxVQUFoQztBQUNBSCxVQUFNLENBQUNwQyxJQUFQLEdBQWNKLEVBQWQ7QUFDQTlELFlBQVEsQ0FBQzBHLGFBQVQsQ0FBdUIsUUFBdkIsRUFBaUNDLFdBQWpDLENBQTZDTCxNQUE3QztBQUVBLFFBQU12QyxJQUFJLEdBQUcvRCxRQUFRLENBQUN1RyxhQUFULENBQXVCLE1BQXZCLENBQWI7QUFDQXhDLFFBQUksQ0FBQzBDLFlBQUwsQ0FBa0IsU0FBbEIsRUFBNkJULFFBQVEsQ0FBQ0UsSUFBVCxDQUFjcEMsRUFBM0M7QUFDQUMsUUFBSSxDQUFDNkMsU0FBTCxHQUFpQmxILEtBQUssQ0FBQ2MsT0FBRCxDQUFMLENBQWUsUUFBZixDQUFqQjtBQUVBLFFBQU1xRyxDQUFDLEdBQUc3RyxRQUFRLENBQUN1RyxhQUFULENBQXVCLEdBQXZCLENBQVY7QUFDQU0sS0FBQyxDQUFDQyxJQUFGLEdBQVNqRSxHQUFUO0FBQ0FnRSxLQUFDLENBQUNuQixNQUFGLEdBQVcsUUFBWDtBQUNBbUIsS0FBQyxDQUFDRixXQUFGLENBQWMzRyxRQUFRLENBQUMrRyxjQUFULENBQXdCWCxRQUF4QixDQUFkO0FBRUEsUUFBTXBDLEVBQUUsR0FBR2hFLFFBQVEsQ0FBQ3VHLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBWDtBQUNBdkMsTUFBRSxDQUFDMkMsV0FBSCxDQUFlRSxDQUFmO0FBQ0E3QyxNQUFFLENBQUMyQyxXQUFILENBQWU1QyxJQUFmO0FBRUEvRCxZQUFRLENBQUMwRyxhQUFULENBQXVCWixrQkFBdkIsRUFBMkNhLFdBQTNDLENBQXVEM0MsRUFBdkQ7QUFDSCxHQTNCbUI7QUFBQSxDQUF4Qjs7QUE2QkEsSUFBTWEsV0FBVyxHQUFHO0FBQ2hCTixPQUFLLEVBQUUsaUJBQWE7QUFDaEIsUUFBTUEsS0FBSyxHQUFHeUMsT0FBTyxDQUFDekMsS0FBUixJQUFpQnlDLE9BQU8sQ0FBQ0MsR0FBdkM7O0FBRGdCLHNDQUFUQyxJQUFTO0FBQVRBLFVBQVM7QUFBQTs7QUFFaEIzQyxTQUFLLENBQUM0QyxJQUFOLE9BQUE1QyxLQUFLLEdBQU15QyxPQUFOLDRCQUFxQ0UsSUFBckMsRUFBTDtBQUNILEdBSmU7QUFLaEJFLE1BQUksRUFBRTtBQUFBOztBQUFBLHVDQUFJRixJQUFKO0FBQUlBLFVBQUo7QUFBQTs7QUFBQSxXQUFhLFlBQUFGLE9BQU8sRUFBQ0ksSUFBUiw2Q0FBb0NGLElBQXBDLEVBQWI7QUFBQSxHQUxVO0FBTWhCRyxPQUFLLEVBQUU7QUFBQTs7QUFBQSx1Q0FBSUgsSUFBSjtBQUFJQSxVQUFKO0FBQUE7O0FBQUEsV0FBYSxhQUFBRixPQUFPLEVBQUNDLEdBQVIsK0NBQW9DQyxJQUFwQyxFQUFiO0FBQUE7QUFOUyxDQUFwQixDIiwiZmlsZSI6IjUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgJ0B1cHB5L2NvcmUvZGlzdC9zdHlsZS5jc3MnO1xuXG5yZXF1aXJlKCd1cHB5L2Rpc3QvdXBweS5taW4uY3NzJyk7XG5pbXBvcnQgJ0B1cHB5L2Rhc2hib2FyZC9kaXN0L3N0eWxlLmNzcyc7XG5cbmNvbnN0IFVwcHkgPSByZXF1aXJlKCdAdXBweS9jb3JlJyk7XG5jb25zdCBEcmFnRHJvcCA9IHJlcXVpcmUoJ0B1cHB5L2RyYWctZHJvcCcpO1xuaW1wb3J0IFhIUlVwbG9hZCBmcm9tICdAdXBweS94aHItdXBsb2FkJztcbmltcG9ydCBQcm9ncmVzc0JhciBmcm9tIFwiQHVwcHkvcHJvZ3Jlc3MtYmFyXCI7XG5cblxucmVxdWlyZSgnc2VsZWN0Mi9kaXN0L2pzL3NlbGVjdDIuZnVsbC5taW4nKTtcbnJlcXVpcmUoJ3NlbGVjdDIvZGlzdC9jc3Mvc2VsZWN0Mi5jc3MnKTtcbnJlcXVpcmUoJ3NlbGVjdDItYm9vdHN0cmFwLXRoZW1lL2Rpc3Qvc2VsZWN0Mi1ib290c3RyYXAuY3NzJyk7XG5yZXF1aXJlKCdpbnB1dG1hc2snKTtcblxuLyoqXG4gKiB0cmFuc2xhdGlvbnNcbiAqIEBwcml2YXRlXG4gKi9cbmxldCBfdGV4dCA9IHtcbiAgICBydToge1xuICAgICAgICAnVXBsb2FkIENWJzogJ9CX0LDQs9GA0YPQt9C60LAg0YDQtdC30Y7QvNC1JyxcbiAgICAgICAgJ1JlcXVlc3QgYSBRdW90ZSc6ICfQl9Cw0LPRgNGD0LfQutCwIFF1b3RlJyxcbiAgICAgICAgJ3JlbW92ZSc6ICfRg9C00LDQu9C40YLRjCcsXG4gICAgICAgICd1cHB5RXJyb3InOiAn0KfRgtC+LdGC0L4g0L/QvtGI0LvQviDQvdC1INGC0LDQuiwg0L/QvtC20LDQu9GD0LnRgdGC0LAsINC/0LXRgNC10LfQsNCz0YDRg9C30LjRgtC1INGB0YLRgNCw0L3QuNGG0YMg0Lgg0L/QvtC/0YDQvtCx0YPQudGC0LUg0LXRidC1INGA0LDQty4nLFxuICAgICAgICAnVXAgdG8gMTAgTUInOiAn0JTQviAxMCDQnNCRJyxcbiAgICAgICAgJ2Ryb3BIZXJlT3InIDogJ9Cf0LXRgNC10YLQsNGJ0LjRgtC1INGE0LDQudC70Ysg0YHRjtC00LAnXG4gICAgfSxcbiAgICBlbjoge1xuICAgICAgICAnVXBsb2FkIENWJzogJ1VwbG9hZCBDVicsXG4gICAgICAgICdSZXF1ZXN0IGEgUXVvdGUnOiAnUmVxdWVzdCBhIFF1b3RlJyxcbiAgICAgICAgJ3JlbW92ZSc6ICdyZW1vdmUnLFxuICAgICAgICAndXBweUVycm9yJzogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgcmVsb2FkIHBhZ2UgYW5kIHRyeSBhZ2Fpbi4nLFxuICAgICAgICAnVXAgdG8gMTAgTUInOiAnVXAgdG8gMTAgTUInLFxuICAgICAgICAnZHJvcEhlcmVPcicgOiAnRHJvcCBIZXJlJ1xuICAgIH0sXG4gICAgZXM6IHtcbiAgICAgICAgJ1VwbG9hZCBDVic6ICdVcGxvYWQgQ1YnLFxuICAgICAgICAnUmVxdWVzdCBhIFF1b3RlJzogJ1JlcXVlc3QgYSBRdW90ZScsXG4gICAgICAgICdyZW1vdmUnOiAncmVtb3ZlJyxcbiAgICAgICAgJ3VwcHlFcnJvcic6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHJlbG9hZCBwYWdlIGFuZCB0cnkgYWdhaW4uJyxcbiAgICAgICAgJ1VwIHRvIDEwIE1CJzogJ1VwIHRvIDEwIE1CJyxcbiAgICAgICAgJ2Ryb3BIZXJlT3InIDogJ0Ryb3AgSGVyZSBPcidcbiAgICB9XG59O1xubGV0IGZpbGVzSWRzID0gW107XG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoZSkge1xuXG4gICAgLyoqXG4gICAgICogaWYgZmxhc2ggbWVzc2FnZSBleGlzdCwgY2xlYXIgNiBzZWMgYWZ0ZXJcbiAgICAgKiBAdHlwZSB7alF1ZXJ5fEhUTUxFbGVtZW50fVxuICAgICAqL1xuICAgIGxldCBmbGFzaGVzID0gJCgnLmZsYXNoZXMnKTtcbiAgICBpZiAoJChmbGFzaGVzKS5oYXMoJCgnZGl2JykpLmxlbmd0aCkge1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJy5mbGFzaGVzJykuZW1wdHkoKTtcbiAgICAgICAgfSwgNjAwMCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogZ2V0IGxvY2FsZVxuICAgICAqIEB0eXBlIHtqUXVlcnl8dW5kZWZpbmVkfGFueX1cbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIGxldCBfbG9jYWxlID0gJCgnLmxhbmd1YWdlcyBsaS5kLW5vbmUgYScpLmF0dHIoJ2NsYXNzJyksXG4gICAgICAgIHNsdWc7XG5cbiAgICAvKipcbiAgICAgKiAgZ2V0IGZvcm1EYXRhXG4gICAgICovXG4gICAgJCgnLnJlcXVlc3RRdW90ZSwgLklucHV0RmlsZUxhYmVsJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBzdGFydExvYWRlcigpO1xuICAgICAgICBzbHVnID0gJCh0aGlzKS5hdHRyKCdkYXRhLXNsdWcnKTtcbiAgICAgICAgZ2V0Q3VzdG9tRm9ybShzbHVnLCBfbG9jYWxlLCBfdGV4dCk7XG4gICAgfSk7XG5cbiAgICAvKipcbiAgICAgKiBjdiBvciBxdW90ZSBmb3JtIG9wZW4vY2xvc2VcbiAgICAgKi9cbiAgICAkKCcuY2xvc2VNb2RhbCcpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICQoJy5ib2R5QmxhY2snKS5yZW1vdmVDbGFzcygnb3BlbicpO1xuICAgICAgICAkKCcuY2xpZW50Q3VzdG9tRm9ybXMnKS5yZW1vdmVDbGFzcygnb3BlbicpO1xuICAgICAgICAkKCcuY2xpZW50Q3VzdG9tRm9ybXNEYXRhJykuaHRtbCgnJyk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmFkZC1sYW5ndWFnZS1jb2xsZWN0aW9uLXdpZGdldCwgLmFkZC1hcmVhcy1vZi1leHBlcnRpc2Utd2lkZ2V0JywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgYWRkVGFnKHRoaXMpXG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnc3BhbltkYXRhLWlkXScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcG9zdEZpbGVSZW1vdmUoJCh0aGlzKS5hdHRyKCdkYXRhLWlkJyksIF9sb2NhbGUpO1xuICAgIH0pO1xuXG4gICAgLy8gY3Ygb3IgcXVvdGUgZm9ybSBzdWJtaXRcbiAgICAkKGRvY3VtZW50KS5vbignc3VibWl0JywgJ2Zvcm1bbmFtZT1cInF1b3RlXCJdLCBmb3JtW25hbWU9XCJjdlwiXScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHN0YXJ0TG9hZGVyKCk7XG4gICAgfSk7XG5cbn0pO1xuXG4vKipcbiAqIGFkZCBuZXcgdGFnIGZvcm0gY2xpZW50IGZvcm1cbiAqIEBwYXJhbSBlbGVtXG4gKiBAcGFyYW0gYWxsb3dEZWxldGVcbiAqL1xuZnVuY3Rpb24gYWRkVGFnKGVsZW0sIGFsbG93RGVsZXRlID0gdHJ1ZSkge1xuICAgIHZhciBsaXN0ID0galF1ZXJ5KGpRdWVyeShlbGVtKS5hdHRyKCdkYXRhLWxpc3Qtc2VsZWN0b3InKSk7XG4gICAgLy8gVHJ5IHRvIGZpbmQgdGhlIGNvdW50ZXIgb2YgdGhlIGxpc3Qgb3IgdXNlIHRoZSBsZW5ndGggb2YgdGhlIGxpc3RcbiAgICB2YXIgY291bnRlciA9IGxpc3QuZGF0YSgnd2lkZ2V0LWNvdW50ZXInKSB8fCBsaXN0LmNoaWxkcmVuKCkubGVuZ3RoO1xuICAgIC8vIGdyYWIgdGhlIHByb3RvdHlwZSB0ZW1wbGF0ZVxuICAgIHZhciBuZXdXaWRnZXQgPSBsaXN0LmF0dHIoJ2RhdGEtcHJvdG90eXBlJyk7XG4gICAgLy8gcmVwbGFjZSB0aGUgXCJfX25hbWVfX1wiIHVzZWQgaW4gdGhlIGlkIGFuZCBuYW1lIG9mIHRoZSBwcm90b3R5cGVcbiAgICAvLyB3aXRoIGEgbnVtYmVyIHRoYXQncyB1bmlxdWUgdG8geW91ciBlbWFpbHNcbiAgICAvLyBlbmQgbmFtZSBhdHRyaWJ1dGUgbG9va3MgbGlrZSBuYW1lPVwiY29udGFjdFtlbWFpbHNdWzJdXCJcbiAgICBuZXdXaWRnZXQgPSBuZXdXaWRnZXQucmVwbGFjZSgvX19uYW1lX18vZywgY291bnRlcik7XG4gICAgLy8gSW5jcmVhc2UgdGhlIGNvdW50ZXJcbiAgICBjb3VudGVyKys7XG4gICAgLy8gQW5kIHN0b3JlIGl0LCB0aGUgbGVuZ3RoIGNhbm5vdCBiZSB1c2VkIGlmIGRlbGV0aW5nIHdpZGdldHMgaXMgYWxsb3dlZFxuICAgIGxpc3QuZGF0YSgnd2lkZ2V0LWNvdW50ZXInLCBjb3VudGVyKTtcbiAgICAvLyBjcmVhdGUgYSBuZXcgbGlzdCBlbGVtZW50IGFuZCBhZGQgaXQgdG8gdGhlIGxpc3RcbiAgICB2YXIgbmV3RWxlbSA9IGpRdWVyeShsaXN0LmF0dHIoJ2RhdGEtd2lkZ2V0LXRhZ3MnKSkuaHRtbChuZXdXaWRnZXQpO1xuICAgIG5ld0VsZW0uYXBwZW5kVG8obGlzdCk7XG4gICAgJChcInNlbGVjdC5zZWxlY3RlZDJcIikuc2VsZWN0Mih7XG4gICAgICAgIHRoZW1lOiBcImJvb3RzdHJhcFwiLFxuICAgICAgICBtaW5pbXVtUmVzdWx0c0ZvclNlYXJjaDogSW5maW5pdHlcbiAgICB9KTtcblxuICAgIGlmKGFsbG93RGVsZXRlKXtcbiAgICAgICAgYWRkVGFnRm9ybURlbGV0ZUxpbmsobmV3RWxlbSk7XG4gICAgfVxufVxuXG4vKipcbiAqIGNyZWF0ZSByZW1vdmUgdGFnIGJ1dHRvbiBmb3IgY2xpZW50IGZvcm1cbiAqIEBwYXJhbSAkdGFnRm9ybUxpXG4gKi9cbmZ1bmN0aW9uIGFkZFRhZ0Zvcm1EZWxldGVMaW5rKCR0YWdGb3JtTGkpIHtcbiAgICB2YXIgJHJlbW92ZUZvcm1CdXR0b24gPSAkKCc8YnV0dG9uIGNsYXNzPVwiY29sbFJlbW92ZVwiIHR5cGU9XCJidXR0b25cIj7DlzwvYnV0dG9uPicpO1xuICAgICR0YWdGb3JtTGkuYXBwZW5kKCRyZW1vdmVGb3JtQnV0dG9uKTtcbiAgICAkcmVtb3ZlRm9ybUJ1dHRvbi5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAkdGFnRm9ybUxpLnJlbW92ZSgpO1xuICAgIH0pO1xufVxuXG5cbi8qKlxuICogZ2V0IGZvcm1EYXRhIHJlcXVlc3RcbiAqXG4gKiBAcGFyYW0gc2x1Z1xuICogQHBhcmFtIF9sb2NhbGVcbiAqIEBwYXJhbSBfdGV4dFxuICovXG5mdW5jdGlvbiBnZXRDdXN0b21Gb3JtKHNsdWcsIF9sb2NhbGUsIF90ZXh0KSB7XG4gICAgbGV0IHBhcmVudEZvcm0gPSAkKCcuY2xpZW50Q3VzdG9tRm9ybXMnKSxcbiAgICAgICAgcGFyZW50Rm9ybURhdGEgPSAkKCcuY2xpZW50Q3VzdG9tRm9ybXMgLmNsaWVudEN1c3RvbUZvcm1zRGF0YScpLFxuICAgICAgICBwYXJlbnRGb3JtSDQgPSAkKCcuY2xpZW50Q3VzdG9tRm9ybXMgaDQnKTtcbiAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCksIHVybCA9IF9sb2NhbGUgPT09ICdlbicgPyBgL2N1c3RvbS1mb3JtLyR7c2x1Z31gIDogYC8ke19sb2NhbGV9L2N1c3RvbS1mb3JtLyR7c2x1Z31gO1xuICAgIHhoci5vcGVuKFwiR0VUXCIsIHVybCk7XG5cbiAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkge1xuICAgICAgICAgICAgJCgnLm1lc3NhZ2UgLmNvbC0xMicpLmh0bWwoJycpO1xuICAgICAgICAgICAgJChwYXJlbnRGb3JtRGF0YSkuaHRtbCgnJyk7XG4gICAgICAgICAgICAkKHBhcmVudEZvcm1INCkuaHRtbCgnJyk7XG4gICAgICAgICAgICAkKCcuYmxhY2tNb2RhbCcpLnJlbW92ZSgpO1xuICAgICAgICAgICAgbGV0IGRhdGEgPSBKU09OLnBhcnNlKHhoci5yZXNwb25zZVRleHQpO1xuICAgICAgICAgICAgJCgnLmJvZHlCbGFjaycpLmFkZENsYXNzKCdvcGVuJyk7XG4gICAgICAgICAgICAkKHBhcmVudEZvcm0pLmFkZENsYXNzKCdvcGVuJyk7XG4gICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChzbHVnKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2N2JzpcbiAgICAgICAgICAgICAgICAgICAgICAgICQocGFyZW50Rm9ybUg0KS5odG1sKF90ZXh0W19sb2NhbGVdWydVcGxvYWQgQ1YnXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncXVvdGUnOlxuICAgICAgICAgICAgICAgICAgICAgICAgJChwYXJlbnRGb3JtSDQpLmh0bWwoX3RleHRbX2xvY2FsZV1bJ1JlcXVlc3QgYSBRdW90ZSddKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICQocGFyZW50Rm9ybURhdGEpLmFwcGVuZChkYXRhLmZvcm1faHRtbCk7XG4gICAgICAgICAgICAgICAgLy8gc2VsZWN0IDJcbiAgICAgICAgICAgICAgICAkKFwic2VsZWN0LnNlbGVjdGVkMlwiKS5zZWxlY3QyKHtcbiAgICAgICAgICAgICAgICAgICAgdGhlbWU6IFwiYm9vdHN0cmFwXCIsXG4gICAgICAgICAgICAgICAgICAgIG1pbmltdW1SZXN1bHRzRm9yU2VhcmNoOiBJbmZpbml0eVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSB1cHB5IHVwbG9hZGVyXG4gICAgICAgICAgICAgICAgY3JlYXRlVXBweShfbG9jYWxlKTtcbiAgICAgICAgICAgICAgICAvLyBjcmVhdGUgZm9ybSB0YWcgZm9yIGxhbmd1YWdlIHNraWxsc1xuICAgICAgICAgICAgICAgIGlmICgkKCcuYWRkLWxhbmd1YWdlLWNvbGxlY3Rpb24td2lkZ2V0JykubGVuZ3RoKXtcbiAgICAgICAgICAgICAgICAgICAgYWRkVGFnKCQoJy5hZGQtbGFuZ3VhZ2UtY29sbGVjdGlvbi13aWRnZXQnKSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBjcmVhdGUgZm9ybSB0YWcgZm9yIGFyZWEgb2YgZXhwZXJ0aXNlXG4gICAgICAgICAgICAgICAgaWYgKCQoJy5hZGQtYXJlYXMtb2YtZXhwZXJ0aXNlLXdpZGdldCcpLmxlbmd0aCl7XG4gICAgICAgICAgICAgICAgICAgIGFkZFRhZygkKCcuYWRkLWFyZWFzLW9mLWV4cGVydGlzZS13aWRnZXQnKSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBjcmVhdGUgaW5wdXQgbWFzayBmb3IgcGhvbmUgbnVtYmVyXG4gICAgICAgICAgICAgICAgSW5wdXRtYXNrKFwiOTk5IDk5OSA5OTk5XCIsIHtwbGFjZWhvbGRlcjogXCJcIiwgY2xlYXJNYXNrT25Mb3N0Rm9jdXM6IHRydWV9KS5tYXNrKCcucGhvbmUnKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJChwYXJlbnRGb3JtSDQpLmh0bWwoZGF0YS5tZXNzYWdlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG5cbiAgICB4aHIuc2VuZCgpO1xufVxuXG5cbi8qKlxuICogcmVtb3ZlIGNsaWVudCBmb3JtIGZpbGVcbiAqXG4gKiBAcGFyYW0gaWRcbiAqIEBwYXJhbSBfbG9jYWxlXG4gKi9cbmZ1bmN0aW9uIHBvc3RGaWxlUmVtb3ZlKGlkLCBfbG9jYWxlKSB7XG5cbiAgICBsZXQgc3BhbiA9ICQoJ3NwYW5bZGF0YS1pZD1cIicgKyBpZCArICdcIl0nKSxcbiAgICAgICAgbGkgPSAkKHNwYW4pLmNsb3Nlc3QoJ2xpJylcbiAgICA7XG4gICAgc3RhcnRMb2FkZXIoKTtcbiAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCksXG4gICAgICAgIHVybCA9IF9sb2NhbGUgPT09ICdlbicgPyBgL3JlbW92ZS1mb3JtLWltYWdlLyR7aWR9YCA6IGAvJHtfbG9jYWxlfS9yZW1vdmUtZm9ybS1pbWFnZS8ke2lkfWA7XG4gICAgeGhyLm9wZW4oXCJQT1NUXCIsIHVybCk7XG5cbiAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkge1xuICAgICAgICAgICAgJCgnLmJsYWNrTW9kYWwnKS5yZW1vdmUoKTtcbiAgICAgICAgICAgIGlmICh4aHIuc3RhdHVzID09PSAyMDQpIHtcbiAgICAgICAgICAgICAgICAkKGxpKS5yZW1vdmUoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJCgnLnVwcHlFcnJvcnMnKS50ZXh0KF90ZXh0W19sb2NhbGVdWyd1cHB5RXJyb3InXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgeGhyLnNlbmQoKTtcbn1cblxuLyoqXG4gKiBsb2FkaW5nXG4gKiBAcGFyYW0gdGV4dFxuICogQHBhcmFtIHRpdGxlXG4gKi9cbmZ1bmN0aW9uIHN0YXJ0TG9hZGVyKHRleHQgPSAnJywgdGl0bGUgPSAnJykge1xuXG4gICAgbGV0IGxvYWRlciA9XG4gICAgICAgICc8ZGl2IGNsYXNzPVwiYmxhY2tNb2RhbFwiIGlkPVwiYmxhY2tNb2RhbFwiPicgK1xuICAgICAgICAnICAgICAgICA8ZGl2IGNsYXNzPVwibG9hZGVyXCI+PC9kaXY+JyArXG4gICAgICAgICcgICAgPC9kaXY+JztcbiAgICAkKCdib2R5JykuYXBwZW5kKGxvYWRlcik7XG59XG5cbi8qKlxuICogY3JlYXRlIFVQUFkgdXBsb2FkZXJcbiAqIEBwYXJhbSBfbG9jYWxlXG4gKi9cbmZ1bmN0aW9uIGNyZWF0ZVVwcHkoX2xvY2FsZSkge1xuICAgIGNvbnN0IHVwbG9hZFVSTCA9ICcvZ2FsbGVyaWVzL2ZpbGVzJztcblxuICAgIGNvbnN0IHVwcHkgPSBuZXcgVXBweSh7XG4gICAgICAgIGRlYnVnOiB0cnVlLFxuICAgICAgICBhdXRvUHJvY2VlZDogdHJ1ZSxcbiAgICAgICAgbG9jYWxlOiB7XG4gICAgICAgICAgICBzdHJpbmdzOntcbiAgICAgICAgICAgICAgICBkcm9wSGVyZU9yOiBfdGV4dFtfbG9jYWxlXVsnZHJvcEhlcmVPciddXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGxvZ2dlcjogZGVidWdMb2dnZXIsXG5cbiAgICAgICAgcmVzdHJpY3Rpb25zOiB7XG4gICAgICAgICAgICBtYXhGaWxlU2l6ZTogMTA5MDAwMDAsXG4gICAgICAgICAgICBtYXhOdW1iZXJPZkZpbGVzOiAyMCxcbiAgICAgICAgICAgIG1pbk51bWJlck9mRmlsZXM6IDEsXG4gICAgICAgICAgICBhbGxvd2VkRmlsZVR5cGVzOiBbJ2ltYWdlLyonLCAnLnBkZicsICcuZG9jJywgJy5kb3QnLCAnLndiaycsICcuZG9jeCcsICcuZG9jbScsICcuZG90eCcsICcuZG90bScsICcuZG9jYicsICcudHh0JywgJy5ydGYnLCAnLjd6JywgJy5yYXInLCAnLnhsaWZmJywgJy54bHonLCAnLnhsZicsICcuc2RseGxpZmYnXVxuICAgICAgICB9XG4gICAgfSlcbiAgICAgICAgLnVzZShYSFJVcGxvYWQsIHtlbmRwb2ludDogdXBsb2FkVVJMLCBtZXRob2Q6ICdQT1NUJywgZm9ybURhdGE6IHRydWUsIGZpZWxkTmFtZTogJ2ZpbGVzW10nfSlcbiAgICAgICAgLnVzZShQcm9ncmVzc0Jhciwge3RhcmdldDogJy5Qcm9ncmVzc0JhcicsIGhpZGVBZnRlckZpbmlzaDogdHJ1ZX0pXG4gICAgICAgIC51c2UoRHJhZ0Ryb3AsIHtcbiAgICAgICAgICAgIG5vdGU6IF90ZXh0W19sb2NhbGVdWydVcCB0byAxMCBNQiddLFxuICAgICAgICAgICAgdGFyZ2V0OiAnLkRhc2hib2FyZENvbnRhaW5lcicsXG4gICAgICAgICAgICBlbmRwb2ludDogdXBsb2FkVVJMLFxuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgICAgICBmb3JtRGF0YTogdHJ1ZSxcbiAgICAgICAgICAgIGZpZWxkTmFtZTogJ2ZpbGVzW10nLFxuICAgICAgICB9KVxuICAgICAgICAub24oJ3VwbG9hZC1zdWNjZXNzJywgb25VcGxvYWRTdWNjZXNzKCcudXBsb2FkZWQtZmlsZXMgb2wnLCBfbG9jYWxlKSk7XG59XG5cbi8qKlxuICogc2hvdyB1cGxvYWRlZCBmaWxlIGZyb20gdXBweSBpbiBjbGllbnQgZm9ybVxuICpcbiAqIEBwYXJhbSBlbEZvclVwbG9hZGVkRmlsZXNcbiAqIEBwYXJhbSBfbG9jYWxlXG4gKiBAcmV0dXJucyB7ZnVuY3Rpb24oLi4uWypdPSl9XG4gKi9cbmNvbnN0IG9uVXBsb2FkU3VjY2VzcyA9IChlbEZvclVwbG9hZGVkRmlsZXMsIF9sb2NhbGUpID0+XG4gICAgKGZpbGUsIHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGZpbGVzSWRzLnB1c2gocmVzcG9uc2UuYm9keS5pZCk7XG4gICAgICAgIGNvbnN0IGlkID0gcmVzcG9uc2UuYm9keS5pZDtcbiAgICAgICAgY29uc3QgdXJsID0gcmVzcG9uc2UuYm9keS5kb3dubG9hZF9saW5rO1xuICAgICAgICBjb25zdCBmaWxlTmFtZSA9IGZpbGUubmFtZTtcblxuICAgICAgICBjb25zdCBvcHRpb24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdvcHRpb24nKTtcbiAgICAgICAgb3B0aW9uLnZhbCA9IGlkO1xuICAgICAgICBvcHRpb24uc2V0QXR0cmlidXRlKCdzZWxlY3RlZCcsICdzZWxlY3RlZCcpO1xuICAgICAgICBvcHRpb24udGV4dCA9IGlkO1xuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjZmlsZXMnKS5hcHBlbmRDaGlsZChvcHRpb24pO1xuXG4gICAgICAgIGNvbnN0IHNwYW4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzcGFuJyk7XG4gICAgICAgIHNwYW4uc2V0QXR0cmlidXRlKCdkYXRhLWlkJywgcmVzcG9uc2UuYm9keS5pZCk7XG4gICAgICAgIHNwYW4uaW5uZXJUZXh0ID0gX3RleHRbX2xvY2FsZV1bJ3JlbW92ZSddO1xuXG4gICAgICAgIGNvbnN0IGEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG4gICAgICAgIGEuaHJlZiA9IHVybDtcbiAgICAgICAgYS50YXJnZXQgPSAnX2JsYW5rJztcbiAgICAgICAgYS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShmaWxlTmFtZSkpO1xuXG4gICAgICAgIGNvbnN0IGxpID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGknKTtcbiAgICAgICAgbGkuYXBwZW5kQ2hpbGQoYSk7XG4gICAgICAgIGxpLmFwcGVuZENoaWxkKHNwYW4pO1xuXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWxGb3JVcGxvYWRlZEZpbGVzKS5hcHBlbmRDaGlsZChsaSk7XG4gICAgfTtcblxuY29uc3QgZGVidWdMb2dnZXIgPSB7XG4gICAgZGVidWc6ICguLi5hcmdzKSA9PiB7XG4gICAgICAgIGNvbnN0IGRlYnVnID0gY29uc29sZS5kZWJ1ZyB8fCBjb25zb2xlLmxvZztcbiAgICAgICAgZGVidWcuY2FsbChjb25zb2xlLCBgW1VwcHldIFtkc2FkYXNdYCwgLi4uYXJncyk7XG4gICAgfSxcbiAgICB3YXJuOiAoLi4uYXJncykgPT4gY29uc29sZS53YXJuKGBbVXBweV0gW1dBUk5JTkddYCwgLi4uYXJncyksXG4gICAgZXJyb3I6ICguLi5hcmdzKSA9PiBjb25zb2xlLmxvZyhgW1VwcHldIFtDUklUSUNBTF1gLCAuLi5hcmdzKVxufTtcblxuIl0sInNvdXJjZVJvb3QiOiIifQ==