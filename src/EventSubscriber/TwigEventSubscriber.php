<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\EventSubscriber;

use BasicBuilder\Bundle\EasyBuilderBundle\Entity\MainMenu;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\TemplateControllerInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ArticleGroupTypesRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\MainMenuRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\TemplateSettingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ErrorController;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Twig\Environment;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $templateSettingsRepository;
    private $mainMenuRepository;
    private $groupTypesRepository;
    private $entityManager;
    private $templateDir;
    private $templateDefaultPath;
    private $parameterBag;

    public function __construct(Environment $twig,
                                TemplateSettingsRepository $templateSettingsRepository,
                                MainMenuRepository $mainMenuRepository,
                                ArticleGroupTypesRepository $groupTypesRepository,
                                EntityManagerInterface $entityManager,
                                ParameterBagInterface $parameterBag
                                )
    {
        $this->twig = $twig;
        $this->templateSettingsRepository = $templateSettingsRepository;
        $this->mainMenuRepository = $mainMenuRepository;
        $this->groupTypesRepository = $groupTypesRepository;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->templateDir = $parameterBag->get('templateDir');
        $this->templateDefaultPath = $parameterBag->get('twig.default_path');
    }

    public function onControllerEvent(ControllerEvent $event)
    {

        if($event->getController() instanceof ErrorController === false &&
            is_array($event->getController()) &&
            $event->getController()[0] instanceof TemplateControllerInterface == true ) {

            $templateSettings = $this->templateSettingsRepository->findCurrent();

            $mainMenu = $this->mainMenuRepository->findForMain(MainMenu::IS_MAIN_MENU);
            $footerMenu = $this->mainMenuRepository->findForMain(MainMenu::IS_FOOTER);
            $articleGroupTypes = $this->groupTypesRepository->findForMain();

            if(!$templateSettings) {

                $templateSettings = new TemplateSettings();
                $templateSettings->setName('Merx');
                $templateSettings->setAddress('13636 Ventura Blvd Unit 103, Sherman Oaks, California 91423');
                $templateSettings->setPhone('+1 818-648-8208');
                $templateSettings->setEmail('cs@merxforum.com');
                $templateSettings->setFb('#');
                $templateSettings->setInstagram('#');

                $templateInfo = new TemplateInfo();
                $templateInfo->setBaseDirName('main');
                $templateInfo->setName('Basic');
                $templateSettings->setTemplateInfo($templateInfo);


                $this->entityManager->persist($templateSettings);

                $this->entityManager->flush();
            }


            $this->twig->addGlobal('templateSettings', $templateSettings);
            $this->twig->addGlobal('menus', $mainMenu);
            $this->twig->addGlobal('sections', $articleGroupTypes);
            $this->twig->addGlobal('footerMenu', $footerMenu);

        }



    }

    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }

    private function makeTemplateDir() {

        $fileSystem = new Filesystem();

        $tmpDir = "{$this->templateDefaultPath}/{$this->templateDir}";

        if($fileSystem->exists($tmpDir) === false) {

            try {
                $fileSystem->mkdir($tmpDir, 0755);
                $finder = new Finder();
                $finder->files()->in("{$this->templateDefaultPath}/main");

                if($finder->hasResults()) {

                    foreach ($finder as $file) {

                        $finder->files()->in("{$this->templateDefaultPath}/main");

                        $fileNameWithExtension = $file->getRelativePathname();

                        $fileSystem->copy("{$this->templateDefaultPath}/main/{$fileNameWithExtension}", "{$tmpDir}/{$fileNameWithExtension}" );
                    }
                }

            }catch (\Exception $e){

                throw new HttpException(Response::HTTP_NOT_FOUND, "Sorry the system can't crate directory. Please check directory permissions of template.");
            }

        }
    }
}
