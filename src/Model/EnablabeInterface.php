<?php
/**
 * Created by PhpStorm.
 * User: vaz
 * Date: 6/19/15
 * Time: 5:59 PM
 */
namespace BasicBuilder\Bundle\EasyBuilderBundle\Model;

/**
 * This interface use for active or inactive from admin page
 *
 * Interface ActiveInterface
 * @package AppBundle\Model
 */
interface EnablabeInterface
{
    public function setEnabled($isEnabled);
    public function getEnabled();
}