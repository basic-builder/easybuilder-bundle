<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Model;


interface FileUploadableInterface
{

    public function getFile();
    public function uploadFile();

}