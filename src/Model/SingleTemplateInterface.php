<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Model;


use BasicBuilder\Bundle\EasyBuilderBundle\Entity\SingleTemplate;

interface SingleTemplateInterface
{

    public function getSingleTemplate(): ?SingleTemplate;

    public function setSingleTemplate(?SingleTemplate $singleTemplate);
}