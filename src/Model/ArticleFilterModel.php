<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Model;


use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;

class ArticleFilterModel
{


    /**
     * @var int
     */
    private $limit = 6;

    /**
     * @var array
     */
    private $articleType = [];

    /**
     * @var string
     */
    private $mainMenuSlug;

    /**
     * @var string
     */
    private $subMenuSlug;

    /**
     * @var string
     */
    private $articleGroupSlug;

    /**
     * @var string
     */
    private $articleGroupCategory;

    /**
     * @var array
     */
    private $orderBy = ['field'=>'created', 'order'=>'DESC'];

    /**
     * @var bool
     */
    private $isMain = true;

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return array
     */
    public function getArticleType(): ?array
    {
        return $this->articleType;
    }

    /**
     * @param array $articleType
     */
    public function setArticleType(array $articleType): self
    {
        $this->articleType = $articleType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainMenuSlug(): ?string
    {
        return $this->mainMenuSlug;
    }

    /**
     * @param string $mainMenuSlug
     */
    public function setMainMenuSlug(?string $mainMenuSlug): self
    {
        $this->mainMenuSlug = $mainMenuSlug;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubMenuSlug(): ?string
    {
        return $this->subMenuSlug;
    }

    /**
     * @param string $subMenuSlug
     */
    public function setSubMenuSlug(?string $subMenuSlug): self
    {
        $this->subMenuSlug = $subMenuSlug;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticleGroupSlug(): ?string
    {
        return $this->articleGroupSlug;
    }

    /**
     * @param string $articleGroupSlug
     */
    public function setArticleGroupSlug(?string $articleGroupSlug): self
    {
        $this->articleGroupSlug = $articleGroupSlug;
        return $this;
    }

    /**
     * @return array
     */
    public function getOrderBy(): array
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy): self
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        return $this->isMain;
    }

    /**
     * @param bool $isMain
     */
    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticleGroupCategory(): ?string
    {
        return $this->articleGroupCategory;
    }

    /**
     * @param string $articleGroupCategory
     */
    public function setArticleGroupCategory(?string $articleGroupCategory): self
    {
        $this->articleGroupCategory = $articleGroupCategory;
        return $this;
    }


}