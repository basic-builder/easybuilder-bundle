<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Routing;


use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateInfo;
use BasicBuilder\Bundle\EasyBuilderBundle\Entity\TemplateSettings;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class TemplateLoader extends Loader
{
    private $em;

    private  $isLoaded = false;
    private  $defaultTemplateDir;

    public function __construct(EntityManagerInterface $em,
                                ParameterBagInterface $parameterBag)
    {
        $this->em = $em;
        $this->defaultTemplateDir = $parameterBag->get('templateDir');
    }

    public function load($resource, string $type = null)
    {
        $em  = $this->em;
        $templateSettings = $em->getRepository(TemplateSettings::class)->findCurrent();

        $templateBaseDir = $this->defaultTemplateDir;
        $mainPath = '';
        if($templateSettings instanceof TemplateSettings && $templateSettings->getTemplateInfo() instanceof TemplateInfo) {

            $templateBaseDir = $templateSettings->getTemplateInfo()->getBaseDirName();
        }

        if (true === $this->isLoaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        // Index action home page
        $path = $mainPath.'/';
        $defaults = [
            '_controller' => 'BasicBuilder\Bundle\EasyBuilderBundle\Controller\TemplateController::index',
            'template'=>"{$templateBaseDir}"
        ];

        $route = new Route($path, $defaults, [], [], null, ['http', 'https'], ['GET']);

        // add the new route to the route collection
        $routeName = 'homepage';
        $routes->add($routeName, $route);

        // Index action
        $path = $mainPath.'/home-slider';
        $defaults = [
            '_controller' => 'BasicBuilder\Bundle\EasyBuilderBundle\Controller\TemplateController::menuArticle',
            'template'=>"{$templateBaseDir}"
        ];

        $route = new Route($path, $defaults, [], [], null, ['http', 'https'], ['GET']);

        // add the new route to the route collection
        $routeName = 'menuArticle';
        $routes->add($routeName, $route);

        $path = $mainPath.'/sitemap';
        $defaults = [
            '_controller' => 'BasicBuilder\Bundle\EasyBuilderBundle\Controller\TemplateController::sitemap',
            'template'=>"{$templateBaseDir}"
        ];

        $route = new Route($path, $defaults, [], [], null, ['http', 'https'], ['GET']);

        // add the new route to the route collection
        $routeName = 'expert-sitemap';
        $routes->add($routeName, $route);


        // prepare a new route for main menu
        $path = '/{slug}';
        $defaults = [
            '_controller' => 'BasicBuilder\Bundle\EasyBuilderBundle\Controller\TemplateController::mainMenu',
            'template'=>"{$templateBaseDir}"
        ];
        $requirements = [
            'slug' => '^[a-z0-9-]+$',
        ];
        $route = new Route($path, $defaults, $requirements, [], null, ['http', 'https'], ['GET']);

        // add the new route to the route collection
        $routeName = 'main-menu';
        $routes->add($routeName, $route);

        // prepare a new route for subMenu
        $path = '/{slug}/{subSlug}';
        $defaults = [
            '_controller' => 'BasicBuilder\Bundle\EasyBuilderBundle\Controller\TemplateController::subMenu',
            'template'=>"{$templateBaseDir}"
        ];
        $requirements = [
            'slug' => '^[a-z0-9-]+$',
            'subSlug' => '^[a-z0-9-]+$',
        ];
        $route = new Route($path, $defaults, $requirements, [], null, ['http', 'https'], ['GET']);

        // add the new route to the route collection
        $routeName = 'sub-menu';
        $routes->add($routeName, $route);

        // prepare a new route for Article
        $article = '/{slug}/{subSlug}/{slugArticle}';
        $singleArticle = [
            '_controller' => 'BasicBuilder\Bundle\EasyBuilderBundle\Controller\TemplateController::article',
            'template'=>"{$templateBaseDir}"
        ];
        $requirementsArticle = [
            'slug' => '^[a-z0-9-]+$',
            'subSlug' => '^[a-z0-9-]+$',
            'slugArticle' => '^[a-z0-9-]+$',
        ];

        $routeArticle = new Route($article, $singleArticle, $requirementsArticle, [], null, ['http', 'https'], ['GET']);
//
//        // add the new route to the route collection
        $articleRouteName = 'single-article';
        $routes->add($articleRouteName, $routeArticle);

        $this->isLoaded = true;

        return $routes;
    }

    public function supports($resource, string $type = null)
    {
        return 'template' === $type;
    }
}