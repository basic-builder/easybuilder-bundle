<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field;

use BasicBuilder\Bundle\EasyBuilderBundle\Form\SingleTemplateType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;


class AdminSingleTemplateField implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {

        return (new self())
            ->setProperty($propertyName)
            ->setCssClass('single_template_widget')
            ->setLabel($label)
            ->setTemplatePath('@EasyBuilder/form/fields.html.twig')
            ->setFormType(SingleTemplateType::class)
            ;

    }
}