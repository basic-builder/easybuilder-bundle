<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field;

use BasicBuilder\Bundle\EasyBuilderBundle\Form\ArticleType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;


class AdminTranslationShow implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null, bool $isRaw = false)
    {

        return (new self())
            ->setProperty($propertyName)
            ->setCustomOption('raw', $isRaw)
            ->setCssClass('admin_translation_show_widget')
            ->setLabel($label)
            ->setTemplatePath('@EasyBuilder/admin/show/admin_translation_show.html.twig')
            ;

    }
}