<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field;

use BasicBuilder\Bundle\EasyBuilderBundle\Form\IconType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;


class AdminImagesField implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {

        return (new self())
            ->setProperty($propertyName)
            ->setCssClass('image_widget')
            ->setLabel($label)
            ->setTemplatePath('@EasyBuilder/form/files.html.twig')
            ;

    }
}