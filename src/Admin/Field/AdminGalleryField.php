<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field;


use BasicBuilder\Bundle\EasyBuilderBundle\Form\GalleryType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

class AdminGalleryField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {
        return (new self())
            ->setProperty($propertyName)
            ->setCssClass('gallery_widget')
            ->setLabel($label)
            ->setTemplatePath('@EasyBuilder/form/fields.html.twig')
            ->setFormType(GalleryType::class);
    }
}