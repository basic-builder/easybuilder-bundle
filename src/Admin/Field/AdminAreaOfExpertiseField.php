<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field;

use BasicBuilder\Bundle\EasyBuilderBundle\Form\IconType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;


class AdminAreaOfExpertiseField implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {

        return (new self())
            ->setProperty($propertyName)
            ->setCssClass('expertise_widget')
            ->setLabel($label)
            ->setTemplatePath('@EasyBuilder/form/area_of_expertise.html.twig')
            ;

    }
}