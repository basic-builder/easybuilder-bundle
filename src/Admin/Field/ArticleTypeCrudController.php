<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Admin\Field;

use BasicBuilder\Bundle\EasyBuilderBundle\Form\ArticleType;
use BasicBuilder\Bundle\EasyBuilderBundle\Form\ArticleTypeType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;


class ArticleTypeCrudController implements FieldInterface
{

    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {

        return (new self())
            ->setProperty($propertyName)
            ->setCssClass('article_type_widget')
            ->setLabel($label)
            ->setTemplatePath('@EasyBuilder/admin/show/admin_article_type_show.html.twig')
            ->setFormType(ArticleTypeType::class);

    }
}