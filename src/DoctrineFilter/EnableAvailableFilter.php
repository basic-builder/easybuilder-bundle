<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\DoctrineFilter;

use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\ORM\Mapping\ClassMetaData;

/**
 * Class OperatorFilter
 * @package AppBundle\DoctrineFilter
 */
class EnableAvailableFilter extends SQLFilter
{
	/**
	 * This Doctrine filter return only published Items
	 *
	 * @param ClassMetaData $targetEntity
	 * @param $targetTableAlias
	 * @return string
	 */
	public function addFilterConstraint(ClassMetaData $targetEntity, $targetTableAlias)
	{
		if ($targetEntity->reflClass->implementsInterface('BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface')) {

			return $targetTableAlias . '.enabled = true';

		} /*elseif($targetEntity->reflClass->implementsInterface('AppBundle\Model\CarFreeInterface')) {

			return $targetTableAlias . '.status = 1';
		}*/
		else {
			return "";
		}
	}
}