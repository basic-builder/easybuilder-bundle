<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\EventListener;


use BasicBuilder\Bundle\EasyBuilderBundle\Entity\Article;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\FileUploadableInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class EventListeners
{

    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getObject();

        // only act on some "Product" entity
        if ($entity instanceof FileUploadableInterface && $entity->getFile()) {

            $entity->uploadFile();

        }
    }

    public function preUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getObject();

        // only act on some "Product" entity
        if ($entity instanceof FileUploadableInterface && $entity->getFile()) {

            $entity->uploadFile();

        }
    }
}