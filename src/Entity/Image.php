<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ImageRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\File;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 */
class Image
{
    use GedmoDateable, EnableAvailable, File;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"image-info"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"image-info"})
     */
    private $urlLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"image-info"})
     */
    private $altText;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"image-info"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"image-info"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Gallery::class, inversedBy="item" ,cascade={"persist"})
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $gallery;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="image")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity=TemplateSettings::class, mappedBy="image")
     */
    private $templateSettings;

    /**
     * @ORM\OneToMany(targetEntity=SubMenu::class, mappedBy="image")
     */
    private $subMenus;

    /**
     * @ORM\ManyToOne(targetEntity=Quote::class, inversedBy="images", cascade={"persist"})
     * @ORM\JoinColumn(name="quotes_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $quoteFiles;

    /**
     * @ORM\ManyToOne(targetEntity=CV::class, inversedBy="images", cascade={"persist"})
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $cvFiles;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->templateSettings = new ArrayCollection();
        $this->subMenus = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id ? $this->getDownloadLink() : 'New Image';
        // TODO: Implement __toString() method.
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrlLink(): ?string
    {
        return $this->urlLink;
    }

    public function setUrlLink(?string $urlLink): self
    {
        $this->urlLink = $urlLink;

        return $this;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): self
    {
        $this->altText = $altText;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setImage($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getImage() === $this) {
                $article->setImage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TemplateSettings[]
     */
    public function getTemplateSettings(): Collection
    {
        return $this->templateSettings;
    }

    public function addTemplateSetting(TemplateSettings $templateSetting): self
    {
        if (!$this->templateSettings->contains($templateSetting)) {
            $this->templateSettings[] = $templateSetting;
            $templateSetting->setImage($this);
        }

        return $this;
    }

    public function removeTemplateSetting(TemplateSettings $templateSetting): self
    {
        if ($this->templateSettings->removeElement($templateSetting)) {
            // set the owning side to null (unless already changed)
            if ($templateSetting->getImage() === $this) {
                $templateSetting->setImage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SubMenu[]
     */
    public function getSubMenus(): Collection
    {
        return $this->subMenus;
    }

    public function addSubMenu(SubMenu $subMenu): self
    {
        if (!$this->subMenus->contains($subMenu)) {
            $this->subMenus[] = $subMenu;
            $subMenu->setImage($this);
        }

        return $this;
    }

    public function removeSubMenu(SubMenu $subMenu): self
    {
        if ($this->subMenus->removeElement($subMenu)) {
            // set the owning side to null (unless already changed)
            if ($subMenu->getImage() === $this) {
                $subMenu->setImage(null);
            }
        }

        return $this;
    }

    public function getQuoteFiles(): ?Quote
    {
        return $this->quoteFiles;
    }

    public function setQuoteFiles(?Quote $quoteFiles): self
    {
        $this->quoteFiles = $quoteFiles;

        return $this;
    }

    public function getCvFiles(): ?CV
    {
        return $this->cvFiles;
    }

    public function setCvFiles(?CV $cvFiles): self
    {
        $this->cvFiles = $cvFiles;

        return $this;
    }
}
