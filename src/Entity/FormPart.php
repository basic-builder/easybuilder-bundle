<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\FormPartRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FormPartRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"slug"}, errorPath="name")
 */
class FormPart
{
    use GedmoSlugable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=SingleTemplate::class, inversedBy="formParts", cascade={"persist"})
     * @ORM\JoinColumn(name="form_definition_id", onDelete="SET NULL", referencedColumnName="id")
     * @Assert\NotNull()
     * @Assert\Valid()
     */
    private $formDefinition;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRequired;

    /**
     * @ORM\ManyToOne(targetEntity=FormInfo::class, inversedBy="formPart", cascade={"persist"})
     * @ORM\JoinColumn(name="form_info", onDelete="SET NULL", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $formInfo;

    public function __toString():string
    {
        return $this->id ? $this->getName(): 'New Form Part';
    }

    public function __construct()
    {
        $this->isRequired = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFormDefinition(): ?SingleTemplate
    {
        return $this->formDefinition;
    }

    public function setFormDefinition(?SingleTemplate $formDefinition): self
    {
        $this->formDefinition = $formDefinition;

        return $this;
    }

    public function getIsRequired(): ?bool
    {
        return $this->isRequired;
    }

    public function setIsRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    public function getFormInfo(): ?FormInfo
    {
        return $this->formInfo;
    }

    public function setFormInfo(?FormInfo $formInfo): self
    {
        $this->formInfo = $formInfo;

        return $this;
    }
}
