<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\LanguageSkillsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LanguageSkillsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class LanguageSkills
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.language_blank")
     * @Assert\NotNull(message="form.language_null")
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.level_blank")
     * @Assert\NotNull(message="form.level_null")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity=CV::class, inversedBy="languageSkills", cascade={"persist"})
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $cv;

    public function __toString()
    {
        return $this->id ? $this->language : 'New Language Skills';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getCv(): ?CV
    {
        return $this->cv;
    }

    public function setCv(?CV $cv): self
    {
        $this->cv = $cv;

        return $this;
    }
}
