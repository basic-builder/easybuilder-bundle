<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\SingleTemplateRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=SingleTemplateRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class SingleTemplate implements EnablabeInterface
{

    const IS_TEMPLATE = 1, IS_FORM = 2;

    use GedmoDateable, GedmoSlugable, EnableAvailable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $dirPath;

    /**
     * @ORM\OneToMany(targetEntity=MainMenu::class, mappedBy="singleTemplate")
     */
    private $mainMenus;

    /**
     * @ORM\OneToMany(targetEntity=ArticleGroup::class, mappedBy="singleTemplate")
     */
    private $articleGroups;

    /**
     * @ORM\OneToMany(targetEntity=ArticleGroupTypes::class, mappedBy="template")
     */
    private $articleGroupTypes;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default":1})
     *
     */
    private $definition;

    /**
     * @ORM\OneToMany(targetEntity=FormPart::class, mappedBy="formDefinition")
     */
    private $formParts;


    public function __toString()
    {
        return $this->id ? $this->name : 'New simple Template';
        // TODO: Implement __toString() method.
    }


    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // somehow you have an array of "fake names"

        $fileSystem = new Filesystem();

        switch ($this->definition){
            case self::IS_TEMPLATE:
                $fileRalDir = sprintf("%s/../../../../../%s/%s",__DIR__, 'templates',$this->dirPath);
                break;
            case self::IS_FORM:
                $fileRalDir = sprintf("%s/../Form/Fields/%s.%s",__DIR__,$this->dirPath, 'php');

                break;
        }

        if($fileSystem->exists($fileRalDir) == false) {
            $context->buildViolation("Sorry. The file by dir name not found.")
                ->atPath('dirPath')
                ->addViolation();
        }
    }

    public function __construct()
    {
        $this->mainMenus = new ArrayCollection();
        $this->articleGroups = new ArrayCollection();
        $this->articleGroupTypes = new ArrayCollection();
        $this->enabled = true;
        $this->definition = self::IS_TEMPLATE;
        $this->formParts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDirPath(): ?string
    {
        return $this->dirPath;
    }

    public function setDirPath(string $dirPath): self
    {
        $this->dirPath = $dirPath;

        return $this;
    }

    /**
     * @return Collection|MainMenu[]
     */
    public function getMainMenus(): Collection
    {
        return $this->mainMenus;
    }

    public function addMainMenu(MainMenu $mainMenu): self
    {
        if (!$this->mainMenus->contains($mainMenu)) {
            $this->mainMenus[] = $mainMenu;
            $mainMenu->setSingleTemplate($this);
        }

        return $this;
    }

    public function removeMainMenu(MainMenu $mainMenu): self
    {
        if ($this->mainMenus->removeElement($mainMenu)) {
            // set the owning side to null (unless already changed)
            if ($mainMenu->getSingleTemplate() === $this) {
                $mainMenu->setSingleTemplate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleGroup[]
     */
    public function getArticleGroups(): Collection
    {
        return $this->articleGroups;
    }

    public function addArticleGroup(ArticleGroup $articleGroup): self
    {
        if (!$this->articleGroups->contains($articleGroup)) {
            $this->articleGroups[] = $articleGroup;
            $articleGroup->setSingleTemplate($this);
        }

        return $this;
    }

    public function removeArticleGroup(ArticleGroup $articleGroup): self
    {
        if ($this->articleGroups->removeElement($articleGroup)) {
            // set the owning side to null (unless already changed)
            if ($articleGroup->getSingleTemplate() === $this) {
                $articleGroup->setSingleTemplate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleGroupTypes[]
     */
    public function getArticleGroupTypes(): Collection
    {
        return $this->articleGroupTypes;
    }

    public function addArticleGroupType(ArticleGroupTypes $articleGroupType): self
    {
        if (!$this->articleGroupTypes->contains($articleGroupType)) {
            $this->articleGroupTypes[] = $articleGroupType;
            $articleGroupType->setTemplate($this);
        }

        return $this;
    }

    public function removeArticleGroupType(ArticleGroupTypes $articleGroupType): self
    {
        if ($this->articleGroupTypes->removeElement($articleGroupType)) {
            // set the owning side to null (unless already changed)
            if ($articleGroupType->getTemplate() === $this) {
                $articleGroupType->setTemplate(null);
            }
        }

        return $this;
    }

    public function getDefinition(): ?int
    {
        return $this->definition;
    }

    public function setDefinition(?int $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    /**
     * @return Collection|FormPart[]
     */
    public function getFormParts(): Collection
    {
        return $this->formParts;
    }

    public function addFormPart(FormPart $formPart): self
    {
        if (!$this->formParts->contains($formPart)) {
            $this->formParts[] = $formPart;
            $formPart->setFormDefinition($this);
        }

        return $this;
    }

    public function removeFormPart(FormPart $formPart): self
    {
        if ($this->formParts->removeElement($formPart)) {
            // set the owning side to null (unless already changed)
            if ($formPart->getFormDefinition() === $this) {
                $formPart->setFormDefinition(null);
            }
        }

        return $this;
    }
}
