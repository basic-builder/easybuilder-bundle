<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\GalleryRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GalleryRepository::class)
 */
class Gallery implements EnablabeInterface
{
    use GedmoDateable, EnableAvailable;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="gallery")
     * @ORM\OrderBy({"created":"ASC"})
     */
    private $item;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isMain;

    public function __toString()
    {
        return $this->id ? $this->getTitle() : 'New Gallery';
        // TODO: Implement __toString() method.
    }

    public function __construct()
    {
        $this->item = new ArrayCollection();
        $this->isMain = false;
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getItem(): Collection
    {
        return $this->item;
    }

    public function addItem(Image $item): self
    {
        if (!$this->item->contains($item)) {
            $this->item[] = $item;
            $item->setGallery($this);
        }

        return $this;
    }

    public function removeItem(Image $item): self
    {
        if ($this->item->contains($item)) {
            $this->item->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getGallery() === $this) {
                $item->setGallery(null);
            }
        }

        return $this;
    }

    public function getIsMain(): ?bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }

}
