<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ContactRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Contact
{
    use GedmoDateable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(groups={"insert-message"})
     * @Assert\NotNull(groups={"insert-message"})
     * @Assert\Length(min="3", max="50", groups={"insert-message"}, minMessage="contacts.minName", maxMessage="contacts.maxName")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"insert-message"})
     * @Assert\NotNull(groups={"insert-message"})
     * @Assert\Length(min="3", max="50", groups={"insert-message"})
     * @Assert\Email(
     *     message="contacts.email",groups={"insert-message"},
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"insert-message"})
     * @Assert\NotNull(groups={"insert-message"})
     * @Assert\Length(min="3", max="50", groups={"insert-message"}, minMessage="contacts.minSubject", maxMessage="contacts.maxSubject")
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(groups={"insert-message"})
     * @Assert\NotNull(groups={"insert-message"})
     * @Assert\Length(min="3", max="250", groups={"insert-message"}, minMessage="contacts.minMessage", maxMessage="contacts.maxMessage")
     */
    private $message;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }


    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     * @Assert\Callback(groups={"insert-message"})
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // somehow you have an array of "fake names"

        if (strpos(strtolower($this->message), 'http') !== false || strpos(strtolower($this->message), 'www') !== false) {

            $context->buildViolation("Sorry. In the message field cannot contain url.")
                ->atPath('message')
                ->addViolation();
        }

    }
}
