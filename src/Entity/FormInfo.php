<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\FormInfoRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FormInfoRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"slug"}, errorPath="name")
 */
class FormInfo
{
    use GedmoSlugable, GedmoDateable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=FormPart::class, mappedBy="formInfo", cascade={"persist"})
     * @Assert\Valid()
     */
    private $formPart;

    public function __toString()
    {
        return $this->id ? $this->getName() : 'New Form info';
    }

    public function __construct()
    {
        $this->formPart = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|FormPart[]
     */
    public function getFormPart(): Collection
    {
        return $this->formPart;
    }

    public function addFormPart(FormPart $formPart): self
    {
        if (!$this->formPart->contains($formPart)) {
            $this->formPart[] = $formPart;
            $formPart->setFormInfo($this);
        }

        return $this;
    }

    public function removeFormPart(FormPart $formPart): self
    {
        if ($this->formPart->removeElement($formPart)) {
            // set the owning side to null (unless already changed)
            if ($formPart->getFormInfo() === $this) {
                $formPart->setFormInfo(null);
            }
        }

        return $this;
    }
}
