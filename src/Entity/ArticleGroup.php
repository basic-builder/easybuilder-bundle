<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\SingleTemplateInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ArticleGroupRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoPositionable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArticleGroupRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class ArticleGroup implements EnablabeInterface, SingleTemplateInterface, TranslatableInterface
{
    use GedmoDateable, GedmoSlugable, EnableAvailable, TranslatableTrait, GedmoPositionable;

    const IS_MEMBER = 'member', IS_STATISTICS = 'statistics', CLIENT_TESTIMONIALS = 'client testimonials', IS_PARTNERS = 'partner', IS_SERVICE = 'services';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

//    /**
//     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="articleGroup", cascade={"persist"}, orphanRemoval=true)
//     * @Assert\Valid(groups={"article-group"})
//     */
//    private $article;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=SingleTemplate::class, inversedBy="articleGroups", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $singleTemplate;

    /**
     * @ORM\ManyToOne(targetEntity=ArticleGroupTypes::class, inversedBy="articleGroups", cascade={"persist"})
     * @ORM\JoinColumn(name="group_type_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $groupType;

    /**
     * @ORM\ManyToMany(targetEntity=Article::class, inversedBy="articleGroups", cascade={"persist"})
     *
     */
    private $article;

    public function __construct()
    {
        $this->article = new ArrayCollection();
    }

    public function __call($name, $arguments)
    {
        return $this->proxyCurrentLocaleTranslation($name, $arguments);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSingleTemplate(): ?SingleTemplate
    {
        return $this->singleTemplate;
    }

    public function setSingleTemplate(?SingleTemplate $singleTemplate): self
    {
        $this->singleTemplate = $singleTemplate;

        return $this;
    }

    public function getGroupType(): ?ArticleGroupTypes
    {
        return $this->groupType;
    }

    public function setGroupType(?ArticleGroupTypes $groupType): self
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticle(): Collection
    {
        return $this->article;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->article->contains($article)) {
            $this->article[] = $article;
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        $this->article->removeElement($article);

        return $this;
    }
}
