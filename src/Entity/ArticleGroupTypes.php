<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ArticleGroupTypesRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoPositionable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArticleGroupTypesRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class ArticleGroupTypes
{
    use GedmoDateable, GedmoPositionable, GedmoSlugable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=SingleTemplate::class, inversedBy="articleGroupTypes", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Assert\NotNull(message="The template value cannot be null.")
     * @Assert\Valid()
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity=ArticleGroup::class, mappedBy="groupType", cascade={"persist"})
     */
    private $articleGroups;

    public function __toString()
    {
        return $this->id ? $this->getName() : 'New File';
    }

    public function __construct()
    {
        $this->articleGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemplate(): ?SingleTemplate
    {
        return $this->template;
    }

    public function setTemplate(?SingleTemplate $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return Collection|ArticleGroup[]
     */
    public function getArticleGroups(): Collection
    {
        return $this->articleGroups;
    }

    public function addArticleGroup(ArticleGroup $articleGroup): self
    {
        if (!$this->articleGroups->contains($articleGroup)) {
            $this->articleGroups[] = $articleGroup;
            $articleGroup->setGroupType($this);
        }

        return $this;
    }

    public function removeArticleGroup(ArticleGroup $articleGroup): self
    {
        if ($this->articleGroups->removeElement($articleGroup)) {
            // set the owning side to null (unless already changed)
            if ($articleGroup->getGroupType() === $this) {
                $articleGroup->setGroupType(null);
            }
        }

        return $this;
    }
}
