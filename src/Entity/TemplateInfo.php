<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\TemplateInfoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=TemplateInfoRepository::class)
 * @UniqueEntity(fields={"baseDirName"}, errorPath="baseDirName")
 */
class TemplateInfo
{
    use GedmoDateable, GedmoSlugable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="bd_name", type="string", length=255)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $baseDirName;

    /**
     * @ORM\OneToMany(targetEntity=TemplateSettings::class, mappedBy="templateInfo")
     */
    private $templateSettings;

    public function __construct()
    {
        $this->templateSettings = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id ? $this->getName() : 'New Template';
        // TODO: Implement __toString() method.
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBaseDirName(): ?string
    {
        return $this->baseDirName;
    }

    public function setBaseDirName(string $baseDirName): self
    {
        $this->baseDirName = $baseDirName;

        return $this;
    }

    /**
     * @return Collection|TemplateSettings[]
     */
    public function getTemplateSettings(): Collection
    {
        return $this->templateSettings;
    }

    public function addTemplateSetting(TemplateSettings $templateSetting): self
    {
        if (!$this->templateSettings->contains($templateSetting)) {
            $this->templateSettings[] = $templateSetting;
            $templateSetting->setTemplateInfo($this);
        }

        return $this;
    }

    public function removeTemplateSetting(TemplateSettings $templateSetting): self
    {
        if ($this->templateSettings->contains($templateSetting)) {
            $this->templateSettings->removeElement($templateSetting);
            // set the owning side to null (unless already changed)
            if ($templateSetting->getTemplateInfo() === $this) {
                $templateSetting->setTemplateInfo(null);
            }
        }

        return $this;
    }
}
