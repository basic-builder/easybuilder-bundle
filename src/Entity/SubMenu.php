<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\SubMenuRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoPositionable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\SeoFriendly;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * @ORM\Entity(repositoryClass=SubMenuRepository::class)
 */
class SubMenu implements EnablabeInterface, TranslatableInterface
{
    use GedmoSlugable, GedmoDateable, SeoFriendly, GedmoPositionable, EnableAvailable, TranslatableTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="subMenus", cascade={"persist"})
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $mainArticle;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="subMenu")
     * @ORM\OrderBy({"position":"ASC", "created":"DESC"})
     */
    private $articles;

    /**
     * @ORM\ManyToOne(targetEntity=MainMenu::class, inversedBy="subMenus", cascade={"persist"})
     * @ORM\JoinColumn(name="main_menu_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $mainMenu;

    /**
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $isInMainMenu;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="subMenus", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    public function __toString()
    {
        return $this->id ? $this->getName() : 'New Submenu';
        // TODO: Implement __toString() method.
    }

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->isInMainMenu = true;
    }

    public function __call($name, $arguments)
    {
        return $this->proxyCurrentLocaleTranslation($name, $arguments);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMainArticle(): ?Article
    {
        return $this->mainArticle;
    }

    public function setMainArticle(?Article $mainArticle): self
    {
        $this->mainArticle = $mainArticle;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setSubMenu($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getSubMenu() === $this) {
                $article->setSubMenu(null);
            }
        }

        return $this;
    }

    public function getMainMenu(): ?MainMenu
    {
        return $this->mainMenu;
    }

    public function setMainMenu(?MainMenu $mainMenu): self
    {
        $this->mainMenu = $mainMenu;

        return $this;
    }

    public function getIsInMainMenu(): ?bool
    {
        return $this->isInMainMenu;
    }

    public function setIsInMainMenu(bool $isInMainMenu): self
    {
        $this->isInMainMenu = $isInMainMenu;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }
}
