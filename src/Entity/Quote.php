<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\QuoteRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=QuoteRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"email"}, errorPath="email")
 */
class Quote
{
    use GedmoDateable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.name_blank")
     * @Assert\NotNull(message="form.name_null")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.company_blank")
     * @Assert\NotNull(message="form.company_null")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(message="contacts.email")
     * @Assert\NotBlank(message="form.email_blank")
     * @Assert\NotNull(message="form.email_null")
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $turnaroundTime;

    /**
     * @ORM\Column(type="array")
     * @Assert\NotBlank(message="form.service")
     * @Assert\NotNull(message="form.service")
     */
    private $service = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="quoteFiles", cascade={"persist"})
     */
    private $images;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sourceLanguage;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.target_languages_blank")
     * @Assert\NotNull(message="form.target_languages_null")
     *
     */
    private $targetLanguage;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id ? $this->name : 'New Quote';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDeadline(): ?\DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?\DateTimeInterface $deadline): self
    {
        $this->deadline = $deadline;

        return $this;
    }

    public function getTurnaroundTime(): ?int
    {
        return $this->turnaroundTime;
    }

    public function setTurnaroundTime(?int $turnaroundTime): self
    {
        $this->turnaroundTime = $turnaroundTime;

        return $this;
    }

    public function getService(): ?array
    {
        return $this->service;
    }

    public function setService(array $service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {

        if (is_null($this->getTurnaroundTime()) && is_null($this->getDeadline())) {
            $context->buildViolation('form.deadline_turnaround')
                ->atPath('deadline')
                ->addViolation();
        }

        // somehow you have an array of "fake names"

        if (strpos(strtolower($this->notes), 'http') !== false || strpos(strtolower($this->notes), 'www') !== false) {

            $context->buildViolation("form.http")
                ->atPath('notes')
                ->addViolation();
        }

    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setQuoteFiles($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getQuoteFiles() === $this) {
                $image->setQuoteFiles(null);
            }
        }

        return $this;
    }

    public function getSourceLanguage(): ?string
    {
        return $this->sourceLanguage;
    }

    public function setSourceLanguage(?string $sourceLanguage): self
    {
        $this->sourceLanguage = $sourceLanguage;

        return $this;
    }

    public function getTargetLanguage(): ?string
    {
        return $this->targetLanguage;
    }

    public function setTargetLanguage(string $targetLanguage): self
    {
        $this->targetLanguage = $targetLanguage;

        return $this;
    }

}
