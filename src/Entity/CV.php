<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\CVRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CVRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"email"}, errorPath="email", message="form.uniq_email")
 */
class CV
{

    use GedmoDateable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(message="contacts.email")
     * @Assert\NotBlank(message="form.email_blank")
     * @Assert\NotNull(message="form.email_null")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(message="form.name_blank")
     * @Assert\NotNull(message="form.name_null")
     */
    private $phone;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $role = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $catTools = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.name_blank")
     * @Assert\NotNull(message="form.name_null")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.last_name_blank")
     * @Assert\NotNull(message="form.last_name_null")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motherTongue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $availability;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $weekendWork;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $coverLetter;

    /**
     * @ORM\OneToMany(targetEntity=LanguageSkills::class, mappedBy="cv", cascade={"persist"})
     * @Assert\Valid()
     */
    private $languageSkills;

    /**
     * @ORM\OneToMany(targetEntity=AreasOfExpertise::class, mappedBy="cv", cascade={"persist"})
     * @Assert\Valid()
     */
    private $areasOfExpertises;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="cvFiles", cascade={"persist"})
     */
    private $images;

    public function __construct()
    {

        $this->languageSkills = new ArrayCollection();
        $this->areasOfExpertises = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id ? $this->firstName . ' ' . $this->lastName : 'New CV';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRole(): ?array
    {
        return $this->role;
    }

    public function setRole(?array $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getCatTools(): ?array
    {
        return $this->catTools;
    }

    public function setCatTools(?array $catTools): self
    {
        $this->catTools = $catTools;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getMotherTongue(): ?string
    {
        return $this->motherTongue;
    }

    public function setMotherTongue(string $motherTongue): self
    {
        $this->motherTongue = $motherTongue;

        return $this;
    }

    public function getAvailability(): ?string
    {
        return $this->availability;
    }

    public function setAvailability(?string $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getWeekendWork(): ?bool
    {
        return $this->weekendWork;
    }

    public function setWeekendWork(?bool $weekendWork): self
    {
        $this->weekendWork = $weekendWork;

        return $this;
    }

    public function getCoverLetter(): ?string
    {
        return $this->coverLetter;
    }

    public function setCoverLetter(?string $coverLetter): self
    {
        $this->coverLetter = $coverLetter;

        return $this;
    }

    /**
     * @return Collection|LanguageSkills[]
     */
    public function getLanguageSkills(): Collection
    {
        return $this->languageSkills;
    }

    public function addLanguageSkill(LanguageSkills $languageSkill): self
    {
        if (!$this->languageSkills->contains($languageSkill)) {
            $this->languageSkills[] = $languageSkill;
            $languageSkill->setCv($this);
        }

        return $this;
    }

    public function removeLanguageSkill(LanguageSkills $languageSkill): self
    {
        if ($this->languageSkills->removeElement($languageSkill)) {
            // set the owning side to null (unless already changed)
            if ($languageSkill->getCv() === $this) {
                $languageSkill->setCv(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AreasOfExpertise[]
     */
    public function getAreasOfExpertises(): Collection
    {
        return $this->areasOfExpertises;
    }

    public function addAreasOfExpertise(AreasOfExpertise $areasOfExpertise): self
    {
        if (!$this->areasOfExpertises->contains($areasOfExpertise)) {
            $this->areasOfExpertises[] = $areasOfExpertise;
            $areasOfExpertise->setCv($this);
        }

        return $this;
    }

    public function removeAreasOfExpertise(AreasOfExpertise $areasOfExpertise): self
    {
        if ($this->areasOfExpertises->removeElement($areasOfExpertise)) {
            // set the owning side to null (unless already changed)
            if ($areasOfExpertise->getCv() === $this) {
                $areasOfExpertise->setCv(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCvFiles($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCvFiles() === $this) {
                $image->setCvFiles(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     * @Serializer\VirtualProperty()
     */
    public function getFullName(){
        return $this->firstName . ' ' . $this->lastName;
    }
}
