<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\FileUploadableInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\TemplateSettingsRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\File;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\SeoFriendly;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Yaml\Yaml;

/**
 *
 * @ORM\Entity(repositoryClass=TemplateSettingsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class TemplateSettings implements FileUploadableInterface
{
    use GedmoSlugable, GedmoDateable, File, SeoFriendly;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"template:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"template:read"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"template:read"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"template:read"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"template:read"})
     */
    private $fb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"template:read"})
     */
    private $tw;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"template:read"})
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"template:read"})
     */
    private $linkedin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"template:read"})
     */
    private $youtube;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default":"Merx Forum generated page"})
     */
    private $pageTitle;

    /**
     * @ORM\ManyToOne(targetEntity=TemplateInfo::class, inversedBy="templateSettings", cascade={"persist"})
     */
    private $templateInfo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $analytics;

    /**
     * @ORM\Column(type="json")
     */
    private $languages = ['en'];

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="templateSettings", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    public function __construct()
    {
        $this->name = 'MerxForum generated page';
        $this->pageTitle = 'MerxForum generated page';
        $this->metaTitle = 'MerxForum generated page';
        $this->metaDescription = 'MerxForum generated page';
        $this->fb = 'https://www.facebook.com/MerxForum/';
        $this->linkedin = 'https://www.linkedin.com/company/merx-forum/';
        $this->tw = 'https://twitter.com/MerxForum';
        $this->email = 'cs@merxforum.com';
        $this->phone = '+1 818-648-8208';
        $this->address = '13636 Ventura Blvd Unit 103, Sherman Oaks, California 91423';
        $this->languages = ['en'];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFb(): ?string
    {
        return $this->fb;
    }

    public function setFb(string $fb): self
    {
        $this->fb = $fb;

        return $this;
    }

    public function getTw(): ?string
    {
        return $this->tw;
    }

    public function setTw(string $tw): self
    {
        $this->tw = $tw;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(string $linkedin): self
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getBaseDirName() {

        return $this->getTemplateInfo() ? $this->getTemplateInfo()->getBaseDirName() : 'main';
    }

    public function getLogo() {

        return $this->getFileOriginalName() ? $this->getDownloadLink() : '/build/img/logoColor.png';
    }

    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    public function setPageTitle(string $pageTitle): self
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    public function getTemplateInfo(): ?TemplateInfo
    {
        return $this->templateInfo;
    }

    public function setTemplateInfo(?TemplateInfo $templateInfo): self
    {
        $this->templateInfo = $templateInfo;

        return $this;
    }

    public function getAnalytics(): ?string
    {
        return $this->analytics;
    }

    public function setAnalytics(?string $analytics): self
    {
        $this->analytics = $analytics;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(string $youtube): self
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * @return array
     */
    public function getLanguages(): ?array
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     */
    public function setLanguages(?array $languages): self
    {
        $this->languages = $languages;
        return $this;
    }
    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if(count($this->languages) == 0) {
            $context->buildViolation("Sorry. Language list cannot be empty.")
                ->atPath('languages')
                ->addViolation();
        }
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist
     */
    public function localeSettings() {
        
        $servicesLoaderFile = sprintf('%s/config/services.yaml', __DIR__.'/../../../../..');

        $filesystem = new Filesystem();

        if($filesystem->exists($servicesLoaderFile)) {
            $serviceYaml = Yaml::parseFile($servicesLoaderFile);

            if(is_array($serviceYaml) &&
                array_key_exists('parameters', $serviceYaml)){

                $serviceYaml['parameters']['app.locales'] = $this->languages;

                $yaml = Yaml::dump($serviceYaml);
                $filesystem->dumpFile($servicesLoaderFile, $yaml);

            }
        }
    }

}
