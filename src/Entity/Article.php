<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\FileUploadableInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\ArticleRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\File;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoPositionable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\SeoFriendly;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Article implements EnablabeInterface, FileUploadableInterface, TranslatableInterface
{
    use GedmoSlugable, GedmoDateable, SeoFriendly, GedmoPositionable, File, EnableAvailable, TranslatableTrait;

    const IS_ARTICLE = 0, IS_NEWS = 1;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=MainMenu::class, mappedBy="mainArticle", cascade={"persist"}, orphanRemoval=true)
     */
    private $mainMenus;

    /**
     * @ORM\OneToMany(targetEntity=SubMenu::class, mappedBy="mainArticle", cascade={"persist"})
     */
    private $subMenus;

    /**
     * @ORM\ManyToOne(targetEntity=SubMenu::class, inversedBy="articles")
     * @ORM\JoinColumn(name="submenu_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $subMenu;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isMain;

    /**
     * @ORM\Column(type="smallint", options={"default":0})
     */
    private $articleType;

    /**
     * @ORM\ManyToOne(targetEntity=MainMenu::class, inversedBy="articles", cascade={"persist"})
     * @ORM\JoinColumn(name="main_menu_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $mainMenu;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="articles", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=ArticleGroup::class, mappedBy="article")
     */
    private $articleGroups;

    public function __toString()
    {
        return $this->id ? $this->getName() : 'New Article';
        // TODO: Implement __toString() method.
    }

    public function __construct()
    {
        $this->mainMenus = new ArrayCollection();
        $this->subMenus = new ArrayCollection();
        $this->isMain = false;
        $this->articleType = self::IS_ARTICLE;
        $this->articleGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __call($name, $arguments)
    {
        return $this->proxyCurrentLocaleTranslation($name, $arguments);
    }

    /**
     * @return Collection|MainMenu[]
     */
    public function getMainMenus(): Collection
    {
        return $this->mainMenus;
    }

    public function addMainMenu(MainMenu $mainMenu): self
    {
        if (!$this->mainMenus->contains($mainMenu)) {
            $this->mainMenus[] = $mainMenu;
            $mainMenu->setMainArticle($this);
        }

        return $this;
    }

    public function removeMainMenu(MainMenu $mainMenu): self
    {
        if ($this->mainMenus->contains($mainMenu)) {
            $this->mainMenus->removeElement($mainMenu);
            // set the owning side to null (unless already changed)
            if ($mainMenu->getMainArticle() === $this) {
                $mainMenu->setMainArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SubMenu[]
     */
    public function getSubMenus(): Collection
    {
        return $this->subMenus;
    }

    public function addSubMenu(SubMenu $subMenu): self
    {
        if (!$this->subMenus->contains($subMenu)) {
            $this->subMenus[] = $subMenu;
            $subMenu->setMainArticle($this);
        }

        return $this;
    }

    public function removeSubMenu(SubMenu $subMenu): self
    {
        if ($this->subMenus->contains($subMenu)) {
            $this->subMenus->removeElement($subMenu);
            // set the owning side to null (unless already changed)
            if ($subMenu->getMainArticle() === $this) {
                $subMenu->setMainArticle(null);
            }
        }

        return $this;
    }

    public function getSubMenu(): ?SubMenu
    {
        return $this->subMenu;
    }

    public function setSubMenu(?SubMenu $subMenu): self
    {
        $this->subMenu = $subMenu;

        return $this;
    }

    public function getIsMain(): ?bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }

    public function getArticleType(): ?int
    {
        return $this->articleType;
    }

    public function setArticleType(int $articleType): self
    {
        $this->articleType = $articleType;

        return $this;
    }

    public function getMainMenu(): ?MainMenu
    {
        return $this->mainMenu;
    }

    public function setMainMenu(?MainMenu $mainMenu): self
    {
        $this->mainMenu = $mainMenu;

        return $this;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context)
    {

        switch ($this->articleType) {

            case self::IS_NEWS:
                if($this->mainMenu instanceof MainMenu === false){
                    $context->buildViolation("Sorry. Main menu can't be null for news.")
                        ->atPath('mainMenu')
                        ->addViolation();
                }
                break;
            case self::IS_ARTICLE:
                if($this->subMenu instanceof SubMenu === false){
                    $context->buildViolation("Sorry. Sub menu can't be null for article.")
                        ->atPath('subMenu')
                        ->addViolation();
                }
                break;
            default:

                break;
        }
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|ArticleGroup[]
     */
    public function getArticleGroups(): Collection
    {
        return $this->articleGroups;
    }

    public function addArticleGroup(ArticleGroup $articleGroup): self
    {
        if (!$this->articleGroups->contains($articleGroup)) {
            $this->articleGroups[] = $articleGroup;
            $articleGroup->addArticle($this);
        }

        return $this;
    }

    public function removeArticleGroup(ArticleGroup $articleGroup): self
    {
        if ($this->articleGroups->removeElement($articleGroup)) {
            $articleGroup->removeArticle($this);
        }

        return $this;
    }
}
