<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\AreasOfExpertiseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AreasOfExpertiseRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class AreasOfExpertise
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="form.industry_null")
     * @Assert\NotNull(message="form.industry_blank")
     */
    private $industry;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="form.years_experiense_null")
     * @Assert\NotNull(message="form.years_experiense_blank")
     */
    private $yearsOfExperience;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $yearsOfTraining;

    /**
     * @ORM\ManyToOne(targetEntity=CV::class, inversedBy="areasOfExpertises", cascade={"persist"})
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $cv;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
     return $this->id ? $this->industry : 'New Areas of Expertise';
    }

    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    public function setIndustry(string $industry): self
    {
        $this->industry = $industry;

        return $this;
    }

    public function getYearsOfExperience(): ?int
    {
        return $this->yearsOfExperience;
    }

    public function setYearsOfExperience(int $yearsOfExperience): self
    {
        $this->yearsOfExperience = $yearsOfExperience;

        return $this;
    }

    public function getYearsOfTraining(): ?int
    {
        return $this->yearsOfTraining;
    }

    public function setYearsOfTraining(?int $yearsOfTraining): self
    {
        $this->yearsOfTraining = $yearsOfTraining;

        return $this;
    }

    public function getCv(): ?CV
    {
        return $this->cv;
    }

    public function setCv(?CV $cv): self
    {
        $this->cv = $cv;

        return $this;
    }
}
