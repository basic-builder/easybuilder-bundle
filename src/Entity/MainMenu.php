<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Model\EnablabeInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Model\SingleTemplateInterface;
use BasicBuilder\Bundle\EasyBuilderBundle\Repository\MainMenuRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoPositionable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\SeoFriendly;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * @ORM\Entity(repositoryClass=MainMenuRepository::class)
 */
class MainMenu implements EnablabeInterface, SingleTemplateInterface, TranslatableInterface
{

    use GedmoSlugable, GedmoDateable, SeoFriendly, GedmoPositionable, EnableAvailable, TranslatableTrait;

    const IS_MAIN_MENU = 0, IS_FOOTER = 1, IS_INTERNAL = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="mainMenus", cascade={"persist"})
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $mainArticle;

    /**
     * @ORM\OneToOne(targetEntity=Gallery::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $gallery;

    /**
     * @ORM\OneToMany(targetEntity=SubMenu::class, mappedBy="mainMenu")
     * @ORM\OrderBy({"position":"ASC", "created":"ASC"})
     */
    private $subMenus;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="mainMenu")
     * @ORM\OrderBy({"created":"DESC"})
     */
    private $articles;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $deep = [];

    /**
     * @ORM\ManyToOne(targetEntity=SingleTemplate::class, inversedBy="mainMenus", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $singleTemplate;

    public function __construct()
    {
        $this->subMenus = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->deep = [self::IS_MAIN_MENU];
    }

    public function __toString()
    {
        return $this->id ? $this->name : 'New Menu';
        // TODO: Implement __toString() method.
    }


    public function __call($name, $arguments)
    {
        return $this->proxyCurrentLocaleTranslation($name, $arguments);
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getMainArticle(): ?Article
    {
        return $this->mainArticle;
    }

    public function setMainArticle(?Article $mainArticle): self
    {
        $this->mainArticle = $mainArticle;

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getSubs(): ?Collection
    {

        $result = null;

        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('isInMainMenu', true));

        if($this->getSubMenus()->isEmpty() === false) {

            $result = $this->subMenus->matching($criteria);
        }

        return $result;
    }
    /**
     * @return Collection|SubMenu[]
     */
    public function getSubMenus(): Collection
    {
        return $this->subMenus;
    }

    public function addSubMenu(SubMenu $subMenu): self
    {
        if (!$this->subMenus->contains($subMenu)) {
            $this->subMenus[] = $subMenu;
            $subMenu->setMainMenu($this);
        }

        return $this;
    }

    public function removeSubMenu(SubMenu $subMenu): self
    {
        if ($this->subMenus->contains($subMenu)) {
            $this->subMenus->removeElement($subMenu);
            // set the owning side to null (unless already changed)
            if ($subMenu->getMainMenu() === $this) {
                $subMenu->setMainMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setMainMenu($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getMainMenu() === $this) {
                $article->setMainMenu(null);
            }
        }

        return $this;
    }

    public function getDeep(): ?array
    {
        return $this->deep;
    }

    public function setDeep(?array $deep): self
    {
        $this->deep = $deep;

        return $this;
    }

    public function getSingleTemplate(): ?SingleTemplate
    {
        return $this->singleTemplate;
    }

    public function setSingleTemplate(?SingleTemplate $singleTemplate): self
    {
        $this->singleTemplate = $singleTemplate;

        return $this;
    }
}
