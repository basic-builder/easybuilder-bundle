<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Entity;

use BasicBuilder\Bundle\EasyBuilderBundle\Repository\NewsRepository;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\EnableAvailable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoDateable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\GedmoSlugable;
use BasicBuilder\Bundle\EasyBuilderBundle\Traits\SeoFriendly;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 */
class News
{
    use GedmoSlugable, GedmoDateable, EnableAvailable, SeoFriendly;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
