<?php
/**
 * Created by PhpStorm.
 * User: tigran
 * Date: 8/17/17
 * Time: 5:48 PM
 */

namespace BasicBuilder\Bundle\EasyBuilderBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

trait GedmoPositionable
{

    /**
     * @var
     * @Gedmo\SortablePosition()
     * @ORM\Column(name="position", type="integer", options={"default":0})
     */
    private $position;

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }
}