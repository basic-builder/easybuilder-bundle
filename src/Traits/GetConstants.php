<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Traits;

trait GetConstants
{
    static function getConstants() {
        $oClass = new \ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}
