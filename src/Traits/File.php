<?php

namespace BasicBuilder\Bundle\EasyBuilderBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\VirtualProperty;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * This traits is used for include file
 *
 * Class File
 * @package App\Traits
 */
trait File
{

    /**
     * @Assert\File()
     */
    protected  $file;

    /**
     * @ORM\Column(name="file_original_name", type="string", length=255, nullable=true)
     * @Groups({"realty_list", "cars_list", "finance", "file-or-name"})
     */
    protected $fileOriginalName;

    /**
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     * @Serializer\Groups({"realty_list", "cars_list", "finance", "file-name"})
     */
    protected $fileName;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set FileOriginalName
     *
     * @param string $fileOriginalName
     * @return $this
     */
    public function setFileOriginalName($fileOriginalName)
    {
        $this->fileOriginalName = $fileOriginalName;

        return $this;
    }

    /**
     * @var null
     */
    public $imageFromCache = null;

    /**
     * Get fileOriginalName
     *
     * @return string
     */
    public function getFileOriginalName()
    {
        return $this->fileOriginalName;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * This function is used to return file web path
     *
     * @VirtualProperty()
     * @Groups({"realty_list", "cars_list", "finance", "reservation", "download-link", "image-list"})
     *
     * @return string
     */
    public function getDownloadLink()
    {
        return $this->fileName ? '/' . $this->getUploadDir() . '/' . $this->getPath() . '/' . $this->fileName : '' ;
    }

    /**
     * @return string
     */
    public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->getPath() .'/';
    }

    /**
     * This function is used to return file web path
     *
     * @return string
     */
    public function getUploadRootDir()
    {
        return __DIR__. '/../../../../../public/' . $this->getUploadDir();
    }
    /**
     * @return string
     */
    protected function getPath()
    {
        return 'files';
    }
    /**
     * @return string
     */
    protected function getCachePath()
    {
        return 'cache';
    }
    /**
     * Upload folder name
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads';
    }
    /**
     * Upload folder name
     *
     * @return string
     */
    protected function getCacheUploadDir()
    {
        return 'media';
    }

    /**
     * This function is used to move(physically download) file
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate
     */
    public function uploadFile()
    {
        // the file property can be empty if the field is not required
        if (null == $this->getFile())
        {
            return;
        }

        // check file name
        if($this->getFileName()){
            // get file path
            $path = $this->getAbsolutePath() . $this->getFileName();
            // check file
            if(file_exists($path) && is_file($path)){
                // remove file
                unlink($path);
            }
        }

        // get file originalName
        $this->setFileOriginalName($this->getFile()->getClientOriginalName());

        // get file
        $path_parts = pathinfo($this->getFile()->getClientOriginalName());

        switch ($path_parts['extension']){
            case 'jpg':
                $img = imagecreatefromjpeg($this->getFile());
                break;
            case 'jpeg':
                $img = imagecreatefromjpeg($this->getFile());
                break;
            case 'png':
                $img = imagecreatefrompng($this->getFile());
                break;
            default:
                $img = null;
                break;
        }


        $this->setFileName(md5(microtime()) . '.' . 'webp');

        if(!is_null($img)) {

            imagepalettetotruecolor($img);

            imagealphablending($img, true);

            imagesavealpha($img, true);

            imagewebp($img, sprintf('%s/%s', $this->getAbsolutePath(), $this->getFileName()));

            imagedestroy($img);
        }else{

            // generate filename
            $this->setFileName(md5(microtime()) . '.' . $path_parts['extension']);

            // upload file
            $this->getFile()->move($this->getAbsolutePath(), $this->getFileName());

            // set file to null
            $this->setFile(null);
        }
    }

    /**
     * This function is used to remove file
     *
     * @ORM\PreRemove
     */
    public function preRemove()
    {
        // get origin file path
        $filePath = $this->getAbsolutePath() . $this->getFileName();

        // check file and remove
        if (file_exists($filePath) && is_file($filePath)){
            unlink($filePath);
        }
    }


    /**
     * This function is used to get object class name
     *
     * @return string
     */
    public function getClassName(){
        return get_class($this);
    }

}