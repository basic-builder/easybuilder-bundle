<?php
/**
 * Created by PhpStorm.
 * User: tigran
 * Date: 8/17/17
 * Time: 5:48 PM
 */

namespace BasicBuilder\Bundle\EasyBuilderBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait SeoFriendly
{

    /**
     * @var
     * @ORM\Column(name="meta_title", type="string", length=100, nullable=true, options={"default":" "})
     * @Assert\Length(max="100")
     */
    private $metaTitle;

    /**
     * @var
     * @ORM\Column(name="meta_Description", type="text", nullable=true, options={"default":" "})
     * @Assert\Length(max="180")
     */
    private $metaDescription;

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

}