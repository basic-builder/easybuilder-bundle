<?php


namespace BasicBuilder\Bundle\EasyBuilderBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Filesystem\Filesystem;

class ImagesToWebpConvertor
{

    private $em;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function convertExistingImages() {

        $em = $this->em;

        $entities = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();


        foreach ($entities as $entity) {

            $classMetadata = $em->getClassMetadata($entity);

            if(array_key_exists('fileOriginalName', $classMetadata->getReflectionProperties())) {

                $queryBuilder = $em->createQueryBuilder()
                    ->select('entity')
                    ->from($classMetadata->getName(), 'entity')
                    ->where('entity.fileName is not null')
                ;
                $isLimited = true;
                $limit = 100;
                $page = 1;
                $paginator = new Paginator($queryBuilder->getQuery(),  $fetchJoinCollection = true);
                do{

                    $paginator->getQuery()

                        ->setFirstResult($limit*($page-1))
                        ->setMaxResults($limit)
                    ;

                    if($paginator->count()) {

                        foreach ($paginator->getIterator() as $item) {

                            $dir = $item->getAbsolutePath();

                            $fileSystem = new Filesystem();


                            $fileName = $item->getFileName();

                            $fileDir = sprintf('%s%s', $dir, $fileName);

                            if($fileSystem->exists($fileDir) === false){

                                continue;
                            }
                            $ext = explode('.', $fileName);

                            $img = null;

                            if(array_key_exists(1, $ext)) {

                                switch ($ext[1]){
                                    case 'jpg':
                                        $img = imagecreatefromjpeg($fileDir);
                                        break;
                                    case 'jpeg':
                                        $img = imagecreatefromjpeg($fileDir);
                                        break;
                                    case 'png':
                                        $img = imagecreatefrompng($fileDir);
                                        break;
                                    default:
                                        $img = null;
                                        break;
                                }
                            }

                            if(!is_null($img)) {

                                $newName = sprintf('%s.%s', $ext[0], 'webp');

                                try {

                                    imagepalettetotruecolor($img);
                                    imagealphablending($img, true);
                                    imagesavealpha($img, true);
                                    imagewebp($img, sprintf('%s/%s', $dir, $newName));
                                    imagedestroy($img);

                                    $item->setFileName($newName);
                                    $em->persist($item);

                                }catch (\Exception $e) {

                                }
                            }
                        }

                        try {
                            $this->em->flush();
                        }catch (\Exception $e) {

                        }

                    }else{
                        $isLimited = false;
                    }

                    if($paginator->getIterator()->count() < $limit) {
                        $isLimited = false;
                    }else{
                        $page++;
                    }

                }while($isLimited);
            }
        }

        return true;
    }
}